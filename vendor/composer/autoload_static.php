<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInita62dd3d8eac75f4fcf68bcd83dd1d111
{
    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->classMap = ComposerStaticInita62dd3d8eac75f4fcf68bcd83dd1d111::$classMap;

        }, null, ClassLoader::class);
    }
}
