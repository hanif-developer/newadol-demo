<?php

namespace app\admin\controller;

use app\admin\controller\main;
use comp;

class user extends main{
	
	public function __construct(){
		parent::__construct();
		$this->data['url_path'] = $this->link($this->getProject().$this->getController());
	}

	public function index(){
		$this->data['page_title'] = 'Data User';
		$this->data['pilihan_satker'] = ['' => ['text' => 'SEMUA SATKER']] + $this->adol->getNamaSatker();
		$this->data['pilihan_bidang'] = ['' => ['text' => 'SEMUA BIDANG']] + $this->adol->getBidang();
		$this->showView('index', $this->data, 'theme_admin');
		// $this->debugResponse($this->data);
	}

	protected function script(){
		header('Content-Type: application/javascript');
		$this->subView('script', $this->data);
	}

	protected function template($method){
		$this->subView('template/'.$method, $this->data);
	}

	// protected function generated(){
	// 	$form_user = $this->adol->getTabel('tb_user');
	// 	$default_value = [
	// 		'password_user' => comp\FUNC::encryptor('adol'),
	// 		'level_user' => 'jabatan',
	// 		'disposisi_user' => 0,
	// 		'koordinasi_user' => 0,
	// 		'laporan_user' => 1,
	// 		'draft_user' => 32, // buat draft
	// 	];
	// 	$form_user = $this->adol->paramsFilter($form_user, $default_value);
	// 	$dataUser = [];
	// 	$success = [];

	// 	$query = $this->adol->getData('SELECT jabatan.* FROM tb_jabatan jabatan 
	// 							LEFT JOIN tb_user users ON (jabatan.id_jabatan=users.id_jabatan)
	// 							WHERE ISNULL(users.id_user)', []);

	// 	foreach ($query['value'] as $key => $value) {
	// 		$form_user['id_user']++;
	// 		$form_user['id_jabatan'] = $value['id_jabatan'];
	// 		// $result = $this->adol->save_update('tb_user', $form_user);
	// 		array_push($dataUser, $form_user);
	// 		// array_push($success, $result);
	// 	}
		
	// 	$this->data['dataUser'] = $dataUser;
	// 	$this->data['success'] = $success;

	// 	$this->debugResponse($this->data);
	// }

}
?>
