<?php

namespace app\admin\controller;

use app\adol\controller\service;
use comp;

class login extends service{
	
	public function __construct(){
		parent::__construct();
		$adminSession = $this->getAdminSession();
		if(!empty($adminSession)){
			$this->redirect($this->link($this->getProject()));
		}
		
		$this->data['url_path'] = $this->link($this->getProject().$this->getController());
	}

	public function index(){
		// print_r($_SESSION);
		// $this->setSession('SESSION_LOGIN', ['user' => 'admin']);
		$this->data['page_title'] = 'Administrasi Online';
		$this->showView('index', $this->data, 'theme_admin');
		// $this->debugResponse($this->data);
	}

	protected function script(){
		header('Content-Type: application/javascript');
		$this->subView('script', $this->data);
	}

	protected function validate(){
		$input = $this->post(false);
		if($input){
			session_regenerate_id();
			$input['username'] = comp\FUNC::encryptor('admin');
			$input['password'] = comp\FUNC::encryptor($input['password']);
			$session_id = comp\FUNC::encryptor(session_id());
			$result = [
				'status' => 'error',
				'message' => 'Access denied !!',
			];
			
			$login = $this->adol->adminLogin($input);
			if($login > 0){
				$this->setSession('SESSION_LOGIN', $session_id);
				$result['status'] = 'success';
				$result['message'] = 'You are successfully logged in';
			}
			
			$this->showResponse($result);
		}
	}


}
?>
