<?php

namespace app\admin\controller;

use app\admin\controller\main;

class jabatan extends main{
	
	public function __construct(){
		parent::__construct();
		$this->data['url_path'] = $this->link($this->getProject().$this->getController());
	}

	public function index(){
		$this->data['page_title'] = 'Data Jabatan';
		$this->data['pilihan_satker'] = ['' => ['text' => 'SEMUA SATKER']] + $this->adol->getNamaSatker();
		$this->data['pilihan_bidang'] = ['' => ['text' => 'SEMUA BIDANG']] + $this->adol->getBidang();
		$this->showView('index', $this->data, 'theme_admin');
		// $this->debugResponse($this->data);
	}

	protected function script(){
		header('Content-Type: application/javascript');
		$this->subView('script', $this->data);
	}

	protected function template($method){
		$this->subView('template/'.$method, $this->data);
	}

}
?>
