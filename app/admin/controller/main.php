<?php

namespace app\admin\controller;

use app\adol\controller\service;

class main extends service{
	
	public function __construct(){
		parent::__construct();
		$adminSession = $this->getAdminSession();
		if(empty($adminSession)){
			if($this->runAjax()){
				echo '<script>window.location.reload();</script>';
				die;
			}
			$this->redirect($this->link($this->getProject().'login'));
		}
		
		$this->link_main = $this->link($this->getProject());
		$this->link_regional = $this->link($this->getProject().'regional');
		$this->link_wilayah = $this->link($this->getProject().'wilayah');
		$this->link_satker = $this->link($this->getProject().'satker');
		$this->link_bidang = $this->link($this->getProject().'bidang');
		$this->link_jabatan = $this->link($this->getProject().'jabatan');
		$this->link_personil = $this->link($this->getProject().'personil');
		$this->link_user = $this->link($this->getProject().'user');
	}

	public function index(){
		$this->data['page_title'] = 'Dashboard';
		$this->data['statistik'] = [
			'mobile' => $this->adol->getCountUserRegister('mobile'),
			'desktop' => $this->adol->getCountUserRegister('desktop'),
			'user' => $this->adol->getCountUserActive(),
		];
		$this->data['server'] = [
			'status' => 'online',
			'color' => 'green',
			'status' => 'offline',
			'color' => 'red',
		];
		$this->showView('index', $this->data, 'theme_admin');
		// $this->debugResponse($this->data);
	}

	protected function script(){
		header('Content-Type: application/javascript');
		$this->subView('script', $this->data);
	}

	public function header($data){
		$this->data = $data;
        $this->subView('header', $this->data);
	}

	protected function navbar($data){
		$this->data = $data;
		$this->subView('navbar', $this->data);
	}

	protected function footer($data){
		$this->data = $data;
		$this->subView('footer', $this->data);
	}
	
	protected function modal($id){
		$this->subView('modal-'.$id, $this->data);
	}
	
	protected function logout(){
		$this->desSession();
		$this->redirect($this->link($this->getProject().'login'));
	}

	protected function template($method){
		$this->subView('template/'.$method, $this->data);
	}

	protected function server($id){
		switch ($id) {
			case 'sipp':
				$result = $this->adol->checkServer($this->server_sipp);
				$result['status'] = \ucfirst($result['status']);
				$this->succesMsg['data'] = $result;
				$this->showResponse($this->succesMsg);
				break;
			
			default:
				$this->showResponse($this->errorMsg, $this->errorCode);
				break;
		}
	}

	protected function traffic($id){
		switch ($id) {
			case 'rx': //masuk
			case 'tx': //keluar
				$interface = 'eth0';
				$condition = 'dummy';
				$kbps = \rand(0, 100);
				$bytes = @file_get_contents('/sys/class/net/'.$interface.'/statistics/'.$id.'_bytes');
				if($bytes){ // jika ada isinya
					sleep(1);
					$bytes_2 = @file_get_contents('/sys/class/net/'.$interface.'/statistics/'.$id.'_bytes');
					$bps = intval($bytes_2) - intval($bytes);
					$kbps = \round($bps/1024, 2);
					$condition = 'real';
				}
				
				$this->succesMsg['data'] = [
					'traffic' => [(\microtime(true)) * 1000, intval($kbps)],
					'condition' => $condition
				];
				$this->showResponse($this->succesMsg);
				break;
			
			default:
				$this->showResponse($this->errorMsg, $this->errorCode);
				break;
		}
	}

}
?>
