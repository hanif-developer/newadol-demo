<?php

namespace app\admin\controller;

use app\admin\controller\main;

class personil extends main{
	
	public function __construct(){
		parent::__construct();
		$this->data['url_path'] = $this->link($this->getProject().$this->getController());
	}

	public function index(){
		$this->data['page_title'] = 'Data Personil';
		$this->showView('index', $this->data, 'theme_admin');
		// $this->debugResponse($this->data);
	}

	protected function script(){
		header('Content-Type: application/javascript');
		$this->subView('script', $this->data);
	}

	protected function template($method){
		$this->subView('template/'.$method, $this->data);
	}

}
?>
