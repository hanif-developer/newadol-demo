user = {
    init: function(){
        url_path = "<?= $url_path; ?>";
        modul = app.initModul("#user");

        $(document).find(".select2").select2();
        $(document).on("change", ".filter-table", function(event){
            event.preventDefault();
            modul.table.action.find("#page").val(1);
            let update_form = $(this).data("update-form");
            switch (update_form) {
                case "bidang":
                    user.updateForm("bidang", {satker: $("#satker").val()}, function(response){
                        // console.log(response);
                        update_form = $("#"+update_form);
                        app.updateForm.selectOption(update_form, response.data, "");
                        update_form.val("").trigger("change");
                    });
                    break;
            
                default:
                    user.showTable();
                    break;
            }
        });
        $(document).on("change", ".filter-form", function(event){
            event.preventDefault();
            let update_form = $(this).data("update-form");
            switch (update_form) {
                case "id_bidang":
                    user.updateForm("bidang", {satker: $("#id_satker").val()}, function(response){
                        update_form = $("#"+update_form);
                        app.updateForm.selectOption(update_form, response.data, "");
                        update_form.val("").trigger("change");
                    });
                    break;

                case "id_relasi":
                    user.updateForm("jabatan", {satker: $("#id_satker").val()}, function(response){
                        update_form = $("."+update_form);
                        app.updateForm.selectOption(update_form, response.data, "");
                        update_form.val("").trigger("change");
                    });
                    break;
            
                default:
                    break;
            }
        });
        $(document).on("click", ".user-action", function(event){
            event.preventDefault();
            // console.log(this);
            let jabatan = $(this).data("jabatan");
            let status = $(this).data("status");
            user.showFormUserAction(this.id, jabatan, status);
        });
        $(document).on("click", ".btn-form", function(event){
            event.preventDefault();
            let jabatan = $(this).data("jabatan");
            user.showForm(this.id, jabatan);
        });
        $(document).on("click", ".btn-sipp", function(event){
            event.preventDefault();
            user.checkSIPP();
        });
        $(document).on("click", ".btn-delete", function(event){
            event.preventDefault();
            let id = this.id;
            let message = this.getAttribute("data-message");
            if(confirm(message)){
                let loader = app.createLoader(modul.table.content, "Menghapus data ...");
                setTimeout(function(){
                    app.sendData({
                        url: "/data/user/delete/",
                        data: JSON.stringify({id: id}),
                        token: "<?= $this->token; ?>",
                        onSuccess: function(response){
                            // console.log(response);
                            loader.hide();
                            user.showTable();
                            main.showNotification(response);
                        },
                        onError: function(error){
                            // console.log(error);
                            loader.hide();
                            main.showNotification(error);
                        }
                    });
                }, 500);
            }
        });
        
        user.showTable();
    },
    showTable: function(){
        modul.table.content.hide();
        modul.table.empty.hide();
        modul.table.loader.show();
        setTimeout(function(){
            // console.log(modul.table);
            app.sendData({
                url: "/data/user/list/",
                data: app.serializeArraytoJson(modul.table.action.serializeArray()),
                token: "<?= $this->token; ?>",
                onSuccess: function(response){
                    // console.log(response);
                    modul.table.loader.hide();
                    modul.main.find(".table-count").html("Total: "+app.ribuan(response.data.count)+" Jabatan");
                    modul.main.find(".table-user_active").html("Active: "+app.ribuan(response.data.active_user)+" "+response.data.label);
                    app.createTable({
                        table: modul.table,
                        data: response.data,
                        onShow: function(content){
                            $.each(content.find("tr.status_user"), function(key, val){ 
                                if(response.data.table[key].id_user == ""){
                                    $(val).css({"background-color": "#f5f5f5", "color": "#ccc"});
                                }
                            });
                        },
                        onPagging: function(page){
                            modul.table.action.find("#page").val(page);
                            user.showTable();
                            $(document).scrollTop(0);
                        }
                    });
                },
                onError: function(error){
                    // console.log(error);
                    main.showNotification(error);
                }
            });
        }, 500);
    },
    showForm: function(id, jabatan){
        if(id == "" && jabatan == ""){
            main.showMessage({status: "error", message: {title: "Maaf", text: "ID Jabatan kosong !!"}});
            return false;
        }
        app.sendData({
            url: "/data/user/form/",
            data: JSON.stringify({id: id, jabatan: jabatan}),
            token: "<?= $this->token; ?>",
            onSuccess: function(response){
                // console.log(response);
                var data = response.data;
                var form = data.form;
                var object = {
                    id_user: app.createForm.inputKey("id_user", form.id_user),
                    id_jabatan: app.createForm.inputKey("id_jabatan", data.id_jabatan),
                    id_personil: app.createForm.inputKey("id_personil", data.id_personil),
                    nrp_nip: app.createForm.inputText("nrp_nip", form.id_personil).attr({class: "form-control", required: false}),
                    nama_jabatan: app.createForm.textArea("nama_jabatan", data.nama_jabatan).attr({class: "form-control", readonly: true}),
                    nama_satker: app.createForm.textArea("nama_satker", data.nama_satker).attr({class: "form-control", readonly: true}),
                    nama_bidang: app.createForm.inputText("nama_bidang", data.nama_bidang).attr({class: "form-control", readonly: true}),
                    pangkat_personil: app.createForm.inputText("pangkat_personil", data.pangkat_personil).attr({class: "form-control", readonly: true}),
                    nama_personil: app.createForm.inputText("nama_personil", data.nama_personil).attr({class: "form-control", readonly: true}),
                    password_user: app.createForm.inputText("password_user", form.password_user).attr({class: "form-control", required: true}),
                    level_user: app.createForm.selectOption("level_user", data.pilihan_level, form.level_user).attr({class: "form-control select2", style: "width:100%;", required: true}),
                    disposisi_user: app.createForm.selectOption("disposisi_user", data.pilihan_action, form.disposisi_user).attr({class: "form-control select2", style: "width:100%;", required: true}),
                    koordinasi_user: app.createForm.selectOption("koordinasi_user", data.pilihan_action, form.koordinasi_user).attr({class: "form-control select2", style: "width:100%;", required: true}),
                    laporan_user: app.createForm.selectOption("laporan_user", data.pilihan_action, form.laporan_user).attr({class: "form-control select2", style: "width:100%;", required: true}),
                    draft_user: app.createForm.selectOption("draft_user[]", data.pilihan_action_draft, form.draft_user, true).attr({class: "form-control select2", style: "width:100%;", required: false}), // Multiple Select
                };
                modul.formModal.content.html(modul.formModal.objectForm);
                $.each(object, function(key, val){ modul.formModal.content.find("span[data-form-object='"+key+"']").replaceWith(val); });
                modul.formModal.content.find(".select2").select2();
                modul.formModal.title.html(data.form_title);
                modul.formModal.modal.find(".modal-dialog").css({width: "600px"}); // default
                modul.formModal.modal.modal("show");
                modul.formModal.action.off();
                modul.formModal.action.on("submit", function(event){
                    event.preventDefault();
                    let data = new FormData($(modul.formModal.action)[0]);
                    let loader = app.createLoader(modul.formModal.action, "Menyimpan data ...");
                    setTimeout(function(){
                        app.sendDataMultipart({
                            url: "/data/user/save/",
                            data: data,
                            token: "<?= $this->token; ?>",
                            onSuccess: function(response){
                                // console.log(response);
                                loader.hide();
                                main.showNotification(response);
                                modul.formModal.modal.modal("hide");
                                user.showTable();
                            },
                            onError: function(error){
                                // console.log(error);
                                loader.hide();
                                main.showNotification(error);
                                modul.formModal.modal.modal("hide");
                            }
                        });
                    }, 500);
                });
            },
            onError: function(error){
                // console.log(error);
                main.showNotification(error);
            }
        });
    },
    showFormUserAction: function(id, jabatan, status){
        if(id == "" || jabatan == "" || status == ""){
            main.showMessage({status: "error", message: {title: "Maaf", text: "ID Jabatan kosong !!"}});
            return false;
        }
        app.sendData({
            url: "/user/action/form/",
            data: JSON.stringify({id: id, jabatan: jabatan, status: status}),
            token: "<?= $this->token; ?>",
            onSuccess: function(response){
                // console.log(response);
                var data = response.data;
                var object = {
                    status_action: app.createForm.inputKey("status_action", data.status_action),
                    id_jabatan: app.createForm.inputKey("id_jabatan", data.info_jabatan.id_jabatan),
                    id_satker: app.createForm.selectOption("id_satker", data.pilihan_satker, data.info_jabatan.id_satker).attr({class: "form-control filter-form select2", "data-update-form": "id_bidang", style: "width:100%;", required: true}),
                    id_bidang: app.createForm.selectOption("id_bidang", data.pilihan_bidang, "").attr({class: "form-control filter-form select2", "data-update-form": "id_relasi", style: "width:100%;", required: false}),
                    id_relasi: app.createForm.selectOption("id_relasi", data.pilihan_jabatan, "", true).attr({class: "form-control id_relasi select2", style: "width:100%;", required: true}), // Multiple Select
                };

                $.get(url_path+"/template/form.action", function(objectForm){
                    let form_action = $("<div>").html(objectForm);
                    let table_action = app.initTable(form_action);
                    function showTableAction(){
                        table_action.content.hide();
                        table_action.empty.hide();
                        table_action.loader.show();
                        setTimeout(function(){
                            app.sendData({
                                url: "/user/action/list/",
                                data: app.serializeArraytoJson(table_action.action.serializeArray()),
                                token: "<?= $this->token; ?>",
                                onSuccess: function(response){
                                    // console.log(response);
                                    table_action.loader.hide();
                                    app.createTable({
                                        table: table_action,
                                        data: response.data,
                                        onShow: function(content){
                                            content.find(".btn-trash").on("click", function(event){
                                                event.preventDefault();
                                                // console.log(this);
                                                let id = this.id;
                                                let message = this.getAttribute("data-message");
                                                let status = this.getAttribute("data-status");
                                                if(confirm(message)){
                                                    let loader = app.createLoader(table_action.content, "Menghapus data ...");
                                                    setTimeout(function(){
                                                        app.sendData({
                                                            url: "/user/action/delete/",
                                                            data: JSON.stringify({id: id, status: status}),
                                                            token: "<?= $this->token; ?>",
                                                            onSuccess: function(response){
                                                                // console.log(response);
                                                                loader.hide();
                                                                main.showNotification(response);
                                                                showTableAction();
                                                            },
                                                            onError: function(error){
                                                                // console.log(error);
                                                                loader.hide();
                                                                main.showNotification(error);
                                                            }
                                                        });
                                                    }, 500);
                                                }
                                            });
                                        }
                                    });
                                },
                                onError: function(error){
                                    console.log(error);
                                    main.showNotification(error);
                                }
                            });
                        }, 500);
                    }

                    modul.formModal.content.html(form_action);
                    $.each(object, function(key, val){ modul.formModal.content.find("span[data-form-object='"+key+"']").replaceWith(val); });
                    modul.formModal.content.find(".select2").select2();
                    modul.formModal.content.find("#id_satker").trigger("change");
                    modul.formModal.content.find("#nama_jabatan").html(data.info_jabatan.nama_jabatan);
                    modul.formModal.content.find("#user_action").html(data.user_action);
                    modul.formModal.title.html(data.form_title);
                    // modul.formModal.modal.find(".modal-dialog").css({width: "800px"});
                    modul.formModal.modal.modal("show");
                    modul.formModal.action.off();
                    modul.formModal.action.on("submit", function(event){
                        event.preventDefault();
                        let params = $(modul.formModal.action).serializeArray().concat(table_action.action.serializeArray());
                        let data = JSON.parse(app.serializeArraytoJson(params));
                        console.log(data);
                        let loader = app.createLoader(modul.formModal.action, "Menyimpan data ...");
                        setTimeout(function(){
                            app.sendData({
                                url: "/user/action/save",
                                data: JSON.stringify(data),
                                token: "<?= $this->token; ?>",
                                onSuccess: function(response){
                                    // console.log(response);
                                    loader.hide();
                                    main.showNotification(response);
                                    modul.formModal.content.find("#id_satker").trigger("change");
                                    showTableAction();
                                },
                                onError: function(error){
                                    // console.log(error);
                                    loader.hide();
                                    main.showNotification(error);
                                }
                            });
                        }, 500);
                    });
                    showTableAction();
                });
            },
            onError: function(error){
                // console.log(error);
                main.showNotification(error);
            }
        });
    },
    updateForm: function(endpoint, params, onSuccess){
        app.sendData({
            url: "/data/option/"+endpoint,
            data: JSON.stringify(params),
            token: "<?= $this->token; ?>",
            onSuccess: function(response){
                onSuccess(response);
            },
            onError: function(error){
                console.log(error);
            }
        });
    },
    checkSIPP: function(){
        let loader = app.createLoader(modul.formModal.content, "Check data SIPP");
        setTimeout(function(){
            app.sendData({
                url: "/user/sipp/",
                data: JSON.stringify({nrp: $("#nrp_nip").val()}),
                token: "<?= $this->token; ?>",
                onSuccess: function(response){
                    console.log(response);
                    loader.hide();
                    let data = response.data;
                    $("#id_personil").val(data.id_personil);
                    $("#nama_personil").val(data.nama_personil);
                    $("#pangkat_personil").val(data.pangkat_personil);
                },
                onError: function(error){
                    console.log(error);
                    loader.hide();
                    main.showMessage(error);
                    $("#id_personil").val("");
                    $("#nama_personil").val("");
                    $("#pangkat_personil").val("");
                }
            });
        }, 1000);
    },
};

user.init();