<div class="row">
	<div class="col-md-7 col-xs-7">
		<div class="form-group">
			<label class="control-label">Jabatan</label>
			<span data-form-object="id_user"></span>
			<span data-form-object="id_jabatan"></span>
			<span data-form-object="nama_jabatan"></span>
		</div>
		<div class="form-group">
			<label class="control-label">Bidang</label>
			<span data-form-object="nama_bidang"></span>
		</div>
		<div class="form-group">
			<label class="control-label">Satker</label>
			<span data-form-object="nama_satker"></span>
		</div>
		<div class="form-group">
			<label class="control-label">NRP/NIP</label>
			<div class="row">
				<div class="col-md-7 col-xs-7">
					<span data-form-object="id_personil"></span>
					<span data-form-object="nrp_nip"></span>
				</div>
				<div class="col-md-5 col-xs-5">
					<button type="button" class="btn btn-icon btn-links btn-info btn-sipp">Check SIPP</button>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label">Personil</label>
			<div class="row">
				<div class="col-md-4 col-xs-4">
					<span data-form-object="pangkat_personil"></span>
				</div>
				<div class="col-md-8 col-xs-8">
					<span data-form-object="nama_personil"></span>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label">Password</label>
			<span data-form-object="password_user"></span>
		</div>
	</div>
	<div class="col-md-5 col-xs-5">
		<div class="form-group">
			<label class="control-label" style="margin-bottom:10px;">Level</label>
			<span data-form-object="level_user"></span>
		</div>
		<div class="form-group">
			<label class="control-label" style="margin-bottom:10px;">Disposisi</label>
			<span data-form-object="disposisi_user"></span>
		</div>
		<div class="form-group">
			<label class="control-label" style="margin-bottom:10px;">Koodinasi</label>
			<span data-form-object="koordinasi_user"></span>
		</div>
		<div class="form-group">
			<label class="control-label" style="margin-bottom:10px;">Laporan</label>
			<span data-form-object="laporan_user"></span>
		</div>
		<div class="form-group">
			<label class="control-label" style="margin-bottom:10px;">Draft</label>
			<span data-form-object="draft_user"></span>
		</div>
	</div>
</div>