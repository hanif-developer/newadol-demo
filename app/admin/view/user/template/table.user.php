<style>a.user-action{ cursor: pointer; }</style>
<div class="card-content table-responsive no-padding">
	<table class="table tbl-data table-hover">
	<thead class="text-info">
		<tr>
			<th width="50px">#</th>
			<th width="200px">Jabatan</th>
			<th width="250px">Satker</th>
			<th>User</th>
			<th width="50px">Urutan</th>
			<th width="100px">Aksi</th>
		</tr>
	</thead>
	<tbody>
		<tr class="status_user">
			<td style="vertical-align: top; padding-top: 5px;">{no}</td>
			<td style="vertical-align: top; padding-top: 5px;">
				<strong>{nama_jabatan}</strong> <br>
				<small>BID. {nama_bidang}</small>
			</td>
			<td style="vertical-align: top; padding-top: 5px;">
				<strong>{singkatan_satker}</strong> <br>
				<small>{nama_satker}</small>
			</td>
			<td style="vertical-align: top; padding-top: 5px;">
				<strong>{nama_user}</strong> <br>
				<small>Level: <a>{level_user}</a></small> <br>
				<small>Disposisi: <a id="{id_user}" class="user-action" data-jabatan="{id_jabatan}" data-status="disposisi">{disposisi_user}</a></small> <br>
				<small>Koordinasi: <a id="{id_user}" class="user-action" data-jabatan="{id_jabatan}" data-status="koordinasi">{koordinasi_user}</a></small> <br>
				<small>Laporan: <a id="{id_user}" class="user-action" data-jabatan="{id_jabatan}" data-status="laporan">{laporan_user}</a></small> <br>
				<small>Draft: <a id="{id_user}" class="user-action" data-jabatan="{id_jabatan}" data-status="draft">{draft_user}</a></small> <br>
			</td>
			<td style="vertical-align: top; padding-top: 5px;" align="center">{urutan_jabatan}</td>
			<td style="vertical-align: top;">
				<a title="Ubah Data" id="{id_user}" class="btn btn-icon btn-link btn-success btn-form" data-jabatan="{id_jabatan}"><i class="material-icons">settings</i></a>
				<a title="Hapus Data" id="{id_user}" class="btn btn-icon btn-danger btn-delete" data-message="Yakin data akses user {nama_jabatan} akan dihapus ?"><i class="material-icons">delete</i></a>
			</td>
		</tr>
	</tbody>
	</table>
	<div class="table-pagging" style="border-top: 1px solid #ccc; padding-top: 10px;">
		<ul class="pagination">
			<li class="pagging page-item" number-page="" style="cursor: pointer;"><span class="page-link">{page}</span></li>
		</ul>
	</div>
</div>