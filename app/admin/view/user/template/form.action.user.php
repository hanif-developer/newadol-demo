<div class="row">
	<div class="col-md-12 col-xs-12">
		<div class="form-group">
			<label class="control-label" style="margin-bottom:10px;">Satker</label>
			<span data-form-object="id_satker"></span>
		</div>
		<div class="form-group">
		<label class="control-label" style="margin-bottom:10px;">Bidang</label>
			<span data-form-object="id_bidang"></span>
		</div>
		<div class="form-group">
			<label class="control-label" style="margin-bottom:10px;">Jabatan</label>
			<span data-form-object="id_relasi"></span>
		</div>
	</div>
	<div class="col-md-12 col-xs-12" style="margin-top: 10px; border-top: 1px dashed #ccc;">
		<form class="frmData" onsubmit="return false" autocomplete="off">
			<div class="form-group">
				<div id="nama_jabatan" class="control-label"></div>
				<div id="user_action" class="control-label"></div>
				<span data-form-object="id_jabatan"></span>
				<span data-form-object="status_action"></span>
			</div>
		</form>
		<div class="table-content">
			<?php $this->template('table.action'); ?>
		</div>
		<div class="card-content">
			<div class="query" style="font-size:10pt; margin-bottom:20px;"></div>
			<div class="text-center">
				<div class="spinner-border table-loader" role="status"><div class="loader"></div></div>
			</div>
			<div class="jumbotron jumbotron-fluid text-center table-empty">
				<div class="containers">
					<img src="assets/img/empty-result.png" style="width:100px;" alt="">
					<h4>No user founds</h4>
				</div>
			</div>
		</div>
	</div>
</div>