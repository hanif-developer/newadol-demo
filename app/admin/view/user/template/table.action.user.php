<style>.table-scroll{ height: auto; overflow-y: auto; }</style>
<div class="card-content table-scroll">
	<table class="table tbl-data table-hover">
	<thead class="text-info">
		<tr>
			<th>Jabatan</th>
			<th width="50px">Aksi</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="vertical-align: top; padding-top: 5px;">
				<strong>{nama_jabatan}</strong> <br>
				<small>SATKER: {singkatan_satker}</small> <br>
			</td>
			<td style="vertical-align: top;">
				<a title="Hapus Data" id="{id_user_jabatan}:{id_jabatan}" class="btn btn-icon btn-link btn-danger btn-trash" data-status="{status_action}" data-message="Yakin data user {nama_jabatan} akan dihapus ?"><i class="material-icons">delete</i></a>
			</td>
		</tr>
	</tbody>
	</table>
	<div class="table-pagging" style="border-top: 0px solid #ccc; padding-top: 10px;">
		<ul class="pagination">
			<li class="pagging page-item" number-page="" style="cursor: pointer;"><span class="page-link">{page}</span></li>
		</ul>
	</div>
</div>