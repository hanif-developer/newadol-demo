satker = {
    init: function(){
        url_path = "<?= $url_path; ?>";
        modul = app.initModul("#satker");

        $(document).find(".select2").select2();
        $(document).on("change", ".filter-table", function(event){
            event.preventDefault();
            modul.table.action.find("#page").val(1);
            satker.showTable();
        });
        $(document).on("click", ".btn-form", function(event){
            event.preventDefault();
            satker.showForm(this.id);
        });
        $(document).on("click", ".btn-delete", function(event){
            event.preventDefault();
            let id = this.id;
            let message = this.getAttribute("data-message");
            if(confirm(message)){
                let loader = app.createLoader(modul.table.content, "Menghapus data ...");
                setTimeout(function(){
                    app.sendData({
                        url: "/data/satker/delete/",
                        data: JSON.stringify({id: id}),
                        token: "<?= $this->token; ?>",
                        onSuccess: function(response){
                            // console.log(response);
                            loader.hide();
                            satker.showTable();
                            main.showNotification(response);
                        },
                        onError: function(error){
                            // console.log(error);
                            loader.hide();
                            main.showNotification(error);
                        }
                    });
                }, 500);
            }
        });
        
        satker.showTable();
    },
    showTable: function(){
        modul.table.content.hide();
        modul.table.empty.hide();
        modul.table.loader.show();
        setTimeout(function(){
            // console.log(modul.table);
            app.sendData({
                url: "/data/satker/list/",
                data: app.serializeArraytoJson(modul.table.action.serializeArray()),
                token: "<?= $this->token; ?>",
                onSuccess: function(response){
                    // console.log(response);
                    modul.table.loader.hide();
                    modul.main.find(".table-count").html("Total: "+app.ribuan(response.data.count)+" "+response.data.label);
                    app.createTable({
                        table: modul.table,
                        data: response.data,
                        onShow: function(content){
                            $.each(content.find("tr.status_satker"), function(key, val){ 
                                if(response.data.table[key].status_satker == 0){
                                    $(val).css({"background-color": "#f5f5f5", "color": "#ccc"});
                                }
                            });
                        },
                        onPagging: function(page){
                            modul.table.action.find("#page").val(page);
                            satker.showTable();
                            $(document).scrollTop(0);
                        }
                    });
                },
                onError: function(error){
                    // console.log(error);
                    main.showNotification(error);
                }
            });
        }, 500);
    },
    showForm: function(id){
        app.sendData({
            url: "/data/satker/form/",
            data: JSON.stringify({id: id}),
            token: "<?= $this->token; ?>",
            onSuccess: function(response){
                // console.log(response);
                var data = response.data;
                var form = data.form;
                var object = {
                    id_satker: app.createForm.inputKey("id_satker", form.id_satker),
                    id_subsatker: app.createForm.selectOption("id_subsatker", data.pilihan_satker, form.id_subsatker).attr({class: "form-control select2", style: "width:100%;", required: false}),
                    id_wilayah: app.createForm.selectOption("id_wilayah", data.pilihan_wilayah, form.id_wilayah).attr({class: "form-control select2", style: "width:100%;", required: false}),
                    kategori_satker: app.createForm.selectOption("kategori_satker", data.pilihan_kategori, form.kategori_satker).attr({class: "form-control select2", style: "width:100%;", required: false}),
                    id_regional: app.createForm.selectOption("id_regional", data.pilihan_regional, form.id_regional).attr({class: "form-control select2", style: "width:100%;", required: true}),
                    nama_satker: app.createForm.inputText("nama_satker", form.nama_satker).attr({class: "form-control", required: true}),
                    singkatan_satker: app.createForm.inputText("singkatan_satker", form.singkatan_satker).attr({class: "form-control", required: true}),
                    status_satker: app.createForm.selectOption("status_satker", data.pilihan_status, form.status_satker).attr({class: "form-control select2", style: "width:100%;", required: true}),
                    urutan_satker: app.createForm.inputText("urutan_satker", form.urutan_satker).attr({class: "form-control", required: true, type: "number"}),  
                    logo_satker: app.createForm.uploadImage("file_logo_satker", data.url_logo_satker, data.upload_mimes, data.upload_description),
                };
                modul.formModal.content.html(modul.formModal.objectForm);
                $.each(object, function(key, val){ modul.formModal.content.find("span[data-form-object='"+key+"']").replaceWith(val); });
                modul.formModal.content.find(".select2").select2();
                modul.formModal.title.html(data.form_title);
                modul.formModal.modal.modal("show");
                modul.formModal.action.off();
                modul.formModal.action.on("submit", function(event){
                    event.preventDefault();
                    let data = new FormData($(modul.formModal.action)[0]);
                    let loader = app.createLoader(modul.formModal.action, "Menyimpan data ...");
                    setTimeout(function(){
                        app.sendDataMultipart({
                            url: "/data/satker/save/",
                            data: data,
                            token: "<?= $this->token; ?>",
                            onSuccess: function(response){
                                // console.log(response);
                                loader.hide();
                                main.showNotification(response);
                                modul.formModal.modal.modal("hide");
                                satker.showTable();
                            },
                            onError: function(error){
                                // console.log(error);
                                loader.hide();
                                main.showNotification(error);
                                modul.formModal.modal.modal("hide");
                            }
                        });
                    }, 500);
                });
            },
            onError: function(error){
                // console.log(error);
                main.showNotification(error);
            }
        });
    },
};

satker.init();