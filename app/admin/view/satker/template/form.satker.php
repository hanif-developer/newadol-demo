<div class="row">
	<div class="col-md-5 col-xs-5">
		<div class="form-group">
			<label class="control-label" style="margin-bottom:10px;">Logo Satker</label>
			<span data-form-object="logo_satker"></span>
		</div>
		<div class="form-group">
			<label class="control-label">Status</label>
			<span data-form-object="status_satker"></span>
		</div>
		<div class="form-group">
			<label class="control-label">No. Urut</label>
			<span data-form-object="urutan_satker"></span>
		</div>
	</div>
	<div class="col-md-7 col-xs-7">
		<div class="form-group">
			<label class="control-label" style="margin-bottom:10px;">Regional</label>
			<span data-form-object="id_satker"></span>
			<span data-form-object="id_regional"></span>
		</div>
		<div class="form-group">
			<label class="control-label">Kategori</label>
			<span data-form-object="kategori_satker"></span>
		</div>
		<div class="form-group">
			<label class="control-label">Wilayah</label>
			<span data-form-object="id_wilayah"></span>
		</div>
		<div class="form-group">
			<label class="control-label">Sub Satker</label>
			<span data-form-object="id_subsatker"></span>
		</div>
		<div class="form-group">
			<label class="control-label">Satker</label>
			<span data-form-object="nama_satker"></span>
		</div>
		<div class="form-group">
			<label class="control-label">Singkatan</label>
			<div class="row">
				<div class="col-md-5">
					<span data-form-object="singkatan_satker"></span>
				</div>
			</div>
		</div>
	</div>
</div>