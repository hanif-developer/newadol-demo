<div class="card-content table-responsive no-padding">
	<table class="table tbl-data table-hover">
	<thead class="text-info">
		<tr>
			<th width="50px">#</th>
			<th>ID</th>
			<th>Satker</th>
			<th>Sub Satker</th>
			<th>Wilayah</th>
			<th>Kategori</th>
			<th>Regional</th>
			<th width="50px">Urutan</th>
			<th width="100px">Aksi</th>
		</tr>
	</thead>
	<tbody>
		<tr class="status_satker">
			<td style="vertical-align: top; padding-top: 5px;">{no}</td>
			<td style="vertical-align: top; padding-top: 5px;">{id_satker}</td>
			<td style="vertical-align: top; padding-top: 5px;">
				<strong>{singkatan_satker}</strong> <br>
				<small>{nama_satker}</small>
			</td>
			<td style="vertical-align: top; padding-top: 5px;">{nama_subsatker}</td>
			<td style="vertical-align: top; padding-top: 5px;">{nama_wilayah}</td>
			<td style="vertical-align: top; padding-top: 5px;">{nama_kategori}</td>
			<td style="vertical-align: top; padding-top: 5px;">{nama_regional}</td>
			<td style="vertical-align: top; padding-top: 5px;" align="center">{urutan_satker}</td>
			<td style="vertical-align: top;">
				<a title="Ubah Data" id="{id_satker}" class="btn btn-icon btn-link btn-success btn-form"><i class="material-icons">mode_edit</i></a>
				<a title="Hapus Data" id="{id_satker}" class="btn btn-icon btn-danger btn-delete" data-message="Yakin data satker {singkatan_satker} akan dihapus ?"><i class="material-icons">delete</i></a>
			</td>
		</tr>
	</tbody>
	</table>
	<div class="table-pagging" style="border-top: 1px solid #ccc; padding-top: 10px;">
		<ul class="pagination">
			<li class="pagging page-item" number-page="" style="cursor: pointer;"><span class="page-link">{page}</span></li>
		</ul>
	</div>
</div>