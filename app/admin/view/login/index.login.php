<body class="login">
<div class="background-filter"></div>
<div class="content">
	<div class="col-md-4 col-sm-offset-4">
		<div class="card card-profile" style="margin-top:100px; background-color: rgba(255,255,255,.8);">
			<div class="card-avatar" style="padding: 20px; background-color: #fff;">
				<img class="img" src="<?= $this->link($this->logo_adol_icon); ?>" />
			</div>
			<div class="content">
				<h4 class="card-title"><?= $page_title; ?></h4>
				<div class="card-content">
					<div class="form-loading"></div>
					<form id="frmInput" method="post" onsubmit="return login();" class="form-login" autocomplete="off">
						<?= comp\BOOTSTRAP::inputText('password', 'password', '', 'class="form-control" style="text-align: center;" required'); ?>
						<button submit="button" id="btnLogin" class="btn btn-primary btn-block btn-round">MASUK</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?= $jsPath; ?>	
<script>
	login = function(url){
		$(".form-login").hide();
		$(".form-loading").html('<div class="loader"></div><br/><br/><span>Authenticating..</span>').show();
		setTimeout(function(){
			$.post("<?= $url_path.'/validate'; ?>", $("#frmInput").serialize(), function(response){
				$(".form-loading").html(response.message);
				setTimeout(function(){
					if(response.status == 'error'){
						$(".form-loading").hide();
						$(".form-login").show();
					}
					else{
						$(".form-loading").html('<div class="loader"></div><br/><br/><span>Redirecting..</span>').show();
						window.location.reload();
					}
				}, 1000);
			}, "json");
		}, 500);

		return false;
	};
</script>
</body>