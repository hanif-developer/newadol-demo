<div class="card-content table-responsive no-padding">
	<table class="table tbl-data table-hover">
	<thead class="text-info">
		<tr>
			<th width="50px">#</th>
			<th>Jabatan</th>
			<th>Satker</th>
			<th>Browser</th>
			<th width="100px">Active User</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="vertical-align: top; padding-top: 5px;">{no}</td>
			<td style="vertical-align: top; padding-top: 5px;">{nama_jabatan}</td>
			<td style="vertical-align: top; padding-top: 5px;">{nama_satker}</td>
			<td style="vertical-align: top; padding-top: 5px;">{user_agent}</td>
			<td  style="vertical-align: top; padding-top: 5px;"align="center"><span class="btn btn-info">{active_user}</span></td>
		</tr>
	</tbody>
	</table>
	<div class="table-pagging" style="border-top: 1px solid #ccc; padding-top: 10px;">
		<ul class="pagination">
			<li class="pagging page-item" number-page="" style="cursor: pointer;"><span class="page-link">{page}</span></li>
		</ul>
	</div>
</div>