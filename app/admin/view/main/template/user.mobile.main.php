<div class="card-content" style="margin: 10px 0px;">
	<form class="frmData" onsubmit="return false" autocomplete="off">
		<div class="row">
			<div class="col-md-3"></div>
		</div>
		<?= comp\BOOTSTRAP::inputKey('register', 'mobile'); ?>
		<?= comp\BOOTSTRAP::inputKey('page', '1'); ?>
	</form>
</div>
<div class="table-content">
	<?php $this->template('user.mobile.table'); ?>
</div>
<div class="card-content">
	<div class="query" style="font-size:10pt; margin-bottom:20px;"></div>
	<div class="text-center">
		<div class="spinner-border table-loader" role="status"><div class="loader"></div></div>
	</div>
	<div class="jumbotron jumbotron-fluid text-center table-empty">
		<div class="containers">
			<img src="assets/img/empty-result.png" style="width:100px;" alt="">
			<h4>No active user</h4>
		</div>
	</div>
</div>