main = {
    dashboard: function(){
        isRealtime = true;
        moduls = {
            "#mobile": app.initModul("#mobile"),
            "#desktop": app.initModul("#desktop"),
        };
        
        trafficChart = {
            in: { title: "Traffic In", series: [] },
            out: { title: "Traffic Out", series: [] },
        };

        $(document).on("shown.bs.tab", "a[data-toggle='tab']", function(){
            modul = moduls[$(this).data("target")];
            main.showTable();
        });

        main.createTrafficChart("#tMasukChart", trafficChart.in, function(series){
            setInterval(function () {
                if(isRealtime){
                    $.get("<?= $main_path.'/traffic/rx'; ?>", function(response) {
                        series.addPoint(response.data.traffic, true, true);
                        // console.log("api", response.data);
                    }, "json");
                }
                else{
                    var x = (new Date()).getTime() + 10, // current time
                        y = Math.random() * 100;
                    series.addPoint([x, y], true, true);
                }
            }, 5000);
        });
        main.createTrafficChart("#tKeluarChart", trafficChart.out, function(series){
            setInterval(function () {
                if(isRealtime){
                    $.get("<?= $main_path.'/traffic/tx'; ?>", function(response) {
                        series.addPoint(response.data.traffic, true, true);
                        // console.log("api", response.data);
                    }, "json");
                }
                else{
                    var x = (new Date()).getTime() + 20, // current time
                        y = Math.random() * 100;
                    series.addPoint([x, y], true, true);
                }
            }, 5000);
        });

        $.get("<?= $main_path.'/server/sipp'; ?>", function(response) {
            let server_sipp = $("#server_sipp");
            server_sipp.find(".status").html(response.data.status);
            server_sipp.find(".detail").html(response.data.detail);
            server_sipp.find(".card-header").attr("data-background-color", response.data.color);
            // console.log("api", response.data);
        }, "json");

        $("a[data-target='#desktop']").trigger("shown.bs.tab");
    },
    showTable: function(){
        modul.table.content.hide();
        modul.table.empty.hide();
        modul.table.loader.show();
        setTimeout(function(){
            app.sendData({
                url: "/user/register/active/",
                data: app.serializeArraytoJson(modul.table.action.serializeArray()),
                token: "<?= $this->token; ?>",
                onSuccess: function(response){
                    // console.log(response);
                    modul.table.loader.hide();
                    app.createTable({
                        table: modul.table,
                        data: response.data,
                        onShow: function(content){
                            // content.find("[data-toggle='tooltip']").tooltip();
                        },
                        onPagging: function(page){
                            modul.table.action.find("#page").val(page);
                            regional.showTable();
                            $(document).scrollTop(0);
                        }
                    });
                },
                onError: function(error){
                    // console.log(error);
                    main.showNotification(error);
                }
            });
        }, 500);
    },
    showMessage: function(response, callback){
        swal({title: response.message.title, text: response.message.text, type: response.status, confirmButtonClass: "btn-primary", confirmButtonText: "OK"}, function(){
            if(typeof callback === "function") callback();
        });
    },
    showNotification: function(response){
        let icon = (response.status == "success") ? "check" : "cancel";
        let type = (response.status == "success") ? "success" : "danger";
    	$.notify({
        	icon: icon,
        	message: response.message.text
        },{
            type: type,
            timer: 500,
            placement: {
                from: "top",
                align: "right"
            }
        });
    },
    createTrafficChart: function(id, data, onLoad){
        $(id).highcharts({
            chart: {
                type: "spline",
                // type: "area",
                marginRight: 10,
                events: {
                    load: function () {
                        var series = this.series[0];
                        onLoad(series);
                    }
                }
            },
            title: {text: data.title.toUpperCase()},
            legend: {enabled: false},
            exporting: {enabled: false},
            credits: {enabled: false},
            xAxis: {
                type: "datetime",
                tickPixelInterval: 150
            },
            yAxis: {
                title: {
                    text: "Kbps rate"
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: "#808080"
                }]
            },
            tooltip: {
                headerFormat: "<b>{series.name}</b><br/>",
                pointFormat: "{point.y:.2f} kbps"
            },
            series: [{
                name: data.title,
                data: (function () {
                    // generate an array of random data
                    var data = [],
                        time = (new Date()).getTime(),
                        i;
        
                    for (i = -10; i <= 0; i += 1) {
                        data.push({
                            x: time + i * 1000,
                            y: Math.random() * 100,
                        });
                    }
                    // console.log(data);
                    return data;
                }())
            }]
        });
    }
};