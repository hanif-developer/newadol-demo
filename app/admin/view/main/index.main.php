<body>
<div class="wrapper">
	<?php $this->getView('admin', 'main', 'navbar', []); ?>
	<div class="main-panel">
		<?php $this->getView('admin', 'main', 'header', $data); ?>
		<div class="content">
			<div class="container-fluid">
				<section class="content">
					<div class="row">
						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="card card-stats">
								<div class="card-header" data-background-color="green">
									<i class="material-icons">phone_android</i>
								</div>
								<div class="card-content">
									<p class="category">Mobile Online</p>
									<h3 class="title mobile"><?= $statistik['mobile']; ?><small> User</small></h3>
								</div>
								<div class="card-footer">
									<div class="stats">
										<i class="material-icons">update</i> Just Updated
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="card card-stats">
								<div class="card-header" data-background-color="blue">
									<i class="material-icons">desktop_windows</i>
								</div>
								<div class="card-content">
									<p class="category">Computer Online</p>
									<h3 class="title computer"><?= $statistik['desktop']; ?><small> User</small></h3>
								</div>
								<div class="card-footer">
									<div class="stats">
										<i class="material-icons">update</i> Just Updated
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="card card-stats">
								<div class="card-header" data-background-color="purple">
									<i class="material-icons">people</i>
								</div>
								<div class="card-content">
									<p class="category">Total User</p>
									<h3 class="title user"><?= $statistik['user']; ?><small> User</small></h3>
								</div>
								<div class="card-footer">
									<div class="stats">
										<i class="material-icons">update</i> Just Updated
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-6">
							<div id="server_sipp" class="card card-stats">
								<div class="card-header" data-background-color="">
									<i class="material-icons">vpn_lock</i>
								</div>
								<div class="card-content">
									<p class="category">Server SIPP</p>
									<h3 class="title status"></small></h3>
								</div>
								<div class="card-footer">
									<div class="stats">
										<i class="material-icons">update</i> <span class="detail">Just Updated</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="card card-nav-tabs">
								<div class="card-header" data-background-color="blue">
									<div class="nav-tabs-navigation">
										<div class="nav-tabs-wrapper">
											<span class="nav-tabs-title">Active User :</span>
											<ul class="nav nav-tabs" data-tabs="tabs">
												<li class="active">
													<a data-target="#desktop" data-toggle="tab" style="cursor: pointer;">
														<i class="material-icons">desktop_windows</i>
														Desktop<span id="dataRecordComputer"></span>
													<div class="ripple-container"></div></a>
												</li>
												<li>
													<a data-target="#mobile" data-toggle="tab" style="cursor: pointer;">
														<i class="material-icons">phone_android</i>
														Mobile <span id="dataRecordMobile"></span>
													<div class="ripple-container"></div></a>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="card-content">
									<div class="tab-content">
										<div class="tab-pane active" id="desktop">
											<?php $this->template('user.desktop'); ?>
										</div>
										<div class="tab-pane" id="mobile">
											<?php $this->template('user.mobile'); ?>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="card">
								<div class="card-header card-chart" data-background-color="green">
									<div class="ct-chart" id="tMasukChart">
										<div class="loading" style="text-align:center;padding-top:50px;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><br/><br/><span>Loading...</span></div>
									</div>
								</div>
								<div class="card-content">
									<h4 class="title">&nbsp;</h4>
									<p class="category"><span id="trafik_rx" class="text-success"></p>
								</div>
								<div class="card-footer">
									<div class="stats">
										<i class="material-icons">update</i> Just Updated
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="card">
								<div class="card-header card-chart" data-background-color="green">
									<div class="ct-chart" id="tKeluarChart">
										<div class="loading" style="text-align:center;padding-top:50px;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><br/><br/><span>Loading...</span></div>
									</div>
								</div>
								<div class="card-content">
									<h4 class="title">&nbsp;</h4>
									<p class="category"><span id="trafik_tx" class="text-success"></p>
								</div>
								<div class="card-footer">
									<div class="stats">
										<i class="material-icons">update</i> Just Updated
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<?php $this->getView('admin', 'main', 'footer', []); ?>
		</div>
	</div>
</div>
<?= $jsPath; ?>	
<script src="<?= $api_path.'/script'; ?>"></script>
<script src="<?= $main_path.'/script'; ?>"></script>
<script>main.dashboard();</script>
</body>