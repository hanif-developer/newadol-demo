<div class="main-sidebar sidebar" data-color="blue" data-image="assets/img/sidebar-1.jpg">
	<div class="logo">
		<a href="<?= $this->link($this->getProject()); ?>" class="simple-text"><img src="<?= $this->link($this->logo_adol_text); ?>" style="width:200px;" alt="" /></a>
	</div>
	<div class="sidebar-wrapper">
		<ul class="sidebar-menu nav">
			<li id="mn_dashboard"><a class="nav-link pointer" href="<?= $this->link_main; ?>"><i class="material-icons">dashboard</i><p>Dashboard</p></a></li>
			<li style="display:none;" id="mn_regional"><a class="nav-link pointer" href="<?= $this->link_regional; ?>"><i class="material-icons">language</i><p>Regional</p></a></li>
			<li style="display:none;" id="mn_wilayah"><a class="nav-link pointer" href="<?= $this->link_wilayah; ?>"><i class="material-icons">wifi_tethering</i><p>Wilayah</p></a></li>
			<li id="mn_satker"><a class="nav-link pointer" href="<?= $this->link_satker; ?>"><i class="material-icons">store</i><p>Satuan Kerja</p></a></li>
			<li id="mn_bidang"><a class="nav-link pointer" href="<?= $this->link_bidang; ?>"><i class="material-icons">class</i><p>Bidang</p></a></li>
			<li id="mn_jabatan"><a class="nav-link pointer" href="<?= $this->link_jabatan; ?>"><i class="material-icons">weekend</i><p>Jabatan</p></a></li>
			<li id="mn_personil"><a class="nav-link pointer" href="<?= $this->link_personil; ?>"><i class="material-icons">people</i><p>Personil (SIPP)</p></a></li>
			<li id="mn_user"><a class="nav-link pointer" href="<?= $this->link_user; ?>"><i class="material-icons">supervisor_account</i><p>User</p></a></li>
		</ul>
	</div>
</div>