<body>
<div class="wrapper">
	<?php $this->getView('admin', 'main', 'navbar', []); ?>
	<div class="main-panel">
		<?php $this->getView('admin', 'main', 'header', []); ?>
		<div class="content">
			<div class="container-fluid">
				<section class="content">
					
				</section>
			</div>
			<?php $this->getView('admin', 'main', 'footer', []); ?>
		</div>
	</div>
</div>
<?= $jsPath; ?>	
</body>