<nav class="main-header navbar navbar-transparent navbar-absolute">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a id="header-title" class="navbar-brand"><?= $this->data['page_title']; ?></a>
			<span class="session"></span>
		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<p><i class="material-icons">person</i>Administrator</p>
					</a>
					<ul class="dropdown-menu">
						<li><a href="<?= $this->link($this->getProject().$this->getController().'/logout'); ?>">Keluar</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</nav>