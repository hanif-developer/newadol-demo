<div class="card-content table-responsive no-padding">
	<table class="table tbl-data table-hover">
	<thead class="text-info">
		<tr>
			<th width="50px">#</th>
			<th>ID</th>
			<th>Bidang</th>
			<th>Satker</th>
			<th width="100px">Aksi</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="vertical-align: top; padding-top: 5px;">{no}</td>
			<td style="vertical-align: top; padding-top: 5px;">{id_bidang}</td>
			<td style="vertical-align: top; padding-top: 5px;">{nama_bidang}</td>
			<td style="vertical-align: top; padding-top: 5px;">
				<strong>{singkatan_satker}</strong> <br>
				<small>{nama_satker}</small>
			</td>
			<td style="vertical-align: top;">
				<a title="Ubah Data" id="{id_bidang}" class="btn btn-icon btn-link btn-success btn-form"><i class="material-icons">mode_edit</i></a>
				<a title="Hapus Data" id="{id_bidang}" class="btn btn-icon btn-danger btn-delete" data-message="Yakin data bidang {nama_bidang} akan dihapus ?"><i class="material-icons">delete</i></a>
			</td>
		</tr>
	</tbody>
	</table>
	<div class="table-pagging" style="border-top: 1px solid #ccc; padding-top: 10px;">
		<ul class="pagination">
			<li class="pagging page-item" number-page="" style="cursor: pointer;"><span class="page-link">{page}</span></li>
		</ul>
	</div>
</div>