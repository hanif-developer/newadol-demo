bidang = {
    init: function(){
        url_path = "<?= $url_path; ?>";
        modul = app.initModul("#bidang");

        $(document).find(".select2").select2();
        $(document).on("change", ".filter-table", function(event){
            event.preventDefault();
            modul.table.action.find("#page").val(1);
            bidang.showTable();
        });
        $(document).on("click", ".btn-form", function(event){
            event.preventDefault();
            bidang.showForm(this.id);
        });
        $(document).on("click", ".btn-delete", function(event){
            event.preventDefault();
            let id = this.id;
            let message = this.getAttribute("data-message");
            if(confirm(message)){
                let loader = app.createLoader(modul.table.content, "Menghapus data ...");
                setTimeout(function(){
                    app.sendData({
                        url: "/data/bidang/delete/",
                        data: JSON.stringify({id: id}),
                        token: "<?= $this->token; ?>",
                        onSuccess: function(response){
                            // console.log(response);
                            loader.hide();
                            bidang.showTable();
                            main.showNotification(response);
                        },
                        onError: function(error){
                            // console.log(error);
                            loader.hide();
                            main.showNotification(error);
                        }
                    });
                }, 500);
            }
        });
        
        bidang.showTable();
    },
    showTable: function(){
        modul.table.content.hide();
        modul.table.empty.hide();
        modul.table.loader.show();
        setTimeout(function(){
            // console.log(modul.table);
            app.sendData({
                url: "/data/bidang/list/",
                data: app.serializeArraytoJson(modul.table.action.serializeArray()),
                token: "<?= $this->token; ?>",
                onSuccess: function(response){
                    // console.log(response);
                    modul.table.loader.hide();
                    modul.main.find(".table-count").html("Total: "+app.ribuan(response.data.count)+" "+response.data.label);
                    app.createTable({
                        table: modul.table,
                        data: response.data,
                        onShow: function(content){
                            // content.find("[data-toggle='tooltip']").tooltip();
                        },
                        onPagging: function(page){
                            modul.table.action.find("#page").val(page);
                            bidang.showTable();
                            $(document).scrollTop(0);
                        }
                    });
                },
                onError: function(error){
                    // console.log(error);
                    main.showNotification(error);
                }
            });
        }, 500);
    },
    showForm: function(id){
        app.sendData({
            url: "/data/bidang/form/",
            data: JSON.stringify({id: id}),
            token: "<?= $this->token; ?>",
            onSuccess: function(response){
                // console.log(response);
                var data = response.data;
                var form = data.form;
                var object = {
                    id_bidang: app.createForm.inputKey("id_bidang", form.id_bidang),
                    id_satker: app.createForm.selectOption("id_satker", data.pilihan_satker, form.id_satker).attr({class: "form-control select2", style: "width:100%;", required: true}),
                    nama_bidang: app.createForm.inputText("nama_bidang", form.nama_bidang).attr({class: "form-control", required: true}),
                };
                modul.formModal.content.html(modul.formModal.objectForm);
                $.each(object, function(key, val){ modul.formModal.content.find("span[data-form-object='"+key+"']").replaceWith(val); });
                modul.formModal.content.find(".select2").select2();
                modul.formModal.title.html(data.form_title);
                modul.formModal.modal.modal("show");
                modul.formModal.action.off();
                modul.formModal.action.on("submit", function(event){
                    event.preventDefault();
                    let data = new FormData($(modul.formModal.action)[0]);
                    let loader = app.createLoader(modul.formModal.action, "Menyimpan data ...");
                    setTimeout(function(){
                        app.sendDataMultipart({
                            url: "/data/bidang/save/",
                            data: data,
                            token: "<?= $this->token; ?>",
                            onSuccess: function(response){
                                // console.log(response);
                                loader.hide();
                                main.showNotification(response);
                                modul.formModal.modal.modal("hide");
                                bidang.showTable();
                            },
                            onError: function(error){
                                // console.log(error);
                                loader.hide();
                                main.showNotification(error);
                                modul.formModal.modal.modal("hide");
                            }
                        });
                    }, 500);
                });
            },
            onError: function(error){
                // console.log(error);
                main.showNotification(error);
            }
        });
    },
};

bidang.init();