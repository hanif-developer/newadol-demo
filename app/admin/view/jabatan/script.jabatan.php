jabatan = {
    init: function(){
        url_path = "<?= $url_path; ?>";
        modul = app.initModul("#jabatan");

        $(document).find(".select2").select2();
        $(document).on("change", ".filter-table", function(event){
            event.preventDefault();
            modul.table.action.find("#page").val(1);
            let update_form = $(this).data("update-form");
            switch (update_form) {
                case "bidang":
                    jabatan.updateForm(update_form, {satker: $("#satker").val()}, function(response){
                        // console.log(response);
                        update_form = $("#"+update_form);
                        app.updateForm.selectOption(update_form, response.data, "");
                        update_form.val("").trigger("change");
                    });
                    break;
            
                default:
                    jabatan.showTable();
                    break;
            }
        });
        $(document).on("change", ".filter-form", function(event){
            event.preventDefault();
            let update_form = $(this).data("update-form");
            switch (update_form) {
                case "id_bidang":
                    jabatan.updateForm("bidang", {satker: $("#id_satker").val()}, function(response){
                        update_form = $("#"+update_form);
                        response.data[""].text = "-";
                        app.updateForm.selectOption(update_form, response.data, "");
                        update_form.val("").trigger("change");
                    });
                    break;
            
                default:
                    break;
            }
        });
        $(document).on("click", ".btn-form", function(event){
            event.preventDefault();
            jabatan.showForm(this.id, this.getAttribute("data-satker"));
        });
        $(document).on("click", ".btn-delete", function(event){
            event.preventDefault();
            let id = this.id;
            let message = this.getAttribute("data-message");
            if(confirm(message)){
                let loader = app.createLoader(modul.table.content, "Menghapus data ...");
                setTimeout(function(){
                    app.sendData({
                        url: "/data/jabatan/delete/",
                        data: JSON.stringify({id: id}),
                        token: "<?= $this->token; ?>",
                        onSuccess: function(response){
                            // console.log(response);
                            loader.hide();
                            jabatan.showTable();
                            main.showNotification(response);
                        },
                        onError: function(error){
                            // console.log(error);
                            loader.hide();
                            main.showNotification(error);
                        }
                    });
                }, 500);
            }
        });
        
        jabatan.showTable();
    },
    showTable: function(){
        modul.table.content.hide();
        modul.table.empty.hide();
        modul.table.loader.show();
        setTimeout(function(){
            // console.log(modul.table);
            app.sendData({
                url: "/data/jabatan/list/",
                data: app.serializeArraytoJson(modul.table.action.serializeArray()),
                token: "<?= $this->token; ?>",
                onSuccess: function(response){
                    // console.log(response);
                    modul.table.loader.hide();
                    modul.main.find(".table-count").html("Total: "+app.ribuan(response.data.count)+" "+response.data.label);
                    app.createTable({
                        table: modul.table,
                        data: response.data,
                        onShow: function(content){
                            $.each(content.find("tr.status_jabatan"), function(key, val){ 
                                if(response.data.table[key].status_jabatan == 0){
                                    $(val).css({"background-color": "#f5f5f5", "color": "#ccc"});
                                }
                            });
                        },
                        onPagging: function(page){
                            modul.table.action.find("#page").val(page);
                            jabatan.showTable();
                            $(document).scrollTop(0);
                        }
                    });
                },
                onError: function(error){
                    // console.log(error);
                    main.showNotification(error);
                }
            });
        }, 500);
    },
    showForm: function(id, satker = null){
        satker = (satker == null) ? $("#satker").val() : satker;
        if(satker == ""){
            main.showMessage({status: "error", message: {title: "Maaf", text: "Pilih satker terlebih dahulu .."}});
            return false;
        }
        app.sendData({
            url: "/data/jabatan/form/",
            data: JSON.stringify({id: id, satker: satker}),
            token: "<?= $this->token; ?>",
            onSuccess: function(response){
                // console.log(response);
                var data = response.data;
                var form = data.form;
                var object = {
                    id_jabatan: app.createForm.inputKey("id_jabatan", form.id_jabatan),
                    id_satker: app.createForm.selectOption("id_satker", data.pilihan_satker, form.id_satker).attr({class: "form-control filter-form select2", "data-update-form": "id_bidang", style: "width:100%;", required: true}),
                    id_bidang: app.createForm.selectOption("id_bidang", data.pilihan_bidang, form.id_bidang).attr({class: "form-control select2", style: "width:100%;", required: false}),
                    nama_jabatan: app.createForm.inputText("nama_jabatan", form.nama_jabatan).attr({class: "form-control", required: true}),
                    status_jabatan: app.createForm.selectOption("status_jabatan", data.pilihan_status, form.status_jabatan).attr({class: "form-control select2", style: "width:100%;", required: true}),
                    urutan_jabatan: app.createForm.inputText("urutan_jabatan", form.urutan_jabatan).attr({class: "form-control", required: true, type: "number"}),  
                };
                modul.formModal.content.html(modul.formModal.objectForm);
                $.each(object, function(key, val){ modul.formModal.content.find("span[data-form-object='"+key+"']").replaceWith(val); });
                modul.formModal.content.find(".select2").select2();
                // modul.formModal.content.find("#id_satker").trigger("change");
                modul.formModal.title.html(data.form_title);
                modul.formModal.modal.modal("show");
                modul.formModal.action.off();
                modul.formModal.action.on("submit", function(event){
                    event.preventDefault();
                    let data = new FormData($(modul.formModal.action)[0]);
                    let loader = app.createLoader(modul.formModal.action, "Menyimpan data ...");
                    setTimeout(function(){
                        app.sendDataMultipart({
                            url: "/data/jabatan/save/",
                            data: data,
                            token: "<?= $this->token; ?>",
                            onSuccess: function(response){
                                // console.log(response);
                                loader.hide();
                                main.showNotification(response);
                                modul.formModal.modal.modal("hide");
                                jabatan.showTable();
                            },
                            onError: function(error){
                                // console.log(error);
                                loader.hide();
                                main.showNotification(error);
                                modul.formModal.modal.modal("hide");
                            }
                        });
                    }, 500);
                });
            },
            onError: function(error){
                // console.log(error);
                main.showNotification(error);
            }
        });
    },
    updateForm: function(endpoint, params, onSuccess){
        app.sendData({
            url: "/data/option/"+endpoint,
            data: JSON.stringify(params),
            token: "<?= $this->token; ?>",
            onSuccess: function(response){
                onSuccess(response);
            },
            onError: function(error){
                console.log(error);
            }
        });
    },
};

jabatan.init();