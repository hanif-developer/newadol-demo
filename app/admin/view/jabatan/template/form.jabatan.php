<div class="form-group">
	<label class="control-label" style="margin-bottom:10px;">Satker</label>
	<span data-form-object="id_jabatan"></span>
	<span data-form-object="id_satker"></span>
</div>
<div class="form-group">
	<label class="control-label" style="margin-bottom:10px;">Bidang</label>
	<span data-form-object="id_bidang"></span>
</div>
<div class="form-group">
	<label class="control-label">Jabatan</label>
	<span data-form-object="nama_jabatan"></span>
</div>
<div class="form-group">
	<label class="control-label">Status</label>
	<span data-form-object="status_jabatan"></span>
</div>
<div class="form-group">
	<label class="control-label">No. Urut</label>
	<span data-form-object="urutan_jabatan"></span>
</div>