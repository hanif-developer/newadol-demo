<body>
<div class="wrapper">
	<?php $this->getView('admin', 'main', 'modal', 'form'); ?>
	<?php $this->getView('admin', 'main', 'navbar', []); ?>
	<div class="main-panel">
		<?php $this->getView('admin', 'main', 'header', $data); ?>
		<div class="content">
			<div class="container-fluid">
				<section class="content">
					<div id="wilayah" class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header" data-background-color="blue">
									<div class="mdl-card__actions">
										<a id="" class="btn btn-menu btn-form"><i class="material-icons">note_add</i> Tambah Wilayah</a>
										<span class="table-count btn btn-menu pull-right"></span>
									</div>
								</div>
								<div class="card-content" style="margin: 20px 0px;">
									<form class="frmData" onsubmit="return false" autocomplete="off">
										<div class="row">
											<div class="col-md-3">
												<?= comp\BOOTSTRAP::inputSelect('regional', $pilihan_regional, $regional, 'class="form-control filter-table select2" style="width:100%"'); ?>
											</div>
										</div>
										<?= comp\BOOTSTRAP::inputKey('page', '1'); ?>
									</form>
								</div>
								<div class="table-content">
                                    <?php $this->template('table'); ?>
                                </div>
								<div class="card-content">
                                    <div class="query" style="font-size:10pt; margin-bottom:20px;"></div>
                                    <div class="text-center">
                                        <div class="spinner-border table-loader" role="status"><div class="loader"></div></div>
                                    </div>
                                    <div class="jumbotron jumbotron-fluid text-center table-empty">
                                        <div class="containers">
											<img src="assets/img/empty-result.png" style="width:100px;" alt="">
                                            <h4>No results founds</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-content">
									<?php $this->template('form'); ?>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<?php $this->getView('admin', 'main', 'footer', []); ?>
		</div>
	</div>
</div>
<?= $jsPath; ?>	
<script src="<?= $api_path.'/script'; ?>"></script>
<script src="<?= $main_path.'/script'; ?>"></script>
<script src="<?= $url_path.'/script'; ?>"></script>
</body>