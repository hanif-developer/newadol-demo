<div class="card-content table-responsive no-padding">
	<table class="table tbl-data table-hover">
	<thead class="text-info">
		<tr>
			<th width="50px">#</th>
			<th>ID</th>
			<th>Regional</th>
			<th>Server</th>
			<th width="50px">Urutan</th>
			<th width="100px">Aksi</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>{no}</td>
			<td>{id_regional}</td>
			<td>{nama_regional}</td>
			<td>{alamat_server}</td>
			<td align="center">{urutan_regional}</td>
			<td>
				<a title="Ubah Data" id="{id_regional}" class="btn btn-icon btn-link btn-success btn-form"><i class="material-icons">mode_edit</i></a>
				<a title="Hapus Data" id="{id_regional}" class="btn btn-icon btn-danger btn-delete" data-message="Yakin data regional {nama_regional} akan dihapus ?"><i class="material-icons">delete</i></a>
			</td>
		</tr>
	</tbody>
	</table>
	<div class="table-pagging" style="border-top: 1px solid #ccc; padding-top: 10px;">
		<ul class="pagination">
			<li class="pagging page-item" number-page="" style="cursor: pointer;"><span class="page-link">{page}</span></li>
		</ul>
	</div>
</div>