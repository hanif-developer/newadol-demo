<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label">NRP/NIP</label>
			<span data-form-object="id_personil"></span>
		</div>
		<div class="form-group">
			<label class="control-label">Nama Personil</label>
			<span data-form-object="nama_personil"></span>
		</div>
		<div class="form-group">
			<label class="control-label">Pangkat</label>
			<span data-form-object="pangkat_personil"></span>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label">Jabatan</label>
			<span data-form-object="jabatan_personil"></span>
		</div>
		<div class="form-group">
			<label class="control-label">Email</label>
			<span data-form-object="email_personil"></span>
		</div>
		<div class="form-group">
			<label class="control-label">No. Telepon</label>
			<span data-form-object="telp_personil"></span>
		</div>
	</div>
</div>