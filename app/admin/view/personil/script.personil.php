personil = {
    init: function(){
        url_path = "<?= $url_path; ?>";
        modul = app.initModul("#personil");

        $(document).find(".select2").select2();
        $(document).on("change", ".filter-table", function(event){
            event.preventDefault();
            modul.table.action.find("#page").val(1);
            personil.showTable();
        });
        $(document).on("click", ".btn-form", function(event){
            event.preventDefault();
            personil.showForm(this.id);
        });
        $(document).on("click", ".btn-delete", function(event){
            event.preventDefault();
            let id = this.id;
            let message = this.getAttribute("data-message");
            if(confirm(message)){
                let loader = app.createLoader(modul.table.content, "Menghapus data ...");
                setTimeout(function(){
                    app.sendData({
                        url: "/data/personil/delete/",
                        data: JSON.stringify({id: id}),
                        token: "<?= $this->token; ?>",
                        onSuccess: function(response){
                            // console.log(response);
                            loader.hide();
                            personil.showTable();
                            main.showNotification(response);
                        },
                        onError: function(error){
                            // console.log(error);
                            loader.hide();
                            main.showNotification(error);
                        }
                    });
                }, 500);
            }
        });
        
        personil.showTable();
    },
    showTable: function(){
        modul.table.content.hide();
        modul.table.empty.hide();
        modul.table.loader.show();
        setTimeout(function(){
            // console.log(modul.table);
            app.sendData({
                url: "/data/personil/list/",
                data: app.serializeArraytoJson(modul.table.action.serializeArray()),
                token: "<?= $this->token; ?>",
                onSuccess: function(response){
                    // console.log(response);
                    modul.table.loader.hide();
                    modul.main.find(".table-count").html("Total: "+app.ribuan(response.data.count)+" "+response.data.label);
                    app.createTable({
                        table: modul.table,
                        data: response.data,
                        onShow: function(content){
                            // content.find("[data-toggle='tooltip']").tooltip();
                        },
                        onPagging: function(page){
                            modul.table.action.find("#page").val(page);
                            personil.showTable();
                            $(document).scrollTop(0);
                        }
                    });
                },
                onError: function(error){
                    // console.log(error);
                    main.showNotification(error);
                }
            });
        }, 500);
    },
    showForm: function(id){
        app.sendData({
            url: "/data/personil/form/",
            data: JSON.stringify({id: id}),
            token: "<?= $this->token; ?>",
            onSuccess: function(response){
                // console.log(response);
                var data = response.data;
                var form = data.form;
                var object = {
                    id_personil: app.createForm.inputText("id_personil", form.id_personil).attr({class: "form-control", required: true}),
                    nama_personil: app.createForm.inputText("nama_personil", form.nama_personil).attr({class: "form-control", required: true}),
                    pangkat_personil: app.createForm.inputText("pangkat_personil", form.pangkat_personil).attr({class: "form-control", required: true}),
                    jabatan_personil: app.createForm.inputText("jabatan_personil", form.jabatan_personil).attr({class: "form-control", required: true}),
                    email_personil: app.createForm.inputText("email_personil", form.email_personil).attr({class: "form-control", required: false}),
                    telp_personil: app.createForm.inputText("telp_personil", form.telp_personil).attr({class: "form-control", required: false}),
                };
                modul.formModal.content.html(modul.formModal.objectForm);
                $.each(object, function(key, val){ modul.formModal.content.find("span[data-form-object='"+key+"']").replaceWith(val); });
                modul.formModal.content.find(".select2").select2();
                modul.formModal.title.html(data.form_title);
                modul.formModal.modal.modal("show");
                modul.formModal.action.off();
                modul.formModal.action.on("submit", function(event){
                    event.preventDefault();
                    let data = new FormData($(modul.formModal.action)[0]);
                    let loader = app.createLoader(modul.formModal.action, "Menyimpan data ...");
                    setTimeout(function(){
                        app.sendDataMultipart({
                            url: "/data/personil/save/",
                            data: data,
                            token: "<?= $this->token; ?>",
                            onSuccess: function(response){
                                // console.log(response);
                                loader.hide();
                                main.showNotification(response);
                                modul.formModal.modal.modal("hide");
                                personil.showTable();
                            },
                            onError: function(error){
                                // console.log(error);
                                loader.hide();
                                main.showNotification(error);
                                modul.formModal.modal.modal("hide");
                            }
                        });
                    }, 500);
                });
            },
            onError: function(error){
                // console.log(error);
                main.showNotification(error);
            }
        });
    },
};

personil.init();