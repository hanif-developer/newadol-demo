var api_url = "<?= $url_path; ?>";
var cookie_expr = (1*24*60*60*1000); // 24 Jam
var loader = "<img src='assets/global/img/loader.gif' alt='' />";
app = {
    initModul: function(id) {
        project = $(id);
        modalForm = $(".form-modal-input");
        result = {
            main: project,
            table: app.initTable(project),
            form: {
                action: project.find(".frmInput"),
                title: project.find(".form-title"),
                content: project.find(".form-contents"),
                loader: project.find(".form-loader"),
                button: project.find(".form-button"),
                objectForm: project.find(".form-content").clone().html(),
            },
            formModal: {
                modal: modalForm,
                action: modalForm.find(".frmInput"),
                title: modalForm.find(".form-modal-title"),
                content: modalForm.find(".form-modal-content"),
                loader: modalForm.find(".form-modal-loader"),
                button: modalForm.find(".form-modal-button"),
                objectForm: project.find(".form-content").clone().html(),
            }
        }
        project.find(".form-content").hide();
        return result;
    },
    initTable: function(obj) {
        return {
            action: obj.find(".frmData"),
            loader: obj.find(".table-loader"),
            content: obj.find(".table-content"),
            empty: obj.find(".table-empty"),
            tbody: obj.find(".table-content .table tbody"),
            tbodyRows: obj.find(".table-content .table tbody tr"),
            pagging: obj.find(".table-content .table-pagging ul.pagination"),
            paggingItem: obj.find(".table-content .table-pagging ul.pagination .page-item"),
            count: obj.find(".table-content .count"),
            query: obj.find(".query"),
        }
    },
    // Custom Active Url
    initMenuActiveUrl: function(pageUrl) {
        var macthUrl = []
        $("a.nav-link.pointer").each(function(k, v){
            if(pageUrl.match(this.href)){ macthUrl.push(this); }
        });
        var element = $(macthUrl[macthUrl.length - 1]);
        element.parent().addClass("active");
    },
    // Custom Error Message
    showErrMessage: function(params){
        var type = (params.status == "error") ? "bg-danger" : "bg-success";
        var errMessage = $("<div>").attr({class: "alert alert-dismissible "+type+" text-white border-0", role: "alert"});
        var closeButton = $("<button>").attr({type: "button", class: "close", "data-dismiss": "alert", "aria-label": "Close"}).html("<span aria-hidden='true'>×</span>")
        var message = $("<span>").html("<strong>"+params.message.title+"</strong> "+params.message.text);
        return errMessage.append(closeButton).append(message);
    },
    createCookie: function(cname, cvalue){
        var d = new Date();
        d.setTime(d.getTime() + cookie_expr);
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    },
    removeCookie: function(cname){
        var d = new Date();
        d.setTime(d.getTime() - cookie_expr);
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=;" + expires + ";path=/";
    },
    createTable: function(params){
        var tview = params.table
        var number = params.data.no;
        var table = params.data.table;
        tview.tbody.html("");
        tview.pagging.html("");
        tview.count.text(params.data.count+" "+params.data.label);
        if(params.data.query != "" && params.data.query !== undefined) tview.query.html("<code>"+params.data.query+"</code>");
        if(params.data.count > 0){
            for(var rows in table){
                var row = tview.tbodyRows.clone();
                var result = row.html().replace("{no}", number++);
                json = table[rows];
                for(key in json){
                    var find = new RegExp("{"+key+"}", "g");
                    result = result.replace(find, json[key]);
                    row.html(result);
                }
                tview.tbody.append(row);
            }

            tview.content.show();
            if(typeof params.onShow === "function") params.onShow(tview.content);
            // Create pagging table
            var page = parseInt(params.data.page);
            var total_pages = Math.ceil(params.data.count / params.data.limit);
            var prev_number = (page > 1) ? page - 1 : 1;
            var next_number = (page < total_pages) ? page + 1 : total_pages;
            var page_number = tview.paggingItem.html();
            
            var btn_first = tview.paggingItem.clone().attr("number-page", 1).html(page_number.replace("{page}", "&laquo;"));
            var btn_last = tview.paggingItem.clone().attr("number-page", total_pages).html(page_number.replace("{page}", "&raquo;"));
            var btn_prev = tview.paggingItem.clone().attr("number-page", prev_number).html(page_number.replace("{page}", "&lsaquo;"));
            var btn_next = tview.paggingItem.clone().attr("number-page", next_number).html(page_number.replace("{page}", "&rsaquo;"));
            var btn_dots = tview.paggingItem.clone().addClass("disabled").html(page_number.replace("{page}", "..."));
            var btn_active = tview.paggingItem.clone().addClass("active").html(page_number.replace("{page}", page));
            
            if(total_pages > 1){
                if(page > 3){
                    tview.pagging.append(btn_first);
                    tview.pagging.append(btn_prev);
                    tview.pagging.append(btn_dots);
                }

                for(i = (page - 2); i < page; i++){
                    if(i < 1) continue;
                    var pages = tview.paggingItem.clone().attr("number-page", i).html(page_number.replace("{page}", i));
                    tview.pagging.append(pages);
                }

                tview.pagging.append(btn_active);

                for(i = (page + 1); i < (page + 3); i++){
                    if(i > total_pages) break;
                    var pages = tview.paggingItem.clone().attr("number-page", i).html(page_number.replace("{page}", i));
                    tview.pagging.append(pages);
                }

                if((page + 2) < total_pages) tview.pagging.append(btn_dots);
                
                if(page < (total_pages - 2)){
                    tview.pagging.append(btn_next);
                    tview.pagging.append(btn_last);
                }
                
                // action page button
                tview.content.find(".pagging[number-page!='']").on("click", function(event){
                    event.preventDefault();
                    var page = $(this).attr("number-page");
                    if(typeof params.onPagging === "function") params.onPagging(page);
                });
            }
        }else{
            tview.empty.show();
        }
    },
    createForm: {
        inputKey: function(id, value){
            return $("<input>").attr({type: "hidden", id: id, name: id, value: value});
        },
        inputText: function(id, value){
            return $("<input>").attr({type: "text", id: id, name: id, value: value, class: "form-control"});
        },
        inputPassword: function(id, value){
            return $("<input>").attr({type: "password", id: id, name: id, value: value, class: "form-control"});
        },
        textArea: function(id, value){
            return $("<textarea>").attr({id: id, name: id, class: "form-control"}).html(value);
        },
        selectOption: function(id, data, value, multiple = false){
            var group = $("<div>");
            var select = $("<select>").attr({id: id, name: id, class: "form-control custom-select", style: "cursor: pointer;"});
            if(multiple) select.attr("multiple", true);
            $.each(data, function(key, val){
                var selected = ($.isArray(value)) ? ($.inArray(key, value) != -1) : (key == value);
                var option = $("<option>").attr({value: key, selected: selected}).text(val.text)
                select.append(option);
            });
            group.append(select);
            return group.children();
        },
        radioButton: function(id, data, value){
            var group = $("<div>");
            $.each(data, function(key, val){
                var radio = $("<input>").attr({type: "radio", id: id, name: id, value: key, checked: (key == value)});
                var label = $("<label>").attr("style", "color: #000; cursor: pointer; margin: 10px;").append(radio).append(" "+val.text);
                group.append(label);
            });
            return group.children();
        },
        checkBox: function(id, data, value){
            var group = $("<div>");
            var value = (typeof value == "string") ? value.split(",") : [value];
            $.each(data, function(key, val){
                var check = $("<input>").attr({type: "checkbox", id: id, name: id, value: val, checked: ($.inArray(val, value) != -1)});
                var label = $("<label>").attr("style", "color: #000; cursor: pointer; margin: 5px;").append(check).append(" "+val);
                group.append(label);
            });
            return group.children();
        },
        uploadImage: function(id, image, mimes, desc){
            var group = $("<div>");
            var preview = $("<div>").attr({class: "image-preview"}).html('<img src="'+image+'" class="img-responsive thumbnail" alt="image" width="100%">');
            var file = $("<input>").attr({type: "file", id: id, name: id, class: "file-image", style: "display: none;", accept: mimes});
            var button = $("<label>").attr({for: id, class: "btn btn-block btn-sm btn-info", style: "cursor: pointer; margin-top: 10px;"}).html("UPLOAD");
            var desc = $("<small>").attr({class: "help-blocks"}).html(desc);
            file.on("change", function(event){
                event.preventDefault();
                // console.log(this);
                var url = this.value;
                var arr = url.split(".");
                var ext = arr[arr.length - 1];
                if(ext == "pdf"){
                    app.pdfPreview(this);
                }else{
                    app.imagePreview(this);
                }
            });
            group.append(preview).append(file).append(button).append(desc);
            return group.children();
        },
    },
    updateForm: {
        selectOption: function(obj, data, value){
            obj.html("");
            $.each(data, function(key, val){
                var selected = ($.isArray(value)) ? ($.inArray(key, value) != -1) : (key == value);
                var option = $("<option>").attr({value: key, selected: selected}).text(val.text)
                obj.append(option);
            });
            return obj;
        },
    },
    createLoader: function(obj, msg){
        let loader = {
            show: function(){
                obj.waitMe({
                    effect: "ios",
                    text: msg,
                    // bg: "rgba(28,34,96,0.9)",
                    // color: "#fff"
                    bg: "rgba(255,255,255,0.9)",
                    color: "#555"
                });
            },
            hide: function(){
                obj.waitMe("hide");
            }
        }
        loader.show();
        return loader;
    },
    validate: function(e) {
        if(e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 13 || e.keyCode == 46 || e.keyCode == 37 || e.keyCode == 39) return true;
        else if( e.charCode >= 48 && e.charCode <= 57 ) return true;
        else return false;
    },
    ribuan: function(s) {
        var reverse = s.toString().split("").reverse().join(""),
            ribuan = reverse.match(/\d{1,3}/gi);
            ribuan = ribuan.join(".").split("").reverse().join("");
        return ribuan;
    },
    imagePreview: function(obj){
        if(obj.files && obj.files[0]){
            var reader = new FileReader();
            var preview = $(obj).siblings(".image-preview");
            reader.onload = function(e){
                preview.html('<img src="'+e.target.result+'" class="img-responsive thumbnail" alt="image" width="100%">');
            };
            reader.readAsDataURL(obj.files[0]);
        }
    },
    pdfPreview: function(obj) {
        if(obj.files && obj.files[0]){
            var reader = new FileReader();
            var preview = $(obj).siblings(".image-preview");
            reader.onload = function(e){
                PDFJS.disableWorker = true;
                PDFJS.getDocument(reader.result).then(function renderPDF(pdf) {
                    pdf.getPage(1).then(function renderPage(page) {
                        var scale = 0.5;
                        var viewport = page.getViewport(scale);
                        var canvas = document.createElement('canvas');
                        var context = canvas.getContext('2d');
                        canvas.height = viewport.height;
                        canvas.width = viewport.width;
                        preview.html(canvas).attr({style: "overflow: auto;"});
                        var task = page.render({
                            canvasContext: context,
                            viewport: viewport
                        })
                        task.promise.then(function() {
                            // console.log(canvas.toDataURL('image/jpeg'));
                        });
                    });
                }, function(error) {
                        // console.log(error);
                });
            };
            reader.readAsArrayBuffer(obj.files[0]);
        }
    },
    showPDF: function(params) {
        params.container.html("");
        function renderPage(page) {
            var viewport = page.getViewport(1);
            var scale = params.container.parent().width() / viewport.width;
            var canvas = document.createElement('canvas');
            var context = canvas.getContext('2d');
            viewport = page.getViewport(scale);
            canvas.height = viewport.height;
            canvas.width = viewport.width;
            params.container.css({
                "height" : canvas.height,
                "width" : canvas.width,
                "overflow-y" : "scroll"
            });
            params.container.append(canvas);
            var task = page.render({
                canvasContext: context,
                viewport: viewport
            })
            task.promise.then(function() {
                // console.log(canvas.toDataURL('image/jpeg'));
            });
        }
        function renderPages(pdfDoc) {
            for(var num = 1; num <= pdfDoc.numPages; num++) {
                pdfDoc.getPage(num).then(renderPage)
            }
        }
        PDFJS.disableWorker = true;
        PDFJS.getDocument(params.url).then(function(pdfDoc) {
            for(var num = 1; num <= pdfDoc.numPages; num++) {
                pdfDoc.getPage(num).then(renderPage)
            }
        }).catch(function(error) {
            params.container
            .html('<div role="alert" class="alert alert-danger" style="margin: 20px;">File tidak bisa dibaca/ditemukan</div>')
            .css({
                "height" : "100%",
                "width" : "100%",
                "overflow-y" : "none"
            });
        });
    },
    serializeArraytoJson: function(data){
        // Kelompok data array dengan index name yang sama
        var temp = Object.create(null),
        toArray = data.reduce(function(r,a){
            if(!temp[a.name]){
                temp[a.name] = {name: a.name, data: []};
                r.push(temp[a.name]);
            }
            temp[a.name].data.push({value: a.value});
            return r;
        }, []);
        
        // Hilangkan index key name dan value
        var toJson = {}
        toArray.forEach(element => {
            if(element.data.length > 1){
                var data = [];
                element.data.forEach(elements => {
                    data.push(elements.value);
                });
                toJson[element.name] = data
            }
            else{
                toJson[element.name] = element.data[0].value;
            }
        });

        return JSON.stringify(toJson);
    },
    sendData: function(params){
        var token = (params.token == undefined) ? "app_token" : params.token;
        $.ajax({
            url: api_url+params.url,
            type: "POST",
            data: params.data,
            headers: {"Token": token},
            dataType: "json",
            async: false,
            success: params.onSuccess,
            error: function (e) {
                // console.log(e);
                if(e.responseJSON.status == "expired"){
                    alert(e.responseJSON.message.text);
                    window.location.reload();
                    return;
                }
                
                if(e.status !== 200){
                    if(typeof params.onError === "function") params.onError(e.responseJSON);
                }
            },
            complete: function() {
                console.clear();
            }
        });
    },
    sendDataMultipart: function(params){
        var token = (params.token == undefined) ? "app_token" : params.token;
        $.ajax({
            url: api_url+params.url,
            type: "POST",
            enctype: "multipart/form-data",
            // data: new FormData(params.data), // for (var value of formData.entries()) { console.info(value[0], value[1]); }
            data: params.data,
            headers: {"Token": token},
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            datatype: "json",
            xhr: function () {
                var jxhr = null;
                if(window.ActiveXObject) 
                    jxhr = new window.ActiveXObject( "Microsoft.XMLHTTP" );
                else
                    jxhr = new window.XMLHttpRequest();

                if(jxhr.upload) {
                    jxhr.upload.addEventListener("progress", function (evt) {
                        if(evt.lengthComputable) {
                            let percent = Math.round((evt.loaded / evt.total) * 100);
                            if(typeof params.onProgress === "function") params.onProgress(percent);
                        }
                    }, false);
                }
                return jxhr;
            },
            success: params.onSuccess,
            error: function (e) {
                // console.log(e);
                if(e.status !== 200){
                    if(e.responseJSON.status == "expired"){
                        alert(e.responseJSON.message.text);
                        window.location.reload();
                        return;
                    }

                    if(typeof params.onError === "function") params.onError(e.responseJSON);
                }
            },
            complete: function() {
                console.clear();
            }
        });
    },
}

app.initMenuActiveUrl(window.location.toString());