<?php

namespace app\service\controller;

use app\adol\controller\service;
use app\adol\controller\fcm;
use comp;

class v1 extends service{ 
	
	public function __construct(){
		parent::__construct();
		$this->fcm = new fcm();
		$this->data['url_path'] = $this->link($this->getProject().$this->getController());
	}

	protected function index(){
		$this->showResponse($this->errorMsg, 404);
	}

	protected function testfcm(){
		$to = [
			'cwVcH7DPQyKRA1iOfoNOCF:APA91bFHkuj9l4EaT30gFWxeyUx4ynL3miznIXa2XD0spWb2zvf6qoeD7Y0Py_RcLQhZ6AZ2UyotPGMXKSmC8gJQnY4MvlZg-425N8PXvLC0kIYbaw3WM8bIKu5JhtqCohl9m7LubF5m',
			'cGb_4u12R0iyC5B-MBnF8k:APA91bE4Yv7AsuDWGmp3JXfc7Rg3RVhDg2YXmFlIN0JVAh9_3gB8xfAwfSiBauaMEHZ9AWg4gLgLfYr6lOeDUv6odvTnwyw56S48wCTurHJSu9eFYKPpssXIKH5lEupZevGqcjMf6-55',
			// 'e_y3EHKfQPy4t-YnfLcWHt:APA91bFZBgM4tIWgVwMtjVlXIKzDlidRFdBLGLTdsGyOEp-zncnil3wzIe27QRYfYnGgE5ZwKDUC8-wpW9DzQw6ME8wuf8fjlGRKyIkJAUGyk_n5B9qByyb0cF6hvumEnFERmkVI2Z4E',
			// 'd8jOzbeEQYCAXUKtVRtj20:APA91bFqiuBYD4KE_0qbrBs7Dmu7EiMlAYEZLsq_A4rXjdS17bquNSnLjvR7HR1dXUEwwMbj36yxA1asmP_54Bga8_tMOayid0Sh6gZTVJYHzB35n-HKqIDmh3BNw7ePK3l4Jqam3VBq',
		];
		
		//$this->fcm->setImage('https://www.androidhive.info/wp-content/uploads/2016/10/android-firebase-notifications-with-image.png');
		$response = $this->fcm->send($to, $this->fcm->getPush());
		$this->showResponse($response);
	}

	protected function script(){
		header('Content-Type: application/javascript');
		$this->subView('script', $this->data);
	}

	/**
	 * General Function
	 */
	protected function getInput($params = []){
		$input = $this->inputValidate($params);
		return (!empty($params)) ? $this->paramsFilter($params, $input) : $input;
	}

	protected function getParams($input, $params){ // Filter parameter dari getInput (Sub Params)
		if(empty($input)){
			$this->errorMsg['message']['text'] = 'Requires Parameter';
			$this->errorMsg['message']['params'] = $params;
			$this->showResponse($this->errorMsg, $this->errorCode);
			die;
		}

		return $this->paramsFilter($params, $input);
	}

	protected function showResult($result, $message = 'Data berhasil disimpan'){
		$this->errorCode = 200;
		$this->errorMsg = ($result['success'] > 0) ? 
			array('status' => 'success', 'message' => array(
				'title' => 'Sukses',
				'text' => $message,
			)) : 
			array('status' => 'error', 'message' => array(
				'title' => 'Maaf',
				'text' => $result['message'],
			)); 

		$this->showResponse($this->errorMsg, $this->errorCode);
		die;
	}

	protected function getActiveSession($input){
		$userSession = $this->adol->userSession($input);
		if(empty($userSession)){
			$this->errorMsg['status'] = 'expired';
			$this->errorMsg['message']['text'] = 'Session not found !!';
			$this->showResponse($this->errorMsg, $this->errorCode);
			die;
		}
		
		return $userSession;
	}

	/**
	 * Api List
	 */
	protected function data($method = '', $action = ''){
		$this->checkAuthToken($this->token);
		$method = $this->getValidate($method);
		switch ($method) {
			case 'option':
				$this->method_option($action);
				break;
			
			case 'regional':
				switch ($action) {
					case 'list':
						$input = $this->getInput(['page' => '']);
						$result = $this->adol->getTabelRegional($input);
						$this->succesMsg['data'] = $result;
						$this->showResponse($this->succesMsg);
						break;
					
					case 'form':
						$input = $this->getInput(['id' => '']);
						$result = $this->adol->getFormRegional($input['id']);
						$this->succesMsg['data'] = $result;
						$this->showResponse($this->succesMsg);
						break;

					case 'save':
						$input = $this->postValidate(false);
						$dataRegional = $this->adol->getFormRegional($input['id_regional']);
						$dataRegional = $this->adol->paramsFilter($dataRegional['form'], $input);
						$dataRegional['id_regional'] = \strtoupper($dataRegional['id_regional']);
						$dataRegional['nama_regional'] = \strtoupper($dataRegional['nama_regional']);
						$result = $this->adol->save_update('tb_regional', $dataRegional);
						$this->showResult($result, 'Data regional telah disimpan');
						break;

					case 'delete':
						$input = $this->getInput(['id' => '']);
						$result = $this->adol->delete('tb_regional', ['id_regional' => $input['id']]);
						$this->showResult($result, 'Data regional telah dihapus');
						break;
					
					default:
						$this->showResponse($this->errorMsg, $this->errorCode);
						break;
				}
				break;
			
			case 'wilayah':
				switch ($action) {
					case 'list':
						$input = $this->getInput(['page' => '', 'regional' => '']);
						$result = $this->adol->getTabelWilayah($input);
						$this->succesMsg['data'] = $result;
						$this->showResponse($this->succesMsg);
						break;
					
					case 'form':
						$input = $this->getInput(['id' => '', 'regional' => '']);
						$result = $this->adol->getFormWilayah($input['id'], $input['regional']);
						$this->succesMsg['data'] = $result;
						$this->showResponse($this->succesMsg);
						break;

					case 'save':
						$input = $this->postValidate(false);
						$dataWilayah = $this->adol->getFormWilayah($input['id_wilayah'], $input['id_regional']);
						$dataWilayah = $this->adol->paramsFilter($dataWilayah['form'], $input);
						$dataWilayah['nama_wilayah'] = \strtoupper($dataWilayah['nama_wilayah']);
						$result = $this->adol->save_update('tb_wilayah', $dataWilayah);
						$this->showResult($result, 'Data wilayah telah disimpan');
						break;

					case 'delete':
						$input = $this->getInput(['id' => '']);
						$result = $this->adol->delete('tb_wilayah', ['id_wilayah' => $input['id']]);
						$this->showResult($result, 'Data wilayah telah dihapus');
						break;
					
					default:
						$this->showResponse($this->errorMsg, $this->errorCode);
						break;
				}
				break;
			
			case 'satker':
				switch ($action) {
					case 'list':
						$input = $this->getInput(['page' => '', 'regional' => '', 'kategori' => '', 'wilayah' => '', 'cari' => '']);
						$result = $this->adol->getTabelSatker($input);
						$this->succesMsg['data'] = $result;
						$this->showResponse($this->succesMsg);
						break;
					
					case 'form':
						$input = $this->getInput(['id' => '', 'regional' => '']);
						$result = $this->adol->getFormSatker($input['id'], $input['regional']);
						$this->succesMsg['data'] = $result;
						$this->showResponse($this->succesMsg);
						break;

					case 'save':
						$input = $this->postValidate(false);
						$dataSatker = $this->adol->getFormSatker($input['id_satker'], $input['id_regional']);
						$dataSatker = $this->adol->paramsFilter($dataSatker['form'], $input);
						// Upload file
						$logo_satker = \strtolower('logo_'.\str_replace(' ', '_', $dataSatker['singkatan_satker']));
						$upload = $this->uploadImage('file_logo_satker', $this->adol->path_image_satker, $logo_satker, $this->file_type_image);
						// Check error upload
						if($upload['status'] != 'success'){
							$this->errorMsg['message']['text'] =  $upload['errorMsg'];
							$this->showResponse($this->errorMsg, $this->errorCode);
							die;
						}
						// Check Upload File
						if(!empty($upload['UploadFile'])) $dataSatker['logo_satker'] = $upload['UploadFile'];
						$result = $this->adol->save_update('tb_satker', $dataSatker);
						$this->showResult($result, 'Data Satker telah disimpan');
						$this->showResponse([
							'input' => $input,
							'dataSatker' => $dataSatker,
						]);
						break;

					case 'delete':
						$input = $this->getInput(['id' => '']);
						$result = $this->adol->delete('tb_satker', ['id_satker' => $input['id']]);
						$this->showResult($result, 'Data Satker telah dihapus');
						break;
					
					default:
						$this->showResponse($this->errorMsg, $this->errorCode);
						break;
				}
				break;

			case 'bidang':
				switch ($action) {
					case 'list':
						$input = $this->getInput(['page' => '', 'satker' => '', 'cari' => '']);
						$result = $this->adol->getTabelBidang($input);
						$this->succesMsg['data'] = $result;
						$this->showResponse($this->succesMsg);
						break;
					
					case 'form':
						$input = $this->getInput(['id' => '', 'satker' => '']);
						$result = $this->adol->getFormBidang($input['id'], $input['satker']);
						$this->succesMsg['data'] = $result;
						$this->showResponse($this->succesMsg);
						break;

					case 'save':
						$input = $this->postValidate(false);
						$dataBidang = $this->adol->getFormBidang($input['id_bidang'], $input['id_satker']);
						$dataBidang = $this->adol->paramsFilter($dataBidang['form'], $input);
						$dataBidang['nama_bidang'] = \strtoupper($dataBidang['nama_bidang']);
						$result = $this->adol->save_update('tb_bidang', $dataBidang);
						$this->showResult($result, 'Data bidang telah disimpan');
						$this->showResponse([
							'input' => $input,
							'dataBidang' => $dataBidang,
						]);
						break;

					case 'delete':
						$input = $this->getInput(['id' => '']);
						$result = $this->adol->delete('tb_bidang', ['id_bidang' => $input['id']]);
						$this->showResult($result, 'Data bidang telah dihapus');
						break;
					
					default:
						$this->showResponse($this->errorMsg, $this->errorCode);
						break;
				}
				break;

			case 'jabatan':
				switch ($action) {
					case 'list':
						$input = $this->getInput(['page' => '', 'satker' => '', 'bidang' => '', 'cari' => '']);
						$result = $this->adol->getTabelJabatan($input);
						$this->succesMsg['data'] = $result;
						$this->showResponse($this->succesMsg);
						break;
					
					case 'form':
						$input = $this->getInput(['id' => '', 'satker' => '']);
						$result = $this->adol->getFormJabatan($input['id'], $input['satker']);
						$this->succesMsg['data'] = $result;
						$this->showResponse($this->succesMsg);
						break;

					case 'save':
						$input = $this->postValidate(false);
						$dataJabatan = $this->adol->getFormJabatan($input['id_jabatan'], $input['id_satker']);
						$dataJabatan = $this->adol->paramsFilter($dataJabatan['form'], $input);
						$dataJabatan['nama_jabatan'] = \strtoupper($dataJabatan['nama_jabatan']);
						$result = $this->adol->save_update('tb_jabatan', $dataJabatan);
						$this->showResult($result, 'Data jabatan telah disimpan');
						$this->showResponse([
							'input' => $input,
							'dataJabatan' => $dataJabatan,
						]);
						break;

					case 'delete':
						$input = $this->getInput(['id' => '']);
						$result = $this->adol->delete('tb_jabatan', ['id_jabatan' => $input['id']]);
						$this->showResult($result, 'Data jabatan telah dihapus');
						break;
					
					default:
						$this->showResponse($this->errorMsg, $this->errorCode);
						break;
				}
				break;

			case 'personil':
				switch ($action) {
					case 'list':
						$input = $this->getInput(['page' => '', 'cari' => '']);
						$result = $this->adol->getTabelPersonil($input);
						$this->succesMsg['data'] = $result;
						$this->showResponse($this->succesMsg);
						break;
					
					case 'form':
						$input = $this->getInput(['id' => '']);
						$result = $this->adol->getFormPersonil($input['id']);
						$this->succesMsg['data'] = $result;
						$this->showResponse($this->succesMsg);
						break;

					case 'save':
						$input = $this->postValidate(false);
						$dataPersonil = $this->adol->getFormPersonil($input['id_personil']);
						$dataPersonil = $this->adol->paramsFilter($dataPersonil['form'], $input);
						$dataPersonil['nama_personil'] = \strtoupper($dataPersonil['nama_personil']);
						$dataPersonil['pangkat_personil'] = \strtoupper($dataPersonil['pangkat_personil']);
						$dataPersonil['jabatan_personil'] = \strtoupper($dataPersonil['jabatan_personil']);
						$result = $this->adol->save_update('tb_personil', $dataPersonil);
						$this->showResult($result, 'Data personil telah disimpan');
						$this->showResponse([
							'input' => $input,
							'dataPersonil' => $dataPersonil,
						]);
						break;

					case 'delete':
						$input = $this->getInput(['id' => '']);
						$result = $this->adol->delete('tb_personil', ['id_personil' => $input['id']]);
						$this->showResult($result, 'Data personil telah dihapus');
						break;
					
					default:
						$this->showResponse($this->errorMsg, $this->errorCode);
						break;
				}
				break;

			case 'user':
				switch ($action) {
					case 'list':
						$input = $this->getInput(['page' => '', 'satker' => '', 'bidang' => '', 'cari' => '']);
						$result = $this->adol->getTabelUser($input);
						$this->succesMsg['data'] = $result;
						$this->showResponse($this->succesMsg);
						break;
					
					case 'form':
						$input = $this->getInput(['id' => '', 'jabatan' => '']);
						$result = $this->adol->getFormUser($input['id'], $input['jabatan']);
						if(empty($result)){
							$this->errorMsg['message']['text'] = 'Data Jabatan not found !!';
							$this->showResponse($this->errorMsg, $this->errorCode);
							die;
						}

						$this->succesMsg['data'] = $result;
						$this->showResponse($this->succesMsg);
						break;

					case 'save':
						$input = $this->postValidate(false);
						$dataUser = $this->adol->getFormUser($input['id_user'], $input['id_jabatan']);
						$dataUser = $this->adol->paramsFilter($dataUser['form'], $input);
						$dataUser['password_user'] = comp\FUNC::encryptor($dataUser['password_user']);
						$dataUser['draft_user'] = isset($input['draft_user']) ? $this->adol->setUserActionDraftValue($dataUser['draft_user']) : 0;
						$result = $this->adol->save_update('tb_user', $dataUser);
						$this->showResult($result, 'Data user telah disimpan');
						$this->showResponse([
							'input' => $input,
							'dataUser' => $dataUser,
						]);
						break;

					case 'delete':
						$input = $this->getInput(['id' => '']);
						$result = $this->adol->delete('tb_user', ['id_user' => $input['id']]);
						$this->showResult($result, 'Data user telah dihapus');
						break;
					
					default:
						$this->showResponse($this->errorMsg, $this->errorCode);
						break;
				}
				break;
			
			default:
				$this->showResponse($this->errorMsg, $this->errorCode);
				break;
		}
	}

	/**
	 * Sub Method Data
	 */
	private function method_option($action){
		switch ($action) {
			case 'regional':
				$this->succesMsg['data'] = ['' => ['text' => '-- Pilih Regional --']] + $this->adol->getRegional();
				$this->showResponse($this->succesMsg);
				break;

			case 'kategori':
				$this->succesMsg['data'] = ['' => ['text' => '-- Pilih Kategori --']] + $this->adol->getKategori();
				$this->showResponse($this->succesMsg);
				break;

			case 'satker':
				$input = $this->getInput(['regional' => '', 'kategori' => '']);
				$this->succesMsg['data'] = ['' => ['text' => '-- Pilih Satker --']] + $this->adol->getGroupSatker($input['regional'], $input['kategori']);
				$this->showResponse($this->succesMsg);
				break;

			case 'bidang': // Digunakan pada admin modul jabatan dan user
				$input = $this->getInput(['satker' => '']);
				$this->succesMsg['data'] = ['' => ['text' => 'SEMUA BIDANG']] + $this->adol->getNamaBidang($input['satker']);
				$this->showResponse($this->succesMsg);
				break;
			
			case 'jabatan':
				$input = $this->getInput(['satker' => '']);
				$this->succesMsg['data'] = $this->adol->getSatkerJabatan($input['satker']);
				$this->showResponse($this->succesMsg);
				break;
			
			default:
				$this->showResponse($this->errorMsg, $this->errorCode);
				break;
		}
	}

	protected function user($method = '', $action = ''){
		$this->checkAuthToken($this->token);
		$method = $this->getValidate($method);
		switch ($method) {
			case 'login':
				session_regenerate_id();
				$input = $this->getInput(['username' => '', 'password' => '', 'phone_type' => '', 'device_id' => '']);
				$input['password'] = comp\FUNC::encryptor($input['password']);
				$input['session_id'] = comp\FUNC::encryptor(session_id());
				
				$userCheck = $this->adol->userCheck($input);
				if($userCheck == 0){
					$this->errorMsg['message']['text'] = 'User not found !!';
					$this->showResponse($this->errorMsg, $this->errorCode);
					die;
				}

				$userLogin = $this->adol->userLogin($input);
				if(empty($userLogin)){
					$this->errorMsg['message']['text'] = 'Wrong password !!';
					$this->showResponse($this->errorMsg, $this->errorCode);
					die;
				}
				
				$this->succesMsg['data'] = $userLogin;
				$this->succesMsg['data']['message'] = 'You are successfully logged in';
				$this->showResponse($this->succesMsg);
				break;

			case 'session':
				$input = $this->getInput(['session_id' => '']);
				$userSession = $this->getActiveSession($input);
				if(empty($userSession)){
					$this->errorMsg['message']['text'] = 'Session not found !!';
					$this->showResponse($this->errorMsg, $this->errorCode);
					die;
				}
				
				$this->succesMsg['data'] = $userSession;
				$this->showResponse($this->succesMsg);
				break;

			case 'logout':
				$input = $this->getInput(['session_id' => '']);
				$userSession = $this->getActiveSession($input);
				if(empty($userSession)){
					$this->errorMsg['message']['text'] = 'Session not found !!';
					$this->showResponse($this->errorMsg, $this->errorCode);
					die;
				}

				$result = $this->adol->userLogout($input);
				if($result['success']){
					$this->errorMsg['status'] = 'success';
					$this->errorMsg['message']['title'] = 'Good Bye ..';
					$this->errorMsg['message']['text'] = 'You have successfully logged out !';
				}
				else{
					$this->errorMsg['message']['text'] = $result['message'];
				}
				
				$this->showResponse($this->errorMsg);
				break;

			case 'info':
				$input = $this->getInput(['session_id' => '']);
				$userSession = $this->getActiveSession($input);
				$userInfo = $this->adol->userInfo($userSession['id_user']);
				$userChat = $this->adol->getActiveUserChat($userSession, $userInfo);
				if(empty($userInfo)){
					$this->errorMsg['message']['text'] = 'User not found !!';
					$this->showResponse($this->errorMsg, $this->errorCode);
					die;
				}

				$data['user_session'] = $userSession;
				$data['user_info'] = $userInfo;
				$data['user_chat'] = $userChat;
				$data['administrasi'] = [
					'masuk' => $this->adm->getJmlAdmMasuk($userSession),
					'keluar' => $this->adm->getJmlAdmKeluar($userSession),
					'filter_list_masuk' => $this->adol->pilihanTabelAdmMasuk($userSession),
					'filter_list_keluar' => $this->adol->pilihanTabelAdmKeluar($userSession),
					'notif_masuk' => count($this->adm->getAdmNotifMasuk($userSession)),
				];
				$data['draft'] = [
					'masuk' => $this->draft->getJmlDraftMasuk($userSession),
					'keluar' => $this->draft->getJmlDraftKeluar($userSession),
					'filter_list_masuk' => $this->adol->pilihanTabelDraftMasuk($userSession),
					'filter_list_keluar' => $this->adol->pilihanTabelDraftKeluar($userSession),
					'notif_masuk' => count($this->draft->getDraftNotifMasuk($userSession)),
				];
				
				$this->succesMsg['data'] = $data;
				$this->showResponse($this->succesMsg);
				break;
			
			case 'sipp':
				$input = $this->getInput(['nrp' => '']);
				$dataPersonil = $this->adol->getDataSIPP($input['nrp']);
				if(empty($dataPersonil)){
					$this->errorMsg['message']['text'] = 'Data tidak ditemukan, silahkan isi secara manual ..';
					$this->showResponse($this->errorMsg, $this->errorCode);
					die;
				}
				$this->succesMsg['data'] = $dataPersonil;
				$this->showResponse($this->succesMsg);
				break;

			case 'profil':
				switch ($action) {
					case 'form':
						$input = $this->getInput(['session_id' => '']);
						$userSession = $this->getActiveSession($input);
						$result = [
							'dataUser' => $this->adol->getFormUser($userSession['id_user'], $userSession['id_jabatan'])['form'],
							'dataPersonil' => $this->adol->getFormPersonil($userSession['id_personil'])['form'],
						];
						$this->succesMsg['data'] = $result;
						$this->showResponse($this->succesMsg);
						break;

					case 'save':
						$request_user = $this->adol->getTabel('tb_user');
						$request_personil = $this->adol->getTabel('tb_personil');
						$input = $this->getInput(['session_id' => ''] + $request_user + $request_personil);
						$userSession = $this->getActiveSession($input);

						// Update data personil
						$dataPersonil = $this->adol->getDataTabel('tb_personil', ['id_personil', $input['id_personil']]);
						$dataPersonil = $this->adol->paramsFilter($dataPersonil, $input);
						$result = $this->adol->save_update('tb_personil', $dataPersonil);
						// Hapus id personil yang masih digunakan pada tabel user
						$result = $this->adol->update('tb_user', ['id_personil' => ''], ['id_personil' => $dataPersonil['id_personil']]);
						// Simpan id personil baru pada tabel user
						$result = $this->adol->update('tb_user', ['id_personil' => $dataPersonil['id_personil']], ['id_user' => $userSession['id_user']]);
						$this->showResult($result, 'Data profil telah diubah');
						break;
					
					default:
						$this->showResponse($this->errorMsg, $this->errorCode);
						break;
				}
				break;

			case 'password':
				switch ($action) {
					case 'form':
						$input = $this->getInput(['session_id' => '']);
						$userSession = $this->getActiveSession($input);
						$result = [
							'dataUser' => $this->adol->getFormUser($userSession['id_user'])['form'],
						];
						$this->succesMsg['data'] = $result;
						$this->showResponse($this->succesMsg);
						break;

					case 'save':
						$request_user = $this->adol->getTabel('tb_user');
						$input = $this->getInput(['session_id' => '', 'password_baru' => '', 'password_lagi' => ''] + $request_user);
						$userSession = $this->getActiveSession($input);

						// Check kombinasi password baru
						if($input['password_baru'] != $input['password_lagi']){
							$this->errorMsg['message']['text'] = 'Kombinasi password baru tidak sama ..';
							$this->showResponse($this->errorMsg, $this->errorCode);
							die;
						}
						// Check password lama
						$password_user = comp\FUNC::encryptor($input['password_user']);
						$dataUser = $this->adol->getData('SELECT * FROM tb_user WHERE (id_user = ?) AND (password_user = ?)', [$userSession['id_user'], $password_user]);
						if($dataUser['count'] == 0){
							$this->errorMsg['message']['text'] = 'Password lama Anda salah ..';
							$this->showResponse($this->errorMsg, $this->errorCode);
							die;
						}
						
						// Simpan password baru pada tabel user
						$password_baru = comp\FUNC::encryptor($input['password_baru']);
						$result = $this->adol->update('tb_user', ['password_user' => $password_baru], ['id_user' => $userSession['id_user']]);
						$this->showResult($result, 'Password lama telah diubah');
						break;
					
					default:
						$this->showResponse($this->errorMsg, $this->errorCode);
						break;
				}
				break;

			case 'register':
				switch ($action) {
					case 'active': // khusus admin
						$input = $this->getInput(['page' => '', 'register' => '']);
						$this->succesMsg['data'] = $this->adol->getTabelUserRegister($input);
						$this->showResponse($this->succesMsg);
						break;

					case 'mobile': // khusus user
						$input = $this->getInput(['session_id' => '']);
						$userSession = $this->getActiveSession($input);
						$this->succesMsg['data'] = $this->adol->getTabelUserRegisterMobile($userSession);
						$this->showResponse($this->succesMsg);
						break;

					case 'chart': // khusus user
						$input = $this->getInput(['session_id' => '']);
						$userSession = $this->getActiveSession($input);
						$this->succesMsg['data'] = $this->adol->getChartUserRegisterMobile($userSession);
						$this->showResponse($this->succesMsg);
						break;
					
					default:
						$this->showResponse($this->errorMsg, $this->errorCode);
						break;
				}
				break;

			case 'action':
				switch ($action) {
					case 'list':
						$input = $this->getInput(['id_jabatan' => '', 'status_action' => '']);
						$result = $this->adol->getTabelUserAction($input);
						$this->succesMsg['data'] = $result;
						$this->showResponse($this->succesMsg);
						break;
					
					case 'form':
						$input = $this->getInput(['id' => '', 'jabatan' => '', 'status' => '']);
						// $info_jabatan = $this->adol->infoJabatan(''); // Test error jabatan
						$info_jabatan = $this->adol->infoJabatan($input['jabatan']);
						if(empty($info_jabatan)){
							$this->errorMsg['message']['text'] = 'Data Jabatan not found !!';
							$this->showResponse($this->errorMsg, $this->errorCode);
							die;
						}

						$result['info_jabatan'] = $info_jabatan;
						$result['status_action'] = $input['status'];
						$result['user_action'] = $input['status'].' kepada :';
						$result['form_title'] = $info_jabatan['nama_jabatan'].' - User '.\ucfirst($input['status']);
						$result['pilihan_satker'] = ['' => ['text' => '-- Pilih Satker --']] + $this->adol->getNamaSatker();
						$result['pilihan_bidang'] = ['' => ['text' => 'SEMUA BIDANG']];
						$this->succesMsg['data'] = $result;
						$this->showResponse($this->succesMsg);
						break;

					case 'save':
						$input = $this->getInput(['id_jabatan' => '', 'id_relasi' => '', 'status_action' => '']);
						if(!\in_array($input['status_action'], ['disposisi', 'koordinasi', 'laporan', 'draft'])){
							$this->errorMsg['message']['text'] = 'Status Action not found !!';
							$this->showResponse($this->errorMsg, $this->errorCode);
							die;
						}

						// Simpan data sesuai dengan jumlah id_relasi
						$tbl_name = 'tb_user_'.$input['status_action'];
						$data = $this->adol->getTabel($tbl_name);
						$data = $this->adol->paramsFilter($data, $input);
						$success = [];
						// Ubah data id_relasi ke array jika single value
						$data['id_relasi'] = (\is_array($data['id_relasi'])) ? $data['id_relasi'] : [$data['id_relasi']];
						$dataAction = $data;
						foreach ($data['id_relasi'] as $key => $value) {
							$dataAction['id_relasi'] = $value;
							$result = $this->adol->save_update($tbl_name, $dataAction);
							if($result['success'] > 0) array_push($success, $result);
							// array_push($success, $dataAction);
						}

						$result['success'] = count($success);
						$this->showResult($result, 'Data user telah disimpan');
						$this->showResponse([
							'input' => $input,
							'dataAction' => $dataAction,
							// 'success' => $success,
						]);
						break;

					case 'delete':
						$input = $this->getInput(['id' => '', 'jabatan' => '', 'status' => '']);
						if(!\in_array($input['status'], ['disposisi', 'koordinasi', 'laporan', 'draft'])){
							$this->errorMsg['message']['text'] = 'Status Action not found !!';
							$this->showResponse($this->errorMsg, $this->errorCode);
							die;
						}

						$tbl_name = 'tb_user_'.$input['status'];
						$params = \explode(':', $input['id']); // 0: id_jabatan | 1: id_relasi
						$result = $this->adol->delete($tbl_name, ['id_jabatan' => $params[0], 'id_relasi' => $params[1]]);
						$this->showResult($result, 'Data user telah dihapus');
						break;
					
					default:
						$this->showResponse($this->errorMsg, $this->errorCode);
						break;
				}
				break;
			
			default:
				$this->showResponse($this->errorMsg, $this->errorCode);
				break;
		}
	}

	protected function adm($method = '', $action = ''){
		$this->checkAuthToken($this->token);
		$method = $this->getValidate($method);
		switch ($method) {
			case 'notif':
				$input = $this->getInput(['session_id' => '']);
				$userSession = $this->getActiveSession($input);
				$notif = $this->adm->getAdmNotifMasuk($userSession);
				$this->succesMsg['data'] = [
					'count' => count($notif),
					'value' => $notif,
				];
				$this->showResponse($this->succesMsg);
				break;

			case 'list':
				$input = $this->getInput(['session_id' => '', 'list_request' => '']);
				$userSession = $this->getActiveSession($input);
				switch ($action) {
					case 'masuk':
						$list_request = $this->getParams($input['list_request'], ['page' => '1', 'pil_cari' => 'perihal_adm', 'status_baca' => '-1', 'tembusan' => '', 'cari' => '']);
						$this->succesMsg['data'] = $this->adm->getTabelAdmMasuk($list_request, $userSession);
						$this->showResponse($this->succesMsg);
						break;

					case 'keluar':
						$list_request = $this->getParams($input['list_request'], ['page' => '1', 'pil_cari' => 'perihal_adm', 'status_baca' => '-1', 'tembusan' => '', 'cari' => '']);
						$this->succesMsg['data'] = $this->adm->getTabelAdmKeluar($list_request, $userSession);
						$this->showResponse($this->succesMsg);
						break;

					case 'monitor':
						$list_request = $this->getParams($input['list_request'], ['page' => '1', 'pil_cari' => 'perihal_adm', 'status_baca' => '-1', 'tembusan' => '', 'cari' => '']);
						$this->succesMsg['data'] = $this->adm->getTabelAdmMonitor($list_request, $userSession);
						$this->showResponse($this->succesMsg);
						break;
						
					case 'riwayat':
						$list_request = $this->getParams($input['list_request'], ['id' => '']); // id_adm
						$this->succesMsg['data'] = $this->adm->getTabelRiwayatAdm($list_request['id']);
						$this->showResponse($this->succesMsg);
						break;
					
					default:
						$this->showResponse($this->errorMsg, $this->errorCode);
						break;
				}
				break;

			case 'detail':
				$input = $this->getInput(['session_id' => '', 'id_adm' => '']);
				$userSession = $this->getActiveSession($input);
				switch ($action) {
					case 'masuk':
					case 'keluar':
						// $input['id_adm'] = '';
						$dataAdm = $this->adm->getDetailAdm($input['id_adm'], $action, $userSession);
						if(empty($dataAdm)){
							$this->errorMsg['message']['text'] = 'Data tidak ditemukan !!';
							$this->showResponse($this->errorMsg, $this->errorCode);
							die;
						}
						$this->succesMsg['data'] = $dataAdm;
						$this->showResponse($this->succesMsg);
						break;

					case 'monitor':
						$dataAdm = $this->adm->getDetailAdmRiwayat($input['id_adm'], $userSession);
						if(empty($dataAdm)){
							$this->errorMsg['message']['text'] = 'Data tidak ditemukan !!';
							$this->showResponse($this->errorMsg, $this->errorCode);
							die;
						}
						$this->succesMsg['data'] = $dataAdm;
						$this->showResponse($this->succesMsg);
						break;
					
					default:
						$this->showResponse($this->errorMsg, $this->errorCode);
						break;
				}
				break;

			/**
			 * User Action level agendaris | assistant | jabatan
			 * Laporan, Disposisi, Koordinasi
			 * Teruskan & Distribusi (Agendaris) cooming soon !!
			 */
			case 'laporan':	
			case 'disposisi':	
			case 'koordinasi':	
				switch ($action) {
					case 'form':
						$input = $this->getInput(['session_id' => '', 'id_adm' => '']);
						$userSession = $this->getActiveSession($input);
						$form_title = $this->adol->getFormTitleUserAction();
						$user_action = [
							'laporan' => $this->adm->getUserLaporan($userSession),
							'disposisi' => $this->adm->getUserDisposisi($userSession),
							'koordinasi' => $this->adm->getUserKoordinasi($userSession),
						];

						if(\in_array($method, ['laporan', 'disposisi', 'koordinasi'])){ // Level user agendaris | assistant | jabatan
							$pilihan_tujuan = isset($user_action[$method]) ? $user_action[$method] : [];
						}
						else{
							$this->showResponse($this->errorMsg, $this->errorCode);
							die;
						}
						
						$this->succesMsg['data'] = [
							'pilihan_tujuan' => !empty($pilihan_tujuan) ? $pilihan_tujuan : ['' => ['text' => '-- DATA MASIH KOSONG --']],
							'form_title' => $form_title[$method]['text'],
							'user_action' => $method,
							'id_adm' => $input['id_adm'],
						];
						$this->showResponse($this->succesMsg);
						break;

					/**
					 * Request via Form Data (POST) karena ada upload file ...
					 */
					case 'save':
						$input = $this->postValidate(false);
						$userSession = $this->getActiveSession($input);
						$userInfo = $this->adol->userInfo($userSession['id_user']);
						$action_adm_riwayat = $this->adol->getActionAdmRiwayat('form');
						$satker = \str_replace(' ', '_', $userInfo['singkatan_satker']);
						$upload = $this->uploadLampiran('file_adm_riwayat', $this->adol->path_lampiran_dokumen, 'ADM_RIWAYAT_'.$satker, $this->file_type_pdf);
						
						// Check error upload
						if($upload['status'] != 'success'){
							$this->errorMsg['message']['text'] =  $upload['errorMsg'];
							$this->showResponse($this->errorMsg, $this->errorCode);
							die;
						}
						
						// $input['user_action'] = 'disposisi';
						$data = $this->adol->getTabel('tb_adm_riwayat');
						$data = $this->adol->paramsFilter($data, $input);
						
						// Check Upload File
						if(!empty($upload['UploadFile'])) $data['lampiran_adm_riwayat'] = $upload['UploadFile'];

						// Simpan data sesuai dengan jumlah tujuan adm riwayat
						$id = 1;
						$success = [];
						$id_jabatan_success = [];
						// Ubah data tujuan adm ke object array jika single value
						$data['tujuan_adm_riwayat'] = (is_array($data['tujuan_adm_riwayat'])) ? $data['tujuan_adm_riwayat'] : array($data['tujuan_adm_riwayat']);
						$dataAdmRiwayat = $data;
						$dataAdmRiwayat['status_adm_riwayat'] = '0'; // belum dibaca
						$dataAdmRiwayat['action_adm_riwayat'] = $action_adm_riwayat[$input['user_action']];
						$dataAdmRiwayat['pengirim_adm_riwayat'] = $userSession['id_jabatan'];
						$dataAdmRiwayat['personil_pengirim'] = $userInfo['nrp_personil'];
						foreach ($data['tujuan_adm_riwayat'] as $key => $value) {
							if(!empty($value)){
								$infoJabatan = $this->adol->userInfoJabatan($value);
								$dataAdmRiwayat['id_adm_riwayat']++;
								$dataAdmRiwayat['tujuan_adm_riwayat'] = $value;
								$dataAdmRiwayat['personil_tujuan'] = $infoJabatan['nrp_personil'];
								$result = $this->adol->save_update('tb_adm_riwayat', $dataAdmRiwayat);
								if($result['success'] > 0) {
									array_push($success, $result);
									array_push($id_jabatan_success, $value); // simpan id_jabatan yang sukses kirim action
								}
							}
						}

						/**
						 * Send Push Notif FCM
						 */

						// Ambil id device jabatan tersebut
						$to = $this->adol->getDeviceIdByJabatan($id_jabatan_success);
						$this->fcm->setTitle($userInfo['nama_jabatan'].', '.$userInfo['singkatan_satker']);
						$this->fcm->setMessage(\ucfirst($input['user_action']).': '.$dataAdmRiwayat['keterangan_adm_riwayat']);
						// $this->fcm->setMessage('Mengirim '.\ucfirst($input['user_action']));
						//$this->fcm->setImage('https://www.leskompi.com/wp-content/uploads/2018/08/Cara-Mengganti-Background-Foto-Online.png');
						$fcmResult = $this->fcm->send($to, $this->fcm->getPush());
						/**---------------- */

						$result['success'] = count($success);
						// Pesan gagal dikirim, karena tujuan adm nilai ya kosong (bukan array)
						$result['message'] = isset($result['message']) ? $result['message'] : 'Pesan gagal dikirim !!!';
						// $this->showResult($result, $to);
						$this->showResult($result, 'Pesan telah dikirim');
						break;
					
					default:
						$this->showResponse($this->errorMsg, $this->errorCode);
						break;
				}
				break;

			/**
			 * User Action level agendaris | assistant 
			 * Input, Kirim
			 */
			case 'input':	
			case 'kirim':	
				switch ($action) {
					case 'form':
						$input = $this->getInput(['session_id' => '', 'id_adm' => '']);
						$userSession = $this->getActiveSession($input);
						$form_status = ($method == 'input') ? [] : $userSession; // Jika input tidak perlu parameter user session
						$result = $this->adm->getFormAdm($form_status);
						$this->succesMsg['data'] = $result;
						$this->showResponse($this->succesMsg);
						break;

					/**
					 * Request via Form Data (POST) karena ada upload file ...
					 */
					case 'save':
						$input = $this->postValidate(false);
						$userSession = $this->getActiveSession($input);
						$userInfo = $this->adol->userInfo($userSession['id_user']);
						$satker = \str_replace(' ', '_', $userInfo['singkatan_satker']);
						$upload = $this->uploadLampiran('file_adm', $this->adol->path_lampiran_dokumen, 'ADM_'.$satker, $this->file_type_pdf);
						
						// Check error upload
						if($upload['status'] != 'success'){
							$this->errorMsg['message']['text'] =  $upload['errorMsg'];
							$this->showResponse($this->errorMsg, $this->errorCode);
							die;
						}
						
						$data = $this->adol->getDataTabel('tb_adm', ['id_adm', $input['id_adm']]);
						$data = $this->adol->paramsFilter($data, $input);
						
						// Check Upload File
						if(!empty($upload['UploadFile'])) $data['lampiran_adm'] = $upload['UploadFile'];
						$data['id_draft'] = ''; // id_draft pertama 0
						
						/**
						 * Input: 0 | Kirim: 1
						 */
						switch ($method) {
							case 'input': // Modul input hanya untuk level agendaris dan assistant (agendaris bidang)
								if($userSession['level_user'] == 'assistant'){
									$data['bidang_adm'] = $userSession['id_bidang'];
									$data['status_bidang_adm'] = '1'; // sudah dibaca
								}

								$data['status_adm'] = '1'; // sudah dibaca
								$data['action_adm'] = '0'; // input adm
								$data['tujuan_adm'] = $userSession['id_satker'];
								// print_r($data); die;
								$result = $this->adol->save_update('tb_adm', $data);
								$this->showResult($result, 'Data telah disimpan');
								break;

							case 'kirim': // Modul kirim hanya untuk modul agendaris saja 
								$data['status_adm'] = '0'; // belum dibaca
								$data['action_adm'] = '1'; // kirim adm
								$data['pengirim_adm'] = $userSession['id_satker'];
								
								// Simpan data sesuai dengan jumlah tujuan adm
								$id = 1;
								$success = [];
								// Ubah data tujuan adm ke object array jika single value
								$data['tujuan_adm'] = (is_array($data['tujuan_adm'])) ? $data['tujuan_adm'] : array($data['tujuan_adm']);
								$dataAdm = $data;
								foreach ($data['tujuan_adm'] as $key => $value) {
									if(!empty($value)){
										$dataAdm['id_adm']++;
										$dataAdm['tujuan_adm'] = $value;
										$result = $this->adol->save_update('tb_adm', $dataAdm);
										if($result['success'] > 0) array_push($success, $result);
									}
								}
								$result['success'] = count($success);
								// Pesan gagal dikirim, karena tujuan adm nilai ya kosong (bukan array)
								$result['message'] = isset($result['message']) ? $result['message'] : 'Data gagal dikirim !!!';
								$this->showResult($result, 'Data telah dikirim');
								break;
							
							default:
								$this->showResponse($this->errorMsg, $this->errorCode);
								break;
						}
						break;
					
					default:
						$this->showResponse($this->errorMsg, $this->errorCode);
						break;
				}
				break;

			default:
				$this->showResponse($this->errorMsg, $this->errorCode);
				break;
		}
	}

	protected function draft($method = '', $action = ''){
		$this->checkAuthToken($this->token);
		$method = $this->getValidate($method);
		$draft_action = []; // ['buat', 'lapor', 'tolak', 'revisi', 'setuju', 'finish'];
		foreach ($this->adol->getActionDraft() as $key => $value) { $draft_action[$key] = $value['id']; }
		switch ($method) {
			case 'notif':
				$input = $this->getInput(['session_id' => '']);
				$userSession = $this->getActiveSession($input);
				$notif = $this->draft->getDraftNotifMasuk($userSession);
				$this->succesMsg['data'] = [
					'count' => count($notif),
					'value' => $notif,
				];
				$this->showResponse($this->succesMsg);
				break;

			case 'list':
				$input = $this->getInput(['session_id' => '', 'list_request' => '']);
				$userSession = $this->getActiveSession($input);
				switch ($action) {
					case 'masuk':
						$list_request = $this->getParams($input['list_request'], ['page' => '1', 'pil_cari' => 'perihal_draft', 'status_baca' => '-1', 'cari' => '']);
						$this->succesMsg['data'] = $this->draft->getTabelDraftMasuk($list_request, $userSession);
						$this->showResponse($this->succesMsg);
						break;

					case 'keluar':
						$list_request = $this->getParams($input['list_request'], ['page' => '1', 'pil_cari' => 'perihal_draft', 'status_baca' => '-1', 'cari' => '']);
						$this->succesMsg['data'] = $this->draft->getTabelDraftKeluar($list_request, $userSession);
						$this->showResponse($this->succesMsg);
						break;
						
					case 'riwayat':
						$list_request = $this->getParams($input['list_request'], ['id' => '']); // id_draft
						$this->succesMsg['data'] = $this->draft->getTabelRiwayatDraft($list_request['id']);
						$this->showResponse($this->succesMsg);
						break;
					
					default:
						$this->showResponse($this->errorMsg, $this->errorCode);
						break;
				}
				break;

			case 'detail':
				$input = $this->getInput(['session_id' => '', 'id' => '']); // id_draft_riwayat
				$userSession = $this->getActiveSession($input);
				switch ($action) {
					case 'masuk':
					case 'keluar':
						$dataDraft = $this->draft->getDetailDraft($input['id'], $action, $userSession);
						if(empty($dataDraft)){
							$this->errorMsg['message']['text'] = 'Data tidak ditemukan !!';
							$this->showResponse($this->errorMsg, $this->errorCode);
							die;
						}
						$this->succesMsg['data'] = $dataDraft;
						$this->showResponse($this->succesMsg);
						break;
					
					default:
						$this->showResponse($this->errorMsg, $this->errorCode);
						break;
				}
				break;

			/**
			 * User Action 
			 * ['buat', 'lapor', 'tolak', 'revisi', 'setuju', 'finish'];
			 */
			case \in_array($method, $draft_action):
				switch ($action) {
					case 'form':
						$input = $this->getInput([
							'session_id' => '',
							'id_draft' => '',
							'id_adm' => '',
							'id_jenis' => '',
							'jenislain_draft' => '',
							'perihal_draft' => '',
							'user_action' => '',
							'template_draft' => '',
							'form_upload' => 1,
						]);
						$userSession = $this->getActiveSession($input);
						$formActionDraft = $this->draft->getFormActionDraft($input, $method, $userSession);
						$this->succesMsg['data'] = $formActionDraft;
						$this->showResponse($this->succesMsg);
						break;

					/**
					 * Request via Form Data (POST) karena ada upload file ...
					 */
					case 'save':
						$input = $this->postValidate(false);
						// print_r($input);
						// print_r($_FILES);
						// die;
						
						$userSession = $this->getActiveSession($input);
						$userInfo = $this->adol->userInfo($userSession['id_user']);
						$action_draft_riwayat = $this->adol->getActionDraftRiwayat();
						$satker = \str_replace(' ', '_', $userInfo['singkatan_satker']);

						// Check blob name (Generated from JSPDF)
						$fileName = 'file_draft_riwayat';
						if(isset($_FILES[$fileName])){
							$_FILES[$fileName]['name'] = ($_FILES[$fileName]['name'] == 'blob') ? 'generated.pdf' : $_FILES[$fileName]['name'];
						}
						$upload = $this->uploadLampiran($fileName, $this->adol->path_lampiran_draft, 'DRAFT_RIWAYAT_'.$satker, $this->file_type_pdf);
						
						// Check error upload
						if($upload['status'] != 'success'){
							$this->errorMsg['message']['text'] =  $upload['errorMsg'];
							$this->showResponse($this->errorMsg, $this->errorCode);
							die;
						}

						if($input['user_action'] == 'buat'){
							$dataDraft = $this->adol->getTabel('tb_draft');
							$dataDraft = $this->adol->paramsFilter($dataDraft, $input);
							$dataDraft['id_satker'] = $userSession['id_satker'];
							$dataDraft['status_draft'] = 0; // 0: Drafting | 1: Acc | 2: Final
							$result = $this->adol->save_update('tb_draft', $dataDraft);

							if(!empty($input['id_adm'])){ 
								// jika ada id_adm, maka tambahkan id_draft pada adm
								$result = $this->adol->update('tb_adm', ['id_draft' => $dataDraft['id_draft']], ['id_adm' => $input['id_adm']]);
							}
						}

						if($input['user_action'] == 'setuju'){ 
							// ubah status draft dari draftting, menjadi acc
							$result = $this->adol->update('tb_draft', ['status_draft' => '1'], ['id_draft' => $input['id_draft']]);
						}

						if($input['user_action'] == 'selesai'){ 
							// ubah status draft dari acc, menjadi final
							$result = $this->adol->update('tb_draft', ['status_draft' => '2'], ['id_draft' => $input['id_draft']]);
							$this->showResult($result);
						}
					
						// Simpan data sesuai dengan jumlah tujuan draft riwayat
						$id = 1;
						$success = [];
						$id_jabatan_success = [];
						$dataLastDraft = $this->draft->getLastDataDraft($input['id_draft']);
						$dataRiwayat = $this->adol->getTabel('tb_draft_riwayat');
						$dataRiwayat = $this->adol->paramsFilter($dataRiwayat, $input);
						$dataRiwayat['lampiran_draft_riwayat'] = $dataLastDraft['lampiran_draft_riwayat'];
						// Check Upload File
						if(!empty($upload['UploadFile'])) $dataRiwayat['lampiran_draft_riwayat'] = $upload['UploadFile'];
						
						// Ubah data tujuan draft ke object array jika single value
						$dataRiwayat['tujuan_draft_riwayat'] = (is_array($dataRiwayat['tujuan_draft_riwayat'])) ? $dataRiwayat['tujuan_draft_riwayat'] : array($dataRiwayat['tujuan_draft_riwayat']);
						$dataDraftRiwayat = $dataRiwayat;
						$dataDraftRiwayat['status_draft_riwayat'] = '0'; // belum dibaca
						$dataDraftRiwayat['action_draft_riwayat'] = $action_draft_riwayat[$input['user_action']];
						$dataDraftRiwayat['pengirim_draft_riwayat'] = $userSession['id_jabatan'];
						$dataDraftRiwayat['personil_pengirim'] = $userInfo['nrp_personil'];
						foreach ($dataRiwayat['tujuan_draft_riwayat'] as $key => $value) {
							if(!empty($value)){
								$infoJabatan = $this->adol->userInfoJabatan($value);
								$dataDraftRiwayat['id_draft_riwayat']++;
								$dataDraftRiwayat['tujuan_draft_riwayat'] = $value;
								$dataDraftRiwayat['personil_tujuan'] = $infoJabatan['nrp_personil'];
								$result = $this->adol->save_update('tb_draft_riwayat', $dataDraftRiwayat);
								if($result['success'] > 0) {
									array_push($success, $result);
									array_push($id_jabatan_success, $value); // simpan id_jabatan yang sukses kirim action
								}
							}
						}

						/**
						 * Send Push Notif FCM
						 */

						// Ambil id device jabatan tersebut
						$to = $this->adol->getDeviceIdByJabatan($id_jabatan_success);
						$this->fcm->setTitle($userInfo['nama_jabatan'].', '.$userInfo['singkatan_satker']);
						$this->fcm->setMessage('Mengirim draft');
						$fcmResult = $this->fcm->send($to, $this->fcm->getPush());
						/**---------------- */

						$result['success'] = count($success);
						// Pesan gagal dikirim, karena tujuan draft nilai ya kosong (bukan array)
						$result['message'] = isset($result['message']) ? $result['message'] : 'Pesan gagal dikirim !!!';
						// $this->showResult($result, $to);
						$this->showResult($result, 'Pesan telah dikirim');
						break;
					
					default:
						$this->showResponse($this->errorMsg, $this->errorCode);
						break;
				}
				break;

			default:
				$this->showResponse($this->errorMsg, $this->errorCode);
				break;
		}
	}

	protected function chat($method = '', $action = ''){
		$this->checkAuthToken($this->token);
		$method = $this->getValidate($method);
		switch ($method) {
			case 'contact':
				$input = $this->getInput(['session_id' => '']);
				$userSession = $this->getActiveSession($input);
				$user_contact = $this->adol->getChatUserContact($userSession);
				// $user_contact = $this->adol->getChatUserContact($userSession['id_satker']);
				// $user_contact = !empty($user_contact) ? $user_contact : new \stdClass();
				// $user_contact = [];
				$this->succesMsg['data'] = [
					'user_contact' => $user_contact
				];
				$this->showResponse($this->succesMsg);
				break;

			case 'user':
				$input = $this->getInput(['session_id' => '', 'room' => '']);
				$userSession = $this->getActiveSession($input);
				$userInfo = $this->adol->userInfo($userSession['id_user']);
				switch ($action) {
					case 'conversation':
						$user_conversation = $this->adol->getChatUserConversation($userSession);
						$this->succesMsg['data'] = [
							'user_conversation' => $user_conversation
						];
						$this->showResponse($this->succesMsg);
						break;

					case 'message':
						// update status baca pesan yang lain
						$read_message = $this->adol->updateReadMessage($userSession['id_jabatan'], $input['room']);
						$user_message = $this->adol->getChatUserMessage($userSession, $userInfo, $input['room']);
						$this->succesMsg['data'] = [
							'user_message' => $user_message
						];
						$this->showResponse($this->succesMsg);
						break;
					
					default:
						$this->showResponse($this->errorMsg, $this->errorCode);
						break;
				}
				break;

			case 'send':
				switch ($action) {
					case 'conversation':
						$request_room = $this->adol->getTabel('tb_chat_room');
						$input = $this->getInput(['session_id' => ''] + $request_room);
						$userSession = $this->getActiveSession($input);
						$dataRoom = $this->adol->paramsFilter($request_room, $input);
						$dataRoom['users'] = implode(',', $dataRoom['users']);
						$result = $this->adol->save_update('tb_chat_room', $dataRoom);
						// $this->showResult($result, $dataRoom);
						$this->showResult($result, 'User conversation has been created');
						break;

					case 'message':
						$request_message = $this->adol->getTabel('tb_chat_message');
						$input = $this->getInput(['session_id' => ''] + $request_message);
						$userSession = $this->getActiveSession($input);
						$dataMessage = $this->adol->paramsFilter($request_message, $input);
						$dataMessage['read_message'] = $dataMessage['user']; // sudah dibaca sama pengirim
						$result = $this->adol->save_update('tb_chat_message', $dataMessage);
						// update status baca pesan yang lain
						$read_message = $this->adol->updateReadMessage($userSession['id_jabatan'], $input['room']);
						// $this->showResult($result, $dataMessage);
						$this->showResult($result, 'Message has been sent');
						break;
					
					default:
						$this->showResponse($this->errorMsg, $this->errorCode);
						break;
				}
				break;
			
			default:
				$this->showResponse($this->errorMsg, $this->errorCode);
				break;
		}
	}
}
?>
