<?php
namespace app\defaults\controller;

use system\Controller;
use system\Config;
use comp\FUNC;

class application extends Controller{
	
	public function __construct(){
        parent::__construct();
	}

	protected function index(){
        $this->showResponse($this->errorMsg, $this->errorCode);
	}
    
    public function createCookie($session){
        $cookie = $this->cookie;
        setcookie($cookie, $session, time() + COOKIE_EXP, '/');
    }
    
    public function removeCookie(){
        $cookie = $this->cookie;
        unset($_COOKIE[$cookie]);
        setcookie($cookie, '', time() - COOKIE_EXP, '/');
    }

    // protected function uploadImage($file, $prefix, $action = ''){
    /**
     * Action: resize(800px) | crop (400px)
     */
    protected function uploadImage($file, $folder, $prefix, $file_type, $action = ''){
        ini_set('memory_limit', '-1');
        $result['status'] = 'success';
        $result['errorMsg'] = 'file tidak dilampirkan';
        $result['UploadFile'] = '';
        if(isset($_FILES[$file]) && !empty($_FILES[$file]['name'])){
            $file = $_FILES[$file];
            $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
            $opt_upload = array(
                'fileName' => empty($action) ? $prefix.'_'.date('dmYHis').'.'.$ext : date('dmYHis').'.'.$ext,
                'fileType' => $file_type,
                'maxSize' => $this->max_size,
                'folder' => $folder,
                'session' => false,
            );
            $result = $this->files->upload($file, $opt_upload);
            if(empty($action)) return $result;
            if($result['status'] == 'success'){
                $src = $folder.'/'.$result['UploadFile'];
                $dst = $folder.'/'.$prefix.'_'.$result['UploadFile'];
                $result['UploadFile'] = $prefix.'_'.$result['UploadFile'];
                if($action == 'resize'){
                    FUNC::resizeImage(800, $src, $ext, $dst);
                }else if($action == 'crop'){
                    FUNC::cropImage(400, $src, $ext, $dst);
                }
            }
        }
        return $result;
    }

    protected function uploadLampiran($file, $folder, $prefix, $file_type){
        ini_set('memory_limit', '-1');
        $result['status'] = 'success';
        $result['errorMsg'] = 'file tidak dilampirkan';
        $result['UploadFile'] = '';
        if(isset($_FILES[$file]) && !empty($_FILES[$file]['name'])){
            $file = $_FILES[$file];
            $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
            $opt_upload = array(
                'fileName' => $prefix.'_'.date('dmYHis').'.'.$ext,
                'fileType' => $file_type,
                'maxSize' => $this->max_size,
                'folder' => $folder,
                'session' => false,
            );
            $result = $this->files->upload($file, $opt_upload);
        }
        return $result;
    }

    protected function FileExists($file, $action = ''){
        $exist = false;
        if(file_exists($file)) $exist = true;
        if($exist == true && $action == 'delete') unlink($file);
        return $exist;
    } 

    protected function sendMessagePost($data){
        /**
         * Params:
         * $data['url']
         * $data['header']
         * $data['fields']
         */
        // Open connection
        $ch = curl_init();
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $data['url']);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $data['header']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);  
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data['fields']);
        $result = curl_exec($ch);           
        if($result === FALSE){
            // die('Curl failed: ' . curl_error($ch));
            return [];
        }
        curl_close($ch);
        return $result;
    }
	
}
?>
