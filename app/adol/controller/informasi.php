<?php

namespace app\adol\controller;

use app\adol\controller\main;

class informasi extends main{
	
	public function __construct(){
		parent::__construct();
		$this->data['url_path'] = $this->link($this->getProject().$this->getController());
	}

	public function index(){
		$this->redirect($this->link_info.'/profil');
	}

	protected function script(){
		header('Content-Type: application/javascript');
		$this->subView('script', $this->data);
	}

	public function profil(){
		$this->data['active_page'] = 'Profil';
		$this->data['page_title'] = 'Informasi Personil';
		$this->data['dataUser'] = $this->adol->getFormUser($this->data['user_session']['id_user'], $this->data['user_session']['id_jabatan'])['form'];
		$this->data['dataPersonil'] = $this->adol->getFormPersonil($this->data['user_session']['id_personil'])['form'];
		$this->showView('profil', $this->data, 'theme_user');
		// $this->debugResponse($this->data);
	}

	public function register(){
		$this->data['active_page'] = 'Register';
		$this->data['page_title'] = 'Informasi Register';
		$this->data['user_chart'] = $this->adol->getChartUserRegisterMobile($this->data['user_session']);
		$this->showView('register', $this->data, 'theme_user');
		// $this->debugResponse($this->data);
	}

	/**
	 * Form Informasi
	 */
	protected function form($method = ''){
		switch ($method) {
			case 'personil':
			case 'password':
				$this->subView('form/'.$method, $this->data);
				break;
			
			default:
				echo '<div class="alert alert-danger" role="alert">Form tidak ditemukan !!</div>';
				break;
		}
	}

	/**
	 * Table Informasi
	 */
	protected function table($method = '', $action = ''){
		switch ($method) {
			case 'register':
				switch ($action) {
					case 'mobile':
						$this->subView('table/'.$method.'.'.$action, $this->data);
						break;
					
					default:
						echo '<div class="alert alert-danger" role="alert">Table tidak ditemukan !!</div>';
						break;
				}
				break;
			
			default:
				echo '<div class="alert alert-danger" role="alert">Table tidak ditemukan !!</div>';
				break;
		}
	}

}
?>
