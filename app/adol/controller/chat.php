<?php

namespace app\adol\controller;

use app\adol\controller\main;

class chat extends main{
	
	public function __construct(){
		parent::__construct();
		$this->data['url_path'] = $this->link($this->getProject().$this->getController());
		$this->data['dataUser'] = $this->adol->getFormUser($this->data['user_session']['id_user'], $this->data['user_session']['id_jabatan'])['form'];
		$this->data['dataPersonil'] = $this->adol->getFormPersonil($this->data['user_session']['id_personil'])['form'];
		$this->data['active_user'] = $this->adol->getActiveUserChat($this->data['user_session'], $this->data['user_info']);
	}

	public function index(){
		$this->data['active_page'] = 'Profil';
		$this->data['page_title'] = 'Informasi Personil';
		$this->showView('template', $this->data, 'theme_chat');
	}

	protected function script(){
		header('Content-Type: application/javascript');
		$this->subView('script', $this->data);
	}

}
?>
