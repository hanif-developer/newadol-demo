<?php

namespace app\adol\controller;

use app\defaults\controller\application;
use app\adol\model\adol;
use app\adol\model\adol_adm;
use app\adol\model\adol_draft;

class service extends application{
	
	public function __construct(){
        parent::__construct();
        $this->adol = new adol();
        $this->adm = new adol_adm();
        $this->draft = new adol_draft();
        $this->data['api_path'] = $this->link('api/v1');
        $this->data['main_path'] = $this->link($this->getProject().'main');
	}

	protected function index(){
		$this->showResponse($this->errorMsg, 404);
    }

    protected function getSessionID(){
        $cookie = $this->cookie;
		if(!empty($_COOKIE[$cookie])){
			$session_id = $_COOKIE[$cookie];
		}else{
			$session_id = 'sUFgzb3A5MHpJdFJvRUF5K1pVMUpRUHl2QzBKbHJPQVlYdmgyU3JjMFZCTDRLTDNydDY5djFaRGRsMEwvdHpuSA==';
		}
		return $session_id;
	}
	
	protected function getUserSession($session_id = ''){
        $session_id = empty($session_id) ? $this->getSessionID() : $session_id;
        $params = ['session_id' => $session_id];
        $userSession = $this->adol->userSession($params);
        return $userSession;
    }

    protected function getAdminSession(){
        $adminSession = $this->getSession('SESSION_LOGIN');
        return !empty($adminSession) ? $adminSession : [];
    }

}
?>
