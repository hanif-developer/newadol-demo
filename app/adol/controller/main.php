<?php

namespace app\adol\controller;

use app\adol\controller\service;

class main extends service{
	
	public function __construct(){
		parent::__construct();
		$session_id = $this->getSessionID();
		$user_session = $this->getUserSession($session_id);
		// $this->debugResponse($user_session);die;
		if(empty($user_session)){
			if($this->runAjax()){
				echo '<script>window.location.reload();</script>';
				die;
			}
			
			$this->redirect($this->link('login'));
		}
		
		$this->link_main = $this->link($this->getProject());
		$this->link_adm = $this->link($this->getProject().'administrasi');
		$this->link_draft = $this->link($this->getProject().'draft');
		$this->link_info = $this->link($this->getProject().'informasi');
		$this->link_pengaturan = $this->link($this->getProject().'pengaturan');
		$this->data['session_id'] = $session_id;
		$this->data['user_session'] = $user_session;
		$this->data['user_info'] = $this->adol->userInfo($user_session['id_user']);
		$this->data['administrasi'] = [
			'masuk' => $this->adm->getJmlAdmMasuk($user_session),
			'keluar' => $this->adm->getJmlAdmKeluar($user_session),
			'notif_masuk' => count($this->adm->getAdmNotifMasuk($user_session)),
		];
		$this->data['draft'] = [
			'masuk' => $this->draft->getJmlDraftMasuk($user_session),
			'keluar' => $this->draft->getJmlDraftKeluar($user_session),
			'notif_masuk' => count($this->draft->getDraftNotifMasuk($user_session)),
		];
		// $this->debugResponse($this->data); die;
	}

	public function index(){
		$this->data['active_page'] = 'Dashboard';
		$this->data['page_title'] = 'SELAMAT DATANG, DI ADMINISTRASI ONLINE';
		$this->showView('index', $this->data, 'theme_user');
		// $this->debugResponse($this->data);
	}

	protected function script(){
		header('Content-Type: application/javascript');
		$this->subView('script', $this->data);
	}

	public function header(){
        $this->subView('header', $this->data);
	}

	protected function navbar(){
		$this->subView('navbar', $this->data);
	}

	protected function footer(){
		$this->subView('footer', $this->data);
	}
	
	protected function modal($id){
		$this->subView('modal-'.$id, $this->data);
	}

	protected function notifikasi($id){
		switch ($id) {
			case 'administrasi':
				$this->data['link_detail'] = $this->link_adm.'/detail/masuk/';
				$this->data['dataAdm'] = $this->adm->getAdmNotifMasuk($this->data['user_session']);
				if($this->data['user_session']['level_user'] === 'agendaris'){
					$this->data['nama_satker'] = $this->adol->getNamaSatker();
					$this->subView('notif/administrasi/notifikasiagendaris', $this->data);
				}
				elseif($this->data['user_session']['level_user'] === 'assistant'){
					$this->data['nama_satker'] = $this->adol->getNamaSatker();
					$this->subView('notif/administrasi/notifikasiassistant', $this->data);
				}
				elseif($this->data['user_session']['level_user'] === 'jabatan'){
					$this->data['nama_pengirim'] = $this->adm->getAdmRiwayatPengirim();
					$this->subView('notif/administrasi/notifikasijabatan', $this->data);
				}
				break;

			case 'draft':
				$this->data['link_detail'] = $this->link_draft.'/detail/masuk/';
				$this->data['dataDraft'] = $this->draft->getDraftNotifMasuk($this->data['user_session']);
				$this->data['nama_pengirim'] = $this->draft->getDraftRiwayatPengirim();
				$this->subView('notif/draft/notifikasi', $this->data);
				break;
			
			default:
				# code...
				break;
		}
	}
	
	protected function logout(){
		$result = $this->adol->userLogout(['session_id' => $this->data['session_id']]);
		$this->removeCookie();
		$this->redirect($this->link('login'));
	}

}
?>
