<?php

namespace app\adol\controller;

use app\adol\controller\main;

class pengaturan extends main{
	
	public function __construct(){
		parent::__construct();
		$this->data['url_path'] = $this->link($this->getProject().$this->getController());
	}

	public function index(){
		$this->redirect($this->link_adm.'/masuk');
	}

	protected function script(){
		header('Content-Type: application/javascript');
		$this->subView('script', $this->data);
	}

	protected function user($method = ''){
		if(\in_array($method, ['disposisi', 'koordinasi', 'laporan', 'draft'])){
			$pilihan_user_jabatan = $this->adol->getSatkerJabatan($this->data['user_session']['id_satker']);
			foreach ($pilihan_user_jabatan as $key => $value) {
				if(isset($value['group'])) $pilihan_user_jabatan[$key]['group'] = '';
			}
			$this->data['active_page'] = 'User '.\ucwords($method);
			$this->data['page_title'] = \strtoupper('User '.$method.' '.$this->data['user_info']['nama_jabatan']);
			$this->data['status_action'] = \strtolower($method);
			$this->data['pilihan_user_jabatan'] = ['' => ['text' => '-- Pilih Jabatan --']] + $pilihan_user_jabatan;
			$this->showView('index', $this->data, 'theme_user');
		}
		else{
			$this->redirect($this->link_pengaturan.'/user/disposisi');
		}
		// $this->debugResponse($this->data);
	}

	/**
	 * Table User Action
	 */
	protected function table($action){
		if(\in_array($action, ['disposisi', 'koordinasi', 'laporan', 'draft'])){
			$this->subView('table', $this->data);
		}
		else{
			echo '<div class="alert alert-danger" role="alert">Table tidak ditemukan !!</div>';
		}
	}

}
?>
