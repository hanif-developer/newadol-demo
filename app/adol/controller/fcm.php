<?php

namespace app\adol\controller;

use app\defaults\controller\application;

class fcm extends application{

    private $title = 'Test';
    private $message = 'Test kirim notifikasi dari server';
    private $image = '';
    private $payload = '';
    private $result = [];
	
	public function __construct(){
        parent::__construct();
	}

	protected function index(){
		$this->showResponse($this->errorMsg, 404);
    }

    public function setTitle($title) {
        $this->title = $title;
    }
 
    public function setMessage($message) {
        $this->message = $message;
    }
 
    public function setImage($imageUrl) {
        $this->image = $imageUrl;
    }
 
    public function setPayload($data) {
        $this->data = $data;
    }

    public function getPush() {
        $result['title'] = $this->title;
        $result['message'] = $this->message;
        $result['image'] = $this->image;
        $result['payload'] = empty($this->payload) ? new \stdClass() : $this->payload;
        $result['timestamp'] = date('Y-m-d G:i:s');
        return $result;
    }

    /**
     * sending push message to single user by firebase reg id or 
     * multiple users by firebase registration ids
     */
    public function send($to, $message) {
        $fields = (\is_array($to)) ? ['registration_ids' => $to] : ['to' => $to];
        $fields['data'] = ['data' => $message];

        return $this->sendPushNotification($fields);
    }
 
    // Sending message to a topic by topic name
    public function sendToTopic($to, $message) {
        $fields = array(
            'to' => '/topics/' . $to,
            'data' => ['data' => $message],
        );

        return $this->sendPushNotification($fields);
    }
 
    // function makes curl request to firebase servers
    private function sendPushNotification($fields) {
        $result = $this->sendMessagePost([
            'url' => 'https://fcm.googleapis.com/fcm/send',
            'header' => [
                'Authorization: key=' . $this->fcm_api_key,
                'Content-Type: application/json'
            ],
            'fields' => \json_encode($fields)
        ]);

        return \json_decode($result, true);
    }

}
?>
