<?php

namespace app\adol\controller;

use app\adol\controller\main;

class draft extends main{
	
	public function __construct(){
		parent::__construct();
		$this->data['url_path'] = $this->link($this->getProject().$this->getController());
		$this->data['jenis_template_draft'] = $this->adol->getJenisTemplateDraft();
	}

	public function index(){
		$this->redirect($this->link_adm.'/masuk');
	}

	protected function script(){
		header('Content-Type: application/javascript');
		$this->subView('script', $this->data);
	}

	protected function masuk(){
		$this->data['active_page'] = 'Masuk';
		$this->data['page_title'] = 'Data Draft Masuk';
		$this->data['pilihan_cari'] = $this->adol->pilihanTabelDraftMasuk();
		$this->data['pilihan_status'] = $this->adol->getStatusBaca();
		$this->data['pil_cari'] = 'perihal_draft';
		$this->data['status_baca'] = '-1';
		$this->showView('masuk', $this->data, 'theme_user');
		// $this->debugResponse($this->data);
	}

	protected function keluar(){
		$this->data['active_page'] = 'Keluar';
		$this->data['page_title'] = 'Data Draft Keluar';
		$this->data['pilihan_cari'] = $this->adol->pilihanTabelDraftKeluar();
		$this->data['pilihan_status'] = $this->adol->getStatusBaca();
		$this->data['pil_cari'] = 'perihal_draft';
		$this->data['status_baca'] = '-1';
		$this->showView('keluar', $this->data, 'theme_user');
		// $this->debugResponse($this->data);
	}

	protected function detail($action = '', $id = ''){
		$this->data['active_page'] = 'Detail';
		switch ($action) {
			case 'masuk':
				$dataDraft = $this->draft->getDetailDraft($id, $action, $this->data['user_session']);
				// $this->debugResponse($dataDraft);
				if(empty($dataDraft)){
					$this->redirect($this->link_draft.'/masuk');
				}
				$this->data['page_title'] = 'Detail Draft Masuk';
				$this->data['dataDraft'] = $dataDraft;
				$this->showView('detail.'.$action, $this->data, 'theme_user');
				// $this->debugResponse($this->data);
				break;

			case 'keluar':
				$dataDraft = $this->draft->getDetailDraft($id, $action, $this->data['user_session']);
				// $this->debugResponse($dataDraft);
				if(empty($dataDraft)){
					$this->redirect($this->link_draft.'/keluar');
				}
				$this->data['page_title'] = 'Detail Draft Keluar';
				$this->data['dataDraft'] = $dataDraft;
				$this->showView('detail.'.$action, $this->data, 'theme_user');
				// $this->debugResponse($this->data);
				break;
			
			default:
				$this->redirect($this->link_draft.'/masuk');
				break;
		}
	}

	protected function buat($action = '', $id = ''){
		switch ($action) {
			case 'upload':
				if($this->data['user_session']['draft_user_action']['buat'] != 1){
					$this->redirect($this->link_draft.'/masuk');
				}
				
				$pilihan_jenis = $this->adol->getJenisAdm();
				foreach ($pilihan_jenis as $key => $value) {
					if (in_array($key, $this->data['jenis_template_draft'])) {
						$pilihan_jenis[$key]['text'] = '[ TEMPLATE ] - '.$pilihan_jenis[$key]['text']; 
					}
				}
				$this->data['active_page'] = 'Buat';
				$this->data['page_title'] = 'Buat Draft';
				$this->data['pilihan_jenis'] = ['' => ['text' => '-- Pilih Jenis --']] + $pilihan_jenis;
				$this->data['id_draft'] = ''; // Default kosong
				$this->data['id_adm'] = $id;
				$this->data['user_action'] = 'buat';
				$this->data['perihal_draft'] = 'TEST DEVELOPER';
				$this->showView('form/buat', $this->data, 'theme_user');
				break;

			case 'editor':
				$input = $this->post();
				// $this->debugResponse($input);die;
				if(!$input){
					$this->redirect($this->link_draft.'/masuk');
				}

				ob_start();
				$this->template($input['id_jenis']);
				$template_draft = \base64_encode(ob_get_contents());
				ob_end_clean();
		
				$this->data['active_page'] = 'Draft Editor';
				$this->data['page_title'] = 'Draft Template';
				$this->data['template_draft'] = !empty($input['template_draft']) ? $input['template_draft'] : $template_draft;
				$this->data += $input;
				$this->showView('form/editor', $this->data, 'theme_user');
				break;
			
			default:
				$this->redirect($this->link_draft.'/masuk');
				break;
		}

		// $this->debugResponse($this->data);
	}

	/**
	 * Form Draft
	 */
	protected function form($method, $action = ''){
		switch ($method) {
			case 'actiondraft':
				$input = $this->postValidate();
				$draft_action = []; // ['buat', 'lapor', 'tolak', 'revisi', 'setuju', 'finish'];
				foreach ($this->adol->getActionDraft() as $key => $value) { $draft_action[$key] = $value['id']; }
				if(\in_array($action, $draft_action)){
					$formActionDraft = $this->draft->getFormActionDraft($input, $action, $this->data['user_session']);
					// $this->debugResponse($formActionDraft);
					$this->data += $formActionDraft; 
					$this->subView('form/action', $this->data);
				}
				else{
					echo '<div class="alert alert-danger" role="alert">Form tidak ditemukan !!</div>';
					die;
				}
				break;
			
			default:
				echo '<div class="alert alert-danger" role="alert">Form tidak ditemukan !!</div>';
				break;
		}
	}

	/**
	 * Table Draft
	 */
	protected function table($action){
		switch ($action) {
			case 'masuk':
			case 'keluar':
				$this->subView('table-'.$action, $this->data);
				break;

			default:
				echo '<div class="alert alert-danger" role="alert">Table tidak ditemukan !!</div>';
				break;
		}
	}

	/**
	 * Riwayat Draft
	 */
	protected function riwayat(){
		$this->subView('riwayat', $this->data);
	}

	/**
	 * Template Drafter
	 */
	protected function template($jenis){
		$this->subView('template/'.$jenis, $this->data);
	}
}
?>
