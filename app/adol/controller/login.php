<?php

namespace app\adol\controller;

use app\adol\controller\service;

class login extends service{
	
	public function __construct(){
		parent::__construct();
		$userSession = $this->getUserSession();
		if(!empty($userSession)){
			$this->redirect($this->link());
		}
		$this->adol_version = 1; // 1: full regional | 2: satker only
		$this->data['url_path'] = $this->link($this->getProject().$this->getController());
	}

	public function index(){
		switch ($this->adol_version) {
			case 1:
				$this->data['page_title'] = 'Sign In';
				$this->data['regional'] = '';
				$this->data['kategori'] = '';
				$this->showView('index', $this->data, 'theme_user');
				break;

			case 2:
				$this->data['page_title'] = 'Sign In';
				$this->data['regional'] = '';
				$this->data['kategori'] = '';
				$this->showView('index2', $this->data, 'theme_user');
				break;
			
			default:
				# code...
				break;
		}
	}

	protected function init(){
		switch ($this->adol_version) {
			case 1:
				$this->data['pilihan_regional'] = ['' => ['text' => '-- Pilih Regional --']] + $this->adol->getRegional();
				$this->data['pilihan_kategori'] = ['' => ['text' => '-- Pilih Kategori --']] + $this->adol->getKategori();
				$this->data['regional'] = 'JTG';
				$this->data['kategori'] = '';
				$this->subView('init', $this->data);
				break;

			case 2:
				$this->data['pilihan_satker'] = ['' => ['text' => '-- Pilih Satker --']] + $this->adol->getGroupSatker('JTG', 'polda');
				$this->data['pilihan_jabatan'] = ['' => ['text' => '-- Pilih Jabatan --']] + $this->adol->getSatkerJabatan('');
				$this->subView('init2', $this->data);
				break;
			
			default:
				# code...
				break;
		}	
	}

	protected function satker(){
		$input = $this->postValidate();
		if(\in_array($input['kategori'], ['mabes', 'polda', 'polres', 'polsek'])){
			$this->data['pilihan_satker'] = ['' => ['text' => '-- Pilih Satker --']] + $this->adol->getGroupSatker($input['regional'], $input['kategori']);
			$this->subView('satker', $this->data);
		}
	}

	protected function jabatan(){
		$input = $this->postValidate();
		$pilihan_jabatan = $this->adol->getSatkerJabatan($input['satker']);
		$this->data['pilihan_jabatan'] = ['' => ['text' => '-- Pilih Jabatan --']] + $pilihan_jabatan;
		$this->subView('jabatan', $this->data);
		// $this->debugResponse($pilihan_jabatan);
	}

	protected function password(){
		$input = $this->postValidate();
		// $this->data['data_satker'] = $this->adol->getFormSatker($input['satker'], $input['regional'])['form'];
		$this->data['data_satker'] = $this->adol->getFormSatker($input['satker'], '');
		$this->data['jabatan'] = $input['jabatan'];
		$this->subView('password', $this->data);
		// $this->debugResponse($this->data['data_satker']);
    }

	protected function script(){
		header('Content-Type: application/javascript');
		$this->subView('script', $this->data);
	}

}
?>
