<?php

namespace app\adol\controller;

use app\adol\controller\main;

class administrasi extends main{
	
	public function __construct(){
		parent::__construct();
		$this->data['url_path'] = $this->link($this->getProject().$this->getController());
	}

	public function index(){
		$this->redirect($this->link_adm.'/masuk');
	}

	protected function script(){
		header('Content-Type: application/javascript');
		$this->subView('script', $this->data);
	}

	protected function input(){
		switch ($this->data['user_session']['level_user']) {
			case 'agendaris':
			case 'assistant':
				$this->data['active_page'] = 'Input Administrasi';
				$this->data['page_title'] = 'Input Data Administrasi';
				$dataAdm = $this->adm->getFormAdm();
				// Hapus data satker sendiri
				unset($dataAdm['pilihan_satker'][$this->data['user_session']['id_satker']]);
				$this->data['dataAdm'] = $dataAdm;
				$this->showView('input', $this->data, 'theme_user');
				// $this->debugResponse($this->data['user_session']);
				// $this->debugResponse($this->data['dataAdm']);
				// $this->debugResponse($this->data);
				break;
			
			default:
				// $this->redirect($this->link($this->getProject()));
				$this->redirect($this->link_adm.'/masuk');
				break;
		}
	}

	protected function kirim(){
		switch ($this->data['user_session']['level_user']) {
			case 'agendaris':
				$this->data['active_page'] = 'Kirim Administrasi';
				$this->data['page_title'] = 'Kirim Data Administrasi';
				$dataAdm = $this->adm->getFormAdm($this->data['user_session']);
				// Hapus data satker sendiri
				unset($dataAdm['pilihan_satker'][$this->data['user_session']['id_satker']]);
				$this->data['dataAdm'] = $dataAdm;
				$this->showView('kirim', $this->data, 'theme_user');
				// $this->debugResponse($this->data['user_session']);
				// $this->debugResponse($this->data['dataAdm']);
				// $this->debugResponse($this->data);
				break;
			
			default:
				// $this->redirect($this->link($this->getProject()));
				$this->redirect($this->link_adm.'/masuk');
				break;
		}
	}

	protected function edit($id){
		// Check Data Adm
		$dataAdm = $this->adm->getDetailAdm($id, 'masuk', $this->data['user_session']);
		if(empty($dataAdm)){
			$this->redirect($this->link_adm.'/masuk');
		}

		switch ($this->data['user_session']['level_user']) {
			case 'agendaris':
			case 'assistant':
				$this->data['active_page'] = 'Edit Administrasi';
				$this->data['page_title'] = 'Edit Data Administrasi';
				$this->data['dataAdm'] = $this->adm->getFormAdm('', $id);
				$this->showView('edit', $this->data, 'theme_user');
				// $this->debugResponse($this->data['user_session']);
				// $this->debugResponse($this->data['dataAdm']);
				// $this->debugResponse($this->data);
				break;
			
			default:
				// $this->redirect($this->link($this->getProject()));
				$this->redirect($this->link_adm.'/masuk');
				break;
		}
	}

	protected function masuk(){
		$this->data['active_page'] = 'Masuk';
		$this->data['page_title'] = 'Data Administrasi Masuk';
		$this->data['pilihan_cari'] = $this->adol->pilihanTabelAdmMasuk($this->data['user_session']);
		$this->data['pilihan_status'] = $this->adol->getStatusBaca();
		$this->data['pil_cari'] = 'perihal_adm';
		$this->data['status_baca'] = '-1';
		$this->showView('masuk', $this->data, 'theme_user');
		// $this->debugResponse($this->data);
	}

	protected function keluar(){
		switch ($this->data['user_session']['level_user']) {
			case 'agendaris':
			case 'jabatan':
				$this->data['active_page'] = 'Keluar';
				$this->data['page_title'] = 'Data Administrasi Keluar';
				$this->data['pilihan_cari'] = $this->adol->pilihanTabelAdmKeluar($this->data['user_session']);
				$this->data['pilihan_status'] = $this->adol->getStatusBaca();
				$this->data['pil_cari'] = 'perihal_adm';
				$this->showView('keluar', $this->data, 'theme_user');
				// $this->debugResponse($this->data);
				break;
			
			default:
				// $this->redirect($this->link($this->getProject()));
				$this->redirect($this->link_adm.'/masuk');
				break;
		}
	}

	protected function monitor(){
		switch ($this->data['user_session']['level_user']) {
			case 'agendaris':
			case 'assistant':
				$this->data['active_page'] = 'Monitor';
				$this->data['page_title'] = 'Data Monitor Administrasi';
				$this->data['pilihan_cari'] = $this->adol->pilihanTabelAdmMonitor();
				$this->data['pilihan_status'] = $this->adol->getStatusBaca();
				$this->data['pil_cari'] = 'perihal_adm';
				$this->showView('monitor', $this->data, 'theme_user');
				// $this->debugResponse($this->data);
				break;
			
			default:
				// $this->redirect($this->link($this->getProject()));
				$this->redirect($this->link_adm.'/masuk');
				break;
		}
	}

	protected function detail($action = '', $id = ''){
		$this->data['active_page'] = 'Detail';
		switch ($action) {
			case 'masuk':
				$dataAdm = $this->adm->getDetailAdm($id, $action, $this->data['user_session']);
				if(empty($dataAdm)){
					$this->redirect($this->link_adm.'/masuk');
				}
				$this->data['page_title'] = 'Detail Administrasi Masuk';
				$this->data['dataAdm'] = $dataAdm;
				$this->showView('detail.'.$action, $this->data, 'theme_user');
				// $this->debugResponse($this->data);
				break;

			case 'keluar':
				$dataAdm = $this->adm->getDetailAdm($id, $action, $this->data['user_session']);
				if(empty($dataAdm)){
					$this->redirect($this->link_adm.'/masuk');
				}
				$this->data['page_title'] = 'Detail Administrasi Keluar';
				$this->data['dataAdm'] = $dataAdm;
				$this->showView('detail.'.$action, $this->data, 'theme_user');
				// $this->debugResponse($this->data);
				break;

			case 'monitor':
				$dataAdm = $this->adm->getDetailAdmRiwayat($id, $this->data['user_session']);
				if(empty($dataAdm)){
					$this->redirect($this->link_adm.'/masuk');
				}
				$this->data['page_title'] = 'Detail Monitor Administrasi';
				$this->data['dataAdm'] = $dataAdm;
				$this->showView('detail.'.$action, $this->data, 'theme_user');
				// $this->debugResponse($this->data);
				break;
			
			default:
				$this->redirect($this->link_adm.'/masuk');
				break;
		}
	}

	/**
	 * Form Administrasi
	 */
	protected function form($method, $action = '', $id = ''){
		switch ($method) {
			case 'groupsatker':
				$this->data['pilihan_satker_mabes'] = $this->adol->getGroupSatker('', 'mabes');
				$this->data['pilihan_satker_polda'] = $this->adol->getGroupSatker($this->data['user_session']['id_regional'], 'polda');
				$this->data['pilihan_satker_polres'] = $this->adol->getGroupSatker($this->data['user_session']['id_regional'], 'polres');
				$this->data['pilihan_satker_polsek'] = $this->adol->getGroupSatker($this->data['user_session']['id_regional'], 'polsek');
				$this->subView('form/'.$method, $this->data);
				break;

			case 'actionadm':
				$form_title = $this->adol->getFormTitleUserAction();
				$user_action = [
					'laporan' => $this->adm->getUserLaporan($this->data['user_session']),
					'disposisi' => $this->adm->getUserDisposisi($this->data['user_session']),
					'koordinasi' => $this->adm->getUserKoordinasi($this->data['user_session']),
				];

				if(\in_array($action, ['laporan', 'disposisi', 'koordinasi'])){ // Level user agendaris | assistant | jabatan
					$pilihan_tujuan = isset($user_action[$action]) ? $user_action[$action] : [];
				}
				else{
					echo '<div class="alert alert-danger" role="alert">Form tidak ditemukan !!</div>';
					die;
				}

				$this->data['pilihan_tujuan'] = !empty($pilihan_tujuan) ? $pilihan_tujuan : ['' => ['text' => '-- DATA MASIH KOSONG --']];
				$this->data['form_title'] = $form_title[$action]['text'];
				$this->data['user_action'] = $action;
				$this->data['id_adm'] = $id;
				$this->subView('form/'.$method, $this->data);
				break;
			
			default:
				echo '<div class="alert alert-danger" role="alert">Form tidak ditemukan !!</div>';
				break;
		}
	}

	/**
	 * Table Administrasi
	 */
	protected function table($action){
		switch ($action) {
			case 'masuk':
				if(\in_array($this->data['user_session']['level_user'], ['agendaris', 'assistant', 'jabatan'])){
					$this->subView('table/masuk/'.$this->data['user_session']['level_user'], $this->data);
				}
				else{
					echo '<div class="alert alert-danger" role="alert">Table tidak ditemukan !!</div>';
				}
				break;

			case 'keluar':
				if(\in_array($this->data['user_session']['level_user'], ['agendaris', 'jabatan'])){
					$this->subView('table/keluar/'.$this->data['user_session']['level_user'], $this->data);
				}
				else{
					echo '<div class="alert alert-danger" role="alert">Table tidak ditemukan !!</div>';
				}
				break;

			case 'monitor':
				if(\in_array($this->data['user_session']['level_user'], ['agendaris', 'assistant'])){
					$this->subView('table/monitor', $this->data);
				}
				else{
					echo '<div class="alert alert-danger" role="alert">Table tidak ditemukan !!</div>';
				}
				break;
			
			default:
				echo '<div class="alert alert-danger" role="alert">Table tidak ditemukan !!</div>';
				break;
		}
	}

	/**
	 * Riwayat Administrasi
	 */
	protected function riwayat(){
		$this->subView('riwayat', $this->data);
	}

}
?>
