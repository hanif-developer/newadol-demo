<?php
namespace app\adol\model;

use system\Model;
use app\adol\model\adol;
use comp;

class adol_adm extends adol{
	
	public function __construct(){
		parent::__construct();
    }

    public function getAdmRiwayatPengirim(){
        $result = [];
        $data = $this->getData('SELECT pengirim_adm_riwayat, nama_jabatan FROM tb_adm_riwayat a, tb_jabatan b WHERE (a.pengirim_adm_riwayat=b.id_jabatan)', []);
        foreach ($data['value'] as $key => $value) { $result[$value['pengirim_adm_riwayat']] = ['text' => $value['nama_jabatan']]; }
        return $result;
    }
	
	public function getAdmRiwayatTujuan(){
        $result = [];
        $data = $this->getData('SELECT tujuan_adm_riwayat, nama_jabatan FROM tb_adm_riwayat a, tb_jabatan b WHERE (a.tujuan_adm_riwayat=b.id_jabatan)', []);
        foreach ($data['value'] as $key => $value) { $result[$value['tujuan_adm_riwayat']] = ['text' => $value['nama_jabatan']]; }
        return $result;
    }

    public function getJmlAdmMasuk($user){
		$dataJabatan = $this->getData('SELECT * FROM tb_jabatan WHERE (id_jabatan = ?)', array($user['id_jabatan']));
		if($user['level_user'] == 'agendaris')
			$data = $this->getData('SELECT id_adm FROM tb_adm WHERE (tujuan_adm = ?)', array($dataJabatan['value'][0]['id_satker']));
		if($user['level_user'] == 'assistant')
			$data = $this->getData('SELECT id_adm FROM tb_adm WHERE (bidang_adm = ?)', array($dataJabatan['value'][0]['id_bidang']));
		if($user['level_user'] == 'jabatan')
			$data = $this->getData('SELECT id_adm_riwayat FROM tb_adm_riwayat WHERE (tujuan_adm_riwayat = ?)', array($user['id_jabatan']));
        return $data['count'];
    }
	
	public function getJmlAdmKeluar($user){
		$dataJabatan = $this->getData('SELECT * FROM tb_jabatan WHERE (id_jabatan = ?)', array($user['id_jabatan']));
		if($user['level_user'] == 'agendaris')
			$data = $this->getData('SELECT id_adm FROM tb_adm WHERE (pengirim_adm = ?)', array($dataJabatan['value'][0]['id_satker']));
		if($user['level_user'] == 'assistant')
			$data['count'] = 0; 
		if($user['level_user'] == 'jabatan')
			$data = $this->getData('SELECT id_adm_riwayat FROM tb_adm_riwayat WHERE (pengirim_adm_riwayat = ?)', array($user['id_jabatan']));
        return $data['count'];
    }

    public function getAdmNotifMasuk($user){
        $result = [];
        if($user['level_user'] == 'agendaris'){
            $keys = ['0', '1', $user['id_satker']]; // action_adm 1 : kirim, 0 : input
            $data = $this->getData('SELECT * FROM tb_adm a, tb_satker b WHERE (a.pengirim_adm=b.id_satker) AND (status_adm = ?) AND (action_adm = ?) AND (tujuan_adm = ?) ORDER BY waktu_adm DESC', $keys);
            $result = $data['value'];
        }
		if($user['level_user'] == 'assistant'){
            $keys = ['0', '0', $user['id_bidang']]; // action_adm 1 : kirim, 0 : input
            $data = $this->getData('SELECT * FROM tb_adm a, tb_satker b WHERE (a.pengirim_adm=b.id_satker) AND (status_bidang_adm = ?) AND (action_adm = ?) AND (bidang_adm = ?) ORDER BY waktu_adm DESC', $keys);
            $result = $data['value'];
        }
		if($user['level_user'] == 'jabatan'){
            $keys = ['0', $user['id_jabatan']];
            $data = $this->getData('SELECT * FROM tb_adm_riwayat a, tb_adm b, tb_jabatan c WHERE (a.id_adm=b.id_adm) AND (a.tujuan_adm_riwayat=c.id_jabatan) AND (status_adm_riwayat = ?) AND (tujuan_adm_riwayat = ?)  ORDER BY waktu_adm_riwayat DESC', $keys);
            $result = $data['value'];
        }

        return $result;
    }

    public function getUserLaporan($user) {
        $result = [];
        $keys = [$user['id_jabatan']];
        $data = $this->getData('SELECT id_relasi, nama_jabatan, singkatan_satker FROM tb_user_laporan a, tb_jabatan b, tb_satker c WHERE (id_relasi=b.id_jabatan) AND (b.id_satker=c.id_satker) AND (a.id_jabatan=?) ORDER BY urutan_satker, urutan_jabatan', $keys);
        //foreach ($data['value'] as $key => $value) { $result[$value['id_relasi']] = ['text' => $value['nama_jabatan']]; }
        foreach ($data['value'] as $key => $value) { 
            $result[$value['id_relasi']] = [
                'text' => $value['nama_jabatan'],
                'info' => $value['singkatan_satker'],
            ]; 
        }
        return $result;
    }

    public function getUserDisposisi($user){
        $result = [];
        $keys = [$user['id_jabatan']];
        $data = $this->getData('SELECT id_relasi, nama_jabatan, singkatan_satker FROM tb_user_disposisi a, tb_jabatan b, tb_satker c WHERE (id_relasi=b.id_jabatan) AND (b.id_satker=c.id_satker) AND (a.id_jabatan=?) ORDER BY urutan_satker, urutan_jabatan', $keys);
        //foreach ($data['value'] as $key => $value) { $result[$value['id_relasi']] = ['text' => $value['nama_jabatan']]; }
        foreach ($data['value'] as $key => $value) { 
            $result[$value['id_relasi']] = [
                'text' => $value['nama_jabatan'],
                'info' => $value['singkatan_satker'],
            ]; 
        }
        return $result;
    }

    public function getUserKoordinasi($user){
        $result = [];
        $keys = [$user['id_jabatan']];
        $data = $this->getData('SELECT id_relasi, nama_jabatan, singkatan_satker FROM tb_user_koordinasi a, tb_jabatan b, tb_satker c WHERE (id_relasi=b.id_jabatan) AND (b.id_satker=c.id_satker) AND (a.id_jabatan=?) ORDER BY urutan_satker, urutan_jabatan', $keys);
        //foreach ($data['value'] as $key => $value) { $result[$value['id_relasi']] = ['text' => $value['nama_jabatan']]; }
        foreach ($data['value'] as $key => $value) { 
            $result[$value['id_relasi']] = [
                'text' => $value['nama_jabatan'],
                'info' => $value['singkatan_satker'],
            ]; 
        }
        return $result;
    }

    public function getFormAdm($userSession = [], $id = ''){
		$group_satker = $this->getGroupSatker();
		if(!empty($userSession)){
			/**
			 * Mode Kirim
			 * Jika polsek, pilihan satker terdiri dari polres diatasnya dan polsek lainnya dalam polres tesebut
			 * Jika polres, pilihan satker terdiri dari satker polda dan polres lainnya dalam satu regional
			 * jika polda, pilihan satker terdiri dari semua satker dalam satu regional
			 */
			// $userSession['kategori_satker'] = 'polsek'; // buat tester kategori
			switch ($userSession['kategori_satker']) {
				case 'polsek':
					$keys = ['1', $userSession['id_regional'], $userSession['id_subsatker'], $userSession['id_subsatker']];
					$data = $this->getData('SELECT * FROM tb_satker WHERE (status_satker LIKE ?) AND (id_regional = ?) AND ((id_satker = ?) OR (id_subsatker = ?)) ORDER BY urutan_satker', $keys);
					foreach ($data['value'] as $key => $value) { $pilihan_satker[$value['id_satker']] = $group_satker[$value['id_satker']]; }
					break;

				case 'polres':
					$keys = ['1', $userSession['id_regional'], 'polda,polres']; // Jangan dipisahkan komanya !!!
					$data = $this->getData('SELECT * FROM tb_satker WHERE (status_satker LIKE ?) AND (id_regional = ?) AND (FIND_IN_SET(kategori_satker, ?)) ORDER BY urutan_satker', $keys);
					foreach ($data['value'] as $key => $value) { $pilihan_satker[$value['id_satker']] = $group_satker[$value['id_satker']]; }
					break;
				
				default: // polda
					$pilihan_satker = $this->getGroupSatker($userSession['id_regional'], '');
					break;
			}
		}
		else{
			// Mode Input
			$pilihan_satker = $group_satker + ['XYZ123' => ['text' => 'LAINNYA']];
		}

		$result['form'] = $this->getDataTabel('tb_adm', ['id_adm', $id]);
		$result['form_title'] = empty($id) ? 'Input Data Administrasi' : 'Edit Data Administrasi';
		$result['form_status'] = empty($id) ? 'input' : 'edit';
		$result['pilihan_jenis'] = ['' => ['text' => '-- Pilih Jenis --']] + $this->getJenisAdm();
		$result['pilihan_klasifikasi'] = ['' => ['text' => '-- Pilih Klasifikasi --']] + $this->getKlasifikasiAdm();
		$result['pilihan_satker'] = $pilihan_satker;
		$result['pilihan_derajat'] = ['' => ['text' => '-- Pilih Derajat --']] + $this->getDerajatAdm();
		$result['upload_mimes'] = $this->mimes_pdf;
		$result['upload_description'] = $this->upload_description_pdf;
		return $result;
    }
    
    // Buat Agendaris
    public function getDetailAdm($id, $action, $userSession){
        $option_satker = $this->getNamaSatker();
        $option_jenis_adm = $this->getJenisAdm();
        $option_klasifikasi = $this->getKlasifikasiAdm();
        $option_derajat = $this->getDerajatAdm();
        
        // Check Action ADM
        if($action == 'masuk'){
            // Check detail adm sesuai level user
            if($userSession['level_user'] == 'agendaris'){
                $data = $this->getData('SELECT * FROM tb_adm WHERE (id_adm = ?) AND (tujuan_adm = ?)', [$id, $userSession['id_satker']]);
            }
            if($userSession['level_user'] === 'assistant'){
                $data = $this->getData('SELECT * FROM tb_adm WHERE (id_adm = ?) AND (bidang_adm = ?)', [$id, $userSession['id_bidang']]);
            }
            if($userSession['level_user'] === 'jabatan'){
                $data = $this->getData('SELECT * FROM tb_adm_riwayat a, tb_adm b WHERE (a.id_adm=b.id_adm) AND (id_adm_riwayat = ?) AND (tujuan_adm_riwayat = ?)', [$id, $userSession['id_jabatan']]);
                // $data = $this->getData('SELECT * FROM tb_adm_riwayat a, tb_adm b WHERE (a.id_adm=b.id_adm) AND (id_adm_riwayat = ?)', [$id]);
            }
            $dataAdm = ($data['count'] > 0) ? $data['value'][0] : [];
            if(empty($dataAdm)) return [];
            // return $data;

            // update status baca !!!
            $waktu_baca = date('Y-m-d H:i:s');
            if($userSession['level_user'] == 'agendaris'){
                $result = $this->update('tb_adm', ['status_adm' => '1', 'waktu_dibaca_adm' => $waktu_baca], ['id_adm' => $id]);
            }
            if($userSession['level_user'] === 'assistant'){
                $result = $this->update('tb_adm', ['status_bidang_adm' => '1', 'waktu_dibaca_adm' => $waktu_baca], ['id_adm' => $id]);
            }
            if($userSession['level_user'] === 'jabatan'){
                $result = $this->update('tb_adm_riwayat', ['status_adm_riwayat' => '1', 'waktu_dibaca_adm_riwayat' => $waktu_baca], ['id_adm_riwayat' => $id]);
            }
        }
        else if($action == 'keluar'){
            // Check detail adm sesuai level user
            if($userSession['level_user'] == 'agendaris'){
                $data = $this->getData('SELECT * FROM tb_adm WHERE (id_adm = ?) AND (pengirim_adm = ?)', [$id, $userSession['id_satker']]);
            }
            if($userSession['level_user'] === 'jabatan'){
                $data = $this->getData('SELECT * FROM tb_adm_riwayat a, tb_adm b WHERE (a.id_adm=b.id_adm) AND (id_adm_riwayat = ?) AND (pengirim_adm_riwayat = ?)', [$id, $userSession['id_jabatan']]);
            }
            $dataAdm = ($data['count'] > 0) ? $data['value'][0] : [];
            if(empty($dataAdm)) return [];
        }
        else{
            return [];
        }

        $jenis_adm = $dataAdm['jenislain_adm'];
        if($dataAdm['id_jenis'] != 'JNSXX'){
            if($dataAdm['id_jenis'] != 'JNS99'){
                $jenis_adm = !empty($option_jenis_adm[$dataAdm['id_jenis']]) ? $option_jenis_adm[$dataAdm['id_jenis']]['text'] : $jenis_adm;
            }
        }
        
        $nama_pengirim = $dataAdm['pengirimlain_adm'];
        if($dataAdm['pengirim_adm'] != 'XYZ123'){
            $nama_pengirim = !empty($option_satker[$dataAdm['pengirim_adm']]) ? $option_satker[$dataAdm['pengirim_adm']]['text'] : '-';
        }
        
        $nama_tujuan = !empty($option_satker[$dataAdm['tujuan_adm']]) ? $option_satker[$dataAdm['tujuan_adm']]['text'] : '-';
        $nama_klasifikasi = !empty($option_klasifikasi[$dataAdm['id_klasifikasi']]) ? $option_klasifikasi[$dataAdm['id_klasifikasi']]['text'] : '-';
        $nama_derajat = !empty($option_derajat[$dataAdm['id_derajat']]) ? $option_derajat[$dataAdm['id_derajat']]['text'] : '-';
        $lampiran_adm = (!empty($dataAdm['lampiran_adm']) && file_exists($this->path_lampiran_dokumen.$dataAdm['lampiran_adm'])) ? $this->getUrl->baseUrl.$this->path_lampiran_dokumen.$dataAdm['lampiran_adm'] : '';

        $result = [
            'id_adm' => $dataAdm['id_adm'],
            'id_draft' => $dataAdm['id_draft'],
            'nama_jenis' => $jenis_adm,
            'nomer_adm' => $dataAdm['nomer_adm'],
            'nomer_agenda' => $dataAdm['nomer_agenda'],
            'nama_pengirim' => $nama_pengirim,
            'nama_tujuan' => $nama_tujuan,
            'pengirim_adm' => $dataAdm['pengirim_adm'],
            'tujuan_adm' => $dataAdm['tujuan_adm'],
            'tanggal_adm' => comp\FUNC::tanggal($dataAdm['tanggal_adm'], 'long_date'),
            'nama_klasifikasi' => $nama_klasifikasi,
            'nama_derajat' => $nama_derajat,
            'perihal_adm' => $dataAdm['perihal_adm'],
            'tembusan_adm' => $dataAdm['tembusan_adm'],
            'keterangan_adm' => $dataAdm['keterangan_adm'],
            'lampiran_adm' => $lampiran_adm,
            'status_input' => ($dataAdm['action_adm'] == 0) ? true : false, // 0: input(bisa diedit) | 1: kirim
        ];

        if($userSession['level_user'] == 'jabatan'){
            $action_adm_riwayat = $this->getActionAdmRiwayat('list');
            $option_pengirim_riwayat = $this->getAdmRiwayatPengirim();
            $result['aksi_riwayat'] = $action_adm_riwayat[$dataAdm['action_adm_riwayat']];
            $result['pengirim_aksi'] = !empty($option_pengirim_riwayat[$dataAdm['pengirim_adm_riwayat']]) ? $option_pengirim_riwayat[$dataAdm['pengirim_adm_riwayat']]['text'] : '-';
            $result['isi_pesan'] = $dataAdm['keterangan_adm_riwayat'];
        }

		return $result;
    }

    // Buat Monitoring
    public function getDetailAdmRiwayat($id, $userSession){
        $option_satker = $this->getNamaSatker();
        $option_jenis_adm = $this->getJenisAdm();
        $option_klasifikasi = $this->getKlasifikasiAdm();
        $option_derajat = $this->getDerajatAdm();
        $action_adm_riwayat = $this->getActionAdmRiwayat('list');
        $option_pengirim_riwayat = $this->getAdmRiwayatPengirim();
        
        // Check Action ADM Riwayat
        if(in_array($userSession['level_user'], ['agendaris', 'assistant'])){
            $data = $this->getData('SELECT * FROM tb_adm_riwayat a, tb_adm b WHERE (a.id_adm=b.id_adm) AND (id_adm_riwayat = ?)', array($id));
            $dataAdm = ($data['count'] > 0) ? $data['value'][0] : [];
            if(empty($dataAdm)) return [];
        }

        $jenis_adm = $dataAdm['jenislain_adm'];
        if($dataAdm['id_jenis'] != 'JNSXX'){
            if($dataAdm['id_jenis'] != 'JNS99'){
                $jenis_adm = !empty($option_jenis_adm[$dataAdm['id_jenis']]) ? $option_jenis_adm[$dataAdm['id_jenis']]['text'] : $jenis_adm;
            }
        }
        
        $nama_pengirim = $dataAdm['pengirimlain_adm'];
        if($dataAdm['pengirim_adm'] != 'XYZ123'){
            $nama_pengirim = !empty($option_satker[$dataAdm['pengirim_adm']]) ? $option_satker[$dataAdm['pengirim_adm']]['text'] : '-';
        }
        
        $nama_tujuan = !empty($option_satker[$dataAdm['tujuan_adm']]) ? $option_satker[$dataAdm['tujuan_adm']]['text'] : '-';
        $nama_klasifikasi = !empty($option_klasifikasi[$dataAdm['id_klasifikasi']]) ? $option_klasifikasi[$dataAdm['id_klasifikasi']]['text'] : '-';
        $nama_derajat = !empty($option_derajat[$dataAdm['id_derajat']]) ? $option_derajat[$dataAdm['id_derajat']]['text'] : '-';
        $lampiran_adm = (!empty($dataAdm['lampiran_adm']) && file_exists($this->path_lampiran_dokumen.$dataAdm['lampiran_adm'])) ? $this->getUrl->baseUrl.$this->path_lampiran_dokumen.$dataAdm['lampiran_adm'] : '';

        $result = [
            'id_adm' => $dataAdm['id_adm'],
            'nama_jenis' => $jenis_adm,
            'nomer_adm' => $dataAdm['nomer_adm'],
            'nomer_agenda' => $dataAdm['nomer_agenda'],
            'nama_pengirim' => $nama_pengirim,
            'nama_tujuan' => $nama_tujuan,
            'pengirim_adm' => $dataAdm['pengirim_adm'],
            'tujuan_adm' => $dataAdm['tujuan_adm'],
            'tanggal_adm' => comp\FUNC::tanggal($dataAdm['tanggal_adm'], 'long_date'),
            'nama_klasifikasi' => $nama_klasifikasi,
            'nama_derajat' => $nama_derajat,
            'perihal_adm' => $dataAdm['perihal_adm'],
            'tembusan_adm' => $dataAdm['tembusan_adm'],
            'keterangan_adm' => $dataAdm['keterangan_adm'],
            'lampiran_adm' => $lampiran_adm,
            'status_input' => ($dataAdm['action_adm'] == 0) ? true : false, // 0: input(bisa diedit) | 1: kirim
            'aksi_riwayat' => $action_adm_riwayat[$dataAdm['action_adm_riwayat']],
            'pengirim_aksi' => !empty($option_pengirim_riwayat[$dataAdm['pengirim_adm_riwayat']]) ? $option_pengirim_riwayat[$dataAdm['pengirim_adm_riwayat']]['text'] : '-',
            'isi_pesan' => $dataAdm['keterangan_adm_riwayat'],
        ];

		return $result;
    }

    public function getTabelAdmMasuk($input, $userSession){
		$cari = '%'.$input['cari'].'%';
        $page = $input['page'];
        $keys = [$cari];
        $query = '';
        $order = '';
        if($userSession['level_user'] === 'agendaris'){
            $option_pengirim = $this->getNamaSatker();
            $query = 'FROM tb_adm a LEFT JOIN tb_satker b ON (a.pengirim_adm=b.id_satker) LEFT JOIN tb_jenis c ON (a.id_jenis=c.id_jenis) ';
            $query .= 'WHERE ('.$input['pil_cari'].' LIKE ?) AND (tujuan_adm = ?)';
            $order = ' ORDER BY waktu_adm DESC';
            array_push($keys, $userSession['id_satker']);

            if(\in_array($input['status_baca'], ['0', '1'])){
                $query .= ' AND (status_adm = ?)';
                array_push($keys, $input['status_baca']);
            }
        }
        if($userSession['level_user'] === 'assistant'){
            $option_pengirim = $this->getNamaSatker();
            $query = 'FROM tb_adm a LEFT JOIN tb_satker b ON (a.pengirim_adm=b.id_satker) LEFT JOIN tb_jenis c ON (a.id_jenis=c.id_jenis) ';
            $query .= 'WHERE ('.$input['pil_cari'].' LIKE ?) AND (bidang_adm = ?)';
            $order = ' ORDER BY waktu_adm DESC';
            array_push($keys, $userSession['id_bidang']);

            if(\in_array($input['status_baca'], ['0', '1'])){
                $query .= ' AND (status_bidang_adm = ?)';
                array_push($keys, $input['status_baca']);
            }
        }
        if($userSession['level_user'] === 'jabatan'){
            $option_pengirim = $this->getAdmRiwayatPengirim();
            $query = 'FROM tb_adm_riwayat a LEFT JOIN tb_adm b ON (a.id_adm=b.id_adm) LEFT JOIN tb_jabatan c ON (a.pengirim_adm_riwayat=c.id_jabatan) LEFT JOIN tb_jenis d ON (b.id_jenis=d.id_jenis) ';
            $query .= 'WHERE ('.$input['pil_cari'].' LIKE ?) AND (tujuan_adm_riwayat = ?)';
            $order = ' ORDER BY waktu_adm_riwayat DESC';
            array_push($keys, $userSession['id_jabatan']);

            if(\in_array($input['status_baca'], ['0', '1'])){
                $query .= ' AND (status_adm_riwayat = ?)';
                array_push($keys, $input['status_baca']);
            }
        }

        if($input['tembusan'] == '1'){
            $query .= ' AND (tembusan_adm = ?)';
            array_push($keys, $input['tembusan']);
        }

        $limit = 20;
        $cursor = ($page - 1) * $limit;
        $dataCount = $this->getData('SELECT COUNT(*) AS counts '.$query, $keys);
        $dataValue = $this->getData('SELECT * '.$query.$order.' LIMIT '.$cursor.','.$limit, $keys);
        $listValue = [];
        $option_bidang = $this->getBidang($userSession['id_satker']);
        $option_jenis_adm = $this->getJenisAdm();
        $option_status_baca = $this->getStatusBaca();
        $action_adm_riwayat = $this->getActionAdmRiwayat('list');
        foreach ($dataValue['value'] as $key => $value) {
            if(in_array($userSession['level_user'], ['agendaris', 'assistant'])){
                $id_adm = $value['id_adm'];
                $status_baca_adm = ($userSession['level_user'] === 'agendaris') ? $option_status_baca[$value['status_adm']] : $option_status_baca[$value['status_bidang_adm']];
                $tanggal_adm = comp\FUNC::tanggal($value['tanggal_adm'], 'long_date');
                $nama_pengirim = $value['pengirimlain_adm'];
                if($value['pengirim_adm'] != 'XYZ123'){
                    $nama_pengirim = !empty($option_pengirim[$value['pengirim_adm']]) ? $option_pengirim[$value['pengirim_adm']]['text'] : '-';
                }
                $bidang_distribusi = !empty($option_bidang[$value['bidang_adm']]) ? $option_bidang[$value['bidang_adm']]['nama_bidang'] : '-';

                $listValue[$key]['id_adm'] = $id_adm;
                $listValue[$key]['status_baca'] = $status_baca_adm['text'];
                $listValue[$key]['status_baca_color'] = $status_baca_adm['color'];
                $listValue[$key]['tanggal_adm'] = $tanggal_adm;
                $listValue[$key]['nama_pengirim'] = $nama_pengirim;
                $listValue[$key]['pengirim_adm'] = $value['pengirim_adm'];
                $listValue[$key]['bidang_distribusi'] = $bidang_distribusi; // khusus assistant
                $listValue[$key]['keterangan_adm'] = '';
            }

            if($userSession['level_user'] === 'jabatan'){
                $id_adm = $value['id_adm_riwayat'];
                $status_baca_adm = $option_status_baca[$value['status_adm_riwayat']];
                $aksi_adm = $action_adm_riwayat[$value['action_adm_riwayat']];
                $tanggal_adm = comp\FUNC::tanggal($value['tanggal_adm'], 'long_date');
                $waktu_adm = comp\FUNC::tanggal($value['waktu_adm_riwayat'], 'long_date_time');
                $nama_pengirim = !empty($option_pengirim[$value['pengirim_adm_riwayat']]) ? $option_pengirim[$value['pengirim_adm_riwayat']]['text'] : '-';

                $listValue[$key]['id_adm'] = $id_adm;
                $listValue[$key]['status_baca'] = $status_baca_adm['text'];
                $listValue[$key]['status_baca_color'] = $status_baca_adm['color'];
                $listValue[$key]['aksi_adm'] = $aksi_adm;
                $listValue[$key]['tanggal_adm'] = $tanggal_adm;
                $listValue[$key]['waktu_adm'] = $waktu_adm;
                $listValue[$key]['nama_pengirim'] = $nama_pengirim;
                $listValue[$key]['pengirim_adm'] = $value['pengirim_adm_riwayat'];
                $listValue[$key]['keterangan_adm'] = $value['keterangan_adm_riwayat'];
            }

            $jenis_adm = $value['jenislain_adm'];
            if($value['id_jenis'] != 'JNSXX'){
                if($value['id_jenis'] != 'JNS99'){
                    $jenis_adm = !empty($option_jenis_adm[$value['id_jenis']]) ? $option_jenis_adm[$value['id_jenis']]['text'] : $jenis_adm;
                }
            }
            $listValue[$key]['jenis_adm'] = $jenis_adm;
            $listValue[$key]['nomer_adm'] = $value['nomer_adm'];
            $listValue[$key]['nomer_agenda'] = $value['nomer_agenda'];
            $listValue[$key]['perihal_adm'] = $value['perihal_adm'];
        }
        
        $result['no'] = $cursor + 1;
        $result['page'] = $page;
        $result['limit'] = $limit;
        // $result['level'] = $userSession['level_user'];
        $result['count'] = $dataCount['value'][0]['counts'];
        $result['table'] = $listValue;
		$result['query'] = $dataValue['query'];
        $result['query'] = '';
        return $result;
    }

    public function getTabelAdmKeluar($input, $userSession){
		$cari = '%'.$input['cari'].'%';
        $page = $input['page'];
        $keys = [$cari];
        $query = '';
        $order = '';
        if($userSession['level_user'] === 'agendaris'){
            $option_tujuan = $this->getNamaSatker();
            $query = 'FROM tb_adm a LEFT JOIN tb_satker b ON (a.tujuan_adm=b.id_satker) LEFT JOIN tb_jenis c ON (a.id_jenis=c.id_jenis) ';
            $query .= 'WHERE ('.$input['pil_cari'].' LIKE ?) AND (pengirim_adm = ?)';
            $order = ' ORDER BY waktu_adm DESC';
            array_push($keys, $userSession['id_satker']);

            if(\in_array($input['status_baca'], ['0', '1'])){
                $query .= ' AND (status_adm = ?)';
                array_push($keys, $input['status_baca']);
            }
        }
        if($userSession['level_user'] === 'jabatan'){
            $option_tujuan = $this->getAdmRiwayatTujuan();
            $query = 'FROM tb_adm_riwayat a LEFT JOIN tb_adm b ON (a.id_adm=b.id_adm) LEFT JOIN tb_jabatan c ON (a.tujuan_adm_riwayat=c.id_jabatan) LEFT JOIN tb_jenis d ON (b.id_jenis=d.id_jenis) ';
            $query .= 'WHERE ('.$input['pil_cari'].' LIKE ?) AND (pengirim_adm_riwayat = ?)';
            $order = ' ORDER BY waktu_adm_riwayat DESC';
            array_push($keys, $userSession['id_jabatan']);

            if(\in_array($input['status_baca'], ['0', '1'])){
                $query .= ' AND (status_adm_riwayat = ?)';
                array_push($keys, $input['status_baca']);
            }
        }

        if($input['tembusan'] == '1'){
            $query .= ' AND (tembusan_adm = ?)';
            array_push($keys, $input['tembusan']);
        }

        $limit = 20;
        $cursor = ($page - 1) * $limit;
        $dataCount = $this->getData('SELECT COUNT(*) AS counts '.$query, $keys);
        $dataValue = $this->getData('SELECT * '.$query.$order.' LIMIT '.$cursor.','.$limit, $keys);
        $listValue = [];
        $option_jenis_adm = $this->getJenisAdm();
        $option_status_baca = $this->getStatusBaca();
        $action_adm_riwayat = $this->getActionAdmRiwayat('list');

        foreach ($dataValue['value'] as $key => $value) {
            if(in_array($userSession['level_user'], ['agendaris'])){
                $id_adm = $value['id_adm'];
                $status_baca_adm = $option_status_baca[$value['status_adm']];
                $tanggal_adm = comp\FUNC::tanggal($value['tanggal_adm'], 'long_date');
                $nama_tujuan = !empty($option_tujuan[$value['tujuan_adm']]) ? $option_tujuan[$value['tujuan_adm']]['text'] : '-';

                $listValue[$key]['id_adm'] = $id_adm;
                $listValue[$key]['status_baca'] = $status_baca_adm['text'];
                $listValue[$key]['status_baca_color'] = $status_baca_adm['color'];
                $listValue[$key]['tanggal_adm'] = $tanggal_adm;
                $listValue[$key]['nama_tujuan'] = $nama_tujuan;
                $listValue[$key]['tujuan_adm'] = $value['tujuan_adm'];
            }

            if($userSession['level_user'] === 'jabatan'){
                $id_adm = $value['id_adm_riwayat'];
                $status_baca_adm = $option_status_baca[$value['status_adm_riwayat']];
                $aksi_adm = $action_adm_riwayat[$value['action_adm_riwayat']];
                $tanggal_adm = comp\FUNC::tanggal($value['tanggal_adm'], 'long_date');
                $waktu_adm = comp\FUNC::tanggal($value['waktu_adm_riwayat'], 'long_date_time');
                $nama_tujuan = !empty($option_tujuan[$value['tujuan_adm_riwayat']]) ? $option_tujuan[$value['tujuan_adm_riwayat']]['text'] : '-';

                $listValue[$key]['id_adm'] = $id_adm;
                $listValue[$key]['status_baca'] = $status_baca_adm['text'];
                $listValue[$key]['status_baca_color'] = $status_baca_adm['color'];
                $listValue[$key]['aksi_adm'] = $aksi_adm;
                $listValue[$key]['tanggal_adm'] = $tanggal_adm;
                $listValue[$key]['waktu_adm'] = $waktu_adm;
                $listValue[$key]['nama_tujuan'] = $nama_tujuan;
                $listValue[$key]['tujuan_adm'] = $value['tujuan_adm_riwayat'];
                $listValue[$key]['keterangan_adm'] = $value['keterangan_adm_riwayat'];
            }

            $jenis_adm = $value['jenislain_adm'];
            if($value['id_jenis'] != 'JNSXX'){
                if($value['id_jenis'] != 'JNS99'){
                    $jenis_adm = !empty($option_jenis_adm[$value['id_jenis']]) ? $option_jenis_adm[$value['id_jenis']]['text'] : $jenis_adm;
                }
            }
            $listValue[$key]['jenis_adm'] = $jenis_adm;
            $listValue[$key]['nomer_adm'] = $value['nomer_adm'];
            $listValue[$key]['nomer_agenda'] = $value['nomer_agenda'];
            $listValue[$key]['perihal_adm'] = $value['perihal_adm'];
        }
        
        $result['no'] = $cursor + 1;
        $result['page'] = $page;
        $result['limit'] = $limit;
        $result['count'] = $dataCount['value'][0]['counts'];
        $result['table'] = $listValue;
		$result['query'] = $dataValue['query'];
		$result['query'] = '';
        return $result;
    }

    public function getTabelAdmMonitor($input, $userSession){
		$cari = '%'.$input['cari'].'%';
        $page = $input['page'];
        $keys = [$cari];
        $query = '';
        $order = '';
        if($userSession['level_user'] === 'agendaris'){
            $query = 'FROM tb_adm_riwayat a LEFT JOIN tb_adm b ON (a.id_adm=b.id_adm) LEFT JOIN tb_jabatan c ON (a.tujuan_adm_riwayat=c.id_jabatan) LEFT JOIN tb_jenis d ON (b.id_jenis=d.id_jenis) ';
            $query .= 'WHERE ('.$input['pil_cari'].' LIKE ?) AND (id_satker = ?) AND (pengirim_adm_riwayat NOT LIKE ?)';
            $order = ' ORDER BY waktu_adm_riwayat DESC';
            array_push($keys, $userSession['id_satker'], $userSession['id_satker'].'%');
        }
        if($userSession['level_user'] === 'assistant'){
            $query = 'FROM tb_adm_riwayat a LEFT JOIN tb_adm b ON (a.id_adm=b.id_adm) LEFT JOIN tb_jabatan c ON (a.tujuan_adm_riwayat=c.id_jabatan) LEFT JOIN tb_jenis d ON (b.id_jenis=d.id_jenis) ';
            $query .= 'WHERE ('.$input['pil_cari'].' LIKE ?) AND (id_bidang = ?) AND (pengirim_adm_riwayat NOT LIKE ?)';
            $order = ' ORDER BY waktu_adm_riwayat DESC';
            array_push($keys, $userSession['id_bidang'], $userSession['id_satker'].'%');
        }

        if($input['tembusan'] == '1'){
            $query .= ' AND (tembusan_adm = ?)';
            array_push($keys, $input['tembusan']);
        }

        if(\in_array($input['status_baca'], ['0', '1'])){
            $query .= ' AND (status_adm_riwayat = ?)';
            array_push($keys, $input['status_baca']);
        }

        $limit = 20;
        $cursor = ($page - 1) * $limit;
        $dataCount = $this->getData('SELECT COUNT(*) AS counts '.$query, $keys);
        $dataValue = $this->getData('SELECT * '.$query.$order.' LIMIT '.$cursor.','.$limit, $keys);
        $listValue = [];
        $option_bidang = $this->getBidang($userSession['id_satker']);
        $option_pengirim = $this->getAdmRiwayatPengirim();
        $option_tujuan = $this->getAdmRiwayatTujuan();
        $option_jenis_adm = $this->getJenisAdm();
        $option_status_baca = $this->getStatusBaca();
        $action_adm_riwayat = $this->getActionAdmRiwayat('list');
        foreach ($dataValue['value'] as $key => $value) {
            if(in_array($userSession['level_user'], ['agendaris', 'assistant'])){
                $id_adm = $value['id_adm_riwayat'];
                $status_baca_adm = $option_status_baca[$value['status_adm_riwayat']];
                $aksi_adm = $action_adm_riwayat[$value['action_adm_riwayat']];
                $tanggal_adm = comp\FUNC::tanggal($value['tanggal_adm'], 'long_date');
                $waktu_adm = comp\FUNC::tanggal($value['waktu_adm_riwayat'], 'long_date_time');
                $nama_pengirim = !empty($option_pengirim[$value['pengirim_adm_riwayat']]) ? $option_pengirim[$value['pengirim_adm_riwayat']]['text'] : '-';
                $nama_tujuan = !empty($option_tujuan[$value['tujuan_adm_riwayat']]) ? $option_tujuan[$value['tujuan_adm_riwayat']]['text'] : '-';

                $listValue[$key]['id_adm'] = $id_adm;
                $listValue[$key]['status_baca'] = $status_baca_adm['text'];
                $listValue[$key]['status_baca_color'] = $status_baca_adm['color'];
                $listValue[$key]['aksi_adm'] = $aksi_adm;
                $listValue[$key]['tanggal_adm'] = $tanggal_adm;
                $listValue[$key]['waktu_adm'] = $waktu_adm;
                $listValue[$key]['nama_pengirim'] = $nama_pengirim;
                $listValue[$key]['nama_tujuan'] = $nama_tujuan;
                $listValue[$key]['pengirim_adm'] = $value['pengirim_adm_riwayat'];
                $listValue[$key]['tujuan_adm'] = $value['tujuan_adm_riwayat'];
                $listValue[$key]['keterangan_adm'] = $value['keterangan_adm_riwayat'];
            }

            $jenis_adm = $value['jenislain_adm'];
            if($value['id_jenis'] != 'JNSXX'){
                if($value['id_jenis'] != 'JNS99'){
                    $jenis_adm = !empty($option_jenis_adm[$value['id_jenis']]) ? $option_jenis_adm[$value['id_jenis']]['text'] : $jenis_adm;
                }
            }
            $listValue[$key]['jenis_adm'] = $jenis_adm;
            $listValue[$key]['nomer_adm'] = $value['nomer_adm'];
            $listValue[$key]['nomer_agenda'] = $value['nomer_agenda'];
            $listValue[$key]['perihal_adm'] = $value['perihal_adm'];
        }
        
        $result['no'] = $cursor + 1;
        $result['page'] = $page;
        $result['limit'] = $limit;
        $result['count'] = $dataCount['value'][0]['counts'];
        $result['table'] = $listValue;
		$result['query'] = $dataValue['query'];
		$result['query'] = '';
        return $result;
    }

    public function getTabelRiwayatAdm($id_adm){
        $action_adm_riwayat = $this->getActionAdmRiwayat('list');
        $option_status_baca = $this->getStatusBaca();
        $option_pengirim = $this->getAdmRiwayatPengirim();
        $option_tujuan = $this->getAdmRiwayatTujuan();
        $data = $this->getData('SELECT * FROM tb_adm_riwayat WHERE (id_adm = ?) ORDER BY waktu_adm_riwayat DESC', array($id_adm));
        $listValue = [];
        foreach ($data['value'] as $key => $value) {
            $aksi_riwayat = $action_adm_riwayat[$value['action_adm_riwayat']];
            $status_baca_adm = $option_status_baca[$value['status_adm_riwayat']];
            $waktu_riwayat = comp\FUNC::tanggal($value['waktu_adm_riwayat'], 'long_date_time');
            $nama_pengirim = !empty($option_pengirim[$value['pengirim_adm_riwayat']]) ? $option_pengirim[$value['pengirim_adm_riwayat']]['text'] : '-';
            $nama_tujuan = !empty($option_tujuan[$value['tujuan_adm_riwayat']]) ? $option_tujuan[$value['tujuan_adm_riwayat']]['text'] : '-';

            $listValue[$key]['status_baca'] = $status_baca_adm['text'];
            $listValue[$key]['status_baca_color'] = $status_baca_adm['color'];
            $listValue[$key]['aksi_riwayat'] = $aksi_riwayat;
            $listValue[$key]['waktu_riwayat'] = $waktu_riwayat;
            $listValue[$key]['nama_pengirim'] = $nama_pengirim;
            $listValue[$key]['nama_tujuan'] = $nama_tujuan;
            $listValue[$key]['isi_pesan'] = $value['keterangan_adm_riwayat'];
        }
        
        $result['no'] = 1;
        $result['page'] = 1;
        $result['limit'] = $data['count'];
        $result['count'] = $data['count'];
        $result['table'] = $data['value'];
        $result['table'] = $listValue;
		$result['query'] = $data['query'];
		$result['query'] = '';
        return $result;
    }
}
?>