<?php
namespace app\adol\model;

use system\Model;
use comp;

class adol extends Model{
	
	public function __construct(){
		parent::__construct();
        parent::setConnection('db_user');
		parent::setDefaultValue(array(
			//tb_adm
            'id_adm' => date('Ymdhis'),
            'tanggal_adm' => date('Y-m-d'),
            'waktu_adm' => date('Y-m-d H:i:s'),
            'waktu_dibaca_adm' => date('Y-m-d H:i:s'),
            'tembusan_adm' => 0,
			'status_adm' => 0,
			'status_bidang_adm' => 0,
            'action_adm' => 0,
            //tb_adm-riwayat
            'id_adm_riwayat' => date('Ymdhis'),
            'waktu_adm_riwayat' => date('Y-m-d H:i:s'),
            'waktu_dibaca_adm_riwayat' => date('Y-m-d H:i:s'),
            'status_adm_riwayat' => 0,
			'action_adm_riwayat' => 0,
            //tb_draft
            'id_draft' => date('Ymdhis'),
            'status_draft' => 0,
            //tb_draft_riwayat
            'id_draft_riwayat' => date('Ymdhis'),
            'waktu_draft_riwayat' => date('Y-m-d H:i:s'),
            'waktu_dibaca_draft_riwayat' => date('Y-m-d H:i:s'),
            'status_draft_riwayat' => 0,
            'action_draft_riwayat' => 0,
            //tb_regional
            'alamat_server' => 'localhost',
            'urutan_regional' => 0,
            //tb_satker
            'kategori_satker' => '',
            'status_satker' => 0,
            //tb_jabatan
            'status_jabatan' => 0,
            //tb_user
			'id_user' => date('Ymdhis'),
            'level_user' => 'agendaris',
            'disposisi_user' => 0,
            'koordinasi_user' => 0,
            'laporan_user' => 0,
            'draft_user' => 0,
            //tb_user_disposisi
            'id_user_disposisi' => date('Ymdhis'),
            //tb_user_koordinasi
            'id_user_koordinasi' => date('Ymdhis'),
            //tb_user_laporan
            'id_user_laporan' => date('Ymdhis'),
            //tb_user_draft
			'id_user_draft' => date('Ymdhis'),
			//tb_chat_room
			'room' => $this->randomKey(),
			//tb_chat_message
			'id_message' => date('Ymdhis'),
			'datetime' => date('Y-m-d H:i:s'),
		));

		$this->no_image_satker = $this->logo_adol_icon;
		$this->path_image_satker = 'upload/image/logo_satker/';
		$this->path_lampiran_dokumen = 'upload/dokumen/';
		$this->path_lampiran_disposisi = 'upload/disposisi/';
		$this->path_lampiran_draft = 'upload/draft/';
		
		$this->mimes_pdf = $this->files->getMimeTypes($this->file_type_pdf);
		$this->mimes_image = $this->files->getMimeTypes($this->file_type_image);
		$this->mimes_image_pdf = $this->files->getMimeTypes($this->file_type_image_pdf);

		$this->upload_description_pdf = '*) File Type : '.$this->file_type_pdf.', Max Size : '.($this->max_size / 1024 /1024).'Mb';
		$this->upload_description_image = '*) File Type : '.$this->file_type_image.', Max Size : '.($this->max_size / 1024 /1024).'Mb';
		$this->upload_description_image_pdf = '*) File Type : '.$this->file_type_image_pdf.', Max Size : '.($this->max_size / 1024 /1024).'Mb';
	}
	
	public function randomKey(){
		return substr(str_shuffle(str_repeat("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", 15)), 0, 15);
    }

	/**
	 * Data Option
	 */
	
	public function getStatus(){ 
		return [
			'0' => ['text' => 'Non Aktif'],
			'1' => ['text' => 'Aktif'],
		];
    }
	
	public function getUserLevel(){ 
		return [
			'agendaris' => ['text' => 'Agendaris'],
			'assistant' => ['text' => 'Assistant'],
			'jabatan' => ['text' => 'Jabatan'],
		];
    }
	
	public function getUserAction(){ 
		return [
			'0' => ['text' => 'Tidak'],
			'1' => ['text' => 'Ya'],
		];
	}

	public function getActionDraft(){ 
		return [
			[ 'id' => 'buat', 'text' => 'Buat Draft'], // 0
			[ 'id' => 'lapor', 'text' => 'Melaporkan Draft'], // 1
			[ 'id' => 'tolak', 'text' => 'Menolak Draft'], // 2
			[ 'id' => 'revisi', 'text' => 'Merevisi Draft'], // 3
			[ 'id' => 'setuju', 'text' => 'Menyetujui Draft'], // 4
			[ 'id' => 'selesai', 'text' => 'Finishing Draft'], // 5
		];
	}

	public function getUserActionDraft(){ 
		$result = [];
		foreach ($this->getActionDraft() as $key => $value) { $result[$value['id']] = $value; }
		return $result;
	}

	// 1: Buat Draft(32), 2: Melaporkan(16), 3: Menolak(8), 4: Merevisi(4), 5: Menyetujui(2), 6: Selesai(1)
	public function getBinerUserDraft($draft_user){
		$draft_user = decbin($draft_user);
		$draft_user = substr('000000',0,6 - strlen($draft_user)).$draft_user;
		return $draft_user;
	}

	public function setUserActionDraftValue($draft_user){
		$draft_action = []; // ['buat' => 0, 'lapor' => 0, 'tolak' => 0, 'revisi' => 0, 'setuju' => 0, 'selesai' => 0];
		foreach ($this->getActionDraft() as $key => $value) { $draft_action[$value['id']] = 0; }
		if(!empty($draft_user)){foreach($draft_user as $key => $value){$draft_action[$value] = 1;}}
		$draft_user = implode('', $draft_action);
		return bindec($draft_user);
	}

	public function getUserActionDraftValue($draft_user){
		$arrDraft = [];
		$draft_action = []; // ['buat', 'lapor', 'tolak', 'revisi', 'setuju', 'selesai'];
		foreach ($this->getActionDraft() as $key => $value) { $draft_action[$key] = $value['id']; }
		$draft_user = $this->getBinerUserDraft($draft_user);
		for($i = 0; $i < count($draft_action); $i++){
			if($draft_user[$i] != '0') array_push($arrDraft, $draft_action[$i]);
		}
		return $arrDraft;
	}

	public function getUserActionDraftTabel($draft_user){ 
		$arrDraft = []; // ['Buat Draft', 'Melaporkan Draft', 'Menolak Draft', 'Merevisi Draft', 'Menyetujui Draft', 'Selesai Draft'];
		$draft_action = [];
		foreach ($this->getActionDraft() as $key => $value) { $draft_action[$key] = $value['text']; }
		$draft_user = $this->getBinerUserDraft($draft_user);
		for($i = 0; $i < count($draft_action); $i++){
			if($draft_user[$i] != '0') array_push($arrDraft, $draft_action[$i]);
		}
		return implode(', ', $arrDraft);
	}
	
	public function getStatusBaca(){
		return [
			'-1' => ['text' => 'Semua Pesan', 'color' => ''],
			'0' => ['text' => 'Belum Dibaca', 'color' => 'danger'],
			'1' => ['text' => 'Sudah Dibaca', 'color' => 'info'],
		];
    }

	public function getJenisAdm(){
        $result = [];
        $data = $this->getData('SELECT * FROM tb_jenis ORDER BY id_jenis', []);
		foreach ($data['value'] as $key => $value) { $result[$value['id_jenis']] = ['text' => $value['nama_jenis']]; }
		$result['JNSXX'] = ['text' => 'LAINNYA'];
        return $result;
	}

	public function getKlasifikasiAdm(){
		$result = [];
        $data = $this->getData('SELECT * FROM tb_klasifikasi ORDER BY id_klasifikasi', []);
        foreach ($data['value'] as $key => $value) { $result[$value['id_klasifikasi']] = ['text' => $value['nama_klasifikasi']]; }
        return $result;
    }
	
	public function getDerajatAdm(){
		$result = [];
        $data = $this->getData('SELECT * FROM tb_derajat ORDER BY id_derajat', []);
        foreach ($data['value'] as $key => $value) { $result[$value['id_derajat']] = ['text' => $value['nama_derajat']]; }
        return $result;
    }
	
	public function getActionAdmRiwayat($opt = ''){
		if($opt === 'list')
			return ['0' => 'Laporan', '1' => 'Koordinasi', '2' => 'Disposisi'];
		else if($opt === 'form')
			return ['laporan' => '0', 'koordinasi' => '1', 'disposisi' => '2'];
    }
	
	// public function getActionDraftRiwayat($opt = ''){
	// 	if($opt === 'list')
	// 		return array('0' => 'Buat', '1' => 'Lapor', '2' => 'Tolak', '3' => 'Revisi', '4' => 'Setuju', '5' => 'Selesai');
	// 	else if($opt === 'form')
	// 		return array('buat' => '0', 'lapor' => '1', 'tolak' => '2', 'revisi' => '3', 'setuju' => '4', 'selesai' => '5');
	// }
	
	public function getActionDraftRiwayat(){
		$draft_action = []; // ['buat' => 0, 'lapor' => 1, 'tolak' => 2, 'revisi' => 3, 'setuju' => 4, 'selesai' => 5];
		foreach ($this->getActionDraft() as $key => $value) { $draft_action[$value['id']] = $key; }
		return $draft_action;
    }

    public function getStatusDraft(){
		return [
			'0' => ['text' => 'Drafting'],
			'1' => ['text' => 'ACC'],
			'2' => ['text' => 'Final'],
		];
	}

	public function getJenisTemplateDraft(){
		return [
			'JNS06', // SURAT PERINTAH/SURAT TUGAS
			// 'JNS08', // SURAT BIASA
			'JNS09', // NOTA DINAS
			// 'JNS10', // SURAT TELEGRAM
		];
	}
	
	public function pilihanTabelAdmMasuk($userSession){
		$result = [
			'agendaris' => [
				'perihal_adm' => ['text' => 'Perihal Surat :'],
				'nomer_adm' => ['text' => 'No. Surat :'],
				'nomer_agenda' => ['text' => 'No. Agenda :'],
				'nama_jenis' => ['text' => 'Jenis Surat :'],
				'jenislain_adm' => ['text' => 'Jenis Surat Lainnya :'],
				'singkatan_satker' => ['text' => 'Satker Pengirim :'],
				'pengirimlain_adm' => ['text' => 'Nama Pengirim Lainnya :'],
				'tanggal_adm' => ['text' => 'Tanggal Surat : ']
			],
			'assistant' => [
				'perihal_adm' => ['text' => 'Perihal Surat :'],
				'nomer_adm' => ['text' => 'No. Surat :'],
				'nomer_agenda' => ['text' => 'No. Agenda :'],
				'nama_jenis' => ['text' => 'Jenis Surat :'],
				'jenislain_adm' => ['text' => 'Jenis Surat Lainnya :'],
				'singkatan_satker' => ['text' => 'Satker Pengirim :'],
				'pengirimlain_adm' => ['text' => 'Nama Pengirim Lainnya :'],
				'tanggal_adm' => ['text' => 'Tanggal Surat : ']
			],
			'jabatan' => [
				'perihal_adm' => ['text' => 'Perihal Surat :'],
				'nomer_adm' => ['text' => 'No. Surat :'],
				'nomer_agenda' => ['text' => 'No. Agenda :'],
				'nama_jabatan' => ['text' => 'Nama Pengirim :'],
				'waktu_adm_riwayat' => ['text' => 'Tanggal Surat : ']
			]
		];

		return $result[$userSession['level_user']];
	}

	public function pilihanTabelAdmKeluar($userSession){
		$result = [
			'agendaris' => [
				'perihal_adm' => ['text' => 'Perihal Surat :'],
				'nomer_adm' => ['text' => 'No. Surat :'],
				'nomer_agenda' => ['text' => 'No. Agenda :'],
				'nama_jenis' => ['text' => 'Jenis Surat :'],
				'jenislain_adm' => ['text' => 'Jenis Surat Lainnya :'],
				'singkatan_satker' => ['text' => 'Satker Tujuan :'],
				'tanggal_adm' => ['text' => 'Tanggal Surat : ']
			],
			'jabatan' => [
				'perihal_adm' => ['text' => 'Perihal Surat :'],
				'nomer_adm' => ['text' => 'No. Surat :'],
				'nomer_agenda' => ['text' => 'No. Agenda :'],
				'nama_jabatan' => ['text' => 'Nama Tujuan :'],
				'waktu_adm_riwayat' => ['text' => 'Tanggal Surat : ']
			]
		];

		return $result[$userSession['level_user']];
	}

	public function pilihanTabelAdmMonitor(){
		$result = [
			'perihal_adm' => ['text' => 'Perihal Surat :'],
			'nomer_adm' => ['text' => 'No. Surat :'],
			'nomer_agenda' => ['text' => 'No. Agenda :'],
			'nama_jabatan' => ['text' => 'Nama Tujuan :'],
			'waktu_adm_riwayat' => ['text' => 'Tanggal Surat : ']
		];

		return $result;
	}

	public function pilihanTabelDraftMasuk(){
		$result = [
			'perihal_draft' => ['text' => 'Perihal Draft :'],
			'nama_jabatan' => ['text' => 'Nama Pengirim :'],
			'waktu_draft_riwayat' => ['text' => 'Tanggal Draft : ']
		];

		return $result;
	}

	public function pilihanTabelDraftKeluar(){
		$result = [
			'perihal_draft' => ['text' => 'Perihal Draft :'],
			'nama_jabatan' => ['text' => 'Nama Tujuan :'],
			'waktu_draft_riwayat' => ['text' => 'Tanggal Draft : ']
		];

		return $result;
	}

	public function getFormTitleUserAction(){
		return [
			'laporan' => ['text' => 'Kirim Laporan'],
			'disposisi' => ['text' => 'Kirim Disposisi'],
			'koordinasi' => ['text' => 'Kirim Koordinasi'],
			'teruskan' => ['text' => 'Teruskan ke Agendaris Satker'],
			'distribusi' => ['text' => 'Distribusikan ke Agendaris Bagian'],
		];
	}
	
	public function getFormTitleDraft(){
		return [
			'buat' => ['text' => 'Buat Draft'],
			'lapor' => ['text' => 'Laporkan Draft'],
			'tolak' => ['text' => 'Tolak Draft'],
			'revisi' => ['text' => 'Revisi Draft'],
			'setuju' => ['text' => 'ACC Draft'],
			'selesai' => ['text' => 'Draft Selesai'],
		];
	}
	
	public function getKategori(){ 
		return [ 
			// 'mabes' => ['text' => 'MABES'], 
			'polda' => ['text' => 'POLDA'], 
			'polres' => ['text' => 'POLRES'], 
			'polsek' => ['text' => 'POLSEK'], 
		];
	}

	public function getRegional(){ 
		$result = [];
		$data = $this->getData('SELECT * FROM tb_regional ORDER BY urutan_regional', []);
		foreach ($data['value'] as $key => $value) {
			$result[$value['id_regional']] = [
				'text' => $value['nama_regional'],
				'alias' => $value['singkatan_regional']
			];
		}
		return $result;
	}

	public function getSatker($regional = '', $kategori = '', $status = '1'){ 
		$result = [];
		$keys = ['%'.$regional.'%', '%'.$kategori.'%', $status];
		$data = $this->getData('SELECT * FROM tb_satker WHERE (id_regional LIKE ?) AND (kategori_satker LIKE ?) AND (status_satker = ?) ORDER BY urutan_satker', $keys);
		foreach ($data['value'] as $key => $value) { $result[$value['id_satker']] = $value; }
		return $result;
	}

	// Untuk pilihan nama satker pada data pengirim dan penerima adm/draft
	public function getNamaSatker(){ 
		$result = [];
		$satker = $this->getSatker();
		foreach ($satker as $key => $value) { $result[$key] = ['text' => $value['singkatan_satker']]; }
		return $result;
	}

	public function getWilayah(){ 
		$result = [];
		$data = $this->getData('SELECT * FROM tb_wilayah ORDER BY urutan_wilayah', []);
		foreach ($data['value'] as $key => $value) { 
			$result[$value['id_wilayah']] = $value; 
			$result[$value['id_wilayah']]['text'] = $value['nama_wilayah']; 
		}
		return $result;
	}

	public function getGroupSatker($regional = '', $kategori = ''){ 
		$result = [];
		// Info Regional
		$getRegional = $this->getRegional();
		// Info Group Polres
		$wilayah = $this->getWilayah();
		// Info Group Polsek
		$polres = $this->getSatker($regional, 'polres');
		// Data Satker
		$satker = $this->getSatker($regional, $kategori);
		foreach ($satker as $key => $value) {
			$result[$key]['text'] = $value['singkatan_satker'];
			// $result[$key]['value'] = $value;
			$info_regional = isset($getRegional[$value['id_regional']]) ? 'POLDA '.$getRegional[$value['id_regional']]['alias'] : '';
			if($value['kategori_satker'] == 'polda'){
				// $result[$key]['group'] = $info_regional;
			}
			if($value['kategori_satker'] == 'polsek'){
				$result[$key]['group'] = isset($polres[$value['id_subsatker']]) ? $info_regional.' - '.$polres[$value['id_subsatker']]['singkatan_satker'] : '';
			}
			if($value['kategori_satker'] == 'polres'){
				$result[$key]['group'] = isset($wilayah[$value['id_wilayah']]) ? $info_regional.' - '.$wilayah[$value['id_wilayah']]['nama_wilayah'] : '';
			}
		}
		
		return $result;
	}

	public function getBidang($satker = ''){ 
		$result = [];
		$data = $this->getData('SELECT * FROM tb_bidang WHERE (id_satker = ?) ORDER BY id_bidang', [$satker]);
		foreach ($data['value'] as $key => $value) { $result[$value['id_bidang']] = $value; }
		return $result;
	}

	// Untuk pilihan nama bidang pada input jabatan
	public function getNamaBidang($satker){ 
		$result = [];
		$bidang = $this->getBidang($satker);
		foreach ($bidang as $key => $value) { $result[$key] = ['text' => $value['nama_bidang']]; }
		return $result;
	}

	public function getSatkerJabatan($satker, $status = '1'){ 
		$result = [];
		// Info Group Bidang
		$bidang = $this->getBidang($satker);
		$data = $this->getData('SELECT *, IF(id_bidang = "", "ZZZZ", id_bidang) AS order_bidang FROM tb_jabatan WHERE (id_satker = ?) AND (status_jabatan = 1) ORDER BY id_bidang, urutan_jabatan ASC', [$satker]);
		$data = $this->getData('SELECT *, IF(id_bidang = "", "ZZZZ", id_bidang) AS order_bidang FROM tb_jabatan WHERE (id_satker = ?) ORDER BY order_bidang, urutan_jabatan ASC', [$satker]);
		// $result['query'] = $data['query'];
		foreach ($data['value'] as $key => $value) {
			$result[$value['id_jabatan']]['text'] = $value['nama_jabatan'];
			$result[$value['id_jabatan']]['group'] = isset($bidang[$value['id_bidang']]) ? $bidang[$value['id_bidang']]['nama_bidang'] : '';
			// $result[$value['id_jabatan']]['urutan'] = $value['urutan_jabatan'];
		}
		
		return $result;
	}

	/**
	 * Data Chat
	 */
	
	public function createUserContact($data) {
		return [
			'text' => $data['nama_jabatan'],
			'info' => $data['singkatan_satker'],
			'image' => 'https://ui-avatars.com/api/?name='.str_replace(' ', '+', $data['nama_jabatan']),
			'room' => !empty($data['room']) ? $data['room'] : $this->randomKey(),
		];
	}

	public function createUserChat($id, $userContact) {
		return [
			'id' => $id,
			'name' => $userContact[$id]['text'],
			'info' => $userContact[$id]['info'],
			'image' => $userContact[$id]['image']
		];
	}

	public function updateReadMessage($user, $room) {
		return $this->getData('UPDATE tb_chat_message SET read_message = IF((read_message = ""), ?, CONCAT_WS(",", read_message, ?)) WHERE (NOT FIND_IN_SET(?, read_message)) AND (room = ?)', [$user, $user, $user, $room]);
	}

	public function getActiveUserChat($userSession, $userInfo) {
		$userContact[$userSession['id_jabatan']] = $this->createUserContact($userSession+$userInfo);
		return $this->createUserChat($userSession['id_jabatan'], $userContact);
	}

	public function getChatUserContact($userSession){ 
		$result = [];
		$data = $this->getData('SELECT *, 
								IF(id_bidang = "", "ZZZZ", id_bidang) AS order_bidang,
								IFNULL((SELECT room FROM tb_chat_room WHERE FIND_IN_SET(?, users) AND FIND_IN_SET(jab.id_jabatan, users)), 0) AS room 
								FROM tb_jabatan jab
								JOIN tb_satker sat ON (jab.id_satker=sat.id_satker)
								WHERE (jab.id_jabatan <> ?) AND (jab.status_jabatan = 1) AND (jab.id_satker = ? )
								ORDER BY id_bidang, urutan_jabatan ASC', [$userSession['id_jabatan'], $userSession['id_jabatan'], $userSession['id_satker']]);

		foreach ($data['value'] as $key => $value) {
			$result[$value['id_jabatan']] = $this->createUserContact($value);
		}
		// return $data['query'];
		return $result;
	}

	public function getChatUserConversation($userSession){ 
		$result = [];
		$userContact = $this->getChatUserContact($userSession);
		$data = $this->getData('SELECT
								jabatan.*,
								chat_room.*,
								chat_message.*,
								(SELECT COUNT(read_message) FROM tb_chat_message WHERE NOT FIND_IN_SET(?, read_message) AND (room=chat_room.room)) AS unread
								FROM tb_jabatan jabatan, tb_chat_room chat_room, tb_chat_message chat_message 
								WHERE FIND_IN_SET(jabatan.id_jabatan, chat_room.users) 
								AND FIND_IN_SET(?, chat_room.users) 
								AND (jabatan.id_jabatan <> ?)
								AND (chat_message.datetime = (SELECT MAX(datetime) FROM tb_chat_message WHERE (room=chat_room.room)))', [$userSession['id_jabatan'], $userSession['id_jabatan'], $userSession['id_jabatan']]);

		foreach ($data['value'] as $key => $value) {
			$user = $this->createUserChat($value['id_jabatan'], $userContact);
			array_push($result, [
				'user' => $user,
				'room' => $value['room'],
				'text' => $value['message'],
				'time' => date('d M Y', strtotime($value['datetime'])),
				'unread' => $value['unread'],
			]);
		}
		// return $data['query'];
		return $result;
	}

	public function getChatUserMessage($userSession, $userInfo, $room){ 
		$result = [];
		$userContact = $this->getChatUserContact($userSession);
		$userContact[$userSession['id_jabatan']] = $this->createUserContact($userSession+$userInfo); // User Active
		$data = $this->getData('SELECT * FROM tb_chat_message chat_message 
								WHERE (chat_message.room = ?) ORDER BY datetime ASC', [$room]);

		foreach ($data['value'] as $key => $value) {
			$user = $this->createUserChat($value['user'], $userContact);
			$read_message = explode(',', $value['read_message']);
			array_push($result, [
				'user' => $user,
				'room' => $value['room'],
				'text' => $value['message'],
				'time' => date('d M Y, H:i', strtotime($value['datetime'])),
				'unread' => \in_array($userSession['id_jabatan'], $read_message) ? 0 : 1
			]);
		}
		// return $data['query'];
		return $result;
	}

	public function getDeviceIdByJabatan($jabatan = []){ 
		$result = [];
		$query = 'SELECT device_id, jab.id_jabatan FROM `tb_user_login` log
					JOIN `tb_user` usr ON (log.`user_id`=usr.`id_user`)
					JOIN `tb_jabatan` jab ON (usr.`id_jabatan`=jab.`id_jabatan`)
					WHERE (log.`device_id` <> "") AND FIND_IN_SET(jab.`id_jabatan`, ?)';
		$data = $this->getData($query, [implode(',', $jabatan)]);
		foreach ($data['value'] as $key => $value) { 
			array_push($result, $value['device_id']);
		}

		return $result;
		return $data['query'];
	}

	/**
	 * Data Form
	 */
	
	public function createOrderNumber($table){
		$data = $this->getData('SELECT COUNT(*) AS counts FROM '.$table);
		return ($data['count'] > 0) ? intval($data['value'][0]['counts']) + 1 : 0;
	}

	public function createIdWilayah($id_regional){
		$data = $this->getData('SELECT id_wilayah FROM tb_wilayah WHERE (id_regional = ?) ORDER BY id_wilayah DESC LIMIT 1', array($id_regional));
		if(empty($id_regional)){$kode = '';}
		else if(empty($data['value'])){$kode = $id_regional.'W001';}
		else{
			$kode = $data['value'][0]['id_wilayah'];
			$kode = intval(substr($kode,4,3)) + 1;
			$kode = substr('000',0,3- strlen($kode)).$kode;
			$kode = $id_regional.'W'.$kode;
		}
		
		return [
			'id_wilayah' => $kode,
			'id_regional' => $id_regional
		];
	}

	public function createIdSatker($id_regional){
		$data = $this->getData('SELECT id_satker FROM tb_satker WHERE (id_regional = ?) ORDER BY id_satker DESC LIMIT 1', array($id_regional));
		if(empty($id_regional)){$kode = '';}
		else if(empty($data['value'])){$kode = $id_regional.'001';}
		else{
			$kode = $data['value'][0]['id_satker'];
			$kode = intval(substr($kode,3,3)) + 1;
			$kode = substr('000',0,3- strlen($kode)).$kode;
			$kode = $id_regional.$kode;
		}
		
		return [
			'id_satker' => $kode,
			'id_regional' => $id_regional
		];
	}
	
	public function createIdBidang($id_satker){
		$data = $this->getData('SELECT id_bidang FROM tb_bidang WHERE (id_satker = ?) ORDER BY id_bidang DESC LIMIT 1', array($id_satker));
		if(empty($id_satker)){$kode = '';}
		else if(empty($data['value'])){$kode = $id_satker.'A';}
		else{
			$kode = $data['value'][0]['id_bidang'];
			$kode = substr($kode,6,1);
			$kode = ord($kode)+1;
			$kode = chr(intval($kode));
			$kode = $id_satker.$kode;
		}

		return [
			'id_bidang' => $kode,
			'id_satker' => $id_satker
		];
	}

	public function createIdJabatan($id_satker){
		$data = $this->getData('SELECT id_jabatan FROM tb_jabatan WHERE (id_satker = ?) ORDER BY id_jabatan DESC LIMIT 1', array($id_satker));
		if(empty($id_satker)){$kode = '';}
		else if(empty($data['value'])){$kode = $id_satker.'000';} // 000: Agendaris
		else{
			$kode = $data['value'][0]['id_jabatan'];
			$kode = intval(substr($kode,6,3)) + 1;
			$kode = substr('000',0,3- strlen($kode)).$kode;
			$kode = $id_satker.$kode;
		}
		
		return [
			'id_jabatan' => $kode,
			'id_satker' => $id_satker
		];
	}

	public function getFormRegional($id = ''){
		$form = $this->getDataTabel('tb_regional', ['id_regional', $id]);
		$form['urutan_regional'] =  empty($form['urutan_regional']) ? $this->createOrderNumber('tb_regional') : $form['urutan_regional'];
		$result['form'] = $form;
		$result['form_title'] = empty($id) ? 'Tambah Data Regional' : 'Edit Data Regional';
        return $result;
	}

	public function getFormWilayah($id = '', $regional = ''){
		$createIdWilayah = $this->createIdWilayah($regional);
		$form = $this->getDataTabel('tb_wilayah', ['id_wilayah', $id]);
		$form['id_wilayah'] = empty($form['id_wilayah']) ? $createIdWilayah['id_wilayah'] : $form['id_wilayah'];
		$form['id_regional'] =  empty($form['id_regional']) ? $createIdWilayah['id_regional'] : $form['id_regional'];
		$form['urutan_wilayah'] =  empty($form['urutan_wilayah']) ? $this->createOrderNumber('tb_wilayah') : $form['urutan_wilayah'];
		$result['form'] = $form;
		$result['form_title'] = empty($id) ? 'Tambah Data Wilayah' : 'Edit Data Wilayah';
		$result['pilihan_regional'] = $this->getRegional();
        return $result;
	}

	public function getFormSatker($id = '', $regional = ''){
		$createIdSatker = $this->createIdSatker($regional);
		$form = $this->getDataTabel('tb_satker', ['id_satker', $id]);
		$form['id_satker'] = empty($form['id_satker']) ? $createIdSatker['id_satker'] : $form['id_satker'];
		$form['id_regional'] =  empty($form['id_regional']) ? $createIdSatker['id_regional'] : $form['id_regional'];
		$form['urutan_satker'] =  empty($form['urutan_satker']) ? $this->createOrderNumber('tb_satker') : $form['urutan_satker'];
		$result['form'] = $form;
		$result['form_title'] = empty($id) ? 'Tambah Data Satker' : 'Edit Data Satker';
		$result['url_logo_satker'] = $this->getUrl->baseUrl.((!empty($form['logo_satker']) && file_exists($this->path_image_satker.$form['logo_satker'])) ? $this->path_image_satker.$form['logo_satker'] : $this->no_image_satker);
		$result['pilihan_regional'] = ['' => ['text' => '-- Pilih Regional --']] + $this->getRegional();
		$result['pilihan_kategori'] = ['' => ['text' => '-']] + $this->getKategori();
		$result['pilihan_wilayah'] = ['' => ['text' => '-']] + $this->getWilayah();
		$result['pilihan_satker'] = ['' => ['text' => '-']] + $this->getNamaSatker();
		$result['pilihan_status'] = $this->getStatus();
		$result['upload_mimes'] = $this->mimes_image;
		$result['upload_description'] = $this->upload_description_image;
        return $result;
	}

	public function getFormBidang($id = '', $satker = ''){
		$createIdBidang = $this->createIdBidang($satker);
		$form = $this->getDataTabel('tb_bidang', ['id_bidang', $id]);
		$form['id_bidang'] = empty($form['id_bidang']) ? $createIdBidang['id_bidang'] : $form['id_bidang'];
		$form['id_satker'] =  empty($form['id_satker']) ? $createIdBidang['id_satker'] : $form['id_satker'];
		$result['form'] = $form;
		$result['form_title'] = empty($id) ? 'Tambah Data Bidang' : 'Edit Data Bidang';
		$result['pilihan_satker'] = ['' => ['text' => '-- Pilih Satker --']] + $this->getNamaSatker();
        return $result;
	}
	
	public function getFormJabatan($id = '', $satker = ''){
		$createIdJabatan = $this->createIdJabatan($satker);
		$form = $this->getDataTabel('tb_jabatan', ['id_jabatan', $id]);
		$form['id_jabatan'] = empty($form['id_jabatan']) ? $createIdJabatan['id_jabatan'] : $form['id_jabatan'];
		$form['id_satker'] =  empty($form['id_satker']) ? $createIdJabatan['id_satker'] : $form['id_satker'];
		$form['urutan_jabatan'] =  empty($form['urutan_jabatan']) ? $this->createOrderNumber('tb_jabatan') : $form['urutan_jabatan'];
		$result['form'] = $form;
		$result['form_title'] = empty($id) ? 'Tambah Data Jabatan' : 'Edit Data Jabatan';
		$result['pilihan_satker'] = ['' => ['text' => '-- Pilih Satker --']] + $this->getNamaSatker();
		$result['pilihan_bidang'] = ['' => ['text' => '-']] + $this->getNamaBidang($satker);
		$result['pilihan_status'] = $this->getStatus();
        return $result;
	}

	public function getFormPersonil($id = ''){
		$form = $this->getDataTabel('tb_personil', ['id_personil', $id]);
		$result['form'] = $form;
		$result['form_title'] = empty($id) ? 'Tambah Data Personil' : 'Edit Data Personil';
        return $result;
	}

	public function getFormUser($id = '', $jabatan = ''){
		$infoJabatan = $this->infoJabatan($jabatan);
		if(empty([$infoJabatan])){
			return $infoJabatan;
		}

		$form = $this->getDataTabel('tb_user', ['id_user', $id]);
		$dataPersonil = $this->getDataTabel('tb_personil', ['id_personil', $form['id_personil']]);
		$form['draft_user'] = $this->getUserActionDraftValue($form['draft_user']);
		$form['password_user'] = !empty($form['password_user']) ? comp\FUNC::decryptor($form['password_user']) : 'adol'; // default password adol
		$result['form'] = $form;
		$result['form_title'] = empty($id) ? 'Tambah Data User' : 'Edit Data User';
		$result += $dataPersonil;
		// $result['infoJabatan'] = $infoJabatan;
		$result['id_jabatan'] = $infoJabatan['id_jabatan'];
		$result['nama_jabatan'] = $infoJabatan['nama_jabatan'];
		$result['nama_bidang'] = $infoJabatan['nama_bidang'];
		$result['nama_satker'] = $infoJabatan['nama_satker'].' ('.$infoJabatan['singkatan_satker'].')';
		// $result['nama_satker'] = $infoJabatan['singkatan_satker'];

		$result['pilihan_level'] = $this->getUserLevel();
		$result['pilihan_action'] = $this->getUserAction();
		$result['pilihan_action_draft'] = $this->getUserActionDraft();
        return $result;
	}

	public function infoJabatan($id_jabatan){
		$result = [];
		$data = $this->getData('SELECT * FROM tb_jabatan jab 
					LEFT JOIN tb_bidang bid ON (jab.id_bidang=bid.id_bidang)
					LEFT JOIN tb_satker sat ON (jab.id_satker=sat.id_satker) 
					WHERE (jab.id_jabatan = ?)', [$id_jabatan]);
		return ($data['count'] > 0) ? $data['value'][0] : [];
	}
	
	/**
	 * User
	 */
	
	private function userResult($data){
		$response = ['nrp_personil', 'nama_personil', 'pangkat_personil', 'nama_jabatan', 'nama_satker', 'singkatan_satker', 'logo_satker'];
		foreach ($response as $key => $value) { $result[$value] = 'xxx'; } // default value
		if($data['count'] > 0){
			$dataUser = $data['value'][0];
			foreach ($result as $key => $value) { $result[$key] = isset($dataUser[$key]) ? $dataUser[$key] : ''; }
			$logo_satker = (!empty($dataUser['logo_satker']) && file_exists($this->path_image_satker.$dataUser['logo_satker'])) ? $this->path_image_satker.$dataUser['logo_satker'] : $this->no_image_satker;
			$result['nrp_personil'] = !is_null($dataUser['id_personil']) ? $dataUser['id_personil'] : '';
			$result['nama_personil'] = !empty($dataUser['pangkat_personil']) ? $dataUser['pangkat_personil'].'. '.$dataUser['nama_personil'] : $dataUser['nama_personil'];
			$result['logo_satker'] = $this->getUrl->baseUrl.$logo_satker;
		}
		
		// $result['query'] = $data['query'];
		return $result;
	}
	
	public function userInfo($id_user){
		$result = [];
		$data = $this->getData('SELECT * FROM tb_user a 
								LEFT JOIN tb_personil b ON (a.id_personil=b.id_personil)
								LEFT JOIN tb_jabatan c ON (a.id_jabatan=c.id_jabatan)
								LEFT JOIN tb_satker d ON (c.id_satker=d.id_satker)
								WHERE (a.id_user = ?) LIMIT 1', array($id_user));
		return $this->userResult($data);
	}	
	
	public function userInfoJabatan($id_jabatan){
		$result = [];
		$data = $this->getData('SELECT * FROM tb_user a 
								LEFT JOIN tb_personil b ON (a.id_personil=b.id_personil)
								LEFT JOIN tb_jabatan c ON (a.id_jabatan=c.id_jabatan)
								LEFT JOIN tb_satker d ON (c.id_satker=d.id_satker)
								WHERE (a.id_jabatan = ?) LIMIT 1', array($id_jabatan));
		return $this->userResult($data);
	}

	public function userCheck($input){
		$data = $this->getData('SELECT * FROM tb_user WHERE ((id_personil = ?) OR (id_jabatan = ?)) LIMIT 1', [$input['username'], $input['username']]);
		return $data['count'];
	}

	public function userLogin($input){
		$result = [];
		$data = $this->getData('SELECT * FROM tb_user WHERE ((id_personil = ?) OR (id_jabatan = ?)) AND (password_user = ?) LIMIT 1', [$input['username'], $input['username'], $input['password']]);
		if($data['count'] > 0){
			$dataUser = $data['value'][0];
			$dataSession['session_id'] = $input['session_id'];
			$dataSession['device_id'] = $input['device_id'];
			$dataSession['user_id'] = $dataUser['id_user'];
			$dataSession['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
			$dataSession['phone_type'] = $input['phone_type'];
			$result = $this->save_update('tb_user_login', $dataSession);
			return ($result['success']) ? ['session_name' => $this->getUrl->mainConfig['project']['adol']['session'], 'session_value' => $dataSession] : [];
		}
		return $result;
	}

	public function adminLogin($input){
        $data = $this->getData('SELECT * FROM tb_user_admin WHERE (username = ?) AND (password = ?)', array($input['username'], $input['password']));
		$result = $data['count'];
		return $result;
	}

	public function userSession($input){
		$result = [];
		$data = $this->getData('SELECT * FROM tb_user_login a, tb_user b, tb_jabatan c, tb_satker d WHERE (a.user_id=b.id_user) AND (b.id_jabatan=c.id_jabatan) AND (c.id_satker=d.id_satker) AND (a.session_id = ?)', array($input['session_id']));
		if($data['count'] > 0){
			$response = ['id_user', 'id_personil', 'id_jabatan', 'id_bidang', 'id_satker', 'id_subsatker', 'id_wilayah', 'id_regional', 'kategori_satker', 'level_user', 'disposisi_user', 'koordinasi_user', 'laporan_user', 'draft_user'];
			$dataUser = $data['value'][0];
			array_walk($response, function($v, $k) use (&$dataUser, &$result) { if(isset($dataUser[$v])) $result[$v] = $dataUser[$v]; });
			$draft_user = $this->getBinerUserDraft($dataUser['draft_user']);
			$draft_user_action = [];
			$index = 0;
			foreach ($this->getUserActionDraft() as $key => $value) {
				$draft_user_action[$key] = $draft_user[$index];
				$index++;
			}
			
			$result['draft_user_action'] = $draft_user_action;

		}
		return $result;
	}

	public function userLogout($input){
		$result = $this->delete('tb_user_login', ['session_id' => $input['session_id']]);
		return $result;
	}

	/**
	 * Data Table
	 */
	
	public function getTabelRegional($data){
		$page = $data['page'];
		$query = 'FROM tb_regional';
		$q_count = 'SELECT COUNT(*) AS counts '.$query;
		$q_value = 'SELECT * '.$query.' ORDER BY urutan_regional ASC';
		$keys = [];
		$limit = 10;
        $cursor = ($page - 1) * $limit;
		$dataCount = $this->getData($q_count, $keys);
		$dataValue = $this->getData($q_value.' LIMIT '.$cursor.','.$limit, $keys);

        $result['no'] = $cursor + 1;
        $result['page'] = $page;
        $result['limit'] = $limit;
        $result['count'] = $dataCount['value'][0]['counts'];
		$result['table'] = $dataValue['value'];
		$result['label'] = 'Regional';
		$result['query'] = $dataValue['query'];
		$result['query'] = '';
		return $result;
	}
	
	public function getTabelWilayah($data){
		$page = $data['page'];
		$regional = $data['regional'].'%';
		$query = 'FROM tb_wilayah wil 
				JOIN tb_regional reg ON (wil.id_regional=reg.id_regional) 
				WHERE (reg.id_regional LIKE ?)';
		$q_count = 'SELECT COUNT(*) AS counts '.$query;
		$q_value = 'SELECT * '.$query.' ORDER BY urutan_wilayah ASC';
		$keys = [$regional];
		$limit = 10;
        $cursor = ($page - 1) * $limit;
		$dataCount = $this->getData($q_count, $keys);
		$dataValue = $this->getData($q_value.' LIMIT '.$cursor.','.$limit, $keys);

        $result['no'] = $cursor + 1;
        $result['page'] = $page;
        $result['limit'] = $limit;
        $result['count'] = $dataCount['value'][0]['counts'];
		$result['table'] = $dataValue['value'];
		$result['label'] = 'Wilayah';
		$result['query'] = $dataValue['query'];
		$result['query'] = '';
		return $result;
	}
	
	public function getTabelSatker($data){
		$cari = '%'.$data['cari'].'%';
		$regional = '%'.$data['regional'].'%';
		$kategori = '%'.$data['kategori'].'%';
		$wilayah = '%'.$data['wilayah'].'%';
		$page = $data['page'];
		$query = 'FROM tb_satker sat WHERE ((sat.nama_satker LIKE ?) OR (sat.singkatan_satker LIKE ?)) AND (sat.kategori_satker LIKE ?) AND (sat.id_wilayah LIKE ?) AND (sat.id_regional LIKE ?)';
		$q_count = 'SELECT COUNT(*) AS counts '.$query;
		$q_value = 'SELECT * '.$query.' ORDER BY sat.urutan_satker ASC';
		$keys = [$cari, $cari, $kategori, $wilayah, $regional];
		$limit = 10;
        $cursor = ($page - 1) * $limit;
		$dataCount = $this->getData($q_count, $keys);
		$dataValue = $this->getData($q_value.' LIMIT '.$cursor.','.$limit, $keys);
		$listValue = [];
		$option_satker = $this->getNamaSatker();
		$option_wilayah = $this->getWilayah();
		$option_kategori = $this->getKategori();
		$option_regional = $this->getRegional();
		foreach ($dataValue['value'] as $key => $value) {
			$listValue[$key]['id_satker'] = $value['id_satker'];
			$listValue[$key]['nama_satker'] = $value['nama_satker'];
			$listValue[$key]['singkatan_satker'] = $value['singkatan_satker'];
			$listValue[$key]['nama_subsatker'] = isset($option_satker[$value['id_subsatker']]['text']) ? $option_satker[$value['id_subsatker']]['text'] : '-';
			$listValue[$key]['nama_wilayah'] = isset($option_wilayah[$value['id_wilayah']]['text']) ? $option_wilayah[$value['id_wilayah']]['text'] : '-';
			$listValue[$key]['nama_kategori'] = isset($option_kategori[$value['kategori_satker']]['text']) ? $option_kategori[$value['kategori_satker']]['text'] : '-';
			$listValue[$key]['nama_regional'] = isset($option_regional[$value['id_regional']]['text']) ? $option_regional[$value['id_regional']]['alias'] : '-';
			$listValue[$key]['status_satker'] = $value['status_satker'];
			$listValue[$key]['urutan_satker'] = $value['urutan_satker'];
		}

        $result['no'] = $cursor + 1;
        $result['page'] = $page;
        $result['limit'] = $limit;
        $result['count'] = $dataCount['value'][0]['counts'];
		$result['table'] = $listValue;
		$result['label'] = 'Satker';
		$result['query'] = $dataValue['query'];
		$result['query'] = '';
		return $result;
	}
	
	public function getTabelBidang($data){
		$cari = '%'.$data['cari'].'%';
		$satker = '%'.$data['satker'].'%';
		$page = $data['page'];
		$query = 'FROM tb_bidang bid JOIN tb_satker sat ON (bid.id_satker=sat.id_satker) WHERE (bid.nama_bidang LIKE ?) AND (sat.id_satker LIKE ?)';
		$q_count = 'SELECT COUNT(*) AS counts '.$query;
		$q_value = 'SELECT * '.$query.' ORDER BY sat.urutan_satker, bid.id_bidang ASC';
		$keys = [$cari, $satker];
		$limit = 10;
        $cursor = ($page - 1) * $limit;
		$dataCount = $this->getData($q_count, $keys);
		$dataValue = $this->getData($q_value.' LIMIT '.$cursor.','.$limit, $keys);
		$listValue = [];
		foreach ($dataValue['value'] as $key => $value) {
			$listValue[$key]['id_bidang'] = $value['id_bidang'];
			$listValue[$key]['nama_bidang'] = $value['nama_bidang'];
			$listValue[$key]['nama_satker'] = $value['nama_satker'];
			$listValue[$key]['singkatan_satker'] = $value['singkatan_satker'];
		}

        $result['no'] = $cursor + 1;
        $result['page'] = $page;
        $result['limit'] = $limit;
        $result['count'] = $dataCount['value'][0]['counts'];
		$result['table'] = $listValue;
		$result['label'] = 'Bidang';
		$result['query'] = $dataValue['query'];
		$result['query'] = '';
		return $result;
	}
	
	public function getTabelJabatan($data){
		$cari = '%'.$data['cari'].'%';
		$satker = '%'.$data['satker'].'%';
		$bidang = '%'.$data['bidang'].'%';
		$page = $data['page'];
		$query = 'FROM tb_jabatan jab 
					LEFT JOIN tb_bidang bid ON (jab.id_bidang=bid.id_bidang)
					LEFT JOIN tb_satker sat ON (jab.id_satker=sat.id_satker) 
					WHERE (jab.nama_jabatan LIKE ?) AND (bid.id_bidang LIKE ? OR ISNULL(bid.id_bidang)) AND (sat.id_satker LIKE ?)';
		$q_count = 'SELECT COUNT(*) AS counts '.$query;
		$q_value = 'SELECT * '.$query.' ORDER BY sat.urutan_satker ASC, jab.urutan_jabatan ASC';
		$keys = [$cari, $bidang, $satker];
		$limit = 10;
        $cursor = ($page - 1) * $limit;
		$dataCount = $this->getData($q_count, $keys);
		$dataValue = $this->getData($q_value.' LIMIT '.$cursor.','.$limit, $keys);
		$listValue = [];
		foreach ($dataValue['value'] as $key => $value) {
			$listValue[$key]['id_jabatan'] = $value['id_jabatan'];
			$listValue[$key]['nama_jabatan'] = $value['nama_jabatan'];
			$listValue[$key]['nama_bidang'] = !\is_null($value['nama_bidang']) ? $value['nama_bidang'] : '';
			$listValue[$key]['id_satker'] = $value['id_satker'];
			$listValue[$key]['nama_satker'] = $value['nama_satker'];
			$listValue[$key]['singkatan_satker'] = $value['singkatan_satker'];
			$listValue[$key]['status_jabatan'] = $value['status_jabatan'];
			$listValue[$key]['urutan_jabatan'] = $value['urutan_jabatan'];
		}

        $result['no'] = $cursor + 1;
        $result['page'] = $page;
        $result['limit'] = $limit;
        $result['count'] = $dataCount['value'][0]['counts'];
		$result['table'] = $listValue;
		$result['label'] = 'Jabatan';
		$result['query'] = $dataValue['query'];
		$result['query'] = '';
		return $result;
	}
	
	public function getTabelPersonil($data){
		$cari = '%'.$data['cari'].'%';
		$page = $data['page'];
		$query = 'FROM tb_personil WHERE ((id_personil LIKE ?) OR (nama_personil LIKE ?) OR (pangkat_personil LIKE ?) OR (jabatan_personil LIKE ?))';
		$q_count = 'SELECT COUNT(*) AS counts '.$query;
		$q_value = 'SELECT * '.$query.' ORDER BY id_personil DESC';
		$keys = [$cari, $cari, $cari, $cari];
		$limit = 10;
        $cursor = ($page - 1) * $limit;
		$dataCount = $this->getData($q_count, $keys);
		$dataValue = $this->getData($q_value.' LIMIT '.$cursor.','.$limit, $keys);
		$listValue = [];
		foreach ($dataValue['value'] as $key => $value) {
			$listValue[$key]['id_personil'] = $value['id_personil'];
			$listValue[$key]['nama_personil'] = !empty($value['pangkat_personil']) ? $value['pangkat_personil'].'. '.$value['nama_personil'] : $value['nama_personil'];
			$listValue[$key]['jabatan_personil'] = $value['jabatan_personil'];
			$listValue[$key]['email_personil'] = !empty($value['email_personil']) ? $value['email_personil'] : '-';
			$listValue[$key]['telp_personil'] = !empty($value['telp_personil']) ? $value['telp_personil'] : '-';
		}

        $result['no'] = $cursor + 1;
        $result['page'] = $page;
        $result['limit'] = $limit;
        $result['count'] = $dataCount['value'][0]['counts'];
		$result['table'] = $listValue;
		$result['label'] = 'Personil';
		$result['query'] = $dataValue['query'];
		$result['query'] = '';
		return $result;
	}

	public function getTabelUser($data){
		$cari = '%'.$data['cari'].'%';
		$satker = '%'.$data['satker'].'%';
		$bidang = '%'.$data['bidang'].'%';
		$page = $data['page'];
		$query = 'FROM tb_jabatan jab 
					LEFT JOIN tb_bidang bid ON (jab.id_bidang=bid.id_bidang)
					LEFT JOIN tb_satker sat ON (jab.id_satker=sat.id_satker) 
					LEFT JOIN tb_user usr ON (jab.id_jabatan=usr.id_jabatan)
					LEFT JOIN tb_personil prs ON (usr.id_personil=prs.id_personil)
					WHERE (jab.nama_jabatan LIKE ?) AND (bid.id_bidang LIKE ? OR ISNULL(bid.id_bidang)) AND (sat.id_satker LIKE ?)';
		$q_active = 'SELECT COUNT(*) AS counts '.$query.' AND (NOT ISNULL(id_user))';
		$q_count = 'SELECT COUNT(*) AS counts '.$query;
		$q_value = 'SELECT jab.*, bid.*, sat.*, prs.*, usr.id_user, usr.level_user, usr.disposisi_user, usr.koordinasi_user, usr.laporan_user, usr.draft_user '.$query.' ORDER BY sat.urutan_satker ASC, jab.urutan_jabatan ASC';
		$keys = [$cari, $bidang, $satker];
		$limit = 10;
        $cursor = ($page - 1) * $limit;
		$dataActive = $this->getData($q_active, $keys);
		$dataCount = $this->getData($q_count, $keys);
		$dataValue = $this->getData($q_value.' LIMIT '.$cursor.','.$limit, $keys);
		$listValue = [];
		$option_level = $this->getUserLevel();
		$option_action = $this->getUserAction();
		foreach ($dataValue['value'] as $key => $value) {
			foreach ($value as $k => $v) { $value[$k] = \is_null($v) ? '' : $v; }
			$listValue[$key]['id_user'] = $value['id_user'];
			$listValue[$key]['id_jabatan'] = $value['id_jabatan'];
			$listValue[$key]['nama_jabatan'] = $value['nama_jabatan'];
			$listValue[$key]['nama_bidang'] = $value['nama_bidang'];
			$listValue[$key]['nama_satker'] = $value['nama_satker'];
			$listValue[$key]['singkatan_satker'] = $value['singkatan_satker'];
			$listValue[$key]['status_jabatan'] = $value['status_jabatan'];
			$listValue[$key]['urutan_jabatan'] = $value['urutan_jabatan'];
			// User Info
			$listValue[$key]['nama_user'] = !empty($value['pangkat_personil']) ? $value['pangkat_personil'].'. '.$value['nama_personil'] : $value['nama_personil'];
			$listValue[$key]['nama_user'] = ($listValue[$key]['nama_user'] != null) ? $listValue[$key]['nama_user'] : '?';
			$listValue[$key]['level_user'] = isset($option_level[$value['level_user']]['text']) ? $option_level[$value['level_user']]['text'] : '';
			$listValue[$key]['disposisi_user'] = isset($option_action[$value['disposisi_user']]['text']) ? $option_action[$value['disposisi_user']]['text'] : '';
			$listValue[$key]['koordinasi_user'] = isset($option_action[$value['koordinasi_user']]['text']) ? $option_action[$value['koordinasi_user']]['text'] : '';
			$listValue[$key]['laporan_user'] = isset($option_action[$value['laporan_user']]['text']) ? $option_action[$value['laporan_user']]['text'] : '';
			$listValue[$key]['draft_user'] = $this->getUserActionDraftTabel($value['draft_user']);
		}

        $result['no'] = $cursor + 1;
        $result['page'] = $page;
        $result['limit'] = $limit;
		$result['active_user'] = $dataActive['value'][0]['counts'];
        $result['count'] = $dataCount['value'][0]['counts'];
		$result['table'] = $listValue;
		$result['label'] = 'User';
		$result['query'] = $dataValue['query'];
		// $result['query'] = $dataCount['query'];
		$result['query'] = '';
		return $result;
	}

	public function getQueryUserRegister($register){
		/**
		 * Daftar user yang login ke aplikasi via mobile/browser
		 */
		switch ($register) {
			case 'desktop':
				return 'FROM tb_user_login log 
						JOIN tb_user usr ON (log.user_id=usr.id_user)
						JOIN tb_jabatan jab ON (usr.id_jabatan=jab.id_jabatan)
						JOIN tb_satker sat ON (jab.id_satker=sat.id_satker) 
						LEFT JOIN tb_personil prs ON (usr.id_personil=prs.id_personil)
						WHERE (log.device_id = "")';
				break;

			case 'mobile':
				return 'FROM tb_user_login log 
						JOIN tb_user usr ON (log.user_id=usr.id_user)
						JOIN tb_jabatan jab ON (usr.id_jabatan=jab.id_jabatan)
						JOIN tb_satker sat ON (jab.id_satker=sat.id_satker) 
						LEFT JOIN tb_personil prs ON (usr.id_personil=prs.id_personil)
						WHERE (log.device_id <> "")';
				break;
			
			default:
				return '';
				break;
		}
	}

	public function getTabelUserRegister($data){
		/**
		 * Jika Group By not work, masuk ke mysql command ketik :
		 * SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));
		 */
		$register = $data['register'];
		$page = $data['page'];
		if(\in_array($register, ['desktop', 'mobile'])){
			$query = $this->getQueryUserRegister($register).' GROUP BY (jab.id_jabatan)';
		}
		else{
			return [];
		}

		$q_count = 'SELECT COUNT(*) AS counts '.$query;
		$q_value = 'SELECT *, COUNT(jab.id_jabatan) AS active_user '.$query.' ORDER BY sat.urutan_satker ASC, jab.urutan_jabatan ASC';
		$keys = [];
		$limit = 10;
        $cursor = ($page - 1) * $limit;
		$dataCount = $this->getData($q_count, $keys);
		$dataValue = $this->getData($q_value.' LIMIT '.$cursor.','.$limit, $keys);
		$listValue = [];
		foreach ($dataValue['value'] as $key => $value) {
			$listValue[$key] = $value;
			$listValue[$key]['active_user'] = $value['active_user'].' user';
		}

        $result['no'] = $cursor + 1;
        $result['page'] = $page;
        $result['limit'] = $limit;
        $result['count'] = !empty($dataCount['value'][0]['counts']) ? $dataCount['value'][0]['counts']: 0 ;
		$result['table'] = $listValue;
		$result['label'] = 'User';
		$result['query'] = $dataValue['query'];
		$result['query'] = '';
		return $result;
	}

	public function getTabelUserAction($data){
		/**
		 * Hapus duplicate data if field2 is same value
		 * delete from `tb_user_disposisi` where `id_user_disposisi` in (select `id_user_disposisi` from (select id_user_disposisi from `tb_user_disposisi` group by id_jabatan, id_relasi having count(*) > 1) as c);
		 * delete from `tb_user_koordinasi` where `id_user_koordinasi` in (select `id_user_koordinasi` from (select id_user_koordinasi from `tb_user_koordinasi` group by id_jabatan, id_relasi having count(*) > 1) as c);
		 * delete from `tb_user_laporan` where `id_user_laporan` in (select `id_user_laporan` from (select id_user_laporan from `tb_user_laporan` group by id_jabatan, id_relasi having count(*) > 1) as c);
		 */
		$status_action = $data['status_action'];
		if(\in_array($status_action, ['disposisi', 'koordinasi', 'laporan', 'draft'])){
			$query = 'SELECT act.id_jabatan as id_user_jabatan, jab.*, sat.* FROM tb_user_'.$status_action.' act';
		}
		else{
			return [];
		}
		
		$data = $this->getData($query.' JOIN tb_jabatan jab ON (act.id_relasi=jab.id_jabatan)
										JOIN tb_satker sat ON (jab.id_satker=sat.id_satker)
										WHERE (act.id_jabatan = ?) ORDER BY urutan_satker, urutan_jabatan ASC', [$data['id_jabatan']]);
		$listValue = [];
        foreach ($data['value'] as $key => $value) {
			$listValue[$key] = $value;
			$listValue[$key]['status_action'] = $status_action;
		}
		
		$result['no'] = 1;
        $result['page'] = 1;
        $result['limit'] = $data['count'];
        $result['count'] = $data['count'];
        $result['table'] = $listValue;
		$result['query'] = $data['query'];
		$result['query'] = '';
        return $result;
	}

	public function getTabelUserRegisterMobile($userSession){
		$query = $this->getQueryUserRegister('mobile').' AND (sat.id_satker = ?) GROUP BY (jab.id_jabatan) ORDER BY sat.urutan_satker ASC, jab.urutan_jabatan ASC';
		$data = $this->getData('SELECT * '.$query, [$userSession['id_satker']]);
		$listValue = [];
        foreach ($data['value'] as $key => $value) {
			$listValue[$key]['id_jabatan'] = $value['id_jabatan'];
			$listValue[$key]['id_personil'] = $value['id_personil'];
			$listValue[$key]['nama_personil'] = !empty($value['pangkat_personil']) ? $value['pangkat_personil'].'. '.$value['nama_personil'] : $value['nama_personil'];
			$listValue[$key]['nama_jabatan'] = $value['nama_jabatan'];
			$listValue[$key]['phone_type'] = $value['phone_type'];
			// $listValue[$key] = $value;
        }
		
		$result['no'] = 1;
        $result['page'] = 1;
        $result['limit'] = $data['count'];
        $result['count'] = $data['count'];
        $result['table'] = $data['value'];
        $result['table'] = $listValue;
		$result['query'] = $data['query'];
		$result['query'] = '';
        return $result;
	}

	/**
	 * Data Statistik
	 */
	public function getCountUserRegister($register){
		if(\in_array($register, ['desktop', 'mobile'])){
			$query = $this->getQueryUserRegister($register).' GROUP BY (jab.id_jabatan)';
			$data = $this->getData('SELECT COUNT(*) AS counts '.$query, []);
			return !empty($data['value'][0]['counts']) ? $data['value'][0]['counts']: 0;
		}
		else{
			return 0;
		}
	}

	public function getCountUserActive($satker = ''){
		$query = 'FROM tb_jabatan jab 
					LEFT JOIN tb_bidang bid ON (jab.id_bidang=bid.id_bidang)
					LEFT JOIN tb_satker sat ON (jab.id_satker=sat.id_satker) 
					LEFT JOIN tb_user usr ON (jab.id_jabatan=usr.id_jabatan)
					LEFT JOIN tb_personil prs ON (usr.id_personil=prs.id_personil)
					WHERE (sat.id_satker LIKE ?)';

		$data = $this->getData('SELECT COUNT(*) AS counts '.$query.' AND (NOT ISNULL(id_user))', ['%'.$satker.'%']);
		return !empty($data['value'][0]['counts']) ? $data['value'][0]['counts']: 0;
	}

	/**
	 * Data Chart
	 */
	public function getChartUserRegisterMobile($userSession) {
        $result = [
			'title' => 'Data Registrasi Mobile',
			'subtitle' => 'STATISTIK',
			'tooltip' => ' User',
			'xAxis' => [
				'categories' => 'xxx',
			],
			'yAxis' => [
				'title' => 'Jumlah Register (User)',
			],
			'value' => [
				['name' => 'xxx', 'data' => [0]],
				['name' => 'xxx', 'data' => [0]],
			],
		];

		$query_mobile = $this->getQueryUserRegister('mobile').' AND (sat.id_satker = a.id_satker)';
		$data = $this->getData('SELECT nama_satker, 
				IFNULL((SELECT COUNT(*) FROM tb_jabatan b WHERE (a.id_satker=b.id_satker)), 0) AS jumlah_pejabat,
				IFNULL((SELECT COUNT(DISTINCT jab.id_jabatan) '.$query_mobile.'), 0) AS jumlah_register
				FROM tb_satker a 
				WHERE (a.id_satker = ?)', [$userSession['id_satker']]);
		
		$result['query'] = $data['query'];
		if($data['count'] > 0){
			$dataChart = $data['value'][0];
			$result['xAxis']['categories'] = $dataChart['nama_satker'];
			$result['value'][0]['name'] = 'Jumlah Pejabat ('.$dataChart['jumlah_pejabat'].')';
			$result['value'][0]['data'] = [intval($dataChart['jumlah_pejabat'])];
			$result['value'][1]['name'] = 'Jumlah Register ('.$dataChart['jumlah_register'].')';
			$result['value'][1]['data'] = [intval($dataChart['jumlah_register'])];
		} 

		return $result;
    }


	/**
	 * Data SIPP (connect to sipp polri)
	 * Sementara ambil data dari tb_personil
	 */

	public function getDataSIPP($id){
		/**
		 * Check Data Personil yang tidak digunakan
		 * SELECT * from tb_personil where (id_personil NOT IN (select id_personil from tb_user));
		 */
		$data = $this->getData('SELECT * FROM tb_personil WHERE (id_personil = ?)', [$id]);
		return ($data['count'] > 0) ? $data['value'][0] : [];
		
		$personil = [];
		if(!empty($id)){
			$server_sipp = $this->server_sipp;
			$url = $server_sipp['host'].':'.$server_sipp['port'].'/'.$server_sipp['path'];
			$url = \str_replace('{nrp}', $id, $url);
			// Open connection
			$ch = curl_init();
			// Set the url, number of POST vars, POST data
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $server_sipp['header']);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			// Disabling SSL Certificate support temporarly
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);  
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			// curl_setopt($ch, CURLOPT_POSTFIELDS, $data['fields']);
			$result = curl_exec($ch);           
			if($result === FALSE){
				// die('Curl failed: ' . curl_error($ch));
				return $personil;
			}
			curl_close($ch);
			$response = \json_decode($result, true);
			// return \json_decode($result, true); // check error response
			if (!empty($response['personel'])) {
				$personil = $this->getTabel('tb_personil');
				$personil['id_personil'] = $response['personel'][0]['nrp'];
				$personil['pangkat_personil'] = $response['personel'][0]['pangkat'];
				$personil['nama_personil'] = $response['personel'][0]['nama_lengkap'];
				$personil['jabatan_personil'] = $response['personel'][0]['jabatan'];
			}
		}

		return $personil;
	}
	
	public function checkServer($input){
		$result = $input;
		$result['status'] = 'offline';
		$result['color'] = 'red';
		$result['detail'] = '';
		$connection = @fsockopen($input['host'], $input['port'], $errNo, $errStr, 5); // timeout 5 seconds
        if(is_resource($connection)){
			fclose($connection);
			$result['status'] = 'online';
			$result['color'] = 'green';
			$result['detail'] = 'Server Ready';
		}
		else{
			$result['detail'] = $errStr;
		}
        
		return $result;
    }

}
?>