<?php
namespace app\adol\model;

use system\Model;
use app\adol\model\adol;
use comp;

class adol_draft extends adol{
	
	public function __construct(){
		parent::__construct();        
    }

    public function getDraftRiwayatPengirim(){
        $result = [];
        $data = $this->getData('SELECT pengirim_draft_riwayat, nama_jabatan FROM tb_jabatan a, tb_draft_riwayat b WHERE (a.id_jabatan=b.pengirim_draft_riwayat)', []);
        foreach ($data['value'] as $key => $value) { $result[$value['pengirim_draft_riwayat']] = ['text' => $value['nama_jabatan']]; }
        return $result;
    }

    public function getDraftRiwayatTujuan(){
        $result = [];
        $data = $this->getData('SELECT tujuan_draft_riwayat, nama_jabatan FROM tb_jabatan a, tb_draft_riwayat b WHERE (a.id_jabatan=b.tujuan_draft_riwayat)', []);
        foreach ($data['value'] as $key => $value) { $result[$value['tujuan_draft_riwayat']] = ['text' => $value['nama_jabatan']]; }
        return $result;
    }

    public function getJmlDraftMasuk($user){
		$data = $this->getData('SELECT id_draft_riwayat FROM tb_draft_riwayat WHERE (tujuan_draft_riwayat = ?)', [$user['id_jabatan']]);
		return $data['count'];
    }
	
	public function getJmlDraftKeluar($user){
		$data = $this->getData('SELECT id_draft_riwayat FROM tb_draft_riwayat WHERE (pengirim_draft_riwayat = ?)', [$user['id_jabatan']]);
        return $data['count'];
    }

    public function getLastDataDraft($id_draft){
        $data = $this->getData('SELECT
                                draft.*,
                                riwayat.*,
                                IFNULL(adm.id_adm, "") AS id_adm
                                FROM tb_draft draft
                                JOIN tb_draft_riwayat riwayat ON (draft.id_draft=riwayat.id_draft) 
                                LEFT JOIN tb_adm adm ON (draft.id_draft=adm.id_draft)
                                WHERE (draft.id_draft = ?) ORDER BY riwayat.id_draft_riwayat DESC LIMIT 1', [$id_draft]);

        if($data['count'] > 0){
            return $data['value'][0];
        }
        else{
            $dataDraft = $this->getTabel('tb_draft');
            $dataDraft['id_adm'] = '';
            $dataDraft['lampiran_draft_riwayat'] = '';
            return $dataDraft;
        }
    }

    // public function getUserDraft($id_jabatan){
    //     set_time_limit(0);
    //     $data = $this->getData('SELECT id_relasi, nama_jabatan, singkatan_satker FROM tb_user_draft a, tb_jabatan b, tb_satker c WHERE (id_relasi=b.id_jabatan) AND (b.id_satker=c.id_satker) AND (a.id_jabatan=?) ORDER BY urutan_satker, urutan_jabatan', array($id_jabatan));
    //     $dataArr = [];
    //     foreach ($data['value'] as $kol){$dataArr[$kol['id_relasi']] = $kol['nama_jabatan'];}
    //     return $dataArr;
    // }

    public function getUserDraft($user) {
        $result = [];
        $keys = [$user['id_jabatan']];
        $data = $this->getData('SELECT id_relasi, nama_jabatan, singkatan_satker FROM tb_user_draft a, tb_jabatan b, tb_satker c WHERE (id_relasi=b.id_jabatan) AND (b.id_satker=c.id_satker) AND (a.id_jabatan=?) ORDER BY urutan_satker, urutan_jabatan', $keys);
        //foreach ($data['value'] as $key => $value) { $result[$value['id_relasi']] = ['text' => $value['nama_jabatan']]; }
        foreach ($data['value'] as $key => $value) { 
            $result[$value['id_relasi']] = [
                'text' => $value['nama_jabatan'],
                'info' => $value['singkatan_satker'],
            ]; 
        }
        return $result;
    }
	
	// public function getUserDraftPengirim($id_draft, $id_jabatan){ // Untuk Form Tolak Draft
    //     set_time_limit(0);
    //     $data = $this->getData('SELECT a.pengirim_draft_riwayat, b.nama_jabatan FROM tb_draft_riwayat a, tb_jabatan b WHERE (a.pengirim_draft_riwayat=b.id_jabatan) AND (a.id_draft = ?) AND (a.tujuan_draft_riwayat = ?) ORDER BY id_draft_riwayat DESC', array($id_draft, $id_jabatan));
    //     $dataArr = [];
    //     foreach ($data['value'] as $kol){$dataArr[$kol['pengirim_draft_riwayat']] = $kol['nama_jabatan'];}
	// 	return $dataArr;
    // }

    public function getUserDraftPengirim($id, $user) { // Untuk Form Tolak Draft
        $result = [];
        $keys = [$id, $user['id_jabatan']];
        $data = $this->getData('SELECT pengirim_draft_riwayat, nama_jabatan, singkatan_satker FROM tb_draft_riwayat a, tb_jabatan b, tb_satker c WHERE (a.pengirim_draft_riwayat=b.id_jabatan) AND (b.id_satker=c.id_satker) AND (a.id_draft = ?) AND (a.tujuan_draft_riwayat = ?) ORDER BY id_draft_riwayat DESC', $keys);
        // $data = $this->getData('SELECT id_relasi, nama_jabatan, singkatan_satker FROM tb_user_draft a, tb_jabatan b, tb_satker c WHERE (id_relasi=b.id_jabatan) AND (b.id_satker=c.id_satker) AND (a.id_jabatan=?) ORDER BY urutan_satker, urutan_jabatan', $keys);
        //foreach ($data['value'] as $key => $value) { $result[$value['id_relasi']] = ['text' => $value['nama_jabatan']]; }
        foreach ($data['value'] as $key => $value) { 
            $result[$value['pengirim_draft_riwayat']] = [
                'text' => $value['nama_jabatan'],
                'info' => $value['singkatan_satker'],
            ]; 
        }
        return $result;
    }

    public function getDraftNotifMasuk($user){
        $keys = array('0', $user['id_jabatan']);
        $data = $this->getData('SELECT * FROM tb_draft_riwayat a, tb_draft b, tb_jabatan c WHERE (a.id_draft=b.id_draft) AND (a.tujuan_draft_riwayat=c.id_jabatan) AND (status_draft_riwayat = ?) AND (tujuan_draft_riwayat = ?)  ORDER BY waktu_draft_riwayat DESC', $keys);
        if($data['count'] > 0) return $data['value'];
		else return [];
    }

    public function getDetailDraft($id, $action, $userSession){
        $action_draft_riwayat = $this->getActionDraft();
        $option_pengirim = $this->getDraftRiwayatPengirim();
        $option_tujuan = $this->getDraftRiwayatTujuan();
        $option_jenis_draft = $this->getJenisAdm();
        $option_status_draft = $this->getStatusDraft();
        
        // Check Action draft
        if($action == 'masuk'){
            $data = $this->getData('SELECT draft_riwayat.*, draft.*,
                                    IFNULL(adm.id_adm, "") AS id_adm
                                    FROM tb_draft_riwayat draft_riwayat
                                    JOIN tb_draft draft ON (draft_riwayat.id_draft=draft.id_draft) 
                                    LEFT JOIN tb_adm adm ON (draft.id_draft=adm.id_draft)
                                    WHERE (draft_riwayat.id_draft_riwayat = ?) AND (tujuan_draft_riwayat = ?)', [$id, $userSession['id_jabatan']]);

            $dataDraft = ($data['count'] > 0) ? $data['value'][0] : [];
            if(empty($dataDraft)) return [];

            // update status baca !!!
            $waktu_baca = date('Y-m-d H:i:s');
            $result = $this->update('tb_draft_riwayat', ['status_draft_riwayat' => '1', 'waktu_dibaca_draft_riwayat' => $waktu_baca], ['id_draft_riwayat' => $id]);
        }
        else if($action == 'keluar'){
            $data = $this->getData('SELECT draft_riwayat.*, draft.*,
                                    IFNULL(adm.id_adm, "") AS id_adm
                                    FROM tb_draft_riwayat draft_riwayat
                                    JOIN tb_draft draft ON (draft_riwayat.id_draft=draft.id_draft) 
                                    LEFT JOIN tb_adm adm ON (draft.id_draft=adm.id_draft)
                                    WHERE (draft_riwayat.id_draft_riwayat = ?) AND (pengirim_draft_riwayat = ?)', [$id, $userSession['id_jabatan']]);

            $dataDraft = ($data['count'] > 0) ? $data['value'][0] : [];
            if(empty($dataDraft)) return [];
        }
        else{
            return [];
        }

        $jenis_draft = $dataDraft['jenislain_draft'];
        if($dataDraft['id_jenis'] != 'JNSXX'){
            if($dataDraft['id_jenis'] != 'JNS99'){
                $jenis_draft = !empty($option_jenis_draft[$dataDraft['id_jenis']]) ? $option_jenis_draft[$dataDraft['id_jenis']]['text'] : $jenis_draft;
            }
        }
        
        $nama_pengirim = !empty($option_pengirim[$dataDraft['pengirim_draft_riwayat']]) ? $option_pengirim[$dataDraft['pengirim_draft_riwayat']]['text'] : '-';
        $nama_tujuan = !empty($option_tujuan[$dataDraft['tujuan_draft_riwayat']]) ? $option_tujuan[$dataDraft['tujuan_draft_riwayat']]['text'] : '-';
        $lampiran_draft = (!empty($dataDraft['lampiran_draft_riwayat']) && file_exists($this->path_lampiran_draft.$dataDraft['lampiran_draft_riwayat'])) ? $this->getUrl->baseUrl.$this->path_lampiran_draft.$dataDraft['lampiran_draft_riwayat'] : '';
        $status_draft = $option_status_draft[$dataDraft['status_draft']];
        $aksi_riwayat = ucwords($action_draft_riwayat[$dataDraft['action_draft_riwayat']]['id']);

        $result = [
            'id_draft_riwayat' => $dataDraft['id_draft_riwayat'],
            'id_draft' => $dataDraft['id_draft'],
            'id_adm' => $dataDraft['id_adm'],
            'id_jenis' => $dataDraft['id_jenis'],
            'jenislain_draft' => $dataDraft['jenislain_draft'],
            'nama_jenis' => $jenis_draft,
            'nama_pengirim' => $nama_pengirim,
            'nama_tujuan' => $nama_tujuan,
            'pengirim_draft' => $dataDraft['pengirim_draft_riwayat'],
            'tujuan_draft' => $dataDraft['tujuan_draft_riwayat'],
            'tanggal_draft' => comp\FUNC::tanggal($dataDraft['waktu_draft_riwayat'], 'long_date'),
            'perihal_draft' => $dataDraft['perihal_draft'],
            'keterangan_draft' => $dataDraft['keterangan_draft_riwayat'],
            'template_draft' => $dataDraft['template_draft'],
            'lampiran_draft' => $lampiran_draft,
            'aksi_draft' => $aksi_riwayat,
            'status_draft' => $status_draft['text']
        ];

		return $result;
    }

    public function getFormActionDraft($input, $action, $userSession){
		$form_title = $this->getFormTitleDraft();
		$user_action = ($action == 'tolak') ? $this->getUserDraftPengirim($input['id_draft'], $userSession): $this->getUserDraft($userSession);
        $dataLastDraft = $this->getLastDataDraft($input['id_draft']);        
		$result['pilihan_tujuan'] = !empty($user_action) ? $user_action : ['' => ['text' => '-- DATA MASIH KOSONG --']];
		$result['form_title'] = $form_title[$action]['text'];
		$result['user_action'] = $action;
		$result['id_draft'] = !empty($input['id_draft']) ? $input['id_draft'] : $dataLastDraft['id_draft'];
		$result['id_adm'] = !empty($input['id_adm']) ? $input['id_adm'] : $dataLastDraft['id_adm'];
		$result['id_jenis'] = !empty($input['id_jenis']) ? $input['id_jenis'] : $dataLastDraft['id_jenis'];
		$result['jenislain_draft'] = !empty($input['jenislain_draft']) ? $input['jenislain_draft'] : $dataLastDraft['jenislain_draft'];
        $result['perihal_draft'] = !empty($input['perihal_draft']) ? $input['perihal_draft'] : $dataLastDraft['perihal_draft'];
        $result['template_draft'] = !empty($input['template_draft']) ? $input['template_draft'] : '';
        $result['form_upload'] = !empty($input['form_upload']) ? $input['form_upload'] : true;
        $result['upload_mimes'] = $this->mimes_pdf;
        $result['upload_description'] = $this->upload_description_pdf;
        // $result['getActionDraftRiwayat'] = $this->getActionDraftRiwayat();
        return $result;
	}

    public function getTabelDraftMasuk($input, $userSession){
		$cari = '%'.$input['cari'].'%';
        $page = $input['page'];
        $keys = [$cari];
        $query = 'FROM tb_draft_riwayat a LEFT JOIN tb_draft b ON (a.id_draft=b.id_draft) LEFT JOIN tb_jabatan c ON (a.pengirim_draft_riwayat=c.id_jabatan) LEFT JOIN tb_jenis d ON (b.id_jenis=d.id_jenis) ';
        $query .= 'WHERE ('.$input['pil_cari'].' LIKE ?) AND (tujuan_draft_riwayat = ?)';
        $order = ' ORDER BY waktu_draft_riwayat DESC';
        array_push($keys, $userSession['id_jabatan']);

        if(\in_array($input['status_baca'], ['0', '1'])){
            $query .= ' AND (status_draft_riwayat = ?)';
            array_push($keys, $input['status_baca']);
        }

        $limit = 20;
        $cursor = ($page - 1) * $limit;
        $dataCount = $this->getData('SELECT COUNT(*) AS counts '.$query, $keys);
        $dataValue = $this->getData('SELECT * '.$query.$order.' LIMIT '.$cursor.','.$limit, $keys);
        $listValue = [];
        $option_pengirim = $this->getDraftRiwayatPengirim();
        $option_jenis_draft = $this->getJenisAdm();
        $option_status_baca = $this->getStatusBaca();
        $option_status_draft = $this->getStatusDraft();
        foreach ($dataValue['value'] as $key => $value) {
            $status_baca_draft = $option_status_baca[$value['status_draft_riwayat']];
            $status_draft = $option_status_draft[$value['status_draft']];
            $waktu_draft = comp\FUNC::tanggal($value['waktu_draft_riwayat'], 'long_date');
            $nama_pengirim = !empty($option_pengirim[$value['pengirim_draft_riwayat']]) ? $option_pengirim[$value['pengirim_draft_riwayat']]['text'] : '-';

            $listValue[$key]['id_draft'] = $value['id_draft'];
            $listValue[$key]['id_draft_riwayat'] = $value['id_draft_riwayat'];
            $listValue[$key]['status_baca'] = $status_baca_draft['text'];
            $listValue[$key]['status_baca_color'] = $status_baca_draft['color'];
            $listValue[$key]['status_draft'] = $status_draft['text'];
            $listValue[$key]['waktu_draft'] = $waktu_draft;
            $listValue[$key]['nama_pengirim'] = $nama_pengirim;
            $listValue[$key]['pengirim_draft'] = $value['pengirim_draft_riwayat'];
            $listValue[$key]['keterangan_draft'] = $value['keterangan_draft_riwayat'];

            $jenis_draft = $value['jenislain_draft'];
            if($value['id_jenis'] != 'JNSXX'){
                if($value['id_jenis'] != 'JNS99'){
                    $jenis_draft = !empty($option_jenis_draft[$value['id_jenis']]) ? $option_jenis_draft[$value['id_jenis']]['text'] : $jenis_draft;
                }
            }
            $listValue[$key]['jenis_draft'] = $jenis_draft;
            $listValue[$key]['id_jenis'] = $value['id_jenis'];
            $listValue[$key]['jenislain_draft'] = $value['jenislain_draft'];
            $listValue[$key]['perihal_draft'] = $value['perihal_draft'];
        }
        
        $result['no'] = $cursor + 1;
        $result['page'] = $page;
        $result['limit'] = $limit;
        $result['count'] = $dataCount['value'][0]['counts'];
        $result['table'] = $listValue;
		$result['query'] = $dataValue['query'];
        $result['query'] = '';
        return $result;
    }

    public function getTabelDraftKeluar($input, $userSession){
		$cari = '%'.$input['cari'].'%';
        $page = $input['page'];
        $keys = [$cari];
        $query = 'FROM tb_draft_riwayat a LEFT JOIN tb_draft b ON (a.id_draft=b.id_draft) LEFT JOIN tb_jabatan c ON (a.tujuan_draft_riwayat=c.id_jabatan) LEFT JOIN tb_jenis d ON (b.id_jenis=d.id_jenis) ';
        $query .= 'WHERE ('.$input['pil_cari'].' LIKE ?) AND (pengirim_draft_riwayat = ?)';
        $order = ' ORDER BY waktu_draft_riwayat DESC';
        array_push($keys, $userSession['id_jabatan']);

        if(\in_array($input['status_baca'], ['0', '1'])){
            $query .= ' AND (status_draft_riwayat = ?)';
            array_push($keys, $input['status_baca']);
        }

        $limit = 20;
        $cursor = ($page - 1) * $limit;
        $dataCount = $this->getData('SELECT COUNT(*) AS counts '.$query, $keys);
        $dataValue = $this->getData('SELECT * '.$query.$order.' LIMIT '.$cursor.','.$limit, $keys);
        $listValue = [];
        $option_tujuan = $this->getDraftRiwayatTujuan();
        $option_jenis_draft = $this->getJenisAdm();
        $option_status_baca = $this->getStatusBaca();
        $option_status_draft = $this->getStatusDraft();
        foreach ($dataValue['value'] as $key => $value) {
            $status_baca_draft = $option_status_baca[$value['status_draft_riwayat']];
            $status_draft = $option_status_draft[$value['status_draft']];
            $waktu_draft = comp\FUNC::tanggal($value['waktu_draft_riwayat'], 'long_date');
            $nama_tujuan = !empty($option_tujuan[$value['tujuan_draft_riwayat']]) ? $option_tujuan[$value['tujuan_draft_riwayat']]['text'] : '-';

            $listValue[$key]['id_draft'] = $value['id_draft'];
            $listValue[$key]['id_draft_riwayat'] = $value['id_draft_riwayat'];
            $listValue[$key]['status_baca'] = $status_baca_draft['text'];
            $listValue[$key]['status_baca_color'] = $status_baca_draft['color'];
            $listValue[$key]['status_draft'] = $status_draft['text'];
            $listValue[$key]['waktu_draft'] = $waktu_draft;
            $listValue[$key]['nama_tujuan'] = $nama_tujuan;
            $listValue[$key]['tujuan_draft'] = $value['tujuan_draft_riwayat'];
            $listValue[$key]['keterangan_draft'] = $value['keterangan_draft_riwayat'];

            $jenis_draft = $value['jenislain_draft'];
            if($value['id_jenis'] != 'JNSXX'){
                if($value['id_jenis'] != 'JNS99'){
                    $jenis_draft = !empty($option_jenis_draft[$value['id_jenis']]) ? $option_jenis_draft[$value['id_jenis']]['text'] : $jenis_draft;
                }
            }
            $listValue[$key]['jenis_draft'] = $jenis_draft;
            $listValue[$key]['id_jenis'] = $value['id_jenis'];
            $listValue[$key]['jenislain_draft'] = $value['jenislain_draft'];
            $listValue[$key]['perihal_draft'] = $value['perihal_draft'];
        }
        
        $result['no'] = $cursor + 1;
        $result['page'] = $page;
        $result['limit'] = $limit;
        $result['count'] = $dataCount['value'][0]['counts'];
        $result['table'] = $listValue;
		$result['query'] = $dataValue['query'];
        $result['query'] = '';
        return $result;
    }

    public function getTabelRiwayatDraft($id_draft){
        $action_draft_riwayat = $this->getActionDraft();
        $option_pengirim = $this->getDraftRiwayatPengirim();
        $option_tujuan = $this->getDraftRiwayatTujuan();
        $option_jenis_draft = $this->getJenisAdm();
        $option_status_baca = $this->getStatusBaca();
        $option_status_draft = $this->getStatusDraft();
        $data = $this->getData('SELECT * FROM tb_draft_riwayat a, tb_draft b WHERE (a.id_draft=b.id_draft) AND (a.id_draft = ?) ORDER BY waktu_draft_riwayat DESC', array($id_draft));
        $listValue = [];
        foreach ($data['value'] as $key => $value) {
            $aksi_riwayat = ucwords($action_draft_riwayat[$value['action_draft_riwayat']]['id']);
            $status_baca_draft = $option_status_baca[$value['status_draft_riwayat']];
            $status_draft = $option_status_draft[$value['status_draft']];
            $waktu_riwayat = comp\FUNC::tanggal($value['waktu_draft_riwayat'], 'long_date_time');
            $nama_pengirim = !empty($option_pengirim[$value['pengirim_draft_riwayat']]) ? $option_pengirim[$value['pengirim_draft_riwayat']]['text'] : '-';
            $nama_tujuan = !empty($option_tujuan[$value['tujuan_draft_riwayat']]) ? $option_tujuan[$value['tujuan_draft_riwayat']]['text'] : '-';

            $listValue[$key]['id_draft'] = $value['id_draft'];
            $listValue[$key]['id_draft_riwayat'] = $value['id_draft_riwayat'];
            $listValue[$key]['status_baca'] = $status_baca_draft['text'];
            $listValue[$key]['status_baca_color'] = $status_baca_draft['color'];
            $listValue[$key]['status_draft'] = $status_draft['text'];
            $listValue[$key]['aksi_riwayat'] = $aksi_riwayat;
            $listValue[$key]['waktu_riwayat'] = $waktu_riwayat;
            $listValue[$key]['nama_pengirim'] = $nama_pengirim;
            $listValue[$key]['pengirim_draft'] = $value['pengirim_draft_riwayat'];
            $listValue[$key]['nama_tujuan'] = $nama_tujuan;
            $listValue[$key]['tujuan_draft'] = $value['tujuan_draft_riwayat'];
            $listValue[$key]['isi_pesan'] = $value['keterangan_draft_riwayat'];
        }
        
        $result['no'] = 1;
        $result['page'] = 1;
        $result['limit'] = $data['count'];
        $result['count'] = $data['count'];
        $result['table'] = $data['value'];
        $result['table'] = $listValue;
		$result['query'] = $data['query'];
		$result['query'] = '';
        return $result;
    }
	
	/**
	 * Data Draft
	 */

	// public function getDraftRiwayatPengirim(){
    //     set_time_limit(0);
    //     $data = $this->getData('SELECT pengirim_draft_riwayat, nama_jabatan FROM tb_jabatan a, tb_draft_riwayat b WHERE (a.id_jabatan=b.pengirim_draft_riwayat)', []);
    //     $dataArr = [];
    //     foreach ($data['value'] as $kol){$dataArr[$kol['pengirim_draft_riwayat']] = $kol['nama_jabatan'];}
    //     return $dataArr;
    // }
	
	// public function getDraftRiwayatTujuan(){
    //     set_time_limit(0);
    //     $data = $this->getData('SELECT tujuan_draft_riwayat, nama_jabatan FROM tb_jabatan a, tb_draft_riwayat b WHERE (a.id_jabatan=b.tujuan_draft_riwayat)', []);
    //     $dataArr = [];
    //     foreach ($data['value'] as $kol){$dataArr[$kol['tujuan_draft_riwayat']] = $kol['nama_jabatan'];}
    //     return $dataArr;
    // }

	// public function getJmlDraftMasuk($user){
    //     set_time_limit(0);
	// 	$data = $this->getData('SELECT id_draft_riwayat FROM tb_draft_riwayat WHERE (tujuan_draft_riwayat = ?)', array($user['id_jabatan']));
	// 	return $data['count'];
    // }
	
	// public function getJmlDraftKeluar($user){
    //     set_time_limit(0);
	// 	$data = $this->getData('SELECT id_draft_riwayat FROM tb_draft_riwayat WHERE (pengirim_draft_riwayat = ?)', array($user['id_jabatan']));
    //     return $data['count'];
    // }
        
    // public function getDraftDetailUpdate($id_draft_riwayat){
    //     set_time_limit(0);
    //     $waktu_baca = date('Y-m-d H:i:s');
    //     $idKey = array('1',$waktu_baca,$id_draft_riwayat);
    //     $result = $this->getData('UPDATE tb_draft_riwayat SET status_draft_riwayat = ?, waktu_dibaca_draft_riwayat = ? WHERE (id_draft_riwayat = ?)', $idKey);
    // }
	
	// public function getDatadraft($id_draft){
    //     set_time_limit(0);
	// 	$data = $this->getData('SELECT * FROM tb_draft WHERE (id_draft = ?)', array($id_draft));
	// 	if($data['count'] > 0) return $data['value'][0];
	// 	else return false;
    // }
	
	// public function getDataDraft($id_draft){
    //     set_time_limit(0);
	// 	$data = $this->getData('SELECT * FROM tb_draft WHERE (id_draft = ?)', array($id_draft));
	// 	if($data['count'] > 0) return $data['value'][0];
	// 	else return false;
    // }
	
	// public function getDataDraftRiwayat($id_draft_riwayat){
    //     set_time_limit(0);
	// 	$data = $this->getData('SELECT * FROM tb_draft_riwayat a, tb_draft b WHERE (a.id_draft=b.id_draft) AND (id_draft_riwayat = ?)', array($id_draft_riwayat));
	// 	if($data['count'] > 0) return $data['value'][0];
	// 	else return false;
    // }
	
	// public function getRiwayatDraft($id_draft){
    //     set_time_limit(0);
	// 	$data = $this->getData('SELECT * FROM tb_draft_riwayat a, tb_draft b WHERE (a.id_draft=b.id_draft) AND (a.id_draft = ?) ORDER BY waktu_draft_riwayat DESC', array($id_draft));
	// 	if($data['count'] > 0) return $data['value'];
	// 	else return [];
    // }
	
	// public function getUserActionDraft($id_jabatan){
    //     set_time_limit(0);
    //     $data = $this->getData('SELECT draft_user FROM tb_user WHERE (id_jabatan = ?)', array($id_jabatan));
    //     $arrData = array('buat','lapor', 'tolak', 'revisi', 'setuju', 'selesai');
	// 	$dataArr = [];
	// 	$draft_user = decbin($data['value'][0]['draft_user']);
	// 	$draft_user = substr('000000',0,6 - strlen($draft_user)).$draft_user;
	// 	for($i = 0; $i < count($arrData); $i++){
	// 		if($draft_user[$i] != '0')
	// 			if($arrData[$i] != 'buat')
	// 				array_push($dataArr, '<button type="button" class="btn btn-primary btnAction" id="'.$arrData[$i].'">'.ucwords($arrData[$i]).'</button>&nbsp;');
	// 	}
	// 	return $dataArr;
    // }
	
	// public function getUserDraft($id_jabatan){
    //     set_time_limit(0);
    //     $data = $this->getData('SELECT id_relasi, nama_jabatan, singkatan_satker FROM tb_user_draft a, tb_jabatan b, tb_satker c WHERE (id_relasi=b.id_jabatan) AND (b.id_satker=c.id_satker) AND (a.id_jabatan=?) ORDER BY urutan_satker, urutan_jabatan', array($id_jabatan));
    //     $dataArr = [];
    //     foreach ($data['value'] as $kol){$dataArr[$kol['id_relasi']] = $kol['nama_jabatan'];}
    //     return $dataArr;
    // }
	
	// public function getUserDraftPengirim($id_draft, $id_jabatan){ // Untuk Form Tolak Draft
    //     set_time_limit(0);
    //     $data = $this->getData('SELECT a.pengirim_draft_riwayat, b.nama_jabatan FROM tb_draft_riwayat a, tb_jabatan b WHERE (a.pengirim_draft_riwayat=b.id_jabatan) AND (a.id_draft = ?) AND (a.tujuan_draft_riwayat = ?) ORDER BY id_draft_riwayat DESC', array($id_draft, $id_jabatan));
    //     $dataArr = [];
    //     foreach ($data['value'] as $kol){$dataArr[$kol['pengirim_draft_riwayat']] = $kol['nama_jabatan'];}
	// 	return $dataArr;
    // }

	// public function getDraftNotifMasuk($id_jabatan){
    //     set_time_limit(0);
    //     $idKey = array('0', $id_jabatan);
    //     $data = $this->getData('SELECT * FROM tb_draft_riwayat a, tb_draft b, tb_jabatan c WHERE (a.id_draft=b.id_draft) AND (a.tujuan_draft_riwayat=c.id_jabatan) AND (status_draft_riwayat = ?) AND (tujuan_draft_riwayat = ?)  ORDER BY waktu_draft_riwayat DESC', $idKey);
    //     if($data['count'] > 0) return $data['value'];
	// 	else return [];
    // }
	
	// public function getDraftMasukJabatan($id_jabatan){ // Device
    //     set_time_limit(0);
	// 	$query = 'SELECT * FROM tb_draft_riwayat a, tb_draft b, tb_jabatan c, tb_jenis d WHERE (a.id_draft=b.id_draft) AND (a.pengirim_draft_riwayat=c.id_jabatan) AND (b.id_jenis=d.id_jenis) AND (tujuan_draft_riwayat = ?) ORDER BY waktu_draft_riwayat DESC';        
    //     $idKey = array($id_jabatan);
    //     $jmlData = $this->getData($query, $idKey);
    //     $dataArr = $this->getData($query, $idKey);
    //     $result['no'] = 1;
    //     $result['jmlData'] = $jmlData['count'];
    //     $result['dataDraft'] = $dataArr['value'];
	// 	$result['query'] = $dataArr['query'];
	// 	$result['query'] = '';
    //     return $result;
    // }
	
	// public function getDraftKeluarJabatan($id_jabatan){ // Device
    //     set_time_limit(0);
	// 	$query = 'SELECT * FROM tb_draft_riwayat a, tb_draft b, tb_jabatan c, tb_jenis d WHERE (a.id_draft=b.id_draft) AND (a.tujuan_draft_riwayat=c.id_jabatan) AND (b.id_jenis=d.id_jenis) AND (pengirim_draft_riwayat = ?) ORDER BY waktu_draft_riwayat DESC';
    //     $idKey = array($id_jabatan);
    //     $jmlData = $this->getData($query, $idKey);
    //     $dataArr = $this->getData($query, $idKey);
    //     $result['no'] = 1;
    //     $result['jmlData'] = $jmlData['count'];
    //     $result['dataDraft'] = $dataArr['value'];
	// 	$result['query'] = $dataArr['query'];
	// 	$result['query'] = '';
    //     return $result;
    // }
	
	// public function getTabelDraftMasuk($data){
    //     set_time_limit(0);
	// 	$cari = '%'.$data['cari'].'%';
    //     $page = $data['page'];
		
	// 	$query = 'SELECT * FROM tb_draft_riwayat a LEFT JOIN tb_draft b ON (a.id_draft=b.id_draft) LEFT JOIN tb_jabatan c ON (a.pengirim_draft_riwayat=c.id_jabatan) LEFT JOIN tb_jenis d ON (b.id_jenis=d.id_jenis) ';
	// 	$query .= 'WHERE (tujuan_draft_riwayat = ?) AND ('.$data['pil_cari'].' LIKE ?) ORDER BY waktu_draft_riwayat DESC';
	// 	$idKey = array($data['id_jabatan'], $cari);
		
    //     $batas = 20;
    //     $posisi = ($page - 1) * $batas;
    //     $jmlData = $this->getData($query, $idKey);
    //     $dataArr = $this->getData($query.' LIMIT '.$posisi.','.$batas, $idKey);
    //     $result['no'] = $posisi + 1;
    //     $result['page'] = $page;
    //     $result['batas'] = $batas;
    //     $result['jmlData'] = $jmlData['count'];
    //     $result['dataDraft'] = $dataArr['value'];
	// 	$result['query'] = $dataArr['query'];
	// 	$result['query'] = '';
    //     return $result;
    // }
	
	// public function getTabelDraftKeluar($data){
    //     set_time_limit(0);
    //     $cari = '%'.$data['cari'].'%';
    //     $page = $data['page'];
        
	// 	$query = 'SELECT * FROM tb_draft_riwayat a LEFT JOIN tb_draft b ON (a.id_draft=b.id_draft) LEFT JOIN tb_jabatan c ON (a.tujuan_draft_riwayat=c.id_jabatan) LEFT JOIN tb_jenis d ON (b.id_jenis=d.id_jenis) ';
	// 	$query .= 'WHERE (pengirim_draft_riwayat = ?) AND ('.$data['pil_cari'].' LIKE ?) ORDER BY waktu_draft_riwayat DESC';
	// 	$idKey = array($data['id_jabatan'], $cari);
		
    //     $batas = 20;
    //     $posisi = ($page - 1) * $batas;
    //     $jmlData = $this->getData($query, $idKey);
    //     $dataArr = $this->getData($query.' LIMIT '.$posisi.','.$batas, $idKey);
    //     $result['no'] = $posisi + 1;
    //     $result['page'] = $page;
    //     $result['batas'] = $batas;
    //     $result['jmlData'] = $jmlData['count'];
    //     $result['dataDraft'] = $dataArr['value'];
    //     $result['query'] = $dataArr['query'];
	// 	$result['query'] = '';
    //     return $result;
    // }

}
?>