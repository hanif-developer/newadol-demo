<div class="form-group satker">
	<div class="input-group select2-bootstrap-prepend">
		<span class="input-group-addon login-red" style="width:20%;">Satker</span>
		<?= comp\BOOTSTRAP::inputSelect('satker', $pilihan_satker, '', 'class="form-control select2" style="width: 100%;" required'); ?>
	</div>
</div>
<div class="form-group jabatan">
	<div class="input-group select2-bootstrap-prepend">
		<span class="input-group-addon login-red" style="width:20%;">Jabatan</span>
		<?= comp\BOOTSTRAP::inputSelect('jabatan', $pilihan_jabatan, '', 'class="form-control select2" style="width: 100%;" required'); ?>
	</div>
</div>