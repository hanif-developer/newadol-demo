<body class="login">
    <div class="background-filter"></div>
    <div class="logo">
        <a><img src="<?= $this->link($this->logo_adol_text); ?>" style="height: 80px;" alt="" /> </a>
    </div>
    <div class="content">
        <div class="pilihan">
            <form id="frmLogin" onsubmit="return false" autocomplete="off">
                <?= comp\BOOTSTRAP::inputKey('kategori', $kategori); ?>
                <div class="init">
                    <div class="loader"></div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn red btn-block mt-ladda-btn ladda-button btn-login"
                        data-style="expand-left">
                        <span class="ladda-label">Next</span>
                    </button>
                </div>
            </form>
        </div>
        <div class="password"></div>
    </div>
<?= $jsPath; ?>
<script src="<?= $api_path.'/script'; ?>"></script>
<script src="<?= $url_path.'/script'; ?>"></script>
</body>