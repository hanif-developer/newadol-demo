<form id="frmPassword" onsubmit="return false" autocomplete="off">
    <div class="form-group">
        <img class="login-img" style="border-radius: 0;" src="<?= $data_satker['url_logo_satker']; ?>" alt="">
    </div>
    <div class="form-group">
        <?= comp\BOOTSTRAP::inputKey('username', $jabatan); ?>
        <?= comp\BOOTSTRAP::inputText('password', 'password', '', 'class="form-control form-control-solid placeholder-no-fix color-black" style="width:100%" placeholder="Password" required'); ?>
    </div>
    <div class="form-actions">
        <button type="submit" class="btn red btn-block uppercase btn-password" data-style="expand-right">Masuk</button>
    </div>
    <div class="form-actions">
        <div class="pull-left">
            <a id="login-kembali">Kembali</a>
        </div>
    </div>
</form>


