<div class="form-group">
	<div class="input-group select2-bootstrap-prepend">
		<span class="input-group-addon login-red" style="width: 20%;">Regional</span>
		<?= comp\BOOTSTRAP::inputSelect('regional', $pilihan_regional, $regional, 'class="form-control select2" required'); ?>
	</div>
</div>
<?php if($regional !== 'MBS'){ ?>
<div class="form-group">
	<div class="input-group select2-bootstrap-prepend">
		<span class="input-group-addon login-red" style="width: 20%;">Kategori</span>
		<?= comp\BOOTSTRAP::inputSelect('kategori', $pilihan_kategori, $kategori, 'class="form-control select2" required'); ?>
	</div>
</div>
<?php }else{ ?>
<?= comp\BOOTSTRAP::inputKey('kategori', $kategori); ?>
<?php } ?>