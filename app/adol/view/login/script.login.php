login = {
    init: function(){
        url_path = "<?= $url_path; ?>";
        setTimeout(function(){
            $.get(url_path+"/init", function(data){
                $(".init").html(data);
                $(".select2").select2();
                $(".btn-login").show();
            });
        }, 500);
        $(document).on("change", "#regional, #kategori", function(event){
            event.preventDefault();
            if(regional != "" && kategori != ""){
                login.loadSatker($("#regional").val(), $("#kategori").val());
            }
        });
        $(document).on("change", "#satker", function(event){
            event.preventDefault();
            login.loadJabatan($("#satker").val());
        });
        $(document).on("submit", "#frmLogin", function(event){
            event.preventDefault();
            login.loadPassword();
        });
        $(document).on("submit", "#frmPassword", function(event) {
            event.preventDefault();
            login.validate(this);
        });
        $(document).on("click", "#login-kembali", function(event) {
            event.preventDefault();
            $(".pilihan").show();
			$(".password").html("");
        });
    },
    loadSatker: function(regional, kategori){
        if(regional !== "" && kategori !== ""){
            $(".satker").html(loader);
            $.post(url_path+"/satker", {regional : regional, kategori : kategori}, function(data){
                $(".satker").html(data);
                $(".select2").select2();
            });
        }
        else{
            $(".satker").html("");
            $(".jabatan").html("");
        }
    },
    loadJabatan: function(satker){
        if(satker !== ""){
            $(".jabatan").html(loader);
            $.post(url_path+"/jabatan", {satker : satker}, function(data){
                $(".jabatan").html(data);
                $(".select2").select2();
            });
        }
        else{
            $(".jabatan").html("");
        }
    },
    loadPassword: function(){
        $(".pilihan").hide();
        $(".password").html("<div class='loader'></div>");
        setTimeout(function(){
            $.post(url_path+"/password", $("#frmLogin").serialize(), function(data){
                $(".password").html(data);
            });
        }, 500);
    },
    validate: function(obj){
        $(".btn-password").html(loader);
        setTimeout(function(){
            app.sendData({
                url: "/user/login",
                data: app.serializeArraytoJson($(obj).serializeArray()),
                token: "<?= $this->token; ?>",
                onSuccess: function(response){
                    // console.log(response);
                    $(".btn-password").html(response.data.message);
                    app.createCookie(response.data.session_name, response.data.session_value.session_id);
                    window.location.reload();
                },
                onError: function(error){
                    // console.log(error);
                    $(".btn-password").html(error.message.text);
                }
            });
        }, 1000);
    },
};

login.init();