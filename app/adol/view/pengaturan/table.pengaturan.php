<div class="table-responsive">
    <table class="table table-default table-hover">
        <thead>
            <tr>
                <th width="50px">No</th>
                <th>Jabatan</th>
                <th>Satker</th>
                <th width="100px">Action</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{no}</td>
                <td>{nama_jabatan}</td>
                <td>
                    <strong>{singkatan_satker}</strong> <br>
                    <small>{nama_satker}</small>
                </td>
                <td style="vertical-align: top;">
                    <a title="Hapus Data" id="{id_user_jabatan}:{id_jabatan}" class="btn btn-danger btn-delete" style="margin: 5px 5px 0px; font-size: 12pt;" data-status="{status_action}" data-message="Yakin data user {nama_jabatan} akan dihapus ?"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="table-pagging" style="border-top: 1px solid #ccc; padding-top: 10px;">
    <ul class="pagination">
        <li class="pagging page-item" number-page="" style="cursor: pointer;"><span class="page-link">{page}</span></li>
    </ul>
</div>