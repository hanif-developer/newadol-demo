pengaturan = {
    init: function(){
        url_path = "<?= $url_path; ?>";
        modul = app.initModul("#pengaturan");
        modulTab = {};

        $(document).on("click", ".btn-add", function(event) {
            event.preventDefault();
            let id_jabatan = $("#id_jabatan").val();
            let id_relasi = $("#pil_user_jabatan").val();
            let status_action = $("#status_action").val();
            if (id_relasi == "") {
                alert("Pilih jabatan terlebih dahulu");
            }
            else {
                let loader = app.createLoader(modul.table.content, "Menambahkan data ...");
                setTimeout(function(){  
                    app.sendData({
                        url: "/user/action/save/",
                        data: JSON.stringify({id_jabatan, id_relasi, status_action}),
                        token: "<?= $this->token; ?>",
                        onSuccess: function(response){
                            // console.log(response);
                            loader.hide();
                            main.showMessage(response, function(){
                                pengaturan.showTable();
                            });
                        },
                        onError: function(error){
                            // console.log(error);
                            loader.hide();
                            main.showMessage(error);
                        }
                    });
                }, 500);
            }
        });

        $(document).on("click", ".btn-delete", function(event){
            event.preventDefault();
            let id = this.id;
            let message = this.getAttribute("data-message");
            let status = this.getAttribute("data-status");
            if(confirm(message)){
                let loader = app.createLoader(modul.table.content, "Menghapus data ...");
                setTimeout(function(){
                    app.sendData({
                        url: "/user/action/delete/",
                        data: JSON.stringify({id: id, status: status}),
                        token: "<?= $this->token; ?>",
                        onSuccess: function(response){
                            // console.log(response);
                            loader.hide();
                            main.showMessage(response, function(){
                                pengaturan.showTable();
                            });
                        },
                        onError: function(error){
                            // console.log(error);
                            loader.hide();
                            main.showMessage(error);
                        }
                    });
                }, 500);
            }
        });
    },
    showTable: function(){
        modul.table.content.hide();
        modul.table.empty.hide();
        modul.table.loader.show();
        // console.log(modul.table.action.serializeArray());
        setTimeout(function(){
            app.sendData({
                url: "/user/action/list/",
                data: app.serializeArraytoJson(modul.table.action.serializeArray()),
                token: "<?= $this->token; ?>",
                onSuccess: function(response){
                    // console.log(response);
                    modul.table.loader.hide();
                    app.createTable({
                        table: modul.table,
                        data: response.data,
                        onShow: function(content){
                            content.find("[data-toggle='tooltip']").tooltip();
                        },
                        onPagging: function(page){
                            modul.table.action.find("#page").val(page);
                            pengaturan.showTable();
                            $(document).scrollTop(0);
                        }
                    });
                },
                onError: function(error){
                    // console.log(error);
                    main.showMessage(error);
                }
            });
        }, 500);
    },
};

pengaturan.init();