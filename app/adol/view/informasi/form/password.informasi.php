<form role="form" onsubmit="return informasi.updateData(this, 'password');" autocomplete="off">
	<div class="form-group">
		<label class="control-label">Kata sandi sekarang</label>
		<?= comp\BOOTSTRAP::inputKey('id_user', $dataUser['id_user']); ?>
		<?= comp\BOOTSTRAP::inputText('password_user', 'password', '', 'class="form-control" required'); ?>
	</div>
	<div class="form-group">
		<label class="control-label">Kata sandi baru</label>
		<?= comp\BOOTSTRAP::inputText('password_baru', 'password', '', 'class="form-control" required'); ?>
	</div>
	<div class="form-group">
		<label class="control-label">Konfirmasi kata sandi baru</label>
		<?= comp\BOOTSTRAP::inputText('password_lagi', 'password', '', 'class="form-control" required'); ?>
	</div>
	<div class="form-actions">
		<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Ubah</button>
	</div>
</form>                               
