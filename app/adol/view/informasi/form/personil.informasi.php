<form role="form" onsubmit="return informasi.updateData(this, 'profil');" autocomplete="off">
	<div class="form-group">
		<label class="control-label">NRP</label><br />
		<div class="input-group col-md-6">
			<?= comp\BOOTSTRAP::inputKey('id_user', $dataUser['id_user']); ?>
			<?= comp\BOOTSTRAP::inputText('id_personil', 'text', $dataUser['id_personil'], 'class="form-control" style="width:100%" required'); ?>
			<span class="input-group-btn">
				<button id="btnSIPP" class="btn blue" type="button"> Cek NRP </button>
			</span>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label">Jabatan</label>
		<?= comp\BOOTSTRAP::inputText('jabatan_personil', 'text', $dataPersonil['jabatan_personil'], 'class="form-control" readonly'); ?>
	</div>
	<div class="form-group">
		<label class="control-label">Pangkat</label>
		<?= comp\BOOTSTRAP::inputText('pangkat_personil', 'text', $dataPersonil['pangkat_personil'], 'class="form-control" required'); ?>
	</div>                                       
	<div class="form-group">
		<label class="control-label">Nama Lengkap</label>
		<?= comp\BOOTSTRAP::inputText('nama_personil', 'text', $dataPersonil['nama_personil'], 'class="form-control" required'); ?>
	</div>
	<div class="form-group">
		<label class="control-label">Email</label>
		<?= comp\BOOTSTRAP::inputText('email_personil', 'text', $dataPersonil['email_personil'], 'class="form-control" required'); ?>
	</div>
	<div class="form-group">
		<label class="control-label">No HP</label>
		<?= comp\BOOTSTRAP::inputText('telp_personil', 'text', $dataPersonil['telp_personil'], 'class="form-control" required'); ?>
	</div>
	<div class="form-actions">
		<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
	</div>
</form>                               
