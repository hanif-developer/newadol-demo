<div class="table-content">
    <div class="table-responsive">
        <table class="table table-default table-hover">
            <thead>
                <tr>
                    <th>No</th>
                    <th>NIP/NRP</th>
                    <th>Nama Personil</th>
                    <th>Jabatan</th>
                    <!-- <th>Phone</th> -->
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{no}</td>
                    <td>{id_personil}</td>
                    <td>{nama_personil}</td>
                    <td>{nama_jabatan}</td>
                    <!-- <td>{phone_type}</td> -->
                </tr>
            </tbody>
        </table>
    </div>
    <div class="table-pagging" style="border-top: 1px solid #ccc; padding-top: 10px;">
        <ul class="pagination">
            <li class="pagging page-item" number-page="" style="cursor: pointer;"><span class="page-link">{page}</span></li>
        </ul>
    </div>
</div>
<div class="card-body">
    <div class="query" style="font-size:10pt;"></div>
    <div class="text-center">
        <div class="spinner-border table-loader" role="status"><div class="loader"></div></div>
    </div>
    <div class="jumbotron jumbotron-fluid text-center table-empty">
		<div class="containers">
			<img src="assets/pages/img/empty-result.png" style="width:100px;" alt="">
			<h3><strong>No active user</strong></h3>
		</div>
	</div>
</div>