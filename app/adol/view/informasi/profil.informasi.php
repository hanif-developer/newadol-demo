<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <?php $this->getView('adol', 'main', 'header', []); ?>
    <?php $this->getView('adol', 'main', 'modal', 'form'); ?>
    <div class="clearfix"> </div>
    <div class="page-container">
        <?php $this->getView('adol', 'main', 'navbar', []); ?>
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li><span>Index</span><i class="fa fa-circle"></i></li>
                        <li><span>Informasi</span><i class="fa fa-circle"></i></li>
                        <li><span><?= $active_page; ?></span></li>
                    </ul>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-5">
                        <div class="portlet light bordered">
                            <div class="profile-userpic profile-square">
                                <img src="<?= $user_info['logo_satker']; ?>" class="img-responsive" style="border-radius:0px !important" alt="">
                            </div>
                            <div class="profile-usertitle">
                                <div class="profile-usertitle-name"> <?= $user_info['nama_jabatan'];?> </div>
                                <div class="profile-usertitle-job"> <?= empty($user_info['nama_personil']) ? $user_info['nama_satker'] : $user_info['nama_personil'].'<br>'.'NRP. '.$user_info['nrp_personil']; ?></div>
                                <div class="profile-desc-text" style="margin-top: 50px;"> Silahkan perbarui keterangan profil Anda di kolom yang telah disediakan. </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div id="informasi" class="portlet light bordered">
                            <div class="portlet-title tabbable-line">
                                <div class="caption caption-md">
                                    <i class="icon-globe theme-font hide"></i>
                                    <span class="caption-subject font-blue-madison bold uppercase"><?= $page_title; ?></span>
                                </div>
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-target="#tab_data_personil" data-toggle="tab"> Data Personil </a></li>
                                    <li><a data-target="#tab_data_password" data-toggle="tab"> Ubah Kata Sandi </a></li>
                                </ul>
                            </div>
                            <div class="portlet-body">
                                <?php extract($dataUser); ?>
                                <?php extract($dataPersonil); ?>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_data_personil">
                                        <div class="portlet-body">
                                            <?php $this->form('personil') ?>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_data_password">
                                        <div class="portlet-body">
                                            <?php $this->form('password') ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div style="height: 250px; border: 0px solid red;"></div>
                <?php $this->getView('adol', 'main', 'footer', []); ?>
            </div>
        </div>
    </div>
    <?= $jsPath; ?>
    <script src="<?= $api_path.'/script'; ?>"></script>
    <script src="<?= $main_path.'/script'; ?>"></script>
    <script src="<?= $url_path.'/script'; ?>"></script>
</body>