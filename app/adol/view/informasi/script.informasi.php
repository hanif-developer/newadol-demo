informasi = {
    init: function(){
        url_path = "<?= $url_path; ?>";
        modul = app.initModul("#informasi");
        modulTab = {};

        $(document).on("change", "#id_personil", function(event) {
            event.preventDefault();
            informasi.checkSIPP();
        });

        $(document).on("click", "#btnSIPP", function(event) {
            event.preventDefault();
            informasi.checkSIPP();
        });

        $(document).on("shown.bs.tab", "#informasi a[data-toggle='tab']", function(){
            let url = $(this).data("url");
            let target = $(this).data("target");
            let action = $(this).data("action");
            let user_count = $(this).find(".user_count");
            // console.log(action);

            if(modulTab.hasOwnProperty(target) == false){
                modulTab[target] = app.initModul(target);
            }

            let tabs_modul = modulTab[target];
            if(action == "table"){
                if(tabs_modul.table.content.length == 0) return;
                tabs_modul.table.content.hide();
                tabs_modul.table.empty.hide();
                tabs_modul.table.loader.show();
            }
            
            if(action == "chart"){
                informasi.createChart({
                    chart: "#chart-user-mobile",
                    data: JSON.parse(window.atob($(this).data("chart"))),
                });
            }

            if(url === undefined) return;
            setTimeout(function(){
                app.sendData({
                    url: url,
                    data: JSON.stringify({session_id: session_id}),
                    token: "<?= $this->token; ?>",
                    onSuccess: function(response){
                        // console.log(response);
                        if(action == "table"){
                            user_count.html(response.data.count);
                            tabs_modul.table.loader.hide();
                            app.createTable({
                                table: tabs_modul.table,
                                data: response.data,
                                onShow: function(content){
                                    // content.find("[data-toggle='tooltip']").tooltip();
                                },
                                onPagging: function(page){
                                    
                                }
                            });
                        }
                    },
                    onError: function(error){
                        // console.log(error);
                        main.showMessage(error);
                    }
                });
            }, 500);
        });
    },
    checkSIPP: function(){
        let loader = app.createLoader($("#informasi"), "Check data SIPP");
        setTimeout(function(){
            app.sendData({
                url: "/user/sipp",
                data: JSON.stringify({nrp: $("#id_personil").val()}),
                token: "<?= $this->token; ?>",
                onSuccess: function(response){
                    // console.log(response);
                    loader.hide();
                    let data = response.data;
                    $("#jabatan_personil").val(data.jabatan_personil);
                    $("#nama_personil").val(data.nama_personil);
                    $("#pangkat_personil").val(data.pangkat_personil);
                    $("#email_personil").val(data.telp_personil);
                    $("#telp_personil").val(data.telp_personil);
                },
                onError: function(error){
                    // console.log(error);
                    loader.hide();
                    main.showMessage(error);
                    $("#jabatan_personil").val("");
                    $("#nama_personil").val("");
                    $("#pangkat_personil").val("");
                    $("#email_personil").val("-");
                    $("#telp_personil").val("-");
                }
            });
        }, 1000);
    },
    updateData: function(obj, method){
        let formConfirm = {
            profil: {
                title: "Update Personil",
                text: "Klik tombol simpan, untuk menyimpan data",
                confirmButtonText: "Simpan",
                link_success: "<?= $this->link_info.'/profil'; ?>"
            },
            password: {
                title: "Ubah Password",
                text: "Klik tombol ubah, untuk mengubah password",
                confirmButtonText: "Ubah",
                link_success: "<?= $this->link_info.'/profil'; ?>"
            }
        };
        let data = JSON.parse(app.serializeArraytoJson($(obj).serializeArray()));
        data.session_id = session_id;
        swal({
            title: formConfirm[method].title,
            text: formConfirm[method].text,
            type: "info",
            confirmButtonText: formConfirm[method].confirmButtonText,
            cancelButtonText: "Tidak",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            allowEscapeKey : false,
        }, function(){
            setTimeout(function(){
                app.sendData({
                    url: "/user/"+method+"/save",
                    data: JSON.stringify(data),
                    token: "<?= $this->token; ?>",
                    onSuccess: function(response){
                        // console.log(response);
                        main.showMessage(response, function(){
                            window.location = formConfirm[method].link_success
                        });
                    },
                    onError: function(error){
                        // console.log(error);
                        main.showMessage(error);
                    }
                });
            }, 1000);
        });

        return false;
    },
    createChart: function(params){
        var cview = params.chart
        var data = params.data;
        $(cview).highcharts({
                chart: {type: 'bar'},
                title: {text: data.title},
                subtitle: {text: data.subtitle},
                xAxis: {
                    categories: data.xAxis.categories,
                    title: {
                        text: null
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: data.yAxis.title,
                        align: 'high'
                    },
                    labels: {
                        overflow: 'justify'
                    }
                },
                tooltip: {
                    valueSuffix: data.tooltip
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'top',
                    x: -40,
                    y: 80,
                    floating: true,
                    borderWidth: 1,
                    backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                    shadow: true
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                series: data.value
            }
        );
    },
};

informasi.init();