<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <?php $this->getView('adol', 'main', 'header', []); ?>
    <?php $this->getView('adol', 'main', 'modal', 'form'); ?>
    <div class="clearfix"> </div>
    <div class="page-container">
        <?php $this->getView('adol', 'main', 'navbar', []); ?>
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li><span>Index</span><i class="fa fa-circle"></i></li>
                        <li><span>Informasi</span><i class="fa fa-circle"></i></li>
                        <li><span><?= $active_page; ?></span></li>
                    </ul>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12">
                        <div id="informasi" class="portlet light bordered">
                            <div class="portlet-title tabbable-line">
                                <div class="caption caption-md">
                                    <i class="icon-globe theme-font hide"></i>
                                    <span class="caption-subject font-blue-madison bold uppercase"><?= $page_title; ?></span>
                                </div>
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-url="/user/register/mobile" data-action="table" data-target="#tab_data_user_mobile" data-toggle="tab"> <i class="fa fa-android"></i> User Mobile (<span class="user_count">0</span>) </a></li>
                                    <li><a data-chart="<?= base64_encode(json_encode($user_chart)); ?>" data-action="chart" data-target="#tab_data_user_chart" data-toggle="tab"> <i class="fa fa-bar-chart"></i> User Chart </a></li>
                                </ul>
                            </div>
                            <div class="portlet-body">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_data_user_mobile">
                                        <div class="portlet-body">
                                            <?php $this->table('register', 'mobile') ?>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_data_user_chart">
                                        <div class="portlet-body">
                                            <div id="chart-user-mobile" class="chart" style="margin: 10px; height: auto;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div style="height: 250px; border: 0px solid red;"></div>
                <?php $this->getView('adol', 'main', 'footer', []); ?>
            </div>
        </div>
    </div>
    <?= $jsPath; ?>
    <script src="<?= $api_path.'/script'; ?>"></script>
    <script src="<?= $main_path.'/script'; ?>"></script>
    <script src="<?= $url_path.'/script'; ?>"></script>
    <script>$("a[data-target='#tab_data_user_mobile']").trigger("shown.bs.tab");</script>
</body>