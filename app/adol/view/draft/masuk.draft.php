<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <?php $this->getView('adol', 'main', 'header', []); ?>
    <?php $this->getView('adol', 'main', 'modal', 'form'); ?>
    <div class="clearfix"> </div>
    <div class="page-container">
        <?php $this->getView('adol', 'main', 'navbar', []); ?>
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li><span>Index</span><i class="fa fa-circle"></i></li>
                        <li><span>Draft</span><i class="fa fa-circle"></i></li>
                        <li><span><?= $active_page; ?></span></li>
                    </ul>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div id="draft" class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption"><?= $page_title; ?></div>
                            </div>
                            <div class="portlet-body">
                                <form class="frmData form-horizontal" onsubmit="return false;" autocomplete="off">
                                    <?= comp\BOOTSTRAP::inputKey('action', "masuk"); ?>
                                    <?= comp\BOOTSTRAP::inputKey('page', 1); ?>
                                    <div class="form-group">
                                        <div class="col-md-4" style="margin-bottom: 5px;">
                                            <div class="input-group select2-bootstrap-prepend">
                                                <span class="input-group-addon">Cari berdasarkan</span>
                                                <?= comp\BOOTSTRAP::inputSelect('pil_cari', $pilihan_cari, $pil_cari, 'class="form-control select2"'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-3" style="margin-bottom: 5px;">
                                            <?= comp\BOOTSTRAP::inputText('cari', 'text', '', 'class="form-control" style="width:100%;" placeholder="Cari disini.."'); ?>
                                        </div>
                                        <div class="col-md-3" style="margin-bottom: 5px;">
                                        <?= comp\BOOTSTRAP::inputSelect('status_baca', $pilihan_status, '-1', 'class="form-control select2"'); ?>
                                        </div>
                                    </div>
                                </form>
                                <div class="table-content">
                                    <?php $this->table('masuk'); ?>
                                </div>
                                <div class="card-body">
                                    <div class="query" style="font-size:10pt;"></div>
                                    <div class="text-center">
                                        <div class="spinner-border table-loader" role="status"><div class="loader"></div></div>
                                    </div>
                                    <div class="jumbotron jumbotron-fluid text-center table-empty">
                                        <div class="container">
                                            <img src="assets/pages/img/empty-result.png" style="width:100px;" alt="">
                                            <h3><strong>No results founds</strong></h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-content"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div style="height: 250px; border: 0px solid red;"></div>
                <?php $this->getView('adol', 'main', 'footer', []); ?>
            </div>
        </div>
    </div>
    <?= $jsPath; ?>
    <script src="<?= $api_path.'/script'; ?>"></script>
    <script src="<?= $main_path.'/script'; ?>"></script>
    <script src="<?= $url_path.'/script'; ?>"></script>
    <script>draft.showTable();</script>
</body>