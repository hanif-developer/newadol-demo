<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <?php $this->getView('adol', 'main', 'header', []); ?>
    <?php $this->getView('adol', 'main', 'modal', 'form'); ?>
    <div class="clearfix"> </div>
    <div class="page-container">
        <?php $this->getView('adol', 'main', 'navbar', []); ?>
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li><span>Index</span><i class="fa fa-circle"></i></li>
                        <li><span>Draft</span><i class="fa fa-circle"></i></li>
                        <li><span>Masuk</span><i class="fa fa-circle"></i></li>
                        <li><span><?= $active_page; ?></span></li>
                    </ul>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div id="draft" class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption"><?= $page_title; ?></div>
                            </div>
                            <div class="portlet-body">
                                <?php extract($dataDraft); ?>
                                <?php //$this->debugResponse($dataDraft); ?>
                                <div class="tabbable-line">
                                    <ul class="nav nav-tabs ">
                                        <li class="active"><a data-id="<?= $id_draft_riwayat; ?>" data-target="#tab_konsep_draft" data-toggle="tab"> KONSEP DRAFT </a></li>
                                        <li><a data-id="<?= $id_draft; ?>" data-url="/draft/list/riwayat" data-target="#tab_riwayat_draft" data-toggle="tab"> RIWAYAT DRAFT </a></li>
                                        <li><a data-id="<?= $id_adm; ?>" data-url="/adm/list/riwayat" data-target="#tab_riwayat_adm" data-toggle="tab"> RIWAYAT ADMINISTRASI </a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_konsep_draft">
                                            <div class="portlet-body form">
                                                <form action="#" class="form-horizontal form-bordered">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="text-label col-md-3">Status</label>
                                                            <div class="col-md-9"><?= $status_draft; ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-label col-md-3">Aksi</label>
                                                            <div class="col-md-9"><?= $aksi_draft; ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-label col-md-3">Jenis draft</label>
                                                            <div class="col-md-9"><?= $nama_jenis; ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-label col-md-3">Pengirim</label>
                                                            <div class="col-md-9"><?= $nama_pengirim; ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-label col-md-3">Penerima</label>
                                                            <div class="col-md-9"><?= $nama_tujuan; ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-label col-md-3">Tanggal</label>
                                                            <div class="col-md-9"><?= $tanggal_draft; ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-label col-md-3">Perihal</label>
                                                            <div class="col-md-9"><?= $perihal_draft; ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-label col-md-3">Lampiran</label>
                                                            <div class="col-md-9">
                                                                <?= (!empty($lampiran_draft)) ? '<span class="btn btn-primary" onclick="draft.showDocument(\'File Lampiran\', \''.$lampiran_draft.'\');">Tampilkan</span>' : '-' ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-label col-md-3">Keterangan</label>
                                                            <div class="col-md-9"><?= $keterangan_draft; ?></div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <a href="<?= $this->link_draft.'/masuk'; ?>" class="btn red" style="margin: 5px 5px 0px;"><i class="fa fa-reply"></i> Kembali</a>
                                                        <?php 
                                                        $color = ['default', 'primary', 'danger', 'warning', 'success', 'info'];
                                                        $index = 0;
                                                        foreach ($user_session['draft_user_action'] as $key => $value) {
                                                            if($value == 1 && $status_draft != "Final") {
                                                                $params = [
                                                                    'id_draft' => $id_draft,
                                                                    'id_adm' => $id_adm,
                                                                    'id_jenis' => $id_jenis,
                                                                    'jenislain_draft' => $jenislain_draft,
                                                                    'perihal_draft' => $perihal_draft,
                                                                    'user_action' => $key,
                                                                    'template_draft' => $template_draft,
                                                                    'form_upload' => 1,
                                                                ];
                                                                // $this->debugResponse($params);
                                                                $buttonLabel = ($key == 'buat') ? 'Edit' : $key;
                                                                echo '<button type="button" class="btn btn-'.$color[$index].' btnUserAction" style="margin: 5px 5px 0px;" data-confirm=true data-params="'.base64_encode(json_encode($params)).'">'.ucfirst($buttonLabel).'</button>';
                                                            }
                                                            $index++;
                                                        }
                                                        ?>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab_riwayat_draft">
                                            <div class="portlet-body">
                                                <?php $this->riwayat(); ?>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab_riwayat_adm">
                                            <div class="portlet-body">
                                                <?php $this->getView('adol', 'administrasi', 'riwayat', ''); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div style="height: 250px; border: 0px solid red;"></div>
                <?php $this->getView('adol', 'main', 'footer', []); ?>
            </div>
        </div>
    </div>
    <?= $jsPath; ?>
    <script src="<?= $api_path.'/script'; ?>"></script>
    <script src="<?= $main_path.'/script'; ?>"></script>
    <script src="<?= $url_path.'/script'; ?>"></script>
</body>