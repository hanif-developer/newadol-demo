<div class="table-responsive">
    <table class="table table-default table-hover">
        <thead>
            <tr>
                <th width="50px">No</th>
                <th width="100px">Status</th>
                <th width="100px">Posisi</th>
                <th width="100px">Pengirim</th>
                <th width="100px">Tanggal</th>
                <th width="100px">Jenis</th>
                <th>Perihal</th>
                <th>Isi</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{no}</td>
                <td><span class="label label-sm label-{status_baca_color}">{status_baca}</span></td>
                <td>{status_draft}</td>
                <td>{nama_pengirim}</td>
                <td>{waktu_draft}</td>
                <td>{jenis_draft}</td>
                <td><a href="<?= $this->link_draft.'/detail/masuk/'; ?>{id_draft_riwayat}">{perihal_draft}</a></td>
                <td>{keterangan_draft}</td>
            </tr>
        </tbody>
    </table>
</div>
<div class="table-pagging" style="border-top: 1px solid #ccc; padding-top: 10px;">
    <ul class="pagination">
        <li class="pagging page-item" number-page="" style="cursor: pointer;"><span class="page-link">{page}</span></li>
    </ul>
</div>