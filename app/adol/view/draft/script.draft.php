draft = {
    init: function(){
        url_path = "<?= $url_path; ?>";
        jenis_template_draft = <?= json_encode($jenis_template_draft); ?>;
        modul = app.initModul("#draft");
        modulTab = {};

        // Pdf Generator
        pdf = null;
        max_pages = 100;
        page_count = 0;

        $(document).on("change", "#id_jenis", function(event) {
            event.preventDefault();
            $(".divjenislain").hide();
            if($(this).val() === "JNSXX") $(".divjenislain").show();
        });

        $(document).on("change", "#pil_cari", function(event) {
            event.preventDefault();
            if(this.value === "tanggal_draft" || this.value === "waktu_draft_riwayat")
                $("#cari").datepicker({
                    format: "yyyy-mm-dd",
                    language: "id",
                    todayHighlight: 1,
                    autoclose: 1
                }).attr({readonly: "true", placeholder: "Pilih tanggal"});
            else
                $("#cari").removeAttr("readonly").attr({placeholder: "Cari disini.."}).datepicker("remove");
                
            $("#cari").val("").trigger("change");
        });

        $(document).on("change", "#cari, #tembusan, #status_baca", function(event) {
            event.preventDefault();
            modul.table.action.find("#page").val("1");
            draft.showTable();
        });

        $(document).on("submit", "#frmInput", function() {
            let params = JSON.parse(app.serializeArraytoJson($(this).serializeArray()));
            // console.log(params);
            draft.showFormConfirm(params);
        });

        $(document).on("click", ".btnUserAction", function() {
            const params = JSON.parse(window.atob($(this).data("params")));
            const confirm = $(this).data("confirm");
            // console.log(params);
            if (params.user_action === "selesai") {
                let loader = app.createLoader(modul.main, "Finishing Draft");
                setTimeout(function(){
                    let formData = new FormData();
                    formData.append("session_id", session_id);
                    $.each(params, (key, value) => { formData.append(key, value); });
                    draft.simpanDraft({ 
                        url: "/draft/"+params.user_action+"/save", 
                        data: formData, 
                        loader: loader,
                        onSuccess(response){
                            main.showMessage(response, function(){
                                window.location = url_path+"/masuk";
                            });
                        },
                        onError(error){
                            main.showMessage(error);
                        }
                    });
                }, 1000);
            }
            else {
                if (confirm === true) {
                    draft.showFormConfirm(params);
                }
                else {
                    draft.showFormAction(params);
                }
            }
        });

        $(document).find(".datepicker").datepicker({
            format: "yyyy-mm-dd",
            language: "id",
            todayHighlight: 1,
            autoclose: 1
        });

        $(document).on("shown.bs.tab", "#draft a[data-toggle='tab']", function(){
        // $(document).on("click", "#draft a[data-toggle='tab']", function(){
            let id = $(this).data("id");
            let url = $(this).data("url");
            let preview = $(this).data("preview");
            let target = $(this).data("target");

            if(preview !== undefined){
                draft.showPreview();
            }

            if(url === undefined) return;
            if(modulTab.hasOwnProperty(target) == false){
                modulTab[target] = app.initModul(target);
            }

            let tabs_modul = modulTab[target];
            if(tabs_modul.table.content.length == 0) return;

            tabs_modul.table.content.hide();
            tabs_modul.table.empty.hide();
            tabs_modul.table.loader.show();
            setTimeout(function(){
                app.sendData({
                    url: url,
                    data: JSON.stringify({session_id: session_id, list_request: {id: id}}),
                    token: "<?= $this->token; ?>",
                    onSuccess: function(response){
                        // console.log(response);
                        tabs_modul.table.loader.hide();
                        app.createTable({
                            table: tabs_modul.table,
                            data: response.data,
                            onShow: function(content){
                                // content.find("[data-toggle='tooltip']").tooltip();
                            },
                            onPagging: function(page){
                                
                            }
                        });
                    },
                    onError: function(error){
                        // console.log(error);
                        main.showMessage(error);
                    }
                });
            }, 500);
        });

        tinymce.init({
            selector: ".editable",
            menubar: false,
            inline: true,
            toolbar: [
                "undo redo | bold italic underline | fontselect fontsizeselect | forecolor backcolor | alignleft aligncenter alignright alignfull | outdent indent",
            ],
            force_br_newlines: true,
            force_p_newlines: false,
            forced_root_block: '', // Needed for 3.x,
        });
    },
    showTable: function(){
        modul.table.content.hide();
        modul.table.empty.hide();
        modul.table.loader.show();
        setTimeout(function(){
            app.sendData({
                url: "/draft/list/"+$("#action").val(),
                data: JSON.stringify({session_id: session_id, list_request: JSON.parse(app.serializeArraytoJson(modul.table.action.serializeArray()))}),
                token: "<?= $this->token; ?>",
                onSuccess: function(response){
                    // console.log(response);
                    modul.table.loader.hide();
                    app.createTable({
                        table: modul.table,
                        data: response.data,
                        onShow: function(content){
                            // content.find("[data-toggle='tooltip']").tooltip();
                        },
                        onPagging: function(page){
                            modul.table.action.find("#page").val(page);
                            draft.showTable();
                            $(document).scrollTop(0);
                        }
                    });
                },
                onError: function(error){
                    // console.log(error);
                    main.showMessage(error);
                }
            });
        }, 500);
    },
    showDocument(title, url){
        modul.formModal.title.html(title);
        modul.formModal.content.html('<div class="preview-modal-content"><div class="loader"></div></div>');
        modul.formModal.modal.find("button[type='submit']").html("Download");
        // modul.formModal.modal.modal({ backdrop: "static", keyboard: false });
        modul.formModal.modal.modal("show");
        modul.formModal.modal.on("shown.bs.modal", function(){
            setTimeout(function(){
                app.showPDF({
                    container: modul.formModal.content.find(".preview-modal-content"),
                    url: url,
                });
            }, 500);
        });
        modul.formModal.modal.on("hidden.bs.modal", function(){
            modul.formModal.modal.off(); // Fix multiple times load 
        });
        modul.formModal.action.off();
        modul.formModal.action.on("submit", function(event){
            event.preventDefault();
            let download = document.createElement("a");
            download.target = "_blank";
            download.href = url;
            download.click();
        });
    },
    showFormConfirm: function(params){
        // console.log(params);
        // console.log(jenis_template_draft);
        if($.inArray(params.user_action, ["buat", "revisi"]) > -1 && $.inArray(params.id_jenis, jenis_template_draft) > -1) {
            swal({
                title: "Draft Editor",
                text: "Pilih metode pembuatan/edit draft",
                type: "info",
                confirmButtonText: "Edit Template",
                cancelButtonText: "Upload",
                showCancelButton: true,
                allowEscapeKey : false,
            }, function(isConfirm){
                if (isConfirm) {
                    console.log("editor");
                    console.log(params);
                    let form = $("<form>").attr({method: "post", action: url_path+"/buat/editor"});
                    form.append($.map(params, function(value, name) {
                        return $("<input>", {
                            type: "hidden",
                            name,
                            value,
                        });
                    }));
                    $("body").append(form);
                    form.submit();
                }
                else {
                    draft.showFormAction(params);
                }
            });
        }
        else {
            draft.showFormAction(params);
        }
    },
    showFormAction: function(params){
        $.post(url_path+"/form/actiondraft/"+params.user_action, params, function(data){
            // console.log(data)
            modul.formModal.content.html(data);
            modul.formModal.content.find(".bs-select").selectpicker();
            modul.formModal.title.html(modul.formModal.content.find(".form_title").data("text"));
            modul.formModal.modal.find("button[type='submit']").html("Kirim");
            modul.formModal.modal.modal({ backdrop: "static", keyboard: false });
            modul.formModal.modal.modal("show");
            modul.formModal.action.off();
            modul.formModal.action.on("submit", function(event){
                event.preventDefault();
                let loader = app.createLoader(modul.formModal.action, "Mengirim Draft");
                setTimeout(function(){
                    let formData = new FormData(modul.formModal.action[0]);
                    formData.append("session_id", session_id);
                    if(pdf !== null){
                        // console.log("file_draft_riwayat", pdf.output("blob"));
                        // console.log("template_draft", $("#pdf-editor").html());
                        formData.append("file_draft_riwayat", pdf.output("blob"));
                        formData.set("template_draft", window.btoa($("#pdf-editor").html()));
                    }
                    draft.simpanDraft({ 
                        url: "/draft/"+params.user_action+"/save", 
                        data: formData, 
                        loader: loader,
                        onSuccess(response){
                            modul.formModal.modal.modal("hide");
                            main.showMessage(response, function(){
                                window.location = url_path+"/keluar";
                            });
                        },
                        onError(error){
                            modul.formModal.modal.modal("hide");
                            main.showMessage(error);
                        }
                    });
                }, 1000);
            });
        });
    },
    showPreview: function(){
        draft.createPapper(pdf => {
            const iframe = document.createElement("iframe");
            iframe.src = pdf.output("datauristring");
            iframe.classList.add("iframe");
            $("#pdf-preview .iframe-loader").hide();
            $("#pdf-preview .iframe-container").html(iframe);
        });
    },
    simpanDraft: function(params){
        app.sendDataMultipart({
            url: params.url,
            data: params.data,
            token: "<?= $this->token; ?>",
            onSuccess: function(response){
                if (params.loader !== undefined) params.loader.hide();
                console.log(response);
                params.onSuccess(response);
            },
            onError: function(error){
                if (params.loader !== undefined) params.loader.hide();
                console.log(error);
                params.onError(error);
            }
        });

        return false;
    },
    /**
     * Script untuk membuat data dalam div, menjadi halaman A4
     * Jadi, script akan mengambil semua elemen child pada div, lalu
     * akan membuat div seukuran A4, dan memasukan elemen child tsb, ke dalam div
     * dan seterusnya sampai jumlah elemen child yang diambil habis
     */
    snipMe: function() {
        page_count++;
        if (page_count > max_pages) {
            return;
        }
        var long = $(this)[0].scrollHeight - Math.ceil($(this).innerHeight());
        var children = $(this).children().toArray();
        // console.log("long", long);
        // console.log("children", children);
        var removed = [];
        while (long > 0 && children.length > 0) {
            var child = children.pop();
            // console.log("child", child);
            $(child).detach();
            removed.unshift(child);
            // console.log("removed", removed);
            long = $(this)[0].scrollHeight - Math.ceil($(this).innerHeight());
        }
        if (removed.length > 0) {
            var a4 = $('<div class="A4 papper"></div>');
            a4.append(removed);
            $(this).after(a4);
            draft.snipMe.call(a4[0]);
        }
    },
    createPapper: function(callback) {
        const pdfPreview = $("#pdf-editor").clone();
        pdfPreview.find(".A4").removeClass("drafter");
        $("#pdf-editor").addClass("d-none");
        $("#pdf-preview").removeClass("d-none");
        $("#pdf-preview .iframe-loader").show();
        $("#pdf-preview .iframe-container").html(pdfPreview.children());
        const requests = $("#pdf-preview").find(".A4").map((idx, page) => {
            return new Promise((resolve) => {
                resolve(draft.snipMe.call(page));
            });
        })
        
        Promise.all(requests).then(() => {
            setTimeout(() => {
                draft.createPDF($("#pdf-preview").find(".A4"), callback);
            }, 1000);
        });
    },
    createPDF: function(papper, callback) {
        pdf = new jsPDF("P", "mm", "A4");
        const width = pdf.internal.pageSize.width;
        const height = pdf.internal.pageSize.height;
        
    
        const a4 = papper.map(async (idx, page) => {
            $(page).removeClass("papper drafter");
            const canvas = await html2canvas(page, {
                scale: 1
            });
            $(page).addClass("papper drafter");
            return canvas;
        });
    
        Promise.all(a4)
            .then((canvas) => {
                canvas.forEach((image, idx) => {
                    pdf.addImage(image.toDataURL('image/jpeg', 1), 'jpeg', 0, 0, width, height);
                    if (canvas.length > (idx + 1)) pdf.addPage();
                });
                callback(pdf);
                // console.clear();
            })
            .catch(err => console.log(err));
    }
};

draft.init();