<div class="form-body">
    <span class="form_title" data-text="<?= $form_title; ?>"></span>
    <div class="form-group row">
        <?= comp\BOOTSTRAP::inputKey('id_draft', $id_draft); ?>
        <?= comp\BOOTSTRAP::inputKey('id_adm', $id_adm); ?>
        <?= comp\BOOTSTRAP::inputKey('id_jenis', $id_jenis); ?>
        <?= comp\BOOTSTRAP::inputKey('jenislain_draft', $jenislain_draft); ?>
        <?= comp\BOOTSTRAP::inputKey('perihal_draft', $perihal_draft); ?>
        <?= comp\BOOTSTRAP::inputKey('template_draft', $template_draft); ?>
        <?= comp\BOOTSTRAP::inputKey('user_action', $user_action); ?>
        <label class="text-label col-md-3">Kepada</label>
        <div class="col-md-9">
            <?= comp\BOOTSTRAP::inputSelect('tujuan_draft_riwayat[]', $pilihan_tujuan, '', 'class="form-control bs-select" multiple data-actions-box="true" title="-- Pilih Tujuan --" required'); ?>
        </div>
    </div>
    <div class="form-group row">
        <label class="text-label col-md-3">Isi pesan</label>
        <div class="col-md-9">
            <?= comp\BOOTSTRAP::inputTextArea('keterangan_draft_riwayat', '', 'class="form-control" style="height:100px;" required'); ?>
        </div>
    </div>
    <?php if(in_array($user_action, ['buat', 'revisi'])): ?>
    <?php if($form_upload == 1): ?>
    <div class="form-group row">
        <label class="text-label col-md-3">Upload Draft</label>
        <div class="col-md-9">
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <span class="btn green btn-file">
                    <span class="fileinput-new"> Pilih file </span>
                    <span class="fileinput-exists"> Ubah </span>
                    <input id="file_draft_riwayat" type="file" name="file_draft_riwayat" required />
                </span>
                <span class="fileinput-filename"> </span> &nbsp;
                <a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"> </a>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <?php endif; ?>
</div>