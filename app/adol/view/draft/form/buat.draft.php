<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <?php $this->getView('adol', 'main', 'header', []); ?>
    <?php $this->getView('adol', 'main', 'modal', 'form'); ?>
    <div class="clearfix"> </div>
    <div class="page-container">
        <?php $this->getView('adol', 'main', 'navbar', []); ?>
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li><span>Index</span><i class="fa fa-circle"></i></li>
                        <li><span>Draft</span><i class="fa fa-circle"></i></li>
                        <li><span><?= $active_page; ?></span></li>
                    </ul>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div id="administrasi" class="portlet box blue ">
                            <div class="portlet-title">
                                <div class="caption"><?= $page_title; ?></div>
                            </div>
                            <div class="portlet-body form">
                                <!-- <form id="frmInput" action="<?= $this->link_draft.'/buat/editor'; ?>" method="post" class="form-horizontal form-bordered form-row-stripped" onsubmit="return draft.showFormDraft(this);" autocomplete="off"> -->
                                <form id="frmInput" class="form-horizontal form-bordered form-row-stripped" onsubmit="return false;" autocomplete="off">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="text-label col-md-3 text-left">Jenis Draft</label>
                                            <div class="col-md-9">
                                                <?= comp\BOOTSTRAP::inputKey('id_draft', $id_draft); ?>
                                                <?= comp\BOOTSTRAP::inputKey('id_adm', $id_adm); ?>
                                                <?= comp\BOOTSTRAP::inputKey('user_action', $user_action); ?>
                                                <?= comp\BOOTSTRAP::inputKey('form_upload', true); ?>
                                                <?= comp\BOOTSTRAP::inputSelect('id_jenis', $pilihan_jenis, '', 'class="form-control select2" required'); ?>
                                            </div>
                                        </div>
                                        <div class="form-group divjenislain" style="display: none;">
                                            <label class="text-label col-md-3"></label>
                                            <div class="col-md-9">
                                                <?= comp\BOOTSTRAP::inputText('jenislain_draft', 'text', '', 'class="form-control" placeholder="Ketikkan Jenis Lainnya"'); ?>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="text-label col-md-3">Perihal</label>
                                            <div class="col-md-9">
                                                <?= comp\BOOTSTRAP::inputText('perihal_draft', 'text', $perihal_draft, 'class="form-control" required'); ?>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="form-actions">
                                        <button class="btn btn-primary" id="btnInput" type="submit">Upload Draft </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div style="height: 250px; border: 0px solid red;"></div>
                <?php $this->getView('adol', 'main', 'footer', []); ?>
            </div>
        </div>
    </div>
    <?= $jsPath; ?>
    <script src="<?= $api_path.'/script'; ?>"></script>
    <script src="<?= $main_path.'/script'; ?>"></script>
    <script src="<?= $url_path.'/script'; ?>"></script>
</body>