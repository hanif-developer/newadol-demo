<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <?php $this->getView('adol', 'main', 'header', []); ?>
    <?php $this->getView('adol', 'main', 'modal', 'form'); ?>
    <div class="clearfix"> </div>
    <div class="page-container">
        <?php $this->getView('adol', 'main', 'navbar', []); ?>
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li><span>Index</span><i class="fa fa-circle"></i></li>
                        <li><span>Draft</span><i class="fa fa-circle"></i></li>
                        <li><span>Editor</span><i class="fa fa-circle"></i></li>
                    </ul>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div id="draft" class="portlet light bordered">
                            <div class="portlet-title tabbable-line">
                                <div class="caption caption-md">
                                    <i class="icon-globe theme-font hide"></i>
                                    <span
                                        class="caption-subject font-blue-madison bold uppercase"><?= $page_title; ?></span>
                                </div>
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-id="<?= $id_draft_riwayat; ?>"
                                            data-target="#tab_editor_draft" data-toggle="tab"> EDITOR </a></li>
                                    <li><a data-id="<?= $id_draft; ?>" data-preview="pdf"
                                            data-target="#tab_preview_draft" data-toggle="tab"> PREVIEW </a></li>
                                </ul>
                            </div>
                            <div class="portlet-body" style="background-color: #eee; padding-bottom: 8px;">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_editor_draft">
                                        <div id="pdf-editor"><?= base64_decode($template_draft); ?></div>
                                    </div>
                                    <div class="tab-pane" id="tab_preview_draft">
                                        <div id="pdf-preview">
                                            <div class="iframe-loader">
                                                <div class="spinner-border table-loader" role="status"><div class="loader"></div></div>
                                            </div>
                                            <div class="iframe-action">
                                                <button type="button" class="btn btn-primary btnUserAction" style="margin: 10px 0px;" data-confirm=false data-params="<?= base64_encode(json_encode([
                                                    'id_draft' => $id_draft,
                                                    'id_adm' => $id_adm,
                                                    'id_jenis' => $id_jenis,
                                                    'jenislain_draft' => $jenislain_draft,
                                                    'perihal_draft' => $perihal_draft,
                                                    'user_action' => $user_action,
                                                    'template_draft' => $template_draft,
                                                    'form_upload' => false,
                                                ])); ?>">Kirim Draft</button>
                                            </div>
                                            <div class="iframe-container"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <?php $this->getView('adol', 'main', 'footer', []); ?>
            </div>
        </div>
    </div>
    <?= $jsPath; ?>
    <script src="<?= $api_path.'/script'; ?>"></script>
    <script src="<?= $main_path.'/script'; ?>"></script>
    <script src="<?= $url_path.'/script'; ?>"></script>
</body>