<div class="A4 papper drafter">
    <div class="kop space-15">
        <div class="text-center">KEPOLISIAN NEGARA REPUBLIK INDONESIA</div>
        <div class="text-center">DAERAH LAMPUNG</div>
        <div class="text-center"><?= strtoupper($user_info['nama_satker']); ?></div>
    </div>
    <div class="space-2"><br></div>
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="space-15">
                NOTA DINAS <br>
                <span class="nomor">Nomor: <span class="editable single-line">B/ND- /V/DIK. 2.1./2018/Ro
                        SDM</span></span>
            </div>
        </div>
    </div>
    <div class="space-2"><br></div>
    <div style="
        margin: 0 auto;
        width: 15cm;
        ">
        <div class="row space-2">
            <div class="col-md-3">Kepada <span class="float-right">:</span></div>
            <div class="col-md-9 pl-0 editable">Yth. Direktorat Satuan Bhayangkara Polda Lampung</div>
        </div>
        <div class="row space-2">
            <div class="col-md-3">Dari <span class="float-right">:</span></div>
            <div class="col-md-9 pl-0 editable">Kepala <?= ucwords(strtolower($user_info['nama_satker'])); ?> Polda Lampung</div>
        </div>
        <div class="row space-2">
            <div class="col-md-3">Perihal <span class="float-right">:</span></div>
            <div class="col-md-9 pl-0 editable"><u>permintaan personel.</u></div>
        </div>
    </div>
    <div class="space-2"><br></div>
    <div class="space-1"><br></div>
    <div class="row">
        <div class="col-md-12 pl-0">
            <div class="editable space-1">
                1.&nbsp;&nbsp; Rujukan:<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a.&nbsp;&nbsp; Keputusan Kepala Kepolisian
                Negara Republik Indonesia Nomor: Kep/698/XII/2011&nbsp;&nbsp;&nbsp;
                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; tanggal 28 Desember 2011 tentang
                Pedoman Administrasi Ujian Kemampuan
                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jasmani dan&nbsp;&nbsp;&nbsp;
                Pemeriksaan Anthropometrik untuk Penerimaan Pegawai Negeri Polri;<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                b.&nbsp; Surat Telegram Kepala Kepolisian Negara Republik Indonesia Nomor: ST/573
                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; /III/DIK.2.1./2018 tanggal 28
                Februari 2018 tentang Arahan Teknis Pengujian&nbsp;
                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kesamaptaan Jasmani, Renang dan
                Pemeriksaan Anthropometrik Seleksi Penerimaan
                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Anggota Polri T.A.
                2018.<br><br><br>2.&nbsp;&nbsp; Sehubungan dengan rujukan tersebut di atas, dalam rangka penerimaan
                terpadu anggota <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Polri T.A. 2018 Polda Lampung akan melaksanakan Tes
                Kesamaptaan Jasmani yang akan <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; dilaksanakan mulai tanggal 9 s.d. 15
                Mei 2018 bertempat di Stadion Taruna Akpol dan <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kolam Renang Akpol
                Semarang.<br><br>3.&nbsp;&nbsp; Berkaitan dengan hal dimaksud, guna mendukung kelancaran kegiatan
                kiranya Dir. <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; berkenan memerintahkan anggotanya yang bertugas sebagai
                Operator Kesamaptaan <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jasmani dalam giat Penerimaan Anggota Polri
                Terpadu T.A. 2018 agar mengikuti latihan <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; mulai tanggal 7 s.d. 15 Mei
                2018 bertempat di Stadion Taruna Akpol dan Kolam Renang <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Akpol
                Semarang.<br><br>4.&nbsp;&nbsp; Demikian untuk menjadi maklum.
            </div>
            <div class="space-2"><br></div>
            <div class="space-1"><br></div>
        </div>
    </div>
    <div class="ttd">
        <div class="text-center">
            <div class="editable single-lines">xxxxxx, &nbsp;&nbsp;&nbsp;<?= date('M Y'); ?></div>
        </div>
        <div class="space-1"><br></div>
        <div class="text-center">
            <div class="editable">KEPALA <?= strtoupper($user_info['nama_satker']); ?> POLDA LAMPUNG</div>
        </div>
        <div class="space-2"><br><br></div>
        <div class="text-center">
            <div class="editable single-lines"><strong><u>XXXXXXXXXXXXXXXXXX</u></strong></div>
            <div class="editable single-lines">KOMISARIS BESAR POLISI NRP 00000000</div>
        </div>
    </div>
</div>