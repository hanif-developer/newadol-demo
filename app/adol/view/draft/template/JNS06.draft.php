<div class="A4 papper drafter">
    <div class="kop space-15">
        KEPOLISIAN NEGARA REPUBLIK INDONESIA
        <div class="text-center">DAERAH LAMPUNG</div>
    </div>
    <div class="space-2"><br></div>
    <div class="row">
        <div class="col-md-12 text-center">
            <img src="assets/drafter/img/polri.png" class="logo" alt="">
        </div>
        <div class="space-2"><br></div>
        <div class="col-md-12 text-center">
            <div class="space-15">
                SURAT PERINTAH <br>
                <span class="nomor">Nomor: <span
                        class="editable single-line">Sprin/&nbsp;&nbsp;&nbsp;/IV/LIT.5./2019</span></span>
            </div>
        </div>
    </div>
    <div class="space-2"><br></div>
    <div class="row">
        <div class="col-md-3">Pertimbangan <span class="float-right">:</span></div>
        <div class="col-md-9 pl-0">
            <div class="editable space-1">
                bahwa dalam rangka pelaksanaan kegiatan rapat koordinasi bidang kesekretariatan, dipandang perlu
                mengeluarkan surat perintah.
            </div>
            <div class="space-2"><br></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">Dasar <span class="float-right">:</span></div>
        <div class="col-md-9 pl-0">
            <div class="editable space-1">
                Surat Telegram Kepala Kepolisian Negara Republik Indonesia Nomor: ST/875/III/LIT.5./2019tanggal 29Maret
                2019 tentang Rapat Koordinasi Bidang Kesekretariatan.
            </div>
            <div class="space-2"><br></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="space-1">DIPERINTAHKAN</div>
            <div class="space-2"><br></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">Kepada <span class="float-right">:</span></div>
        <div class="col-md-9 pl-0">
            <div class="editable space-1">
                1.&nbsp;&nbsp; <span style="text-decoration: underline;"
                    data-mce-style="text-decoration: underline;">AKBPSRI WAHYUNI, S.H., M.H. NRP
                    61110075</span><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; KASETUM POLDA LAMPUNG<br><br>2.&nbsp;&nbsp;
                <span style="text-decoration: underline;" data-mce-style="text-decoration: underline;">BRIGADIR YUNI
                    NURUL F. NRP 85061330</span><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; BAMIN URRENMIN SETUM POLDA LAMPUNG<br>
            </div>
            <div class="space-2"><br></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">Untuk <span class="float-right">:</span></div>
        <div class="col-md-9 pl-0">
            <div class="editable space-1 test-contents">
                1.&nbsp;&nbsp; di samping melaksanakan tugas dan jabatan sehari-hari, agar&nbsp;
                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; mengikutikegiatan rapat koordinasi bidang kesekretariatan Tahun
                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2019 hari Kamis s.d. Jumattanggal 26 s.d. 27 April 2019diHotel
                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ambhara JalanIskandarsyah Raya 1, Jakarta
                Selatan;<br><br><br>2.&nbsp;&nbsp; melaporkan hasil pelaksanaannya kepada Kapolda Lampung;<br><br><br>3.&nbsp;&nbsp; melaksanakan perintah ini dengan saksama danpenuh rasa
                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; tanggungjawab.
            </div>
            <div class="space-2"><br></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">Selesai.</div>
        <div class="col-md-9 pl-0">
            <div class="space-1"><br><br><br></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5"></div>
        <div class="col-md-7 space-15">
            <div class="row">
                <div class="col-md-6">Dikeluarkan di<span class="float-right">:</span></div>
                <div class="col-md-6 pl-0">
                    <div class="editable single-lines">xxxxxxx</div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">Pada tanggal<span class="float-right">:</span></div>
                <div class="col-md-6 pl-0">
                    <div class="editable single-lines"><?= date('M Y'); ?></div>
                </div>
            </div>
            <div style="border-top: 1px solid #000;"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5"></div>
        <div class="col-md-7">
            <div class="row">
                <div class="col-md-12">
                    <div class="editable space-15">KEPALA KEPOLISIAN DAERAH LAMPUNG</div>
                    <div class="space-1"><br><br><br><br></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="space-15">Tembusan:</div>
            <div class="editable space-15">
                1. xxxx <br>
                2. xxxx <br>
                3. xxxx <br>
            </div>
        </div>
        <div class="col-md-7">
            <div class="row">
                <div class="col-md-12 space-15 text-center">
                    <div class="editable single-lines"><strong><u>XXXXXXXXXXXXXXXXXX</u></strong></div>
                    <div class="editable single-lines">INSPEKTUR JENDERAL POLISI</div>
                </div>
            </div>
        </div>
    </div>
</div>