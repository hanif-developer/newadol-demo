<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <?php $this->getView('adol', 'main', 'header', []); ?>
    <?php $this->getView('adol', 'main', 'modal', 'form'); ?>
    <div class="clearfix"> </div>
    <div class="page-container">
        <?php $this->getView('adol', 'main', 'navbar', []); ?>
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li><span>Index</span><i class="fa fa-circle"></i></li>
                        <li><span>Administrasi</span><i class="fa fa-circle"></i></li>
                        <li><span>Masuk</span><i class="fa fa-circle"></i></li>
                        <li><span><?= $active_page; ?></span></li>
                    </ul>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div id="administrasi" class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption"><?= $page_title; ?></div>
                            </div>
                            <div class="portlet-body">
                                <?php extract($dataAdm); ?>
                                <div class="tabbable-line">
                                    <ul class="nav nav-tabs ">
                                        <?= ($user_session['level_user'] == 'jabatan') ? '<li class="active"><a data-id="'.$id_adm.'" data-target="#tab_aksi_adm" data-toggle="tab"> Data Aksi </a></li>' : ''; ?>
                                        <li class="<?= ($user_session['level_user'] != 'jabatan') ? 'active' : ''; ?>"><a data-id="<?= $id_adm; ?>" data-target="#tab_detail_adm" data-toggle="tab"> DETAIL ADMINISTRASI </a></li>
                                        <li><a data-id="<?= $id_adm; ?>" data-url="/adm/list/riwayat" data-target="#tab_riwayat_adm" data-toggle="tab"> RIWAYAT ADMINISTRASI </a></li>
                                        <li><a data-id="<?= $id_adm; ?>" data-url="/adm/list/riwayat" data-target="#tab_riwayat_draft" data-toggle="tab"> RIWAYAT DRAFT </a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <?php if($user_session['level_user'] == 'jabatan'): ?>
                                        <div class="tab-pane active" id="tab_aksi_adm">
                                            <div class="portlet-body form">
                                                <form action="#" class="form-horizontal form-bordered">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="text-label col-md-3">Aksi</label>
                                                            <div class="col-md-9"><?= $aksi_riwayat; ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-label col-md-3">Pengirim</label>
                                                            <div class="col-md-9"><?= $pengirim_aksi; ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-label col-md-3">Isi Pesan </label>
                                                            <div class="col-md-9"><?= $isi_pesan; ?></div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <a href="<?= $this->link_adm.'/masuk'; ?>" class="btn red" style="margin: 5px 5px 0px;"><i class="fa fa-reply"></i> Kembali</a>
                                                        <?= ($user_session['disposisi_user'] == 1) ? '<button type="button" class="btn btn-primary btnUserAction" style="margin: 5px 5px 0px;" data-adm="'.$id_adm.'" data-action="disposisi">Disposisi</button>' : '' ?>
                                                        <?= ($user_session['koordinasi_user'] == 1) ? '<button type="button" class="btn btn-primary btnUserAction" style="margin: 5px 5px 0px;" data-adm="'.$id_adm.'" data-action="koordinasi">Koordinasi</button>' : '' ?>
                                                        <?= ($user_session['laporan_user'] == 1) ? '<button type="button" class="btn btn-primary btnUserAction" style="margin: 5px 5px 0px;" data-adm="'.$id_adm.'" data-action="laporan">Laporan</button>' : '' ?>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                        <div class="tab-pane <?= ($user_session['level_user'] != 'jabatan') ? 'active' : ''; ?>" id="tab_detail_adm">
                                            <div class="portlet-body form">
                                                <form action="#" class="form-horizontal form-bordered">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="text-label col-md-3">Jenis Administrasi</label>
                                                            <div class="col-md-9"><?= $nama_jenis; ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-label col-md-3">Nomor Surat</label>
                                                            <div class="col-md-9"><?= $nomer_adm; ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-label col-md-3">Nomor Agenda</label>
                                                            <div class="col-md-9"><?= $nomer_agenda; ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-label col-md-3">Pengirim / Satker</label>
                                                            <div class="col-md-9"><?= $nama_pengirim; ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-label col-md-3">Tanggal</label>
                                                            <div class="col-md-9"><?= $tanggal_adm; ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-label col-md-3">Klasifikasi</label>
                                                            <div class="col-md-9"><?= $nama_klasifikasi; ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-label col-md-3">Derajat</label>
                                                            <div class="col-md-9"><?= $nama_derajat; ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-label col-md-3">Perihal</label>
                                                            <div class="col-md-9"><?= $perihal_adm; ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-label col-md-3">
                                                                Lampiran<br />
                                                                <?= ($tembusan_adm == 1) ? '(Tembusan)' : ''; ?>
                                                            </label>
                                                            <div class="col-md-9">
                                                                <?= (!empty($lampiran_adm)) ? '<span class="btn btn-primary" onclick="administrasi.showDocument(\'File Lampiran\', \''.$lampiran_adm.'\');">Tampilkan</span>' : '-' ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-label col-md-3">Keterangan</label>
                                                            <div class="col-md-9"><?= $keterangan_adm; ?></div>
                                                        </div>
                                                    </div>
                                                    <?php if($user_session['level_user'] !== 'jabatan'): ?>
                                                    <div class="form-actions">
                                                        <a href="<?= $this->link_adm.'/monitor'; ?>" class="btn red" style="margin: 5px 5px 0px;"><i class="fa fa-reply"></i> Kembali</a>
                                                    </div>
                                                    <?php endif; ?>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab_riwayat_adm">
                                            <div class="portlet-body">
                                                <?php $this->riwayat(); ?>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab_riwayat_draft">
                                            <div class="portlet-body">
                                                <?php $this->getView('adol', 'draft', 'riwayat', ''); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div style="height: 250px; border: 0px solid red;"></div>
                <?php $this->getView('adol', 'main', 'footer', []); ?>
            </div>
        </div>
    </div>
    <?= $jsPath; ?>
    <script src="<?= $api_path.'/script'; ?>"></script>
    <script src="<?= $main_path.'/script'; ?>"></script>
    <script src="<?= $url_path.'/script'; ?>"></script>
</body>