administrasi = {
    init: function(){
        url_path = "<?= $url_path; ?>";
        modul = app.initModul("#administrasi");
        modulTab = {};

        $(document).on("change", "#id_jenis", function(event) {
            event.preventDefault();
            $(".divjenislain").hide();
            if($(this).val() === "JNSXX") $(".divjenislain").show();
        });

        $(document).on("change", "#pengirim_adm", function(event) {
            event.preventDefault();
            $(".divpengirimlain").hide();
            if($(this).val() === "XYZ123") $(".divpengirimlain").show();
        });
        
        $(document).on("click", "#btnSatkerGrup", function() {
            $.get(url_path+"/form/groupsatker", function(data){
                modul.formModal.title.html("Pilih Group Satker");
                modul.formModal.content.html(data);
                modul.formModal.content.find(".bs-select").selectpicker();
                modul.formModal.modal.find("button[type='submit']").html("Pilih");
                // modul.formModal.modal.modal({ backdrop: "static", keyboard: false });
                modul.formModal.modal.modal("show");
                modul.formModal.action.off();
                modul.formModal.action.on("submit", function(event){
                    event.preventDefault();
                    let loader = app.createLoader(modul.formModal.action, "");
                    setTimeout(function(){
                        let satker = [];
                        let pilihan_satker = modul.formModal.action.serializeArray();
                        pilihan_satker.forEach(element => { satker.push(element.value); });
                        $(".tujuan_adm").val(satker).trigger("change");
                        $("#btnSatkerClear").show();
                        modul.formModal.modal.modal("hide");
                        loader.hide();
                    }, 1000);
                });
            });
        });

        $(document).on("click", "#btnSatkerClear", function(event) {
            event.preventDefault();
            $(".tujuan_adm").val("").trigger("change");
            $(this).hide();
        });

        $(document).on("change", "#pil_cari", function(event) {
            event.preventDefault();
            if(this.value === "tanggal_adm" || this.value === "waktu_adm_riwayat")
                $("#cari").datepicker({
                    format: "yyyy-mm-dd",
                    language: "id",
                    todayHighlight: 1,
                    autoclose: 1
                }).attr({readonly: "true", placeholder: "Pilih tanggal"});
            else
                $("#cari").removeAttr("readonly").attr({placeholder: "Cari disini.."}).datepicker("remove");
                
            $("#cari").val("").trigger("change");
        });

        $(document).on("change", "#cari, #tembusan, #status_baca", function(event) {
            event.preventDefault();
            modul.table.action.find("#page").val("1");
            administrasi.showTable();
        });

        $(document).on("click", ".btnUserAction", function() {
            let user_action = $(this).data("action");
            let id_adm = $(this).data("adm");
            $.get(url_path+"/form/actionadm/"+user_action+"/"+id_adm, function(data){
                modul.formModal.content.html(data);
                modul.formModal.content.find(".bs-select").selectpicker();
                modul.formModal.title.html(modul.formModal.content.find(".form_title").data("text"));
                modul.formModal.modal.find("button[type='submit']").html("Kirim");
                modul.formModal.modal.modal({ backdrop: "static", keyboard: false });
                modul.formModal.modal.modal("show");
                modul.formModal.action.off();
                modul.formModal.action.on("submit", function(event){
                    event.preventDefault();
                    let loader = app.createLoader(modul.formModal.action, "Mengirim "+user_action);
                    setTimeout(function(){
                        let formData = new FormData(modul.formModal.action[0]);
                        formData.append("session_id", session_id);
                        app.sendDataMultipart({
                            url: "/adm/"+user_action+"/save",
                            data: formData,
                            token: "<?= $this->token; ?>",
                            onSuccess: function(response){
                                // console.log(response);
                                modul.formModal.modal.modal("hide");
                                loader.hide();
                                main.showMessage(response, function(){
                                    $("a[data-target='#tab_riwayat_adm']").click();
                                    $(document).scrollTop(0);
                                });
                            },
                            onError: function(error){
                                // console.log(error);
                                modul.formModal.modal.modal("hide");
                                loader.hide();
                                main.showMessage(error);
                            }
                        });
                    }, 1000);
                });
            });
        });

        $(document).find(".datepicker").datepicker({
            format: "yyyy-mm-dd",
            language: "id",
            todayHighlight: 1,
            autoclose: 1
        });

        $(document).on("shown.bs.tab", "#administrasi a[data-toggle='tab']", function(){
        // $(document).on("click", "#administrasi a[data-toggle='tab']", function(){
            let id = $(this).data("id");
            let url = $(this).data("url");
            let target = $(this).data("target");

            if(url === undefined) return;
            if(modulTab.hasOwnProperty(target) == false){
                modulTab[target] = app.initModul(target);
            }

            let tabs_modul = modulTab[target];
            if(tabs_modul.table.content.length == 0) return;

            tabs_modul.table.content.hide();
            tabs_modul.table.empty.hide();
            tabs_modul.table.loader.show();
            setTimeout(function(){
                app.sendData({
                    url: url,
                    data: JSON.stringify({session_id: session_id, list_request: {id: id}}),
                    token: "<?= $this->token; ?>",
                    onSuccess: function(response){
                        // console.log(response);
                        tabs_modul.table.loader.hide();
                        app.createTable({
                            table: tabs_modul.table,
                            data: response.data,
                            onShow: function(content){
                                // content.find("[data-toggle='tooltip']").tooltip();
                            },
                            onPagging: function(page){
                                
                            }
                        });
                    },
                    onError: function(error){
                        // console.log(error);
                        main.showMessage(error);
                    }
                });
            }, 500);
        });
    },
    showTable: function(){
        modul.table.content.hide();
        modul.table.empty.hide();
        modul.table.loader.show();
        setTimeout(function(){
            app.sendData({
                url: "/adm/list/"+$("#action").val(),
                data: JSON.stringify({session_id: session_id, list_request: JSON.parse(app.serializeArraytoJson(modul.table.action.serializeArray()))}),
                token: "<?= $this->token; ?>",
                onSuccess: function(response){
                    // console.log(response);
                    modul.table.loader.hide();
                    app.createTable({
                        table: modul.table,
                        data: response.data,
                        onShow: function(content){
                            // content.find("[data-toggle='tooltip']").tooltip();
                        },
                        onPagging: function(page){
                            modul.table.action.find("#page").val(page);
                            administrasi.showTable();
                            $(document).scrollTop(0);
                        }
                    });
                },
                onError: function(error){
                    // console.log(error);
                    main.showMessage(error);
                }
            });
        }, 500);
    },
    showDocument(title, url){
        modul.formModal.title.html(title);
        modul.formModal.content.html('<div class="preview-modal-content"><div class="loader"></div></div>');
        modul.formModal.modal.find("button[type='submit']").html("Download");
        // modul.formModal.modal.modal({ backdrop: "static", keyboard: false });
        modul.formModal.modal.modal("show");
        modul.formModal.modal.on("shown.bs.modal", function(){
            setTimeout(function(){
                app.showPDF({
                    container: modul.formModal.content.find(".preview-modal-content"),
                    url: url,
                });
            }, 500);
        });
        modul.formModal.modal.on("hidden.bs.modal", function(){
            modul.formModal.modal.off(); // Fix multiple times load 
        });
        modul.formModal.action.off();
        modul.formModal.action.on("submit", function(event){
            event.preventDefault();
            let download = document.createElement("a");
            download.target = "_blank";
            download.href = url;
            download.click();
        });
    },
    simpanAdm: function(obj, method){
        let formConfirm = {
            input: {
                title: "Simpan Data",
                text: "Klik tombol simpan, untuk menyimpan data",
                confirmButtonText: "Simpan",
                link_success: "<?= $this->link_adm.'/masuk'; ?>"
            },
            kirim: {
                title: "Kirim Data",
                text: "Klik tombol kirim, untuk mengirim data",
                confirmButtonText: "Kirim",
                link_success: "<?= $this->link_adm.'/keluar'; ?>"
            }
        };
        let formData = new FormData($(obj)[0]);
        formData.append("session_id", session_id);
        swal({
            title: formConfirm[method].title,
            text: formConfirm[method].text,
            type: "info",
            confirmButtonText: formConfirm[method].confirmButtonText,
            cancelButtonText: "Tidak",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            allowEscapeKey : false,
        }, function(){
            setTimeout(function(){
                app.sendDataMultipart({
                    url: "/adm/"+method+"/save",
                    data: formData,
                    token: "<?= $this->token; ?>",
                    onSuccess: function(response){
                        // console.log(response);
                        main.showMessage(response, function(){
                            window.location = formConfirm[method].link_success
                        });
                    },
                    onError: function(error){
                        // console.log(error);
                        main.showMessage(error);
                    }
                });
            }, 1000);
        });

        return false;
    },
};

administrasi.init();