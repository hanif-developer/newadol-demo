<div class="form-body">
    <span class="form_title" data-text="<?= $form_title; ?>"></span>
    <div class="form-group row">
        <?= comp\BOOTSTRAP::inputKey('id_adm', $id_adm); ?>
        <?= comp\BOOTSTRAP::inputKey('user_action', $user_action); ?>
        <label class="text-label col-md-3">Kepada</label>
        <div class="col-md-9">
            <?= comp\BOOTSTRAP::inputSelect('tujuan_adm_riwayat[]', $pilihan_tujuan, '', 'class="form-control bs-select" multiple data-actions-box="true" title="-- Pilih Tujuan --" required'); ?>
        </div>
    </div>
    <div class="form-group row">
        <label class="text-label col-md-3">Isi <?= $user_action; ?></label>
        <div class="col-md-9">
            <?= comp\BOOTSTRAP::inputTextArea('keterangan_adm_riwayat', '', 'class="form-control" style="height:100px;" required'); ?>
        </div>
    </div>
    <div class="form-group" style="display: none;">
            <label class="text-label col-md-3">Lampiran</label>
            <div class="col-md-9">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <span class="btn green btn-file">
                        <span class="fileinput-new"> Pilih file </span>
                        <span class="fileinput-exists"> Ubah </span>
                        <input id="file_adm_riwayat" type="file" name="file_adm_riwayat" />
                    </span>
                    <span class="fileinput-filename"> </span> &nbsp;
                    <a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"> </a>
                </div>
            </div>
        </div>
</div>
