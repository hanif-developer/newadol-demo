<div class="form-body">
    <div class="form-group row" style="display: none;">
        <label class="text-label col-md-3">Mabes</label>
        <div class="col-md-9">
            <?= comp\BOOTSTRAP::inputSelect('pilihan_satker', $pilihan_satker_mabes, '', 'class="form-control bs-select pilihan_satker" multiple data-actions-box="true" data-live-search="true" title="-- Pilih Satker Mabes --"'); ?>
        </div>
    </div>
    <div class="form-group row">
        <label class="text-label col-md-3">Polda</label>
        <div class="col-md-9">
            <?= comp\BOOTSTRAP::inputSelect('pilihan_satker', $pilihan_satker_polda, '', 'class="form-control bs-select pilihan_satker" multiple data-actions-box="true" data-live-search="true" title="-- Pilih Satker Polda --"'); ?>
        </div>
    </div>
    <div class="form-group row">
        <label class="text-label col-md-3">Polres</label>
        <div class="col-md-9">
            <?= comp\BOOTSTRAP::inputSelect('pilihan_satker', $pilihan_satker_polres, '', 'class="form-control bs-select pilihan_satker" multiple data-actions-box="true" data-live-search="true" title="-- Pilih Satker Polres --"'); ?>
        </div>
    </div>
    <div class="form-group row">
        <label class="text-label col-md-3">Polsek</label>
        <div class="col-md-9">
            <?= comp\BOOTSTRAP::inputSelect('pilihan_satker', $pilihan_satker_polsek, '', 'class="form-control bs-select pilihan_satker" multiple data-actions-box="true" data-live-search="true" title="-- Pilih Satker Polsek --"'); ?>
        </div>
    </div>
</div>
