<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <?php $this->getView('adol', 'main', 'header', []); ?>
    <div class="clearfix"> </div>
    <div class="page-container">
        <?php $this->getView('adol', 'main', 'navbar', []); ?>
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li><span>Index</span><i class="fa fa-circle"></i></li>
                        <li><span>Administrasi</span><i class="fa fa-circle"></i></li>
                        <li><span><?= $active_page; ?></span></li>
                    </ul>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div id="administrasi" class="portlet box blue ">
                            <div class="portlet-title">
                                <div class="caption"><?= $page_title; ?></div>
                            </div>
                            <div class="portlet-body form">
                                <form id="frmInput" class="form-horizontal form-bordered form-row-stripped" onsubmit="return administrasi.simpanAdm(this, 'input');" autocomplete="off">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="text-label col-md-3 text-left">Jenis Administrasi</label>
                                            <div class="col-md-9">
                                                <?php extract($dataAdm); ?>
                                                <?= comp\BOOTSTRAP::inputKey('id_adm', $form['id_adm']); ?>
                                                <?= comp\BOOTSTRAP::inputSelect('id_jenis', $pilihan_jenis, $form['id_jenis'], 'class="form-control select2" required'); ?>
                                            </div>
                                        </div>
                                        <div class="form-group divjenislain" style="display: none;">
                                            <label class="text-label col-md-3"></label>
                                            <div class="col-md-9">
                                                <?= comp\BOOTSTRAP::inputText('jenislain_adm', 'text', $form['jenislain_adm'], 'class="form-control" placeholder="Ketikkan Jenis Lainnya"'); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="text-label col-md-3 text-left">Nomor Surat</label>
                                            <div class="col-md-9">
                                                <?= comp\BOOTSTRAP::inputText('nomer_adm', 'text', $form['nomer_adm'], 'class="form-control" required'); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="text-label col-md-3 text-left">Nomor Agenda</label>
                                            <div class="col-md-9">
                                                <?= comp\BOOTSTRAP::inputText('nomer_agenda', 'text', $form['nomer_agenda'], 'class="form-control" required'); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="text-label col-md-3">Pengirim / Satker</label>
                                            <div class="col-md-9">
                                                <?= comp\BOOTSTRAP::inputSelect('pengirim_adm', $pilihan_satker, $form['pengirim_adm'], 'class="form-control select2" required'); ?>
                                            </div>
                                        </div>
                                        <div class="form-group divpengirimlain" style="display: none;">
                                            <label class="text-label col-md-3"></label>
                                            <div class="col-md-9">
                                                <?= comp\BOOTSTRAP::inputText('pengirimlain_adm', 'text', $form['pengirimlain_adm'], 'class="form-control" placeholder="Ketikkan Nama Pengirim"'); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="text-label col-md-3">Tanggal</label>
                                            <div class="col-md-9">
                                                <?= comp\BOOTSTRAP::inputText('tanggal_adm', 'text', $form['tanggal_adm'], 'class="form-control input-medium datepicker" readonly'); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="text-label col-md-3">Klasifikasi</label>
                                            <div class="col-md-9">
                                                <?= comp\BOOTSTRAP::inputSelect('id_klasifikasi', $pilihan_klasifikasi, $form['id_klasifikasi'], 'class="form-control input-medium select2" required'); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="text-label col-md-3">Derajat</label>
                                            <div class="col-md-9">
                                                <?= comp\BOOTSTRAP::inputSelect('id_derajat', $pilihan_derajat, $form['id_derajat'], 'class="form-control input-medium select2" required'); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="text-label col-md-3">Perihal</label>
                                            <div class="col-md-9">
                                                <?= comp\BOOTSTRAP::inputText('perihal_adm', 'text', $form['perihal_adm'], 'class="form-control" required'); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="text-label col-md-3">Lampiran<br /><br />
                                                <div class="md-checkbox">
                                                    <input type="checkbox" id="tembusan_adm" name="tembusan_adm" value="1">
                                                    <label for="tembusan_adm">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> Tembusan </label>
                                                </div>
                                            </label>
                                            <div class="col-md-9">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn green btn-file">
                                                        <span class="fileinput-new"> Pilih file </span>
                                                        <span class="fileinput-exists"> Ubah </span>
                                                        <input id="file_adm" type="file" name="file_adm" accept="<?= $upload_mimes; ?>" required />
                                                    </span>
                                                    <span class="fileinput-filename"> </span> &nbsp;
                                                    <a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"> </a>
                                                    <br /><?= $upload_description; ?><br />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="text-label col-md-3">Keterangan</label>
                                            <div class="col-md-9">
                                                <?= comp\BOOTSTRAP::inputTextArea('keterangan_adm', $form['keterangan_adm'], 'class="form-control" style="height:200px;"'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button class="btn btn-primary" id="btnInput" type="submit">Input Administrasi </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div style="height: 250px; border: 0px solid red;"></div>
                <?php $this->getView('adol', 'main', 'footer', []); ?>
            </div>
        </div>
    </div>
    <?= $jsPath; ?>
    <script src="<?= $api_path.'/script'; ?>"></script>
    <script src="<?= $main_path.'/script'; ?>"></script>
    <script src="<?= $url_path.'/script'; ?>"></script>
</body>