<div class="table-content">
    <div class="table-responsive">
        <table class="table table-default table-hover">
            <thead>
                <tr>
                    <th width="50px">No</th>
                    <th width="100px">Status</th>
                    <th width="100px">Aksi</th>
                    <th width="150px">Tanggal</th>
                    <th width="200px">Pengirim</th>
                    <th width="200px">Tujuan</th>
                    <th>Isi Pesan</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{no}</td>
                    <td><span class="label label-sm label-{status_baca_color}">{status_baca}</span></td>
                    <td>{aksi_riwayat}</td>
                    <td>{waktu_riwayat}</td>
                    <td>{nama_pengirim}</td>
                    <td>{nama_tujuan}</td>
                    <td>{isi_pesan}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="table-pagging" style="border-top: 1px solid #ccc; padding-top: 10px;">
        <ul class="pagination">
            <li class="pagging page-item" number-page="" style="cursor: pointer;"><span class="page-link">{page}</span></li>
        </ul>
    </div>
</div>
<div class="card-body">
    <div class="query" style="font-size:10pt;"></div>
    <div class="text-center">
        <div class="spinner-border table-loader" role="status"><div class="loader"></div></div>
    </div>
    <div class="table-empty alert alert-info" role="alert">Data riwayat masih kosong ...</div>
</div>