<div class="table-responsive">
    <table class="table table-default table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th width="100px">Status</th>
                <th width="100px">Aksi</th>
                <th width="100px">Waktu</th>
                <th width="100px">Tujuan</th>
                <th width="100px">No Surat</th>
                <th width="100px">No Agenda</th>
                <th>Perihal</th>
                <th>Isi</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{no}</td>
                <td><span class="label label-sm label-{status_baca_color}">{status_baca}</span></td>
                <td>{aksi_adm}</td>
                <td>{waktu_adm}</td>
                <td>{nama_tujuan}</td>
                <td>{nomer_adm}</td>
                <td>{nomer_agenda}</td>
                <td>{perihal_adm}</td>
                <td><a href="<?= $this->link_adm.'/detail/monitor/'; ?>{id_adm}">{keterangan_adm}</a></td>
            </tr>
        </tbody>
    </table>
</div>
<div class="table-pagging" style="border-top: 1px solid #ccc; padding-top: 10px;">
    <ul class="pagination">
        <li class="pagging page-item" number-page="" style="cursor: pointer;"><span class="page-link">{page}</span></li>
    </ul>
</div>