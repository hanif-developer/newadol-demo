main = {
    init: function(){
        session_id = "<?= $session_id; ?>";
        main_path = "<?= $main_path; ?>";

        $(document).on("click", "#notif-adm", function(event) {
            event.preventDefault();
            let jml_adm = $("#jml_adm").text();
            if (jml_adm > 3) {
                $("#content-notif-adm").slimScroll({
                    height: "300px"
                });
            }

            main.notifikasi("administrasi", $("#content-notif-adm"));
        });
        $(document).on("click", "#notif-draft", function(event) {
            event.preventDefault();
            let jml_adm = $("#jml_draft").text();
            if (jml_adm > 3) {
                $("#content-notif-draft").slimScroll({
                    height: "300px"
                });
            }

            main.notifikasi("draft", $("#content-notif-draft"));
        });
    },
    notifikasi: function(id, content){
        content.html("<div style='padding:10px; text-align:center;'>"+loader+"</div>");
        setTimeout(function(){
            $.get(main_path+"/notifikasi/"+id, function(data){
                content.html(data);
            });
        }, 500);
    },
    showMessage: function(response, callback){
        swal({title: response.message.title, text: response.message.text, type: response.status, confirmButtonClass: "btn-primary", confirmButtonText: "OK"}, function(){
            if(typeof callback === "function") callback();
        });
    },
};

main.init();