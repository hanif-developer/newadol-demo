<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <?php $this->getView('adol', 'main', 'header', []); ?>
    <div class="clearfix"> </div>
    <div class="page-container">
        <?php $this->getView('adol', 'main', 'navbar', []); ?>
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li><span>Index</span><i class="fa fa-circle"></i></li>
                        <li><span><?= $active_page; ?></span></li>
                    </ul>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-5">
                        <div class="portlet light bordered">
                            <div class="profile-userpic profile-square">
                                <img src="<?= $user_info['logo_satker']; ?>" class="img-responsive" style="border-radius:0px !important" alt="">
                            </div>
                            <div class="profile-usertitle">
                                <div class="profile-usertitle-name"> <?= $user_info['nama_jabatan'];?> </div>
                                <div class="profile-usertitle-job"> <?= empty($user_info['nama_personil']) ? $user_info['nama_satker'] : $user_info['nama_personil'].'<br>'.'NRP. '.$user_info['nrp_personil']; ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="portlet light bordered">
                            <div class="portlet-title tabbable-line">
                                <div class="caption caption-md">
                                    <i class="icon-globe theme-font hide"></i>
                                    <span class="caption-subject font-blue-madison bold uppercase"><?= $page_title; ?></span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="tab-content">
                                    <!-- ADMINISTRASI -->
                                    <div class="caption caption-md">
                                        <span class="caption-subject font-blue-madison bold uppercase">Administrasi</span>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <a href="<?= $this->link_adm.'/masuk'; ?>" class="dashboard-stat dashboard-stat-v2 blue">
                                                <div class="visual"><i class="fa fa-comments"></i></div>
                                                <div class="details">
                                                    <div class="number">
                                                        <span data-counter="counterup" data-value="<?= $administrasi['masuk'];?>"></span>
                                                    </div>
                                                    <div class="desc"> Administrasi Masuk</div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="<?= $this->link_adm.'/keluar'; ?>" class="dashboard-stat dashboard-stat-v2 red">
                                                <div class="visual"><i class="fa fa-comments"></i></div>
                                                <div class="details">
                                                    <div class="number">
                                                        <span data-counter="counterup" data-value="<?= $administrasi['keluar'];?>"></span>
                                                    </div>
                                                    <div class="desc"> Administrasi Keluar</div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <br /><br />
                                    <!-- DRAFT -->
                                    <div class="caption caption-md">
                                        <span class="caption-subject font-blue-madison bold uppercase">Draft</span>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <a href="<?= $this->link_draft.'/masuk'; ?>" class="dashboard-stat dashboard-stat-v2 blue">
                                                <div class="visual"><i class="fa fa-comments"></i></div>
                                                <div class="details">
                                                    <div class="number">
                                                        <span data-counter="counterup" data-value="<?= $draft['masuk'];?>"></span>
                                                    </div>
                                                    <div class="desc"> Draft Masuk</div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="<?= $this->link_draft.'/keluar'; ?>" class="dashboard-stat dashboard-stat-v2 red">
                                                <div class="visual"><i class="fa fa-comments"></i></div>
                                                <div class="details">
                                                    <div class="number">
                                                        <span data-counter="counterup" data-value="<?= $draft['keluar'];?>"></span>
                                                    </div>
                                                    <div class="desc"> Draft Keluar</div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div style="height: 250px; border: 0px solid red;"></div>
                <?php $this->getView('adol', 'main', 'footer', []); ?>
            </div>
        </div>
    </div>
    <!-- END CONTAINER -->
    <?= $jsPath; ?>
    <script src="<?= $api_path.'/script'; ?>"></script>
    <script src="<?= $main_path.'/script'; ?>"></script>
</body>