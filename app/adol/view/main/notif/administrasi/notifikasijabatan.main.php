<?php
if(!empty($dataAdm)){
    for($i = 0; $i < count($dataAdm); $i++){
        if(!empty($dataAdm[$i]['id_adm_riwayat'])){
            ?>
            <li>
                <a class="pointer" href="<?= $link_detail . $dataAdm[$i]['id_adm_riwayat']; ?>">
                    <span class="subject" style="margin-left:0">
                        <span class="from" style="width:100%"><?= $nama_pengirim[$dataAdm[$i]['pengirim_adm_riwayat']]['text']; ?></span>
                        <span class="time" style="width:100%"><?= comp\FUNC::moments($dataAdm[$i]['waktu_adm_riwayat']); ?></span>
                    </span>
                    <span class="message" style="margin:30px 0 0 0"><?= $dataAdm[$i]['keterangan_adm_riwayat']; ?></span>
                </a>
            </li>
        <?php
        }
    }
}
else{
    ?>
    <li>
        <a class="pointer">
            <span class="message" style="margin:0; text-align:center"> Tidak ada notifikasi.</span>
        </a>
    </li>
<?php } ?>
