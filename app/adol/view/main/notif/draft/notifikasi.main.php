<?php
if(!empty($dataDraft)){
    for($i = 0; $i < count($dataDraft); $i++){
        if(!empty($dataDraft[$i]['id_draft_riwayat'])){
            ?>
            <li>
                <a class="pointer" href="<?= $link_detail.$dataDraft[$i]['id_draft_riwayat']; ?>">
                    <span class="subject" style="margin-left:0">
                        <span class="from" style="width:100%"><?= $nama_pengirim[$dataDraft[$i]['pengirim_draft_riwayat']]['text']; ?></span>
                        <span class="format-date time" style="width:100%"><?= comp\FUNC::moments($dataDraft[$i]['waktu_draft_riwayat']); ?></span>
                    </span>
                    <span class="message" style="margin:30px 0 0 0"> <?= $dataDraft[$i]['keterangan_draft_riwayat']; ?>  </span>
                </a>
            </li>
        <?php }
    }
}
else{ ?>
    <li>
        <a class="pointer">
            <span class="message" style="margin:0; text-align:center"> Tidak ada notifikasi.</span>
        </a>
    </li>
<?php } ?>

