<div id="main-header" class="page-header navbar navbar-fixed-top">
    <div class="page-header-inner ">
        <div class="page-logo">
            <a class="pointer">
                <img src="<?= $this->link($this->logo_adol_text); ?>" style="height: 40px; margin:0px;" alt="logo" class="logo-default logo-main" /> </a>
            <div class="menu-toggler sidebar-toggler">
                <span></span>
            </div>
        </div>
        
        <a class="menu-toggler responsive-toggler pointer" data-toggle="collapse" data-target=".navbar-collapse">
            <span></span>
        </a>
        
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar">
                    <a id="notif-adm" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <i class="icon-envelope-open"></i>
                        <span id="jml_adm" class="jmladm badge badge-default"><?= $administrasi['notif_masuk']; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="external">
                            <h3>Ada <span class="jmladm bold"><?= $administrasi['notif_masuk']; ?></span> Adm Masuk Baru</h3>
                            <a href="<?= $this->link_adm.'/masuk'; ?>" class="pointer">lihat semua</a>
                        </li>
                        <li>
                            <ul id="content-notif-adm" class="dropdown-menu-list" style="height: auto;" data-handle-color="#637283"></ul>
                        </li>
                    </ul>
                </li>
                <li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar" style="display:none;">
                    <a id="notif-draft" class="dropdown-toggle pointer" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <i class="icon-doc"></i>
                        <span id="jml_draft" class="jmldraft badge badge-default"><?= $draft['notif_masuk']; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="external">
                            <h3>Ada <span class="jmldraft bold"><?= $draft['notif_masuk']; ?></span> Draft Masuk Baru</h3>
                            <a href="<?= $this->link_draft.'/masuk'; ?>" class="pointer">lihat semua</a>
                        </li>
                        <li>
                            <ul id="content-notif-draft" class="dropdown-menu-list" style="height: auto;" data-handle-color="#637283"></ul>
                        </li>
                    </ul>
                </li>
                <li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar" style="display:none;">
                    <a href="<?= $this->link('chat'); ?>" class="dropdown-toggle pointer" data-hover="dropdown"><i class="icon-speech"></i></a>
                    <ul class="dropdown-menu"></ul>
                </li>
                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle pointer" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <img alt="" class="img-circle" src="assets/layouts/layout/img/avatar.png" />
                        <span class="username username-hide-on-mobile"> <?= $user_info['nama_jabatan']; ?> [ <?= $user_info['singkatan_satker']; ?> ]</span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li><a href="<?= $this->link_info.'/profil'; ?>"><i class="icon-user"></i> Profil </a></li>
                        <li class="divider"> </li>
                        <li><a href="<?= $this->link_main.'main/logout'; ?>"><i class="fa fa-power-off"></i> Keluar </a></li>
                    </ul>
                </li>
            </ul>
        </div>

    </div>
</div>