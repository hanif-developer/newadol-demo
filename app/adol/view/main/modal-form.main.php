<div class="modal fade form-modal-input" id="" tabindex="-1" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title form-modal-title"></h4>
            </div>
            <form class="frmInput" onsubmit="return false;" autocomplete="off">
                <div class="modal-body">
                    <div class="form-modal-content"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger form-modal-button" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary form-modal-button">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>