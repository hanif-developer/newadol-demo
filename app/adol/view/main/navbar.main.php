<div id="main-menu" class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <li class="heading">
                <h3 class="uppercase">Menu Utama</h3>
            </li>
            <li class="nav-item">
                <a href="<?= $this->link_main; ?>" class="nav-link pointer" style="background: #364150 !important;">
                    <i class="fa fa-home"></i>
                    <span class="title">Beranda</span>
                </a>
            </li>
            <!-- Menu Administrasi -->
            <li class="nav-item active open">
                <a class="nav-link nav-toggle">
                    <i class="fa fa-envelope"></i>
                    <span class="title">Administrasi</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
					<?php if(($user_session['level_user'] === 'agendaris') || ($user_session['level_user'] === 'assistant')){ ?>
                    <li id="menu-adm-input" class="nav-item">
                        <a href="<?= $this->link_adm.'/input'; ?>" class="nav-link pointer">
                            <span class="title">Input Administrasi</span>
                        </a>
                    </li>
					<?php } ?>
					<?php if(($user_session['level_user'] === 'agendaris')){ ?>
                    <li id="menu-adm-kirim" class="nav-item">
                        <a href="<?= $this->link_adm.'/kirim'; ?>" class="nav-link pointer">
                            <span class="title">Kirim Administrasi</span>
                        </a>
                    </li>
					<?php } ?>
                    <li id="menu-adm-masuk" class="nav-item">
                        <a href="<?= $this->link_adm.'/masuk'; ?>" class="nav-link pointer">
                            <span class="title">Administrasi Masuk</span>
                        </a>
                    </li>
					<?php if(($user_session['level_user'] === 'agendaris') || ($user_session['level_user'] === 'jabatan')){ ?>
                    <li id="menu-adm-keluar" class="nav-item ">
                        <a href="<?= $this->link_adm.'/keluar'; ?>" class="nav-link pointer">
                            <span class="title">Administrasi Keluar</span>
                        </a>
                    </li>
					<?php } ?>
					<?php if(($user_session['level_user'] === 'agendaris') || ($user_session['level_user'] === 'assistant')){ ?>
					<li id="menu-adm-monitor" class="nav-item ">
                        <a href="<?= $this->link_adm.'/monitor'; ?>" class="nav-link pointer">
                            <span class="title">Monitor Administrasi</span>
                        </a>
                    </li>
					<?php } ?>
                </ul>
            </li>
            <!-- Menu Draft -->
            <li class="nav-item active open" style="display:none;">
                <a class="nav-link nav-toggle">
                    <i class="fa fa-file-o"></i>
                    <span class="title">Draft</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
					<?php if($user_session['draft_user_action']['buat'] == 1){ ?>
                    <li id="menu-draft-buat" class="nav-item">
                        <a href="<?= $this->link_draft.'/buat/upload'; ?>" class="nav-link pointer">
                            <span class="title">Buat Draft</span>
                        </a>
                    </li>
                    <?php } ?>
                    <li id="menu-draft-masuk" class="nav-item">
                        <a href="<?= $this->link_draft.'/masuk'; ?>" class="nav-link pointer">
                            <span class="title">Draft Masuk</span>
                        </a>
                    </li>
					<li id="menu-draft-keluar" class="nav-item">
                        <a href="<?= $this->link_draft.'/keluar'; ?>" class="nav-link pointer">
                            <span class="title">Draft Keluar</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- Menu Informasi -->
            <li id="grup-informasi" class="nav-item">
            <li class="nav-item active open">
                <a class="nav-link nav-toggle">
                    <i class="icon-user"></i>
                    <span class="title">Informasi</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
					<li id="menu-informasi-profil" class="nav-item">
                        <a href="<?= $this->link_info.'/profil'; ?>" class="nav-link pointer">
                            <span class="title">Profil</span>
                        </a>
                    </li>
                    <!--<li id="menu-informasi-statistik" class="nav-item">
                        <a href="<?= $this->link_info.'/statistik'; ?>" class="nav-link pointer">
                            <span class="title">Statistik</span>
                        </a>
                    </li>-->
					<li id="menu-informasi-register" class="nav-item">
                        <a href="<?= $this->link_info.'/register'; ?>" class="nav-link pointer">
                            <span class="title">Register</span>
                        </a>
                    </li>
                </ul>
            </li>
			<!-- Menu Pengaturan -->
            <li id="grup-pengaturan" class="nav-item" style="display: block;">
            <li class="nav-item active open" style="display: block;">
                <a class="nav-link nav-toggle">
                    <i class="icon-settings"></i>
                    <span class="title">Pengaturan</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
					<?php if($user_session['disposisi_user'] !== '0'){ ?>
                    <li id="menu-pengaturan-disposisi" class="nav-item">
                        <a href="<?= $this->link_pengaturan.'/user/disposisi'; ?>" class="nav-link pointer">
                            <span class="title">User Disposisi</span>
                        </a>
                    </li>
					<?php } ?>
					<?php if($user_session['koordinasi_user'] !== '0'){ ?>
                    <li id="menu-pengaturan-koordinasi" class="nav-item">
                        <a href="<?= $this->link_pengaturan.'/user/koordinasi'; ?>" class="nav-link pointer">
                            <span class="title">User Koordinasi</span>
                        </a>
                    </li>
					<?php } ?>
					<?php if($user_session['laporan_user'] !== '0'){ ?>
                    <li id="menu-pengaturan-laporan" class="nav-item">
                        <a href="<?= $this->link_pengaturan.'/user/laporan'; ?>" class="nav-link pointer">
                            <span class="title">User Laporan</span>
                        </a>
                    </li>
					<?php } ?>
					<?php if($user_session['draft_user'] > 1){ ?>
                    <li id="menu-pengaturan-draft" class="nav-item">
                        <a href="<?= $this->link_pengaturan.'/user/draft'; ?>" class="nav-link pointer">
                            <span class="title">User Draft</span>
                        </a>
                    </li>
					<?php } ?>
                </ul>
            </li>
        </ul>
    </div>
</div>