<body class="toggle-sidebar">
    <?php $this->getView('adol', 'main', 'modal', 'form'); ?>
    <div id="chat" class="content" style="transform: translateX(0);">
        <div class="header header-chat">
            <div class="header-left">
                <a id="menuMain" href="<?= $this->link(); ?>" class="burger-menu"><i data-feather="menu"></i></a>
                <a id="menuBack" href="javascript:void(0);" class="burger-menu d-none"><i
                        data-feather="arrow-left"></i></a>
                <div class="header-search">
                    <i data-feather="search"></i>
                    <input type="search" class="form-control" placeholder="Search for conversations">
                </div>
            </div>

            <div class="header-right">
                <div class="dropdown dropdown-loggeduser">
                    <a href="javascript:void(0);" class="dropdown-link" data-toggle="dropdown">
                        <div class="avatar avatar-sm">
                            <img src="https://via.placeholder.com/500/637382/fff" class="rounded-circle" alt="">
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <div class="dropdown-menu-header">
                            <div class="media align-items-center">
                                <div class="avatar">
                                    <img src="https://via.placeholder.com/500/637382/fff" class="rounded-circle" alt="">
                                </div>
                                <div class="media-body mg-l-10">
                                    <h6><?= $user_info['nama_jabatan']; ?></h6>
                                    <span><?= $user_info['singkatan_satker']; ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="dropdown-menu-body">
                            <a href="<?= $this->link_main.'main/logout'; ?>" class="dropdown-item"><i data-feather="log-out"></i> Sign Out</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body content-body-chat">
            <div class="chat-panel">
                <div class="chat-sidebar">
                    <div class="chat-sidebar-header">
                        <h6 class="tx-14 tx-color-01 mg-b-0">Recent Conversations</h6>
                        <div>
                            <a href="#userContact" class="btn-user-contact" data-toggle="tooltip" title="New Conversation"><i
                            data-feather="user-plus"></i></a>
                        </div>
                    </div>
                    <div class="chat-sidebar-body">
                        <ul class="chat-list chat-user-list"></ul>
                    </div>
                    <div class="chat-sidebar-footer chat-user-info">
                        <div class="avatar avatar-sm avatar-offline">
                            <img src="<?= $active_user['image']; ?>" class="rounded-circle" alt="">
                        </div>
                        <h6 class="chat-loggeduser"><small><?= $active_user['name']; ?></small></h6>
                    </div>
                </div>
                <div class="chat-body" style="
                    background-image: url('https://image.freepik.com/free-vector/online-world-concept-illustration_114360-1007.jpg');
                    background-repeat: no-repeat;
                    background-size: contain;
                    background-position: center;">
                    <div class="chat-body-header bg-white d-none"></div>
                    <div class="chat-body-content d-none">
                        <ul class="chat-msg-list">
                            <li class="msg-item">
                                <div class="msg-body">
                                <?php //$this->debugResponse($this->data); ?>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="chat-body-footer d-none">
                        <form id="chat-form" class="form-inline w-100" onsubmit="return false;" autocomplete="off">
                            <div class="form-group">
                                <?= comp\BOOTSTRAP::inputText('textMessage', 'text', '', 'class="form-control w-100" placeholder="Type something..."'); ?>
                            </div>
                            <button type="submit" class="btn btn-icon"><i class="far fa-comment-dots"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="userContact" class="off-canvas off-canvas-overlay">
        <div class="pd-10">
            <h5 class="tx-color-01 tx-semibold mg-t-50">User Contact</h5>
        </div>
        <div class="user-contact-list chat-sidebar-body"></div>
    </div>
    <div class="off-canvas-backdrop"></div>
    <?= $jsPath; ?>
    <script src="<?= $api_path.'/script'; ?>"></script>
    <script src="<?= $main_path.'/script'; ?>"></script>
    <script src="<?= $url_path.'/script'; ?>"></script>
</body>