chat = {
    init: function(){
        modul = app.initModul("#chat");
        socket = null;
        userList = []; // Daftar percakapan user
        userMessage = []; // Daftar pesan masuk 
        activeUser = <?= json_encode($active_user); ?>;
        activeContact = {};
        chatForm = $("#chat-form");
        chatMsgHeader = $(".chat-body-header");
        chatMsgBody = $(".chat-body-content");
        chatMsgFooter = $(".chat-body-footer");
        chatUserInfo = $(".chat-user-info");
        chatUserList = $(".chat-user-list");
        chatMsgList = $(".chat-msg-list");

        new PerfectScrollbar(".user-contact-list", { suppressScrollX: true });
        new PerfectScrollbar(".chat-sidebar-body", { suppressScrollX: true });
        new PerfectScrollbar(".chat-body-content", { suppressScrollX: true });
        feather.replace();
        
        chat.connectSocket();
        chat.showUserList();
        chatForm.on("submit", function(event) {
            const textMessage = $("#textMessage");
            if(textMessage.val() == "") return;

            chat.sendMessage(textMessage.val());
            textMessage.val("");
            textMessage.focus();
        });

        chatForm.find("#textMessage").on("focus", function(event) {
            // Hubungkan user ke sebuah room, lalu undang user lain yang dikirim pesan untuk masuk ke room juga
            socket.emit("userJoin", ({
                user: activeContact.user, 
                room: activeContact.room
            }));
        });

        $(document).on("click", ".btn-user-contact", function(event) {
            event.preventDefault();
            event.stopPropagation();
            const target = $(this).attr("href");
            chat.showUserContact($(target));
        });

        $(document).on("click touchstart", function(event) {
            // closing of sidebar menu when clicking outside of it
            if(!$(event.target).closest('.off-canvas-menu').length) {
                var offCanvas = $(event.target).closest('.off-canvas').length;
                if(!offCanvas) {
                    $('.off-canvas.show').removeClass('show');
                }
            }
        });

        $(document).on("click", "#menuBack", function(event) {
            event.preventDefault();
            $("body").removeClass("chat-body-show");
            $("#menuMain").removeClass("d-none");
            $("#menuBack").addClass("d-none");
        });

        $("[data-toggle='tooltip']").tooltip();
    },
    connectSocket: function() {
        $_socket = JSON.parse(window.atob("<?= $this->socket; ?>"));
        $_tokens = $_socket.tokens;
        $_sounds = "<?= $this->link(); ?>"+$_socket.sounds;
        $.getScript($_socket.script, function(data, textStatus, jqxhr) {
            socket = io.connect($_socket.server, {
                query: {token: $_tokens}
            });
            socket.on("connected", (client) => {
                console.clear();
                console.log("Connected", client);
                // Hubungkan user ke socket
                socket.emit("userConnect", (activeUser));
                chatUserInfo.find(".avatar").removeClass("avatar-online avatar-offline");
                chatUserInfo.find(".avatar").addClass("avatar-online");
            });
            socket.on("disconnect", (client) => {
                console.clear();
                // Saat koneksi socket terputus
                chatUserInfo.find(".avatar").removeClass("avatar-online avatar-offline");
                chatUserInfo.find(".avatar").addClass("avatar-offline");
            });
            socket.on("userOnline", (users) => {
                console.log("userOnline", users);
                userList.forEach((list, idx) => {
                    const index = users.findIndex(active => active.user.id === list.user.id);
                    const status = (index > -1) ? "avatar-online" : "avatar-offline";
                    list.view.find(".avatar").removeClass("avatar-online avatar-offline");
                    list.view.find(".avatar").addClass(status);
                });
            });
            socket.on("chatMessage", (message) => {
                console.log("chatMessage", message);
                // Jika pesan masuk, saat layar aktif
                const otherUser = (activeContact.user !== undefined) ? activeContact.user : {id: null};
                if (message.user.id == activeUser.id || message.user.id == otherUser.id) {
                    // Tampilkan pada layar chat
                    chat.addMessage(message);
                    // Tampilkan pada user list
                    const li = activeContact.view;
                    li.find(".message-time").html(message.time);
                    li.find(".message-text").html(message.text);
                }
                else {
                    // Tampilkan pesan masuk pada user list
                    userMessage.push(message);
                    const li = chat.addUserList(chatUserList, message);
                    li.addClass("unread"); // status belum dibaca
                    li.find(".message-time").html(message.time);
                    li.find(".message-text").html(message.text);
                    li.find(".message-unread").html(chat.unreadMessage(message.user.id));
                    li.parent().prepend(li);
                    chatUserList.parent().scrollTop(0);
                }
            });
            socket.on("invitedRoom", (room) => {
                // Jika ada undangan masuk room, maka join ke room tersebut
                console.log("invitedRoom", room);
                socket.emit("joinRoom", room);
            });
        });
    },
    unreadMessage: function(user_id) {
        let count = 0;
        userMessage.forEach(message => {
            if (message.user.id == user_id) {
                count++;
            }
        });
        return count;
    },
    readMessage: function(user_id) {
        userMessage = userMessage.filter(message => message.user.id !== user_id);
        // console.log(userMessage);
    },
    saveConversation: function(room, contact_id) {
        setTimeout(function(){
            app.sendData({
                url: "/chat/send/conversation",
                data: JSON.stringify({
                    session_id,
                    room,
                    users: [activeUser.id, contact_id]
                }),
                token: "<?= $this->token; ?>",
                onSuccess: function(response){
                    console.log(response);
                },
                onError: function(error){
                    console.log(error);
                    // main.showMessage(error);
                }
            });
        }, 500);
    },
    saveMessage: function(room, user, message) {
        setTimeout(function(){
            app.sendData({
                url: "/chat/send/message",
                data: JSON.stringify({
                    session_id,
                    room,
                    user,
                    message
                }),
                token: "<?= $this->token; ?>",
                onSuccess: function(response){
                    console.log(response);
                },
                onError: function(error){
                    console.log(error);
                    // main.showMessage(error);
                }
            });
        }, 500);
    },
    showUserContact: function(target){
        const ul = $("<ul>").addClass("chat-list");
        setTimeout(function(){
            app.sendData({
                url: "/chat/contact",
                data: JSON.stringify({session_id}),
                token: "<?= $this->token; ?>",
                onSuccess: function(response){
                    // console.log(response);
                    $.each(response.data.user_contact, (id, val) => {
                        const user = {
                            id,
                            name: val.text,
                            info: val.info,
                            image: val.image,
                        };
                        chat.addUserContact(ul, user, function() {
                            // Jika user dicontact diklik maka buat user conversation
                            chat.saveConversation(val.room, user.id);
                            chat.addUserList(chatUserList, {
                                user, 
                                room: val.room,
                                text: "",
                                time: "",
                                unread: 0
                            }).trigger("click");
                            target.removeClass("show");
                        });
                    });
                    target.find(".user-contact-list").html(ul);
                },
                onError: function(error){
                    console.log(error);
                    // main.showMessage(error);
                }
            });
        }, 500);

        target.addClass("show");
    },
    showUserList: function() {
        chatUserList.html("");
        setTimeout(function(){
            app.sendData({
                url: "/chat/user/conversation",
                data: JSON.stringify({session_id}),
                token: "<?= $this->token; ?>",
                onSuccess: function(response){
                    // console.log(response);
                    $.each(response.data.user_conversation, (idx, data) => {
                        // console.log(data);
                        chat.addUserList(chatUserList, data);
                    });
                },
                onError: function(error){
                    console.log(error);
                    // main.showMessage(error);
                }
            });
        }, 500);
    },
    showUserMessage: function(room) {
        chatMsgList.html("");
        setTimeout(function(){
            app.sendData({
                url: "/chat/user/message",
                data: JSON.stringify({session_id, room}),
                token: "<?= $this->token; ?>",
                onSuccess: function(response){
                    // console.log(response);
                    $.each(response.data.user_message, (idx, data) => {
                        // console.log(data);
                        chat.addMessage(data);
                    });
                },
                onError: function(error){
                    console.log(error);
                    // main.showMessage(error);
                }
            });
        }, 500);
    },
    /**
     * Tampilan user pada menu contact
     */
    addUserContact: function(container, user, onClick) {
        /**
         * Digunakan untuk menampilkan daftar user contact
         */
        const li = $("<li>").addClass("chat-item").css("cursor", "pointer");
        li.html(`<div class="avatar">
                    <img src="${user.image}" class="rounded-circle" alt="" />
                </div>
                <div class="chat-item-body">
                    <div>
                        <span>${user.name}</span>
                    </div>
                    <p>${user.info}</p>
                </div>`);
        
        li.on("click", function(event) {
            event.preventDefault();
            onClick();
        });

        container.append(li);
    },
    /**
     * Tampilan User sebelah kiri
     */
    addUserList: function(container, message) {
        /**
         * Menampilkan daftar user yang pernah/ingin melakukan chat, lalu simpan ke dalam array
         * Jika, dalam array tersebut user sudah ada, maka lewati (agar tidak dobel)
         */
        const index = userList.findIndex(list => list.user.id === message.user.id);
        if (index === -1) {
            const li = $("<li>").addClass("chat-item").css("cursor", "pointer");
            li.html(`<div class="avatar avatar-offline">
                        <img src="${message.user.image}" class="rounded-circle" alt="" />
                    </div>
                    <div class="chat-item-body">
                        <div>
                            <span class="username">${message.user.name}</span>
                            <span class="message-time">${message.time}</span>
                        </div>
                        <p class="message-text">${message.text}</p>
                        <span class="message-unread chat-msg-count">${message.unread}</span>
                    </div>`);

            const userContact = {
                user: message.user,
                room: message.room,
                view: li
            };
            
            userList.push(userContact);
            
            /**
             * Jika daftar tersebut diklik, maka hubungkan user yang aktif dengan user yang diklik tersebut
             * ke dalam room dan tampilkan layar ketik
             */
            li.on("click", function(event) {
                event.preventDefault();
                activeContact = userContact;
                chat.readMessage(message.user.id)
                chat.showUserMessage(message.room);
                chatMsgHeader.html(`<div class="avatar avatar-xs">
                                        <img src="${message.user.image}" class="rounded-circle" alt="" />
                                    </div>
                                    <h6 class="tx-14 tx-color-01 mg-b-0 mg-l-10">${message.user.name}</h6>`);
                chatMsgList.html("");
                chatMsgHeader.removeClass("d-none");
                chatMsgBody.removeClass("d-none");
                chatMsgFooter.removeClass("d-none");

                if (window.matchMedia("(max-width: 767px)").matches) {
                    $("body").addClass("chat-body-show");
                    $("#menuMain").addClass("d-none");
                    $("#menuBack").removeClass("d-none");
                }
    
                $(this).addClass("selected").removeClass("unread");
                $(this).siblings().removeClass("selected");
                $("#textMessage").focus();
            });

            if (message.unread > 0) {
                li.addClass("unread"); // status belum dibaca
            }

            container.prepend(li);
            return li;
        }
        else {
            return userList[index].view;
        }
    },
    addMessage: function(message) {
        /**
         * Menampilkan pesan dari user, jika pesan yang masuk berasal dari user
         * yang terakhir mengirim, maka tampilkan pesannya saja. Jika tidak maka
         * tampilkan info user pengirim
         */
        const lastMessage = chatMsgList.find("li").last();
        if (lastMessage.data("user") == message.user.id) {
            lastMessage.find(".msg-body").append(`<p><span>${message.text}</span></p>`);
        }
        else {
            const position = (message.user.id == activeUser.id) ? " reverse" : "";
            const li = $("<li>").addClass("msg-item"+position).data("user", message.user.id);
            li.html(`<div class="msg-body">
                        <h6 class="msg-user">${message.user.name} <span>${message.time}</span></h6>
                        <p><span>${message.text}</span></p>
                    </div>`);
            chatMsgList.append(li);
        }

        // Otomatis scrolldown, jika ada pesan masuk
        chatMsgList.parent().scrollTop(chatMsgList[0].scrollHeight);
    },
    sendMessage: function(textMessage) {
        chat.saveMessage(activeContact.room, activeUser.id, textMessage);
        socket.emit("sendMessage", {
            room: activeContact.room,
            textMessage
        });
    }
};

chat.init();