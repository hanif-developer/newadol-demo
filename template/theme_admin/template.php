<!DOCTYPE html>
<html lang="en">
    <head>
    <!-- Title -->
    <title><?= $this->web_title; ?></title>
    <base href="<?= $basePath; ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta charset="UTF-8">
    <meta name="description" content="<?= $this->web_description; ?>" />
    <meta name="keywords" content="<?= $this->web_keywords; ?>" />
    <meta name="author" content="<?= $this->web_author; ?>" />
    <link rel="shortcut icon" type="image/x-icon" href="<?= $this->link($this->favicon); ?>">
    <!-- CSS -->
    <?= $cssPath; ?>
    <style>
        .wrapper {
            /* height: auto; */
        }
        .form-content, .table-content, .table-empty{
            display: none;
        }
        .form-group label.control-label {
            font-size: 10pt!important;
        }
        .table td {
            font-size: 10pt!important;
        }
        .btn-login {display: none;}
        .loader,
        .loader:after {
            border-radius: 50%;
            width: 5em;
            height: 5em;
        }

        .loader {
            margin: 60px auto;
            font-size: 10px;
            position: relative;
            text-indent: -9999em;
            border-top: 1.1em solid rgba(200, 200, 200, 0.5);
            border-right: 1.1em solid rgba(200, 200, 200, 0.5);
            border-bottom: 1.1em solid rgba(200, 200, 200, 0.5);
            border-left: 1.1em solid #e12330;
            -webkit-transform: translateZ(0);
            -ms-transform: translateZ(0);
            transform: translateZ(0);
            -webkit-animation: load8 1.1s infinite linear;
            animation: load8 1.1s infinite linear;
        }

        @-webkit-keyframes load8 {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @keyframes load8 {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
    </style>
    </head>
    <?php require_once $viewPath; ?>
</html>