/**
 * frameduzPHP.
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 2.2.0
 * @Date		: 25 Februari 2017
 * @package 	: adolservice.js
 * @Description : 
 */
app = {
	login: function(url){
		$(".form-login").hide();
		$(".form-loading").html('<div class="loader"></div><br/><br/><span>Authenticating..</span>').show();
		setTimeout(function(){
			$.post(url, $("#frmInput").serialize(), function(data){
				var result = JSON.parse(data);
				$(".form-loading").html(result.pesan);
				setTimeout(function(){
					if(result.status == 0){
						$(".form-loading").hide();
						$(".form-login").show();
					}
					else{
						$(".form-loading").html('<div class="loader"></div><br/><br/><span>Redirecting..</span>').show();
						window.location.reload();
					}
				}, 1000);
			});
		}, 2000);
	},
	
	dashboard: function(url){
		setInterval(function(){
			$.get(url, function (data){
				$(".mobile").html(data.user.mobile);
				$(".computer").html(data.user.computer);
				$(".user").html(data.user.user);
				app.tMasukCharts(data.trafik.masuk);
				$("#trafik_rx").html("Traffic In : "+data.trafik.masuk[0][1]+" kbps");
				app.tKeluarCharts(data.trafik.keluar);
				$("#trafik_tx").html("Traffic Out : "+data.trafik.keluar[0][1]+" kbps");
			}, 'json');
		}, 5000);
	},
	
	loadTabel: function(url){
		$("#data-tabel").html("<img src='assets/img/loader.gif' style='width: auto;'>");
		$.post(url, $("#frmData").serialize(), function(data){
			$("#data-tabel").html(data);
		});
	},
	
	loadTabelJabatan: function(url, action){
		$("#tabel-jabatan-"+action).html("<img src='assets/img/loader.gif' style='width: auto;'>");
		$.post(url, $("#frmData").serialize(), function(data){
			$("#tabel-jabatan-"+action).html(data);
		});
	},
	
	loadTabelUser: function(url, action){
		$("#tabel-user-"+action).html("<img src='assets/img/loader.gif' style='width: auto;'>");
		$.post(url, $("#frmData").serialize(), function(data){
			$("#tabel-user-"+action).html(data);
		});
	},
	
	pageTabel: function(url_tabel, obj){
		var v_page = $(obj).attr("number-page");
		$("#page").val(v_page);
		app.loadTabel(url_tabel);
	},
	
	pageTabelJabatan: function(url_tabel, obj, action){
		var v_page = $(obj).attr("number-page");
		$("#page").val(v_page);
		app.loadTabelJabatan(url_tabel, action);
	},
	
	pageTabelUser: function(url_tabel, obj, action){
		var v_page = $(obj).attr("number-page");
		$("#page").val(v_page);
		app.loadTabelUser(url_tabel, action);
	},
	
	showForm: function(url, obj){
		$.post(url, {id : obj.id}, function(data){
			$("#data-form").html(data);
			$("#frmModal").modal("show");
		});
	},
	
	showFormUser: function(url, obj){
		$.post(url, {id : obj.id, jabatan : $(obj).attr("jabatan")}, function(data){
			$("#data-form").html(data);
			$("#frmModal").modal("show");
		});
	},
	
	showDialog: function(url){
		$("#data-dialog").load(url);
		$("#dialogModal").modal("show");
	},
	
	showConfirm: function(url, obj, msg){
		$.post(url, {id : obj.id}, function(data){
			$("#myConfirmModalLabel").html("Konfirmasi Hapus");
			$("#data-confirm").html(msg);
			$("#id_confirm").val(obj.id);
			$("#nm_confirm").val($(obj).attr("nm"));
			$("#confirmModal").modal("show");
		});
	},
	
	simpanData: function(url_simpan, url_tabel){
		$.post(url_simpan, $("#frmInput").serialize(), function(data){
			$("#frmModal").modal("hide");
			//alert(data);
			app.showNotification(JSON.parse(data));
			app.loadTabel(url_tabel);
		});
	},
	
	simpanUser: function(url_simpan, url_tabel, obj){
		var action = $(obj).attr("action");
		var id_jabatan = $(obj).attr("id_jabatan");
		var id_relasi = $(obj).attr("id_relasi");
		var nm_jabatan = $(obj).attr("nm_jabatan");
		$.post(url_simpan, {id_jabatan : id_jabatan, id_relasi : id_relasi, nm_jabatan : nm_jabatan, action : action}, function(data){
			//alert(data);
			app.showNotification(JSON.parse(data));
			app.loadTabelUser(url_tabel, action);
		});
	},
	
	hapusData: function(url_hapus, url_tabel, id_confirm, nm_confirm){
		$.post(url_hapus, {id : id_confirm, nm : nm_confirm}, function(data){
			$("#confirmModal").modal("hide");
			//alert(data);
			app.showNotification(JSON.parse(data));
			app.loadTabel(url_tabel);
		});
	},
	
	hapusUser: function(url_hapus, url_tabel, obj){
		var action = $(obj).attr("action");
		var id_user = $(obj).attr("id_user");
		var nm_jabatan = $(obj).attr("nm_jabatan");
		$.post(url_hapus, {id : id_user, nm : nm_jabatan, action : action}, function(data){
			//	alert(data);
			app.showNotification(JSON.parse(data));
			app.loadTabelUser(url_tabel, action);
		});
	},
	
	cekSIPP: function(url_sipp, obj){
		$(obj).html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>');
		$.post(url_sipp, {nrp : obj.id}, function(data){
			//alert(data);
			var data = JSON.parse(data);
			var number = $(obj).attr("number");
			if(data.status === true){
				$("#nrp_"+number).html(data.id_personil);
				$("#nama_"+number).html(data.nama_personil);
				$("#pangkat_"+number).html(data.pangkat_personil);
				$("#jabatan_"+number).html(data.jabatan_personil);
			}
			$(obj).html('<i class="material-icons">search</i>');
			app.showNotification(data.error_msg);
		});
	},
	
	aktifSatker: function(url_aktif, url_tabel, id_satker, nm_satker, sts_satker){
		$.post(url_aktif, {id : id_satker, satker : nm_satker, status : sts_satker}, function(data){
			app.showNotification(JSON.parse(data));
			app.loadTabel(url_tabel);
		});
	},
	
	aktifJabatan: function(url_aktif, url_tabel, id_jabatan, nm_jabatan, sts_jabatan){
		$.post(url_aktif, {id : id_jabatan, jabatan : nm_jabatan, status : sts_jabatan}, function(data){
			//alert(data);
			app.showNotification(JSON.parse(data));
			app.loadTabel(url_tabel);
		});
	},
	
	registerUserCharts: function(obj, data, title){
		var dataValue = [
            {name : 'Jumlah Pejabat',data : data.jml_pejabat},
            {name : 'Jumlah Register',data : data.jml_register}
        ];
        $(obj).highcharts({
            chart: {type: 'bar'},
            title: {text: title},
			subtitle: {text: "STATISTIK"},
            xAxis: {
                categories: data.nama_satker,
                title: {
                    text: null
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Jumlah Register (User)',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
			tooltip: {
                valueSuffix: ' User'
            },
            legend: {
				layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -40,
                y: 80,
                floating: true,
                borderWidth: 1,
                backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                shadow: true
			},
			plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            series: dataValue
        });
    },
	
	tMasukCharts: function(data){
        $("#tMasukChart").highcharts({
            chart: {zoomType: "x"},
            title: {text: "TRAFFIC IN"},
            xAxis: {type: "datetime"},
            yAxis: {
                title: {
                    text: "Kbps rate"
                }
            },
            legend: {enabled: false},
			/* Warna */
			plotOptions: {
				area: {
					fillColor: {
						linearGradient: {x1: 0,y1: 0,x2: 0,y2: 1},
						stops: [
							[0, Highcharts.getOptions().colors[0]],
							[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get("rgba")]
						]
					},
					marker: {radius: 2},
					lineWidth: 1,
					states: {
						hover: {lineWidth: 1}
					},
					threshold: null
				}
			},
            series: [{
                type: "area", // Warna
                name: "Traffic",
                data: data
            }]
        });
    },
	
	tKeluarCharts: function(data){
        $("#tKeluarChart").highcharts({
            chart: {zoomType: "x"},
            title: {text: "TRAFFIC OUT"},
            xAxis: {type: "datetime"},
            yAxis: {
                title: {
                    text: "Kbps rate"
                }
            },
            legend: {enabled: false},
			/* Warna */
			plotOptions: {
				area: {
					fillColor: {
						linearGradient: {x1: 0,y1: 0,x2: 0,y2: 1},
						stops: [
							[0, Highcharts.getOptions().colors[0]],
							[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get("rgba")]
						]
					},
					marker: {radius: 2},
					lineWidth: 1,
					states: {
						hover: {lineWidth: 1}
					},
					threshold: null
				}
			},
            series: [{
                type: "area", // Warna
                name: "Traffic",
                data: data
            }]
        });
    },
	
	showNotification: function(data){
    	$.notify({
        	icon: data.icon,
        	message: data.message

        },{
            type: data.type,
            timer: 500,
            placement: {
                from: "top",
                align: "right"
            }
        });
	},
}
