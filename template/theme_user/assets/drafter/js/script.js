var max_pages = 100;
var page_count = 0;

function snipMe() {
    page_count++;
    if (page_count > max_pages) {
        return;
    }
    var long = $(this)[0].scrollHeight - Math.ceil($(this).innerHeight());
    var children = $(this).children().toArray();
    // console.log("long", long);
    // console.log("children", children);
    var removed = [];
    while (long > 0 && children.length > 0) {
        var child = children.pop();
        // console.log("child", child);
        $(child).detach();
        removed.unshift(child);
        // console.log("removed", removed);
        long = $(this)[0].scrollHeight - Math.ceil($(this).innerHeight());
    }
    if (removed.length > 0) {
        var a4 = $('<div class="A4 papper"></div>');
        a4.append(removed);
        $(this).after(a4);
        snipMe.call(a4[0]);
    }
}

function createPapper(callback) {
    const pdfPreview = $("#pdf-editor").clone();
    pdfPreview.find(".A4").removeClass("drafter");
    $("#pdf-editor").addClass("d-none");
    $("#pdf-preview").removeClass("d-none");
    $("#pdf-preview .iframe-loader").show();
    $("#pdf-preview .iframe-container").html(pdfPreview.children());
    const requests = $("#pdf-preview").find(".A4").map((idx, page) => {
        return new Promise((resolve) => {
            resolve(snipMe.call(page));
        });
    })
    
    Promise.all(requests).then(() => {
        setTimeout(() => {
            createPDF($("#pdf-preview").find(".A4"), callback);
        }, 1000);
    });
}

function createPDF(papper, callback) {
    const pdf = new jsPDF("P", "mm", "A4");
    const width = pdf.internal.pageSize.width;
    const height = pdf.internal.pageSize.height;
    

    const a4 = papper.map(async (idx, page) => {
        $(page).removeClass("papper drafter");
        const canvas = await html2canvas(page, {
            scale: 1
        });
        $(page).addClass("papper drafter");
        return canvas;
    });

    Promise.all(a4)
        .then((canvas) => {
            canvas.forEach((image, idx) => {
                pdf.addImage(image.toDataURL('image/jpeg', 1), 'jpeg', 0, 0, width, height);
                if (canvas.length > (idx + 1)) pdf.addPage();
            });
            callback(pdf);
            // console.clear();
        })
        .catch(err => console.log(err));
}

$(document).ready(function () {
    tinymce.init({
        selector: '.editable',
        menubar: false,
        inline: true,
        toolbar: [
            'undo redo | bold italic underline | fontselect fontsizeselect | forecolor backcolor | alignleft aligncenter alignright alignfull | outdent indent',
        ],
        force_br_newlines: true,
        force_p_newlines: false,
        forced_root_block: '', // Needed for 3.x,
    });

    $(document).on("click", "#btn-create", function (e) {
        // console.log($(".test-content").html()); return;
        createPapper(pdf => {
            // console.log("uri", pdf.output("datauristring"));
            // console.log("blob", pdf.output("blob"));
            const iframe = document.createElement("iframe");
            iframe.src = pdf.output("datauristring");
            iframe.classList.add("iframe");
            $("#pdf-preview .iframe-loader").hide();
            $("#pdf-preview .iframe-container").html(iframe);
        });
    });

    $(document).on("click", "#pdf-preview button.close", function (e) {
        $("#pdf-preview").addClass("d-none");
        $("#pdf-editor").removeClass("d-none");
    });
});