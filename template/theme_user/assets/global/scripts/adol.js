/**
 * frameduzPHP.
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 2.2.0
 * @Date		: 25 Februari 2017
 * @package 	: adolservice.js
 * @Description : 
 */
adol = {
	init: function(url){
		url_login = url+"login";
		url_main = url+"main";
		url_administrasi = url+"administrasi";
		url_draft = url+"draft";
		url_informasi = url+"informasi";
		url_pengaturan = url+"pengaturan";
		server = "ws://119.252.167.35:15000";
		sounds = document.createElement('audio');
		sounds.setAttribute('src', url+"sounds/Airport_Bell.ogg");
		//sounds.setAttribute('src', url+"sounds/here-i-am.ogg");
		sounds.setAttribute('type', 'audio/ogg');
		
		websocket = new WebSocket(server);
		websocket.onopen = function(ev){console.log("Conecting to Server "+ev.data);};
		websocket.onerror = function(ev){console.log("Error Occurred "+ev.data);};
		websocket.onclose = function(ev){console.log("Connection Closed "+ev.data);};
		websocket.onmessage = function(ev){
			response = JSON.parse(ev.data);
			action = response.action;
			message = response.message;
			if(action ===  "system"){
				$(".server").text(message);
			}
			if(action ===  "user"){
				var jabatan = $("#kode_jabatan").val();
				var bidang = $("#kode_bidang").val();
				var satker = $("#kode_satker").val();
				for(var n in message){
					if(message[n].tujuan === jabatan || message[n].tujuan === bidang || message[n].tujuan === satker){
						if(message[n].status === "notif"){
							toastr.info(message[n].data);
							adol.main.notifikasi();
						}
						if(message[n].status === "login"){
							window.location.reload();
						}
					}
				}
			}
		};
	},
	login: {
		init: function(){
			setTimeout(function(){$(".init").load(url_login+"/init");}, 1000);
		},
		loadSatker: function(regional, kategori){
			if(regional !== "" && kategori !== ""){
				$(".satker").html("<img src='assets/global/img/loader.gif' alt='' />");
				$.post(url_login+"/satker", {regional : regional, kategori : kategori}, function(data){
					$(".satker").html(data);
				});
			}
			else{
				$(".satker").html("");
				$(".jabatan").html("");
			}
		},
		loadJabatan: function(satker){
			if(satker !== ""){
				$(".jabatan").html("<img src='assets/global/img/loader.gif' alt='' />");
				$.post(url_login+"/jabatan", {satker : satker}, function(data){
					$(".jabatan").html(data);
				});
			}
			else{
				$(".jabatan").html("");
			}
		},
		loadPassword: function(){
			$(".pilihan").hide();
			$(".password").html("<div class='loader'></div>");
			$.post(url_login+"/password", $("#frmLogin").serialize(), function(data){
				$(".password").html(data);
			});
		},
		validate: function(){
			$.post(url_login+"/validate", $("#frmPassword").serialize(), function(data){
                $(".btn-password").html(data.pesan);
                if(data.status === 1){
					var msg = {action : "user", message : data.message};
					websocket.send(JSON.stringify(msg));
					setTimeout(function(){window.location.reload();}, 1000);
				}
            }, "json");
		},
		getBack: function(){
			$(".pilihan").show();
			$(".password").html("");
		},
	},
	
	main: {
		notifikasi: function(){
			$.get(url_main+"/notifikasi", function(data){
				$(".jmladm").html(data.jmlAdm);
				$(".jmldraft").html(data.jmlDraft);
				sounds.play();
			}, "json");
		},
	},
	
	administrasi: {
		notifikasi: function(){
			$("#content-notif-adm").html("<span style='padding:10px; text-align:center;'><img src='assets/global/img/loader.gif' alt='' /></span>");
			$("#content-notif-adm").load(url_administrasi+"/notifikasi");
		},
		input: function(obj){
			var url_simpan = url_administrasi+"/simpaninput";
			var url_upload = url_administrasi+"/upload";
			var url_masuk = url_administrasi+"/masuk";
			this.upload(url_upload, function(){
				var response = JSON.parse(this.responseText);
				$("#progressBar").hide();
                $("#progressBar").val(0);
				if(response.status == "success"){
					$(".loading").html("<span style='font-style:italic;'><img src='assets/global/img/loader.gif' alt='' /> Menyimpan Administrasi...</span>");
					$("#lampiran_adm").val(response.UploadFile);
					$.post(url_simpan, $(obj).serialize(), function(data){
						if(data.type == "success"){
							swal({title: data.title, text: data.text, type: data.type, confirmButtonClass: "btn-primary", confirmButtonText: "OK",},
							function(){ window.location = url_masuk; });
						}
						else{
							swal({title: data.title, text: data.text, type: data.type, confirmButtonClass: "btn-primary", confirmButtonText: "OK",});
						}
						$("#btnInput").show();
						$(".loading").html('');
					}, "json");
				}
				else{
					swal("Maaf", response.errorMsg, "error");
					$("#btnInput").show();
                    $(".loading").html('');
				}
			});
		},
		kirim: function(obj){
			var url_simpan = url_administrasi+"/simpankirim";
			var url_upload = url_administrasi+"/upload";
			var url_keluar = url_administrasi+"/keluar";
			this.upload(url_upload, function(){
				var response = JSON.parse(this.responseText);
				$("#progressBar").hide();
                $("#progressBar").val(0);
				if(response.status == "success"){
					$(".loading").html("<span style='font-style:italic;'><img src='assets/global/img/loader.gif' alt='' /> Mengirim Administrasi...</span>");
					$("#lampiran_adm").val(response.UploadFile);
					$.post(url_simpan, $(obj).serialize(), function(data){
						if(data.type == "success"){
							var msg = {action : "user", message : data.notif};
							websocket.send(JSON.stringify(msg));
							swal({title: data.title, text: data.text, type: data.type, confirmButtonClass: "btn-primary", confirmButtonText: "OK",},
							function(){ window.location = url_keluar; });
						}
						else{
							swal({title: data.title, text: data.text, type: data.type, confirmButtonClass: "btn-primary", confirmButtonText: "OK",});
						}
						$("#btnInput").show();
						$(".loading").html('');
					}, "json");
				}
				else{
					swal("Maaf", response.errorMsg, "error");
					$("#btnInput").show();
                    $(".loading").html('');
				}
			});
		},
		upload: function(url, callback){
			var xhr = new XMLHttpRequest();
			var formData = new FormData();
			var file = $("#file_adm").get(0).files[0];
			if(file === undefined){
				swal("Maaf", "File lampiran belum disertakan", "error");
			}
			else{
				$("#btnInput").hide();
				$("#progressBar").show();
				$("#progressBar").val(0);
				$(".loading").html("<span style='font-style:italic;'><img src='assets/global/img/loader.gif' alt='' /> Upload File...</span>");
				formData.append("file", file);
				xhr.upload.addEventListener("progress", function(event){
					var percent = (event.loaded / event.total) * 100;
					$("#progressBar").val(Math.round(percent));
					$(".loading").html("<span style='font-style:italic;'><img src='assets/global/img/loader.gif' alt='' /> "+Math.round(percent)+"% telah terupload</span>");
				});
				xhr.open("POST", url);
                xhr.onreadystatechange = function(){
					if(xhr.readyState === 4){
						if(typeof callback === "function") callback.apply(xhr);
					}
				};
                xhr.send(formData);
			}
		},
		loadTabel: function(action){
			$.post(url_administrasi+"/tabel"+action, $("#frmData").serialize(), function (data){
                $("#data-tabel").html(data);
            });
		},
		tabelPagging: function(action, number){
			$("#page").val(number);
			this.loadTabel(action);
			$(document).scrollTop(0);
		},
		loadDetail: function(action, id){
			window.location = url_administrasi+"/detail"+action+"/"+id;
		},
		loadDetailAdm: function(id_adm){
			$.post(url_administrasi+"/detailadm", {id: id_adm}, function (data){
                $(".detailadm").html(data);
            });
		},
		loadRiwayatAdm: function(id_adm){
			$.post(url_administrasi+"/riwayatadm", {id: id_adm}, function (data){
                $(".riwayatadm").html(data);
            });
		},
		formAction: function(id_adm, status){
            $.post(url_administrasi+"/formactionadm", {id_adm: id_adm, status: status}, function (data){
                $("#data-form-action").html(data);
                $("#frmActionModal").modal("show");
                $(".btnFooter").show();
                $(".loading").html("");
            });
		},
		formConfirm: function(id_adm){
            $.post(url_administrasi+"/formconfirmadm", {id_adm: id_adm}, function (data){
                $("#data-form-confirm").html(data);
                $("#frmConfirmModal").modal("show");
                $(".btnFooter").show();
                $(".loading").html("");
            });
		},
		formGrup: function(){
            $.get(url_administrasi+"/formgrupadm", function(data){
				$("#data-form-grup").html(data);
				$("#frmGrupModal").modal("show");
			});
		},
		formAgendaris: function(id_adm, status){
            $.post(url_administrasi+"/formagendarisadm", {id_adm: id_adm, status: status}, function (data){
                $("#data-form-agendaris").html(data);
                $("#frmAgendarisModal").modal("show");
                $(".btnFooter").show();
                $(".loading").html("");
            });
		},
		kirimAction: function(obj){
            $(".btnFooter").hide();
            $(".loading").html("<span style='font-style:italic;'><img src='assets/global/img/loader.gif' alt='' /> Mengirim " + $(obj).find("#status").val() + "..</span>");
            $.post(url_administrasi+"/simpanactionadm", $(obj).serialize(), function (data){
				$("#frmActionModal").modal("hide");
				if(data.type == "success"){
					var msg = {action : "user", message : data.notif};
					websocket.send(JSON.stringify(msg));
					swal({title: data.title, text: data.text, type: data.type, confirmButtonClass: "btn-primary", confirmButtonText: "OK",},
					function(){ $("#riwayat_adm").click(); $(document).scrollTop(0); });
				}
				else{
					swal({title: data.title, text: data.text, type: data.type, confirmButtonClass: "btn-primary", confirmButtonText: "OK",});
				}
            }, "json");
		},
		kirimAgendaris: function(obj){
            $(".btnFooter").hide();
            $(".loading").html("<span style='font-style:italic;'><img src='assets/global/img/loader.gif' alt='' /> Mengirim Administrasi..</span>");
            $.post(url_administrasi+"/simpanagendarisadm", $(obj).serialize(), function (data){
				$("#frmAgendarisModal").modal("hide");
				if(data.type == "success"){
					var msg = {action : "user", message : data.notif};
					websocket.send(JSON.stringify(msg));
					swal({title: data.title, text: data.text, type: data.type, confirmButtonClass: "btn-primary", confirmButtonText: "OK",});
				}
				else{
					swal({title: data.title, text: data.text, type: data.type, confirmButtonClass: "btn-primary", confirmButtonText: "OK",});
				}
            }, "json");
		},
		hapusAdm: function(obj){
            $(".btnFooter").hide();
            $(".loading").html("<span style='font-style:italic;'><img src='assets/global/img/loader.gif' alt='' /> Menghapus Administrasi..</span>");
            $.post(url_administrasi+"/hapusadm", $(obj).serialize(), function (data){
				$("#frmConfirmModal").modal("hide");
				if(data.type == "success"){
					toastr.success(data.text, data.title);
				}
				else{
					toastr.error(data.text, data.title);
				}
				adol.main.notifikasi();
            }, "json");
		},
		pilihGrup: function(obj){
            $.post(url_administrasi+"/pilihgrup", $(obj).serialize(), function (data){
				$("#frmGrupModal").modal("hide");
				$(".tujuan_adm").val(data).trigger("change");
				$("#btnClear").show();
            }, "json");
		},
		buatDraft: function(id){
			window.location = url_draft+"/buat/"+id;
		},
		getBack: function(action){
			window.location = url_administrasi+"/"+action;
		},
	},
	
	draft: {
		notifikasi: function(){
			$("#content-notif-draft").html("<span style='padding:10px; text-align:center;'><img src='assets/global/img/loader.gif' alt='' /></span>");
			$("#content-notif-draft").load(url_draft+"/notifikasi");
		},
		loadTabel: function(action){
			$.post(url_draft+"/tabel"+action, $("#frmData").serialize(), function (data){
                $("#data-tabel").html(data);
            });
		},
		tabelPagging: function(action, number){
			$("#page").val(number);
			this.loadTabel(action);
			$(document).scrollTop(0);
		},
		loadDetail: function(action, id){
			window.location = url_draft+"/detail"+action+"/"+id;
		},
		loadDetailDraft: function(id_draft){
			$.post(url_draft+"/detaildraft", {id: id_draft}, function (data){
                $(".detaildraft").html(data);
            });
		},
		loadRiwayatDraft: function(id_draft){
			$.post(url_draft+"/riwayatdraft", {id: id_draft}, function (data){
                $(".riwayatdraft").html(data);
            });
		},
		buat: function(obj){
            $.post(url_draft+"/buatdraft", $(obj).serialize(), function(data){
                $("#data-form-action").html(data);
                $("#frmActionModal").modal("show");
				$("#progressBar").hide();
                $("#progressBar").val(0);
                $(".btnFooter").show();
                $(".loading").html("");
            });
		},
		formAction: function(id_draft, status){
            $.post(url_draft+"/formactiondraft", {id_draft: id_draft, status: status}, function (data){
                $("#data-form-action").html(data);
                $("#frmActionModal").modal("show");
                $("#progressBar").hide();
                $("#progressBar").val(0);
                $(".btnFooter").show();
                $(".loading").html("");
            });
		},
		kirimAction: function(obj){
			var url_simpan = url_draft+"/simpanactiondraft";
			var url_upload = url_draft+"/upload";
			var url_keluar = url_draft+"/keluar";
			var action_draft = $("#status").val();
            this.upload(url_upload, action_draft, function(){
				var response = JSON.parse(this.responseText);
				$(".btnFooter").hide();
				$("#progressBar").hide();
                $("#progressBar").val(0);
				if(response.status == "success"){
					$(".loading").html("<span style='font-style:italic;'><img src='assets/global/img/loader.gif' alt='' /> Mengirim Pesan..</span>");
					if(response.UploadFile !== undefined) $("#lampiran_draft_riwayat").val(response.UploadFile);
					$.post(url_simpan, $(obj).serialize(), function(data){
						$("#frmActionModal").modal("hide");
						if(data.type == "success"){
							var msg = {action : "user", message : data.notif};
							websocket.send(JSON.stringify(msg));
							swal({title: data.title, text: data.text, type: data.type, confirmButtonClass: "btn-primary", confirmButtonText: "OK",},
							function(){ window.location = url_keluar; });
						}
						else{
							swal({title: data.title, text: data.text, type: data.type, confirmButtonClass: "btn-primary", confirmButtonText: "OK",});
						}
						$(".btnFooter").show();
						$(".loading").html('');
					}, "json");
				}
				else{
					swal("Maaf", response.errorMsg, "error");
					$(".btnFooter").show();
                    $(".loading").html('');
				}
			});
		},
		upload: function(url, action_draft, callback){
			var xhr = new XMLHttpRequest();
			var formData = new FormData();
			if((action_draft === "buat") || (action_draft === "revisi")){
				var file = $("#file_draft").get(0).files[0];
				if(file === undefined){
					$("#error_lampiran_draft").text("HARUS ADA LAMPIRAN");
				}
				else{
					$(".btnFooter").hide();
					$("#progressBar").show();
					$("#progressBar").val(0);
					$(".loading").html("<span style='font-style:italic;'><img src='assets/global/img/loader.gif' alt='' /> Upload File...</span>");
					formData.append("file", file);
					xhr.upload.addEventListener("progress", function(event){
						var percent = (event.loaded / event.total) * 100;
						$("#progressBar").val(Math.round(percent));
						$(".loading").html("<span style='font-style:italic;'><img src='assets/global/img/loader.gif' alt='' /> "+Math.round(percent)+"% telah terupload</span>");
					});
					xhr.open("POST", url);
					xhr.onreadystatechange = function(){
						if(xhr.readyState === 4){
							if(typeof callback === "function") callback.apply(xhr);
						}
					};
					xhr.send(formData);
				}
			}
			else{
				var msg = {responseText : '{"status":"success"}'};
				if(typeof callback === "function") callback.apply(msg);
			}
		},
		finish: function(id_draft, status){
			var url_simpan = url_draft+"/simpanactiondraft";
			var url_masuk = url_draft+"/masuk";
            $.post(url_simpan, {id_draft: id_draft, status: status}, function (data){
				if(data.type == "success"){
					swal({title: data.title, text: data.text, type: data.type, confirmButtonClass: "btn-primary", confirmButtonText: "OK",},
					function(){ window.location = url_masuk; });
				}
				else{
					swal({title: data.title, text: data.text, type: data.type, confirmButtonClass: "btn-primary", confirmButtonText: "OK",});
				}
            }, "json");
		},
		getBack: function(action){
			window.location = url_draft+"/"+action;
		},
	},
	
	informasi: {
		formPersonil: function(id_user){
			$.post(url_informasi+"/formpersonil", {id_user: id_user}, function (data){
				$(".data-form-personil").html(data);
            });
		},
		formPassword: function(id_user){
			$.post(url_informasi+"/formpassword", {id_user: id_user}, function (data){
				$(".data-form-password").html(data);
            });
		},
		sipp: function(obj){
			$(obj).html('<img src="assets/global/img/loader.gif">');
			$.post(url_informasi+"/sipp", {nrp: $("#id_personil").val()}, function (data){
				var personil = JSON.parse(data);
				$("#nama_personil").val(personil.nama);
				$("#pangkat_personil").val(personil.pangkat);
				$("#jabatan_personil").val(personil.jabatan);
				$(obj).html(" Cek NRP ");
            });
		},
		updatePersonil: function(obj){
            $.post(url_informasi+"/updatepersonil", $(obj).serialize(), function (data){
				if(data.type == "success"){
					swal({title: data.title, text: data.text, type: data.type, confirmButtonClass: "btn-primary", confirmButtonText: "OK",},
					function(){ window.location.reload(); });
				}
				else{
					swal({title: data.title, text: data.text, type: data.type, confirmButtonClass: "btn-primary", confirmButtonText: "OK",});
				}
            }, "json");
		},
		updatePassword: function(obj){
			$.post(url_informasi+"/updatepassword", $(obj).serialize(), function (data){
				if(data.type == "success"){
					swal({title: data.title, text: data.text, type: data.type, confirmButtonClass: "btn-primary", confirmButtonText: "OK",},
					function(){ window.location.reload(); });
				}
				else{
					swal({title: data.title, text: data.text, type: data.type, confirmButtonClass: "btn-primary", confirmButtonText: "OK",});
				}
			}, "json");
		},
		loadTabelRegister: function(id_satker){
			$.post(url_informasi+"/tabelregister", {id_satker : id_satker}, function (data){
                $(".tabel-user-mobile").html(data);
            });
		},
		registerUserCharts: function(obj, data, title){
			var dataValue = [
				{name : 'Jumlah Pejabat ('+data.jml_pejabat+')',data : data.jml_pejabat},
				{name : 'Jumlah Register ('+data.jml_register+')',data : data.jml_register}
			];
			$(obj).highcharts({
				chart: {type: 'bar'},
				title: {text: title},
				subtitle: {text: "STATISTIK"},
				xAxis: {
					categories: data.nama_satker,
					title: {
						text: null
					}
				},
				yAxis: {
					min: 0,
					title: {
						text: 'Jumlah Register (User)',
						align: 'high'
					},
					labels: {
						overflow: 'justify'
					}
				},
				tooltip: {
					valueSuffix: ' User'
				},
				legend: {
					layout: 'vertical',
					align: 'right',
					verticalAlign: 'top',
					x: -40,
					y: 80,
					floating: true,
					borderWidth: 1,
					backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
					shadow: true
				},
				plotOptions: {
					bar: {
						dataLabels: {
							enabled: true
						}
					}
				},
				series: dataValue
			});
		},
	},
	
	pengaturan: {
		loadTabelJabatan: function(){
			$.post(url_pengaturan+"/tabeljabatan", $("#frmData").serialize(), function (data){
                $("#tabel-user-jabatan").html(data);
            });
		},
		loadTabelAction: function(){
			$.post(url_pengaturan+"/tabelaction", $("#frmData").serialize(), function (data){
                $("#tabel-user-action").html(data);
            });
		},
		tabelPagging: function(number){
			$("#page").val(number);
			this.loadTabelJabatan();
			$(document).scrollTop(0);
		},
		simpanUser: function(obj){
			var action = $(obj).attr("action");
			var id_jabatan = $(obj).attr("id_jabatan");
			var id_relasi = $(obj).attr("id_relasi");
			var nm_jabatan = $(obj).attr("nm_jabatan");
			$.post(url_pengaturan+"/simpanuser", {id_jabatan : id_jabatan, id_relasi : id_relasi, nm_jabatan : nm_jabatan, action : action}, function(data){
				if(data.type == "success"){
					toastr.success(data.text, data.title);
				}
				else{
					toastr.error(data.text, data.title);
				}
			}, "json");
		},
		hapusUser: function(obj){
			var action = $(obj).attr("action");
			var id_user = $(obj).attr("id_user");
			var nm_jabatan = $(obj).attr("nm_jabatan");
			$.post(url_pengaturan+"/hapususer", {id_user : id_user, nm_jabatan : nm_jabatan, action : action}, function(data){
				if(data.type == "success"){
					toastr.success(data.text, data.title);
				}
				else{
					toastr.error(data.text, data.title);
				}
			}, "json");
		},
	}
};
