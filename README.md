# frameduz6
Framework Weduz versi 6

# Install Vritual Host MAC OS
sh -c "$(curl -s https://raw.githubusercontent.com/hanifdeveloper/webserver/master/mac_os/virtualhost.sh)"

# Install Vritual Host DEBIAN OS
sh -c "$(curl -s https://raw.githubusercontent.com/hanifdeveloper/webserver/master/debian_os/virtualhost.sh)"

Account Github
user: javaresources
pass: EUYa4EDrKdt78ym

git init
git add .
git commit -m "first commit"
git branch -M master
git remote add origin https://github.com/javaresources/m_adollampung.git
git push -u origin master

## Konfigurasi PHP
```sh
sudo apt-get install php-curl -y
sudo apt-get install php-mysql -y
sudo apt-get install php-gd -y
sudo systemctl reload apache2
```
## Konfigurasi Mysql
```sh
#Install Mysql Server
sudo apt-get install mysql-server

#Masuk ke mysql, dan ubah password root
sudo mysql -uroot

USE mysql;
SELECT user, host, plugin, authentication_string FROM user;
UPDATE user SET plugin = 'mysql_native_password' WHERE User='root';
ALTER USER 'root'@'localhost' IDENTIFIED BY 'root';
FLUSH PRIVILEGES;
```

## Create Database
```sh
#via mysql-server
mysql -u root --password=root -e "CREATE DATABASE IF NOT EXISTS db_newadol CHARACTER SET utf8 COLLATE utf8_general_ci"

#via docker
docker exec -i mysql.server /usr/bin/mysql -u root --password=root -e "CREATE DATABASE IF NOT EXISTS db_newadol CHARACTER SET utf8 COLLATE utf8_general_ci"
```

## Backup Database
```sh
#via mysql-server
mysqldump -u root --password=root db_newadol | sed -e 's/DEFINER=[^*]*\*/\*/'> assets/database.sql

#via docker
docker exec mysql.server /usr/bin/mysqldump -u root --password=root db_newadol | sed -e 's/DEFINER=[^*]*\*/\*/'> assets/database.sql
```

## Restore Database
```sh
#via mysql-server
cat assets/database.sql | mysql -u root --password=root db_newadol

#via docker
cat assets/database.sql | docker exec -i mysql.server /usr/bin/mysql -u root --password=root db_newadol
```

## Create Vhost
```
pico /etc/apache2/sites-available/webapss-newadol.conf

<VirtualHost *:80>
    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/html/webapss-newadol
    <Directory /var/www/html/webapss-newadol>
            Options Indexes FollowSymLinks
            AllowOverride All
            Order allow,deny
            Allow from all
            Require all granted
    </Directory>

    ErrorLog ${APACHE_LOG_DIR}/error.webapss-newadol.log
    CustomLog ${APACHE_LOG_DIR}/access.webapss-newadol.log combined

</VirtualHost>

a2dissite 000-default.conf 
a2ensite webapss-newadol.conf 
systemctl reload apache2

```

## Setting Folder Upload & Composer
```
sudo chown -R $USER:www-data .
sudo find upload -type d -exec chmod 770 {} \;
sudo find upload -type d -exec chmod g+s {} \;
sudo find upload -type f -exec chmod 660 {} \; 2>&1 | grep -v "Operation not permitted"

sudo find vendor -type d -exec chmod 770 {} \;
sudo find vendor -type d -exec chmod g+s {} \;
```

## Remove .DS_Store
```
find . -name ".DS_Store" -delete
```

## Remove History Shell & Mysql
```
rm ~/.bash_history
rm ~/.mysql_history
```

## Permanently authenticating with Git repositories
```sh
git config credential.helper store
git pull
```